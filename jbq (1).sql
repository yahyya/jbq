-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2021 at 06:30 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jbq`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `first_name` varchar(500) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(500) COLLATE utf8_bin NOT NULL,
  `country` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `company_name` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `street_address` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `apt` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `postcode` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `first_name`, `last_name`, `country`, `company_name`, `street_address`, `apt`, `city`, `phone`, `email`, `state`, `zone_id`, `postcode`, `lat`, `lon`) VALUES
(4, '', '', NULL, NULL, NULL, NULL, NULL, '0917139', NULL, NULL, 1, NULL, 29.606044, 52.527853),
(6, '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, 1, NULL, NULL, NULL),
(7, '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, 1, NULL, NULL, NULL),
(48, '', '', NULL, NULL, NULL, NULL, NULL, '09903245679', NULL, NULL, NULL, NULL, 29.61946359746237, 52.51160614192486),
(51, '', '', NULL, NULL, NULL, NULL, NULL, '09171380449', NULL, NULL, NULL, NULL, 29.6203554752474, 52.51621719449759),
(52, '', '', NULL, NULL, NULL, NULL, NULL, '09133464665', NULL, NULL, NULL, NULL, 29.6203554752474, 52.51621719449759),
(53, '', '', NULL, NULL, NULL, NULL, NULL, '9855555', NULL, NULL, NULL, NULL, 0, 0),
(54, '', '', NULL, NULL, NULL, NULL, NULL, '88088080', NULL, NULL, NULL, NULL, 0, 0),
(55, '', '', NULL, NULL, NULL, NULL, NULL, '0855758', NULL, NULL, NULL, NULL, 0, 0),
(56, '', '', NULL, NULL, NULL, NULL, NULL, '5585555', NULL, NULL, NULL, NULL, 0, 0),
(57, '', '', NULL, NULL, NULL, NULL, NULL, '786588', NULL, NULL, NULL, NULL, 0, 0),
(58, '', '', NULL, NULL, NULL, NULL, NULL, '9558880', NULL, NULL, NULL, NULL, 0, 0),
(59, '', '', NULL, NULL, NULL, NULL, NULL, '48456455', NULL, NULL, NULL, NULL, 0, 0),
(60, '', '', NULL, NULL, NULL, NULL, NULL, '098855888555', NULL, NULL, NULL, NULL, 0, 0),
(61, '', '', NULL, NULL, NULL, NULL, NULL, '094785888889', NULL, NULL, NULL, NULL, 0, 0),
(62, '', '', NULL, NULL, NULL, NULL, NULL, '0917525558', NULL, NULL, NULL, NULL, 29.606044, 52.527853),
(63, '', '', NULL, NULL, NULL, NULL, NULL, '088888887', NULL, NULL, NULL, NULL, 0, 0),
(64, '', '', NULL, NULL, NULL, NULL, NULL, '09171171417', NULL, NULL, NULL, NULL, 0, 0),
(65, '', '', NULL, NULL, NULL, NULL, NULL, '09558858778', NULL, NULL, NULL, NULL, 0, 0),
(66, '', '', NULL, NULL, NULL, NULL, NULL, '091758587', NULL, NULL, NULL, NULL, 0, 0),
(67, '', '', NULL, NULL, NULL, NULL, NULL, '09175888', NULL, NULL, NULL, NULL, 0, 0),
(68, '', '', NULL, NULL, NULL, NULL, NULL, '091718535', NULL, NULL, NULL, NULL, 0, 0),
(69, '', '', NULL, NULL, NULL, NULL, NULL, '098588888', NULL, NULL, NULL, NULL, 0, 0),
(70, '', '', NULL, NULL, NULL, NULL, NULL, '4774587', NULL, NULL, NULL, NULL, 0, 0),
(71, '', '', NULL, NULL, NULL, NULL, NULL, '8858888', NULL, NULL, NULL, NULL, 0, 0),
(72, '', '', NULL, NULL, NULL, NULL, NULL, '0948858888', NULL, NULL, NULL, NULL, 0, 0),
(73, '', '', NULL, NULL, NULL, NULL, NULL, '88056588', NULL, NULL, NULL, NULL, 0, 0),
(74, '', '', NULL, NULL, NULL, NULL, NULL, '5766888', NULL, NULL, NULL, NULL, 0, 0),
(75, '', '', NULL, NULL, NULL, NULL, NULL, '588555', NULL, NULL, NULL, NULL, 0, 0),
(76, '', '', NULL, NULL, NULL, NULL, NULL, '056666', NULL, NULL, NULL, NULL, 0, 0),
(77, '', '', NULL, NULL, NULL, NULL, NULL, '800658', NULL, NULL, NULL, NULL, 0, 0),
(78, '', '', NULL, NULL, NULL, NULL, NULL, '88494', NULL, NULL, NULL, NULL, 0, 0),
(79, '', '', NULL, NULL, NULL, NULL, NULL, '0885788', NULL, NULL, NULL, NULL, 0, 0),
(80, '', '', NULL, NULL, NULL, NULL, NULL, '5855', NULL, NULL, NULL, NULL, 0, 0),
(81, '', '', NULL, NULL, NULL, NULL, NULL, '8005888', NULL, NULL, NULL, NULL, 0, 0),
(82, '', '', NULL, NULL, NULL, NULL, NULL, '6585888', NULL, NULL, NULL, NULL, 0, 0),
(83, '', '', NULL, NULL, NULL, NULL, NULL, '064644', NULL, NULL, NULL, NULL, 29.620284649947077, 52.51621652394533),
(84, '', '', NULL, NULL, NULL, NULL, NULL, '484894848', NULL, NULL, NULL, NULL, 0, 0),
(85, '', '', NULL, NULL, NULL, NULL, NULL, '888880', NULL, NULL, NULL, NULL, 0, 0),
(86, '', '', NULL, NULL, NULL, NULL, NULL, '08458988', NULL, NULL, NULL, NULL, 0, 0),
(87, '', '', NULL, NULL, NULL, NULL, NULL, '78808588', NULL, NULL, NULL, NULL, 0, 0),
(88, '', '', NULL, NULL, NULL, NULL, NULL, '484845948', NULL, NULL, NULL, NULL, 0, 0),
(89, '', '', NULL, NULL, NULL, NULL, NULL, '580588', NULL, NULL, NULL, NULL, 0, 0),
(90, '', '', NULL, NULL, NULL, NULL, NULL, '48755757', NULL, NULL, NULL, NULL, 0, 0),
(91, '', '', NULL, NULL, NULL, NULL, NULL, '8080844', NULL, NULL, NULL, NULL, 0, 0),
(92, '', '', NULL, NULL, NULL, NULL, NULL, '0888588', NULL, NULL, NULL, NULL, 0, 0),
(93, '', '', NULL, NULL, NULL, NULL, NULL, '8085555', NULL, NULL, NULL, NULL, 0, 0),
(94, '', '', NULL, NULL, NULL, NULL, NULL, '466451818', NULL, NULL, NULL, NULL, 0, 0),
(95, '', '', NULL, NULL, NULL, NULL, NULL, '48484181', NULL, NULL, NULL, NULL, 0, 0),
(96, '', '', NULL, NULL, NULL, NULL, NULL, '4848484', NULL, NULL, NULL, NULL, 0, 0),
(97, '', '', NULL, NULL, NULL, NULL, NULL, '48467676', NULL, NULL, NULL, NULL, 0, 0),
(98, '', '', NULL, NULL, NULL, NULL, NULL, '44848481', NULL, NULL, NULL, NULL, 0, 0),
(99, '', '', NULL, NULL, NULL, NULL, NULL, '58087888', NULL, NULL, NULL, NULL, 0, 0),
(100, '', '', NULL, NULL, NULL, NULL, NULL, '08785788', NULL, NULL, NULL, NULL, 0, 0),
(101, '', '', NULL, NULL, NULL, NULL, NULL, '0917555889', NULL, NULL, NULL, NULL, 0, 0),
(102, '', '', NULL, NULL, NULL, NULL, NULL, '0888588', NULL, NULL, NULL, NULL, 0, 0),
(103, '', '', NULL, NULL, NULL, NULL, NULL, '0888588', NULL, NULL, NULL, NULL, 0, 0),
(104, '', '', NULL, NULL, NULL, NULL, NULL, '0888558', NULL, NULL, NULL, NULL, 0, 0),
(105, '', '', NULL, NULL, NULL, NULL, NULL, '0888558', NULL, NULL, NULL, NULL, 0, 0),
(106, '', '', NULL, NULL, NULL, NULL, NULL, '07158645546', NULL, NULL, NULL, NULL, 29.6327144564578, 52.48089551925659),
(107, '', '', NULL, NULL, NULL, NULL, NULL, '09544785555', NULL, NULL, NULL, NULL, NULL, NULL),
(108, '', '', NULL, NULL, NULL, NULL, NULL, '06844664644', NULL, NULL, NULL, NULL, NULL, NULL),
(109, '', '', NULL, NULL, NULL, NULL, NULL, '08555551188', NULL, NULL, NULL, NULL, NULL, NULL),
(110, '', '', NULL, NULL, NULL, NULL, NULL, '09595959292', NULL, NULL, NULL, NULL, NULL, NULL),
(111, '', '', NULL, NULL, NULL, NULL, NULL, '05595959592', NULL, NULL, NULL, NULL, NULL, NULL),
(112, '', '', NULL, NULL, NULL, NULL, NULL, '09171245678', NULL, NULL, 3, NULL, 29.6328960148712, 52.48084791004658),
(114, '', '', NULL, NULL, NULL, NULL, NULL, '01661949946', NULL, NULL, NULL, NULL, 29.6203554752474, 52.51621719449759),
(118, '', '', NULL, NULL, NULL, NULL, NULL, '0917355558', NULL, NULL, NULL, NULL, 0, 0),
(119, '', '', NULL, NULL, NULL, NULL, NULL, '09171392755', NULL, NULL, NULL, NULL, 29.635678801030227, 52.51856680959463),
(139, '', '', NULL, NULL, NULL, NULL, NULL, '0917853454', NULL, NULL, NULL, NULL, 0, 0),
(140, '', '', NULL, NULL, NULL, NULL, NULL, '980303030', NULL, NULL, NULL, NULL, 29.6211451, 52.5186901),
(141, '', '', NULL, NULL, NULL, NULL, NULL, '09171364556', NULL, NULL, NULL, NULL, 29.606044, 52.527853),
(142, '', '', NULL, NULL, NULL, NULL, NULL, '09171397255', NULL, NULL, NULL, NULL, 29.6204302, 52.5163261),
(143, '', '', NULL, NULL, NULL, NULL, NULL, '09179464649', NULL, NULL, NULL, NULL, 29.6204302, 52.5163261),
(144, '', '', NULL, NULL, NULL, NULL, NULL, '095588588', NULL, NULL, NULL, NULL, 0, 0),
(145, '', '', NULL, NULL, NULL, NULL, NULL, '0955858888', NULL, NULL, NULL, NULL, 0, 0),
(146, '', '', NULL, NULL, NULL, NULL, NULL, '0985858888', NULL, NULL, NULL, NULL, 29.606044, 52.527853),
(147, '', '', NULL, NULL, NULL, NULL, NULL, '998909858', NULL, NULL, 0, NULL, 29.606044, 52.527853),
(149, '', '', NULL, NULL, NULL, NULL, NULL, '68508808', NULL, NULL, 0, NULL, 29.606044, 52.527853),
(153, '', '', NULL, NULL, NULL, NULL, NULL, '09171392756', NULL, NULL, 0, NULL, 29.606044, 52.527853),
(154, '', '', NULL, NULL, NULL, NULL, NULL, '09175655885', NULL, NULL, 0, NULL, 29.6109175, 52.5742308),
(156, '', '', NULL, NULL, NULL, NULL, NULL, '09172665888', NULL, NULL, 0, NULL, 29.606044, 52.527853),
(159, '', '', NULL, NULL, NULL, NULL, NULL, '09949797979', NULL, NULL, 0, NULL, 29.606044, 52.527853),
(160, '', '', NULL, NULL, NULL, NULL, NULL, '08884888888', NULL, NULL, 0, NULL, 0, 0),
(161, '', '', NULL, NULL, NULL, NULL, NULL, '09171368494', NULL, NULL, 2, NULL, 29.6106597, 52.5747009),
(163, '', '', NULL, NULL, NULL, NULL, NULL, '09599594664', NULL, NULL, 2, NULL, 29.62021120143494, 52.51611191779375),
(164, '', '', NULL, NULL, NULL, NULL, NULL, '09398165521', NULL, NULL, NULL, NULL, 29.61939014835183, 52.50913683325052),
(169, '', '', NULL, NULL, NULL, NULL, NULL, '01664466446', NULL, NULL, 1, NULL, 29.6331434350393, 52.48105745762587),
(170, '', '', NULL, NULL, NULL, NULL, NULL, '04964667644', NULL, NULL, 0, NULL, 29.62038695314266, 52.51639254391193),
(171, '', '', NULL, NULL, NULL, NULL, NULL, '01964649494', NULL, NULL, 0, NULL, 29.606044, 52.527853),
(174, '', '', NULL, NULL, NULL, NULL, NULL, '06653565564', NULL, NULL, 1, NULL, 29.63303881339339, 52.48081739991903),
(176, '', '', NULL, NULL, NULL, NULL, NULL, '071362459', NULL, NULL, 2, NULL, 29.606044, 52.527853),
(177, '', '', NULL, NULL, NULL, NULL, NULL, '098278588', NULL, NULL, 5, NULL, 29.606044, 52.527853),
(178, '', '', NULL, NULL, NULL, NULL, NULL, '098656565', NULL, NULL, 7, NULL, 29.606044, 52.527853),
(179, '', '', NULL, NULL, NULL, NULL, NULL, '36536954', NULL, NULL, 7, NULL, 29.574456372980066, 52.576543428003795),
(180, '', '', NULL, NULL, NULL, NULL, NULL, '09173383797', NULL, NULL, 2, NULL, 29.606044, 52.527853),
(181, '', '', NULL, NULL, NULL, NULL, NULL, '36258754', NULL, NULL, 7, NULL, 29.605898212820943, 52.527395971119404),
(182, '', '', NULL, NULL, NULL, NULL, NULL, '09173153212', NULL, NULL, 3, NULL, 29.607898200850375, 52.52266421914101),
(183, '', '', NULL, NULL, NULL, NULL, NULL, '09173383797', NULL, NULL, 7, NULL, 29.602717564232712, 52.5277017429471),
(184, '', '', NULL, NULL, NULL, NULL, NULL, '09194994646', NULL, NULL, 1, NULL, 29.63321221132673, 52.4811664223671),
(185, '', '', NULL, NULL, NULL, NULL, NULL, '02122442478', NULL, NULL, 2, NULL, 0, 0),
(186, '', '', NULL, NULL, NULL, NULL, NULL, '09173383797', NULL, NULL, 1, NULL, 29.606044, 52.527853),
(187, '', '', NULL, NULL, NULL, NULL, NULL, '09122981019', NULL, NULL, 7, NULL, 0, 0),
(188, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(189, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(190, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(191, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(192, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(193, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(194, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(195, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(196, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(197, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(198, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(199, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(200, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(201, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(202, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(203, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(204, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(205, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(206, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(207, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(208, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(209, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(210, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(211, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(212, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(213, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(214, '1111', '1111', NULL, '1111', '1111', '121212', '1111', NULL, '1111', '1111', NULL, '121212', NULL, NULL),
(215, '121212', '121212', 'AX', '121212', '121212', NULL, '121212', '121212', '121212', '121212', NULL, NULL, NULL, NULL),
(216, '1321', '123', 'AX', '123', '123', '123', '123', NULL, 'yyyy@ggg.com', '123', NULL, '123', NULL, NULL),
(217, 'asdasd', 'asdasd', 'AQ', 'asdasd', 'asd', 'asd', 'asd', '234234234', 'asdasd@gmai.com', 'asdasd', NULL, 'asdasd', NULL, NULL),
(218, 'asdasd', 'asdasd', 'AQ', 'asdasd', 'asd', 'asd', 'asd', '234234234', 'asdasd@gmai.com', 'asdasd', NULL, 'asdasd', NULL, NULL),
(219, 'asdasd', 'asdasd', 'AQ', 'asdasd', 'asd', 'asd', 'asd', '234234234', 'asdasd@gmai.com', 'asdasd', NULL, 'asdasd', NULL, NULL),
(220, 'asdasd', 'asdasd', 'AQ', 'asdasd', 'asd', 'asd', 'asd', '234234234', 'asdasd@gmai.com', 'asdasd', NULL, 'asdasd', NULL, NULL),
(221, 'asdasd', 'asdasd', 'GB', 'asdsd', 'sdfsdf', 'asdf', 'sdfsadf', '24324324', 'y.t.15132@gmail.com', 'sadfsadf', NULL, 'sadfsadf', NULL, NULL),
(222, 'asdasd', 'asdasd', 'GB', 'asdsd', 'sdfsdf', 'asdf', 'sdfsadf', '24324324', 'y.t.15132@gmail.com', 'sadfsadf', NULL, 'sadfsadf', NULL, NULL),
(223, 'asdasd', 'asdasd', 'GB', 'asdsd', 'sdfsdf', 'asdf', 'sdfsadf', '24324324', 'y.t.15132@gmail.com', 'sadfsadf', NULL, 'sadfsadf', NULL, NULL),
(224, 'asdasd', 'asdasd', 'GB', 'asdsd', 'sdfsdf', 'asdf', 'sdfsadf', '24324324', 'y.t.15132@gmail.com', 'sadfsadf', NULL, 'sadfsadf', NULL, NULL),
(225, 'asdasd', 'asdasd', 'GB', 'asdsd', 'sdfsdf', 'asdf', 'sdfsadf', '24324324', 'y.t.15132@gmail.com', 'sadfsadf', NULL, 'sadfsadf', NULL, NULL),
(226, '234234', '234234', 'GB', '23423', '234', '234', '234', '55454564', 'y.t.15132@gmail.com', '234', NULL, '234', NULL, NULL),
(227, 'asdasd', '234234asdasd', 'AS', '23423', '234', '234', 'asdasd', '55454564', 'y.t.15132@gmail.com', '234', NULL, '234', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `family` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `remember_token` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `family`, `mobile`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'مهدی', 'شاه امیریان', '', 'm.shahamirian@gmail.com', '$2y$10$OVV2dERXaJAoPQytmCFXke9xwIplVfbyas3j1D9KuMF/RKW652eeq', 'k0xsJfiLmKxEUZwKHlm6Mma3afuBLAbPKgsP3wZnqPirYjGtvCt6053qE0mk', '2018-02-05 12:53:17', '2018-01-23 10:16:18', NULL),
(3, 'hiva', 'co', '', 'info@hivatec.ir', '$2y$10$rkxcLsvJ/GHGBcWAD0AVMOJCLoqS/4jZgDQteQ0lBhakUFT4LGJFS', 'xSrSuJD4TjwMUBGzGb2KEE4AW4kj9tgcDnbATGFqYZ0BKQ863CMCi4BdrdOD', '2018-04-09 09:48:45', '2018-06-25 09:30:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_has_permission`
--

CREATE TABLE `admin_has_permission` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `permission_id` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin_has_permission`
--

INSERT INTO `admin_has_permission` (`id`, `admin_id`, `permission_id`) VALUES
(104, 2, 1),
(105, 2, 2),
(106, 2, 3),
(107, 2, 4),
(108, 2, 5),
(109, 2, 6),
(110, 2, 7),
(111, 2, 8),
(112, 2, 9),
(113, 2, 10),
(124, 2, 11),
(126, 2, 12),
(128, 2, 13),
(130, 2, 14),
(132, 3, 1),
(133, 3, 2),
(134, 3, 3),
(135, 3, 4),
(136, 3, 5),
(137, 3, 6),
(138, 3, 7),
(139, 3, 8),
(140, 3, 9),
(141, 3, 10),
(142, 3, 11),
(143, 3, 12),
(144, 3, 13),
(145, 3, 14);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title_en` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `title_ar` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1 = kolahe sharee',
  `show_sidebar` tinyint(4) DEFAULT 0,
  `show_index` int(11) DEFAULT NULL,
  `order` int(2) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `title_en`, `title_ar`, `type`, `show_sidebar`, `show_index`, `order`, `image`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 'Power Bank', 'أداة للشحن', 1, 0, 1, 1, NULL, 1, '2020-12-09 20:09:03', '2020-12-09 20:11:06', NULL),
(2, NULL, 'USB Cable', 'كابل USB', NULL, 0, NULL, 2, NULL, 1, '2020-12-09 21:18:11', '2020-12-09 21:18:11', NULL),
(3, NULL, 'Headphones', 'سماعات الرأس', NULL, 0, 1, 0, NULL, 1, NULL, NULL, NULL),
(4, NULL, 'HDMI', 'HDMI', NULL, 0, NULL, 0, NULL, 1, NULL, NULL, NULL),
(5, NULL, 'Travel Charger', 'شاحن السفر', NULL, 1, NULL, 0, NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `item` varchar(300) COLLATE utf8_bin NOT NULL,
  `title` varchar(300) COLLATE utf8_bin NOT NULL,
  `value` varchar(300) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `item`, `title`, `value`) VALUES
(1, 'salesStatus', 'وضعیت دریافت سفارش', '1'),
(2, 'customProductSalesStatus', 'وضعیت ساخت همبر سفارشی', '1'),
(3, 'contactUsMap', 'تماس با ما - نقشه', '29.606044,52.527853'),
(4, 'contactUsInstagram', 'تماس با ما - اینستاگرام', 'http://instagram.com/hivatec.ir'),
(5, 'contactUsWebsite', 'تماس با ما - وب سایت', 'http://www.hivatec.ir'),
(6, 'contactUsEmail', 'تماس با ما - ایمیل', 'info@hivatec.ir'),
(7, 'contactUsSellPhone', 'تماس با ما - فروش', '09173153212'),
(8, 'contactUsSupportPhone', 'تماس با ما - پشتیبانی', '09173153212'),
(9, 'contactUsAddress', 'تماس با ما - آدرس', 'شیراز - بلوار همت جنوبی - انتهای خیابان شیخی'),
(10, 'customProductSalesTxt', 'متن غیر فعال بودن همبر سفارشی', 'با عرض پوزش سفارش گیری برای همبر سفارشی تا اطلاع ثانوی غیر فعال است'),
(11, 'salesStatusTxt', 'متن غیر فعال بودن سفارش گیری', 'با عرض پوزش سفارش گیری تا اطلاع ثانوی غیر فعال است.'),
(12, 'orderTaxPercent', 'درصد مالیات سفارش', '10'),
(13, 'orderBoxingPrice', 'هزینه بسته بندی سفارش', '3000'),
(14, 'reffererPageText', 'متن صفحه ی دوستان', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آی'),
(15, 'reffererLinkText', 'متن لیتک دعوت دوستان', 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آی');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_bin NOT NULL,
  `phone` varchar(50) COLLATE utf8_bin NOT NULL,
  `txt` text COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `phone`, `txt`, `created_at`, `updated_at`) VALUES
(1, 'اشز', '07136263526', 'اشز ایز نات هیر', '2018-06-17 08:57:38', '2018-06-17 08:57:38'),
(2, 'جووون', '۰۴۹۹۴۹۷۹۶۴۶', 'میک ها چی میگی؟', '2018-06-17 08:58:23', '2018-06-17 08:58:23'),
(3, 'اشز 2', '08857694646', 'بابابابابتبتبتب', '2018-06-17 09:05:00', '2018-06-17 09:05:00'),
(4, 'تیدبدتبت', '0946766767', 'نبنبنبدی نی بدیدی دینی یتیمی ی', '2018-06-25 10:43:43', '2018-06-25 10:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8_bin NOT NULL,
  `value` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 as price\n1 as percent',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `used` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `code`, `value`, `type`, `start_date`, `end_date`, `count`, `used`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ashez', 90, 1, '2018-04-04 00:00:00', '2018-07-22 00:00:00', 154, 31, '2018-04-04 11:32:33', '2018-07-18 08:19:32', NULL),
(2, 'ed9701', 10000, 0, '2018-04-05 00:00:00', '2018-07-22 00:00:00', 150, 0, '2018-04-05 05:59:42', '2018-06-10 07:31:45', NULL),
(3, '1234567', 200, 0, '2018-03-21 00:00:00', '2018-04-10 00:00:00', 10, 0, '2018-04-09 12:48:29', '2018-06-10 07:33:07', '2018-06-10 07:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `courier`
--

CREATE TABLE `courier` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `family` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `remember_token` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `desc` text COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `courier`
--

INSERT INTO `courier` (`id`, `name`, `family`, `mobile`, `email`, `password`, `remember_token`, `address`, `image`, `start_date`, `desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'محمد', 'محمدیان', '09176660046', 'mo.hammadian@gmail.com', '$2y$10$8aoPbjdjWPlMU86ipS92KelPZCQEoGIwNI2/5YH6Ey8wJwgAs7fV6', NULL, 'مهدی آباد', 'xdMEuEnj.jpg', '2018-01-24 00:00:00', 'نداره؟', '2018-01-24 06:17:49', '2018-01-24 06:25:14', NULL),
(2, 'امین', 'احمدیان', '09173445544', 'a.ahmadian@gmail.com', '$2y$10$VYw8Cu6VKtN2JUsSO9kmbeNWI5MahGwgruGMat.ml4brmRE7P22L.', 'qf2b9ySugJz5ZguvkJe6IfLyyHPgrGOH5xbEkzogLQSfvjdjHVjl2yL65xUJ', 'خلیلی', 'aNBlfEP2.jpg', '2018-01-25 00:00:00', 'تست ویرایش تصاویر', '2018-01-24 06:27:49', '2018-05-08 02:42:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `credit`
--

CREATE TABLE `credit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `desc` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `credit`
--

INSERT INTO `credit` (`id`, `user_id`, `transaction_id`, `amount`, `admin_id`, `desc`, `created_at`, `updated_at`) VALUES
(1, 4, 272256, -2500, 2, NULL, '2018-02-13 03:21:54', '2018-02-13 03:21:54'),
(2, 4, NULL, -2500, 2, NULL, '2018-02-13 03:22:53', '2018-02-13 03:22:53'),
(3, 4, NULL, 25000, 2, NULL, '2018-02-13 03:26:04', '2018-02-13 03:26:04'),
(4, 15, NULL, 10000, 3, NULL, '2018-04-09 08:21:42', '2018-04-09 08:21:42'),
(5, 1, NULL, 30530, NULL, 'سفارش شماره 184 مرجوع شد', '2018-06-19 05:40:02', '2018-06-19 05:40:02'),
(8, 5, NULL, 149660, 2, 'سفارش شماره 196 مرجوع شد', '2018-06-20 05:45:31', '2018-06-20 05:45:31'),
(9, 4, NULL, 100000, 3, NULL, '2018-06-25 04:41:37', '2018-06-25 04:41:37'),
(10, 4, NULL, 114770, NULL, NULL, '2018-06-25 04:48:39', '2018-06-25 04:48:39'),
(11, 4, NULL, -16560, NULL, NULL, '2018-06-25 06:11:28', '2018-06-25 06:11:28'),
(12, 4, NULL, -19860, NULL, NULL, '2018-06-25 07:26:50', '2018-06-25 07:26:50'),
(13, 4, NULL, -15680, NULL, NULL, '2018-06-25 07:44:17', '2018-06-25 07:44:17'),
(14, 4, NULL, -85530, NULL, NULL, '2018-06-25 07:59:21', '2018-06-25 07:59:21'),
(15, 4, NULL, -3030, NULL, NULL, '2018-06-25 08:09:31', '2018-06-25 08:09:31'),
(16, 4, NULL, -85530, NULL, NULL, '2018-06-26 04:24:35', '2018-06-26 04:24:35'),
(17, 4, NULL, 20000, NULL, NULL, '2018-07-02 08:01:09', '2018-07-02 08:01:09'),
(18, 4, NULL, -28580, NULL, NULL, '2018-07-07 08:52:25', '2018-07-07 08:52:25');

-- --------------------------------------------------------

--
-- Table structure for table `custom_category`
--

CREATE TABLE `custom_category` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_bin NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `min` int(11) DEFAULT NULL,
  `max` int(11) DEFAULT NULL,
  `desc` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `custom_category`
--

INSERT INTO `custom_category` (`id`, `title`, `order`, `min`, `max`, `desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(10, 'قصة العباية', 3, 1, 1, 'حدد القصة المطلوبة', '2020-12-07 05:55:17', '2020-12-07 05:55:17', NULL),
(11, 'العنق', 2, 1, 1, 'حدد خط العنق المطلوب', '2020-12-07 05:55:39', '2020-12-07 05:55:39', NULL),
(12, 'التصميم', 1, 1, 1, 'حدد التصميم  المطلوب', '2020-12-07 05:56:04', '2020-12-07 05:56:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_item`
--

CREATE TABLE `custom_item` (
  `id` int(11) NOT NULL,
  `custom_category_id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_bin NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `custom_item`
--

INSERT INTO `custom_item` (`id`, `custom_category_id`, `title`, `order`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(25, 7, 'Collar', 21, 1, '2020-12-02 04:07:04', '2020-12-02 04:07:04', NULL),
(26, 8, 'Sleeves', 22, 1, '2020-12-02 04:07:26', '2020-12-02 04:07:26', NULL),
(27, 9, 'Skirt', 23, 1, '2020-12-02 04:07:47', '2020-12-02 04:07:47', NULL),
(28, 12, 'sleeve1', 24, 1, '2020-12-09 17:50:12', '2021-03-31 06:57:06', '2021-03-31 06:57:06'),
(29, 12, '1', 24, 1, '2021-03-31 06:57:28', '2021-03-31 06:57:28', NULL),
(30, 11, '1', 25, 1, '2021-03-31 08:53:46', '2021-03-31 08:53:46', NULL),
(31, 10, '1', 26, 1, '2021-03-31 08:55:14', '2021-03-31 08:55:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_product`
--

CREATE TABLE `custom_product` (
  `id` int(11) NOT NULL,
  `title` varchar(300) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `custom_product_has_item`
--

CREATE TABLE `custom_product_has_item` (
  `id` int(11) NOT NULL,
  `custom_sub_item_id` int(11) NOT NULL,
  `custom_product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `custom_rule`
--

CREATE TABLE `custom_rule` (
  `id` int(11) NOT NULL,
  `first_item_id` int(11) NOT NULL,
  `second_item_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `custom_rule`
--

INSERT INTO `custom_rule` (`id`, `first_item_id`, `second_item_id`, `created_at`, `updated_at`) VALUES
(10, 1, 4, '2018-05-27 05:26:26', '2018-05-27 05:26:26'),
(11, 15, 17, '2018-06-13 04:56:17', '2018-06-13 04:56:17');

-- --------------------------------------------------------

--
-- Table structure for table `custom_sub_item`
--

CREATE TABLE `custom_sub_item` (
  `id` int(11) NOT NULL,
  `custom_item_id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_bin NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `price` double NOT NULL DEFAULT 0,
  `main_pic` varchar(50) COLLATE utf8_bin NOT NULL,
  `stack_pic` varchar(50) COLLATE utf8_bin NOT NULL,
  `stack_top_pic` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `count` int(3) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `custom_sub_item`
--

INSERT INTO `custom_sub_item` (`id`, `custom_item_id`, `title`, `order`, `price`, `main_pic`, `stack_pic`, `stack_top_pic`, `active`, `count`, `product_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(46, 28, 'test', 36, 99, 'hVGq5DW2.png', 'A2hwh4Nv.jpg', NULL, 1, 1, 0, '2020-12-09 18:00:59', '2020-12-09 18:00:59', NULL),
(47, 28, 'test2', 37, 0, 'RYjWHYYt.png', 'qMpJzj0F.jpg', NULL, 1, 1, 1, '2020-12-09 18:08:25', '2021-03-31 06:51:20', NULL),
(48, 29, '1', 38, 0, 'yD1JXbYh.png', 'O40TUqRe.jpg', NULL, 1, 1, 17, '2021-03-31 07:37:59', '2021-04-01 06:27:45', NULL),
(49, 29, '2', 39, 0, 'G6xiVf66.png', '7eJp5vKh.jpg', NULL, 1, 1, 17, '2021-03-31 07:39:15', '2021-04-01 06:27:28', NULL),
(50, 31, '3', 40, 0, '6ar5Wm9a.png', '4mrjDD10.jpg', NULL, 1, 1, 17, '2021-03-31 08:51:55', '2021-04-01 06:38:12', NULL),
(51, 31, '4', 41, 0, 'pDGkU3Ua.png', '5fPtsqlK.jpg', NULL, 1, 1, 17, '2021-03-31 08:52:28', '2021-04-01 06:37:53', NULL),
(52, 30, '1', 42, 0, '3P9HXlBc.png', 'KjASDh7e.png', NULL, 1, 1, 17, '2021-04-01 04:37:51', '2021-04-01 06:29:12', NULL),
(53, 30, '2', 43, 0, 'WPoQU1Nc.png', 'mfFLTrxt.png', NULL, 1, 1, 17, '2021-04-01 04:38:29', '2021-04-01 06:28:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `build_number` varchar(300) COLLATE utf8_bin NOT NULL,
  `device_model` varchar(300) COLLATE utf8_bin NOT NULL,
  `imei` varchar(300) COLLATE utf8_bin NOT NULL,
  `os` varchar(100) COLLATE utf8_bin NOT NULL,
  `os_version` varchar(100) COLLATE utf8_bin NOT NULL,
  `token` varchar(300) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token_cloud` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `build_number`, `device_model`, `imei`, `os`, `os_version`, `token`, `user_id`, `token_cloud`, `created_at`, `updated_at`) VALUES
(1, 'VWD7N18226004906', 'HUAWEI/TRT-L21A', '867357039991639', 'Android', '24 (7.0)', 'bd776960a6418b80d3466141229b4bfa7defcf914822767be7633df5710cb220bb8956e1', 5, 'cRjMqzKBMJw:APA91bF_cirNhZc-eO25GXOZV_nDTeEJ8atFWMnn6IBujdtqyu1kRrncxLBOsv6nz8McV0RK39VewdWBhkBo49qCRE3913THIHnTrB3umzOKCbRS3bnYgny5vdXy81QrV41jcgStRTYIUmfTRAhErA4i-Jb_TN7WZw', '2018-06-18 06:26:25', '2018-07-14 13:21:27'),
(3, 'xxxx', 'Simulator', '816E3C93-D887-4F73-9AFD-1D4CD299A603', 'iOS', '11.2', 'b28afbd3f1542e64d90235842f589691cf094966922004ccc4a2356f5937096f8bc6f6ef', 1, NULL, '2018-06-18 07:54:50', '2018-06-18 08:21:24'),
(5, '3c4711c029ae', 'Huawei/G630-U10', '867256021342076', 'Android', '18 (4.3)', 'b93ab812adb21063716f33cf396af1a65463e0162679351fb0cea8da4ea263c170d11467', 12, 'fLLxu5Ne7vI:APA91bE1nTztnPSXmc4phca2-qtY5q0YqaPTn1MCwNxO0WzpPWY5iOvk4NaG1CImcrXuVJ9XaZDUgISI2-_SdXGUHOa9gLFSNGGCEEc9BoQ8So3A8BlNp6-sspc89k2YaeXZ24gbLWu0rXUEqs3TNUb4gQXaNRC0kw', '2018-06-19 08:17:12', '2018-08-25 09:50:34'),
(6, '08120f770068c7b2', 'google/Nexus 5', '353490060891641', 'Android', '25 (7.1.2)', 'cb2e65b6c941e7d611e720f0047938e96c2c4892b7b96e3b3780b1bc97e25d43d8b64b50', 5, NULL, '2018-06-19 22:02:52', '2018-07-08 22:42:35'),
(7, '410043eee44db115', 'samsung/SM-N910C', '356387065322878', 'Android', '23 (6.0.1)', 'ed94e1eed95aba8587b3ae1b0eb4a1a09fd347cb05d736afc6f7ad0a93235d2ad2364f87', 5, NULL, '2018-06-20 06:40:59', '2018-06-20 14:36:12'),
(8, 'xxxx', 'iPhone 6', '4DC5950C-6CDA-44BD-BEE2-B180249DC5EF', 'iOS', '11.2.1', '3d03f129de5f23c33ff9b82b2ac4e0e51ade994cd1b47c02510cf5232bb05004390fd622', NULL, '6a0df027b30a5fa6b25c93b059805644dc5167744e61d23ba39e8271667e96ed', '2018-06-21 07:10:29', '2018-06-21 07:10:34'),
(10, 'xxxx', 'iPhone 7', 'D8D8E33D-9073-4464-8B79-E8038EF926F8', 'iOS', '11.3.1', 'b61fd2b6f270fb163eb9d71bd4e44f1a16ba46a26966fe4e9cb2a6f7f2f0af105129199d', 4, '846c6e1fd1fc514c74ec99e8cdbd5a67113b3c17cc05c22bbdafb3519bfbe32e', '2018-06-21 07:19:55', '2018-06-21 09:29:22'),
(11, 'xxxx', 'Simulator', 'EFE40C6E-B1CC-4058-8F50-66B12F12DAF0', 'iOS', '11.2', 'c2b1b0839729552d9002d09c504a47c4cf094966922004ccc4a2356f5937096f8bc6f6ef', NULL, NULL, '2018-06-21 09:38:02', '2018-06-21 09:38:02'),
(15, 'xxxx', 'iPhone 6', '3426307D-F51F-41B3-B0E0-A185353B4762', 'iOS', '11.2.1', '1707edc8181e2d5ab9b7ebd2f28d73631ade994cd1b47c02510cf5232bb05004390fd622', NULL, '94d971867118698a2a87b501ce07b1c4cbbebe63ca8899d1115e4ca8a6c34307', '2018-06-23 07:04:22', '2018-06-23 07:04:22'),
(16, 'xxxx', 'Simulator', '8483F572-A10B-4F0A-93FC-77F481298F3B', 'iOS', '11.2', '6472781d6878077a8f5c0f359be362e8cf094966922004ccc4a2356f5937096f8bc6f6ef', NULL, NULL, '2018-06-23 08:50:40', '2018-06-23 08:50:40'),
(17, 'xxxx', 'iPhone 6', '088757C2-BA71-418B-90ED-EBB72F55A55B', 'iOS', '11.2.1', 'ba0dbb5acf4893b00b5ba1aafb04c2e01ade994cd1b47c02510cf5232bb05004390fd622', 1, '13a19d90580449f2fb6f35f7cd6f1ca1187bcd32eff2d042a9c5bc72ae9ba1e2', '2018-06-23 09:32:29', '2018-06-23 09:37:45'),
(18, 'unknown', 'google/Android SDK built for x86', 'c24f65a69b2c3b99', 'Android', '26 (8.0.0)', '6dd2844759b68b4cb5b075069e4c7927090705ab0acd9ef91f6239596e0171fef7247324', NULL, NULL, '2018-06-24 08:30:22', '2018-06-24 08:30:22'),
(19, 'xxxx', 'iPhone 6', '336E278D-E296-4BF7-ADC8-AF13E8F726FD', 'iOS', '11.2.1', 'f1daebdab860206bf969695c2e73f2fe1ade994cd1b47c02510cf5232bb05004390fd622', 1, '73f56227d0890e690684077ab7d18ba75633001224417ff1fed3094f84019e8e', '2018-06-25 09:12:50', '2018-06-25 09:35:52'),
(20, 'xxxx', 'iPhone 6', '822CA27A-3EA0-4F61-8EF0-1829C3527DDB', 'iOS', '11.2.1', 'feb9952d503739253290c23a209398481ade994cd1b47c02510cf5232bb05004390fd622', 8, '3107c272a5fa0f06024bc9fe0aad50ebd532300f5a6c43280449911da3269a78', '2018-06-25 10:05:19', '2018-06-26 05:59:14'),
(21, 'xxxx', 'iPhone 7', '70DDBA45-C28E-4B35-A928-AAC26C50A9EA', 'iOS', '11.3.1', 'c26b20e9a56759ba19f49c12594c909816ba46a26966fe4e9cb2a6f7f2f0af105129199d', 4, '4ff0508d0d579f16520d765e7aa853b4963c968d656b52aabea42293bc3f8d0a', '2018-06-25 10:39:15', '2018-06-25 10:40:56'),
(22, 'xxxx', 'iPhone 6', 'B6DBA425-2145-42F6-9091-D11D2E22DAA0', 'iOS', '11.2.1', '56e90c46203f165d525824073c1fbcc11ade994cd1b47c02510cf5232bb05004390fd622', 10, 'c193216e10fa5cb844960ca3c4aec121db7b53302b9ac8f10d6a9cfff42f4624', '2018-06-26 06:36:05', '2018-09-30 12:06:20'),
(23, '1a7406df2d027ece', 'samsung/SM-G965F', '352419097051961', 'Android', '26 (8.0.0)', '64977352dc5650a4e3861e78b55d110616759d94afa9f96f1bf0b67e5586974a7a10b160', 10, 'f3N5JD6gMWc:APA91bHGAgSEsLuatazA6RRw8Zuy_y_Ym14YUG6oxgvW-5DAsg19aeajkd_mIv0MErXF2EfYsAAFYhol1PuLDUlUHsstiP8_OIvPhB5AJYd8xGSYQSrptgTW7-fakT8j3_uB1t8b6qFLk7CYH1MCfDr8d49IzVweKQ', '2018-06-26 10:43:52', '2018-06-28 09:12:52'),
(24, 'unknown', 'google/Android SDK built for x86', '358240051111110', 'Android', '27 (8.1.0)', '99011cecbc5f5df9e3327250d372ad6e090705ab0acd9ef91f6239596e0171fef7247324', NULL, NULL, '2018-06-30 09:33:27', '2018-06-30 09:33:27'),
(25, 'unknown', 'google/Android SDK built for x86', 'N>}$Oy(u~-F\'YPd:9gKI', 'Android', '27 (8.1.0)', '578b3de0e57f8ad3d8201a4568cfe3c8090705ab0acd9ef91f6239596e0171fef7247324', NULL, 'cjluei_NaPA:APA91bGaZIzXoaGqVGERBD-GDrimoFCi5lbu2soiUVPbXMXTjZB3Rf_MOgzWNmljxEHGgykGewvh-aNGfhCdQwiVogF_kSeIrgp6yIIDp5bkwVwYlW_P0A4T17UFMh7fKCD5-88NVG7miGMlQP8ovONBwWQSWvMtLw', '2018-07-01 09:22:58', '2018-07-01 09:23:23'),
(26, 'unknown', 'Android/Android SDK built for x86_64', '+NS0mnjz8-\\J40}L+<ow', 'Android', '22 (5.1.1)', '50f7c6a978a1d255000e737d1afe7d105b77f92e6c40cef98cafd3fd23cb5a2830200daa', NULL, 'f9dBkYSsklM:APA91bFb28shS9Q1pCs4yzpoaQagEKwVKAyjpQMyvezZAYpsQXP2oHWxdbO65b2j_q2dKOFGde4d7ly02ButJyQxMdM4RUBKl7HvB3qK8EpUBWttcQQ0eS0n6iuAJXDLKskxaa_Ta1AdbWyW4_5wc_P-OF8tm_kfsQ', '2018-07-01 09:38:52', '2018-07-01 09:40:16'),
(27, '1a7406df2d027ece', 'samsung/SM-G965F', 'O5XQlGD_w&kW*\\xFPfg|', 'Android', '26 (8.0.0)', '168a3c723ce28c2381f76dea5d59517316759d94afa9f96f1bf0b67e5586974a7a10b160', NULL, 'fsL55WsdaE4:APA91bGYz3w-IFIlGjZBBGdLUBcvFqlFA2JtTo9QujTHSomIEmDXJOizRI-a0uTq6tuNa9RoSl2_GguFgb7BDHOFQdX-DN67GKyWkkH_EWftBCoJIL8aRrjeCMPfWjXKkGzbK11ax_cUtv_KIRVsjSaLay_jykXaJw', '2018-07-05 06:30:29', '2018-07-05 06:30:43'),
(28, '1a7406df2d027ece', 'samsung/SM-G965F', 'xQD! _r(@Y]%XR:oLyU9', 'Android', '26 (8.0.0)', '548fcab2b44c6d17eb9e6616d0fe81dd16759d94afa9f96f1bf0b67e5586974a7a10b160', 10, 'feUd0Xj1tQc:APA91bEK80a3am-zg3xppr2lXyCDhs6N3OcUJxv_ilD9WywHx5GgYpzjV4vuSLi9nTFMzNC2vSliMwd7DUr5m-vFLFtxLU2keWlTVe_BRVZ9v66P61W4zQw8PbQxwgXt3g37bWft4W_CX6DmVJf4d1VhdetzLsvy8w', '2018-07-07 10:57:05', '2018-07-08 08:29:59'),
(29, 'VWD7N18226004906', 'HUAWEI/TRT-L21A', 'nFoaHlY\\6mn@X.%tg/|%', 'Android', '24 (7.0)', 'ddb1ffb938e558ef58e56c21d812d4587defcf914822767be7633df5710cb220bb8956e1', 1, 'c_zcEJYvmrM:APA91bGBnRPJcbp2HXphg1_sWoQDgIcPU-mKkAd9eax_m5ABlRHFqP1YwvFuCVLDcwd2-wKTTjB1qKdpKVgAuk54YaAJ5S7zVyiYgr05kvSH-C6QAkNk4adaJZKUykMoNMKrle0a49JJTnJpDLUkSHzE3JYCfYWmGg', '2018-07-15 09:13:33', '2018-08-29 09:59:57'),
(30, 'xxxx', 'Simulator', 'A6C4C838-F7F8-4294-9809-19A8D6FE88A8', 'iOS', '11.3', '08b4c22a3cbba320ff4a456e7fa169f7cf094966922004ccc4a2356f5937096f8bc6f6ef', NULL, NULL, '2018-07-16 05:34:52', '2018-07-16 05:34:52'),
(31, 'xxxx', 'Simulator', '013C4498-BBD9-4060-AA0E-2F16B37EC75F', 'iOS', '11.3', 'bfd4d433e11c67226541e3695d2f0b3ccf094966922004ccc4a2356f5937096f8bc6f6ef', NULL, NULL, '2018-07-16 09:07:40', '2018-07-16 09:07:40'),
(32, '1a7406df2d027ece', 'samsung/SM-G965F', 'ueL9;R&td\\{\\K=VF:u=8', 'Android', '26 (8.0.0)', '00905a08d07188f701c51f8dbea10e3c16759d94afa9f96f1bf0b67e5586974a7a10b160', NULL, 'djdQLA7iCX4:APA91bGEgu0w9bHXZu27JZJlPlWjf-lDjSKEAwfwf8Zq8Z3Sk0EGElbmoQJyi3V3xFtnyIvIvr_KL41f1HGxZGjnEq0RSVFicDTNl5UUTj8jOieMH56cu7YuEQ12w4O-l8LTRBA9Hs4H3bkLnt4ZSZkv0kUKkLaqOA', '2018-07-19 08:27:05', '2018-07-19 08:27:10'),
(33, 'FA78Y1801272', 'htc/HTC U11', 'D-{m?9pB`,Ez{nm^p4X', 'Android', '26 (8.0.0)', '79564df7cf4034a2576e8bf84d873857660e12df0fb08b488f5af299ba76d8d94ac226ed', 5, 'cCh5HQ3o72M:APA91bGAhrTdKje2U-zepAqzviRKneBCR-LlceOWr5rQ-fXxjZBGfQlPSqQ2SO1nWgCOtV8UxCs9E--ftBUL1oS3B6YUGBdToOmGNyIqqg7s2-DzL7WzZUx1wEAI2PQPV1O9HVxu5Y0W1AuKZe0i84q2Z05Ba1VaFg', '2018-07-21 06:53:24', '2018-07-21 07:15:54'),
(34, 'HGH7N17822001056', 'HONOR/PRA-LA1', 's{@)x}Ts7E$a9hd!CxBy', 'Android', '24 (7.0)', 'f7ed8d9f4fb07219890140aa881b1e60d5618ee8e537d4ff674a0558ca3e9a297dae4bf7', NULL, NULL, '2018-07-21 14:39:46', '2018-07-21 14:39:46'),
(35, 'HGH7N17822001056', 'HONOR/PRA-LA1', '9OGEVe*tbbm?alp3+qgO', 'Android', '24 (7.0)', 'ace5d1e9ad698f560875a79d24ab3fa6d5618ee8e537d4ff674a0558ca3e9a297dae4bf7', NULL, NULL, '2018-07-21 20:27:46', '2018-07-21 20:27:46'),
(36, '419df2a7', 'OnePlus/ONEPLUS A3010', '\\%D\"?F(G]il;u7dYa?%]', 'Android', '26 (8.0.0)', '09e87c28da175a0db4487a8da9f07a7311e2d8f43d32ef3c57691db05e7acf01ec994ead', NULL, NULL, '2018-07-22 05:38:55', '2018-07-22 05:38:55'),
(37, 'FA78Y1801272', 'htc/HTC U11', 'YK`JKO-#,_y,8/6mT&~*', 'Android', '26 (8.0.0)', 'a5ea68ea251dbaad2abe7b20c48535b1660e12df0fb08b488f5af299ba76d8d94ac226ed', 4, NULL, '2018-07-22 05:56:44', '2018-07-22 06:39:51'),
(38, '29c42a4b0504', 'xiaomi/MI 5X', ':S|.!AO3dRhq!/=E#T)%', 'Android', '25 (7.1.2)', '32bef12e6777dab0c8ca8d6e9bea5527e77a1aae6e219e36966ffc873aaf40ef2878197f', NULL, 'fn9yrq6FUO4:APA91bEFOLJWnQB_-jHC9RjktOqHyRaAKEKY0fEufO_W6sC1KVy6I0AmitkbWfYFvnCBP3MlHxxVo_6_LTBZ1JeI93rHZ9zmy_H6XqfmBgp9B63JEgcrbKQEeAoQZLuhiohjcuZ4pVwaGdyapUaPitCnBOi2LpMKJQ', '2018-07-22 07:13:02', '2018-07-22 07:13:04'),
(39, '1a7406df2d027ece', 'samsung/SM-G965F', 'h&`dR\\c:aywu(KTvp\\\\#', 'Android', '26 (8.0.0)', '77cb463b0cafad554d1c6dbbd0ebce7c16759d94afa9f96f1bf0b67e5586974a7a10b160', NULL, 'feiXpFJ1I3g:APA91bHuotSR9O8rmYga_21z0Za9VgsSFeBKTUbNEdI4Ar_og4IqFnh8Nt5EbCJ945xFtRuSvE-GqZnohLMI7tedWCFevNxB8r9QhahVuZrYZfNGp_befeFwQIhqKLInSh4s9w7Wu4pRbA2YRYzg9kNQNEu_gGY6Dg', '2018-07-22 20:18:35', '2018-07-22 20:18:43'),
(40, '08120f770068c7b2', 'google/Nexus 5', 'gu3y~c{1`PI}r])S_g<c', 'Android', '25 (7.1.2)', '67c408cda70846a96a2ca1702cc332846c2c4892b7b96e3b3780b1bc97e25d43d8b64b50', NULL, 'fEIBmghcRKs:APA91bGFdF323MOfXOxOt6pDk-JJ5QECscjFnbFIrKW-RLRB_bzWu1EhgTRQXE7kZLByCBCl4uE1xCH1uvsWWfLpZMNGePeb8nOYCBKS61ik7lTKpZIi7vYYbNKI1s73ApTTWZ_ycvOyHUXWyGO01dK3fqGIdGoC7g', '2018-07-25 19:00:45', '2018-07-25 19:00:49'),
(41, 'unknown', 'google/Android SDK built for x86', 'L-;v\"RrpI0Jo$wn5VB@', 'Android', '28 (9)', 'c4ae007934f85779489b65ca213f90ab090705ab0acd9ef91f6239596e0171fef7247324', NULL, NULL, '2018-07-29 09:21:19', '2018-07-29 09:21:19'),
(42, 'xxxx', 'Simulator', '7EB38803-E7B9-4C5F-89E5-1A1984744E0C', 'iOS', '11.3', '50d501c76fb88fbd9d84ac51f232b36fcf094966922004ccc4a2356f5937096f8bc6f6ef', 1, NULL, '2018-07-31 07:42:19', '2018-08-13 09:06:12'),
(43, 'LGH818dd524c2e', 'lge/LG-H818', 'FG{?fLrAv]Fet0w8VG9N', 'Android', '23 (6.0)', '0c3883f6a84abf22f7260e60a8abeb8b66415bf2de20bdd0b98d1f09b6a1e75165f5cbbf', 11, 'fx97CTIwNs0:APA91bHcP2eRbmyYLwFJ37FL6lF026aDwJgW0q_89hNPUpAIG77jdNHB8pJBbMZnZ-M6TzF4Z0AtdEsfMfyj8ZRw7y8c0_RezWQR7kjElBWY0oCsb-SR2EulRTGbxPVLyuN0RejUTvPLq7Vp3O1e7_FwELAxzUHm5g', '2018-08-09 15:22:30', '2018-08-09 16:45:07'),
(44, 'xxxx', 'Simulator', '541FB845-5FF1-4B27-884C-14FDBB0B33D1', 'iOS', '11.3', '49ac12d366ebecaa6442dd00fa907c9acf094966922004ccc4a2356f5937096f8bc6f6ef', NULL, NULL, '2018-08-13 09:08:54', '2018-08-13 09:08:54'),
(45, '988a1b43583833585a30', 'samsung/SM-N950F', '353759092968332', 'Android', '26 (8.0.0)', 'ed1216fadd80b944dab1005d7fc147cd3b31d91bd84f8339971584fb4a8f4a01436544cf', NULL, 'cr9UnvF0vZ0:APA91bHFBaqvNO8B-mPm3JyRriQ5uU9R1MM7Fbf5bfnZuAJbogRCCq0hL5a9vyk1O2k_gQxbrPFI3kSYM8mDdrfAi8JeTug65RHuyTTsGTBvDD2_4V2G0orzWUBa2lUmiii0UmDPBDuA', '2018-08-14 09:35:39', '2018-09-11 18:55:45'),
(46, '41005e09e4c2c1bd', 'samsung/SM-N910C', 'S|HUfU$+dn(!@6\\[x+2p', 'Android', '23 (6.0.1)', '755e69339ae2dab1f6a8dea17ba2543850913072abe10a53372ee4ccbcc1872c9be75ee6', 16, NULL, '2018-08-29 10:43:12', '2018-09-10 07:05:41'),
(47, 'FA78Y1801272', 'htc/HTC U11', 'G=uLw;row#n[-,u`(sm_', 'Android', '26 (8.0.0)', '3c9f2338b9716e484f6c27e2bbe459ec660e12df0fb08b488f5af299ba76d8d94ac226ed', NULL, NULL, '2018-09-04 07:28:32', '2018-09-04 07:28:32'),
(48, '90435584', 'samsung/SM-G920F', '984535490433575', 'Android', '19 (4.4.4)', '638e6b4a931cd4c227939bfb3351f6a8ffb916dc939c1a1cd8586c81cd0699767504cc9c', 14, 'ct4B0tHkEYo:APA91bFAlgO37la2wpJuF15zMItc2ZGyE_JU9u8-OPw4Qodp6IU8FjniMcVm5sIMs70WYfcXlPmRanFRsw_JypXsNpD8BH8StloRcg_5G-GHUv4W6KqQ0dlY7eTSNIXMKuMqi6DFS7rz', '2018-09-07 13:01:18', '2018-09-07 13:03:15'),
(49, '0123456789ABCDEF', 'GLX/GLX Spider1S', 'sCXWdH:q }!#P~*h*!`W', 'Android', '19 (4.4.2)', 'a3b4432d3020fe5fa2dbf070c4d763bd1f79b17eae50459b2be8edac867ac495e98c5810', 17, 'cargkPt9bSc:APA91bGeRe30i9I_6vhvsoLw6C3ppih5dEdmUqHXTL7NY6OkVEV0DfB5VuH_6q47l1EoVN_bqVeCEhUe96vi9SfBedi5Iib5ibxbEms_dkXRJPOEMN_aHM4FJJ1exMFi33f_gm9LHaJz', '2018-09-10 13:51:07', '2018-09-10 14:21:50'),
(50, 'xxxx', 'iPhone 6', 'A9AF9D9C-8A82-4629-8625-85A215D87B20', 'iOS', '11.4.1', '9e15f6ed4335023380b4083bd15557e11ade994cd1b47c02510cf5232bb05004390fd622', NULL, 'de589ffc40303ff80e0572194f627e4d0a7a8fc28737b9df7034b4a05dc669b6', '2018-09-30 12:09:33', '2018-09-30 12:09:38'),
(51, '29c42a4b0504', 'xiaomi/Mi A1', 'Fp 5}d5:\"f)`WDK!h({', 'Android', '27 (8.1.0)', '5054788dd95b8d774742e0a1e43fb94f4d8be107e43e4fbc7e6fda0ba497eacd89b07f45', NULL, 'ez8u-OBO8lg:APA91bGhacRd3eDWnj4P-Tp-mcE6pxckNBocMvA9HqBAfJw4dN5oNHZqttTCfsB91E-SAQ5wgkXc5DmsUjms9q0kh8yD8ZnKZv9TQZOa7dhqAaRQHltxaFZSBm6b2NFph7Yec4o4cjQJ', '2018-10-17 20:44:54', '2018-10-17 20:44:55'),
(52, '67cb2c3b', 'Xiaomi/MI 6', 'z{~}5Onq=0I9C2(nBYJ', 'Android', '26 (8.0.0)', '7acd0ecd7a3b3fcc1383a36c340c6054626043c19c00660d60fb3b8811735d79f08d2075', NULL, 'fnLUSoVkKxo:APA91bGJ29y0QMm3BHPSKjZUvtJdpjbWleACfoJ3jrHhCcL3uMaGk36HwyzjSSzGOU_oqSo5kpOUuIV2Q0yG1ClyM6-EUQ00L2ptEI0uJ6nxv1BFbZa6PYMoqUzH4nENSbcPr-rQ8H3v', '2018-10-18 22:03:26', '2018-10-18 22:04:22'),
(53, '67cb2c3b', 'Xiaomi/MI 6', 'H6m4]dD1aDgL`#x=0Cmx', 'Android', '26 (8.0.0)', '5eb3b691718c564a80be1e9bb9a46a69626043c19c00660d60fb3b8811735d79f08d2075', NULL, 'fetb_JpHs3A:APA91bEv2FebXdukcjKOVc6tsNk1vIglBbgn3nFIDvLSBV06kJAvOfG4y9B1j8-xLbdncMgjnWY6qylsWIpNtQZUupQOXNXvRro4STDe2kd4jglWpi5ToIaWDoCGFupt7zohRwfHAJZg', '2018-10-19 08:32:42', '2018-10-19 08:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `code` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `used` int(11) DEFAULT NULL,
  `percent` int(11) DEFAULT NULL,
  `price` double(8,3) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `factors`
--

CREATE TABLE `factors` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `boxing_price` double NOT NULL,
  `tax_price` double DEFAULT NULL,
  `discount_price` double DEFAULT NULL,
  `delivery_price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `factors`
--

INSERT INTO `factors` (`id`, `order_id`, `amount`, `total`, `boxing_price`, `tax_price`, `discount_price`, `delivery_price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 99, 99, 0, 0, 0, 0, NULL, NULL, NULL),
(2, 4, 99, 99, 0, NULL, NULL, 0, '2021-09-12 12:57:05', '2021-09-12 12:57:05', NULL),
(3, 5, 99, 99, 0, NULL, NULL, 0, '2021-09-12 12:57:26', '2021-09-12 12:57:26', NULL),
(4, 6, 99, 99, 0, NULL, NULL, 0, '2021-09-12 12:57:38', '2021-09-12 12:57:38', NULL),
(5, 7, 99, 99, 0, NULL, NULL, 0, '2021-09-12 12:58:49', '2021-09-12 12:58:49', NULL),
(6, 8, 99, 99, 0, NULL, NULL, 0, '2021-09-12 13:00:20', '2021-09-12 13:00:20', NULL),
(7, 9, 99, 99, 0, NULL, NULL, 0, '2021-09-12 13:09:14', '2021-09-12 13:09:14', NULL),
(8, 10, 99, 99, 0, NULL, NULL, 0, '2021-09-12 13:09:23', '2021-09-12 13:09:23', NULL),
(9, 11, 99, 99, 0, NULL, NULL, 0, '2021-09-12 13:10:28', '2021-09-12 13:10:28', NULL),
(10, 12, 0, 0, 0, NULL, NULL, 0, '2021-09-12 12:17:44', '2021-09-12 12:17:44', NULL),
(11, 13, 196, 196, 0, NULL, NULL, 0, '2021-09-12 12:20:01', '2021-09-12 12:20:01', NULL),
(12, 14, 0, 0, 0, NULL, NULL, 0, '2021-09-12 12:20:37', '2021-09-12 12:20:37', NULL),
(13, 15, 294, 294, 0, NULL, NULL, 0, '2021-09-12 12:24:36', '2021-09-12 12:24:36', NULL),
(14, 16, 0, 0, 0, NULL, NULL, 0, '2021-09-12 12:36:08', '2021-09-12 12:36:08', NULL),
(15, 17, 0, 0, 0, NULL, NULL, 0, '2021-09-12 12:36:27', '2021-09-12 12:36:27', NULL),
(16, 18, 0, 0, 0, NULL, NULL, 0, '2021-09-12 12:37:21', '2021-09-12 12:37:21', NULL),
(17, 19, 0, 0, 0, NULL, NULL, 0, '2021-09-12 12:38:03', '2021-09-12 12:38:03', NULL),
(18, 20, 294, 294, 0, NULL, NULL, 0, '2021-09-12 12:38:33', '2021-09-12 12:38:33', NULL),
(19, 21, 196, 196, 0, NULL, NULL, 0, '2021-09-12 12:53:34', '2021-09-12 12:53:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `factor_has_items`
--

CREATE TABLE `factor_has_items` (
  `id` int(11) NOT NULL,
  `factor_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `custom_product_id` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `bread_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `factor_has_items`
--

INSERT INTO `factor_has_items` (`id`, `factor_id`, `product_id`, `custom_product_id`, `count`, `bread_id`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(2, 1, 2, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(3, 1, 3, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(4, 1, 4, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(5, 1, 1, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(6, 1, 2, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(7, 1, 6, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(8, 1, 5, NULL, 2, 0, 99, '2020-12-09 20:29:36', '2020-12-08 20:30:00', NULL),
(9, 5, 2, NULL, 1, 0, 99, '2021-09-12 12:58:49', '2021-09-12 12:58:49', NULL),
(10, 6, 2, NULL, 1, 0, 99, '2021-09-12 13:00:20', '2021-09-12 13:00:20', NULL),
(11, 7, 2, NULL, 1, 0, 99, '2021-09-12 13:09:14', '2021-09-12 13:09:14', NULL),
(12, 8, 2, NULL, 1, 0, 99, '2021-09-12 13:09:23', '2021-09-12 13:09:23', NULL),
(13, 9, 2, NULL, 1, 0, 99, '2021-09-12 13:10:28', '2021-09-12 13:10:28', NULL),
(14, 11, 1, NULL, 2, 0, 98, '2021-09-12 12:20:01', '2021-09-12 12:20:01', NULL),
(15, 13, 3, NULL, 3, 0, 98, '2021-09-12 12:24:36', '2021-09-12 12:24:36', NULL),
(16, 18, 1, NULL, 3, 0, 98, '2021-09-12 12:38:33', '2021-09-12 12:38:33', NULL),
(17, 19, 1, NULL, 2, 0, 98, '2021-09-12 12:53:34', '2021-09-12 12:53:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gateway_transactions`
--

CREATE TABLE `gateway_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `port` enum('MELLAT','JAHANPAY','PARSIAN','PASARGAD','PAYLINE','SADAD','ZARINPAL','SAMAN','ASANPARDAKHT','PAYPAL') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `ref_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('INIT','SUCCEED','FAILED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INIT',
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `factor_id` int(11) DEFAULT NULL,
  `payment_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gateway_transactions`
--

INSERT INTO `gateway_transactions` (`id`, `port`, `price`, `ref_id`, `tracking_code`, `card_number`, `status`, `ip`, `user_id`, `order_id`, `factor_id`, `payment_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(152758477557, 'PARSIAN', '52300.00', NULL, NULL, NULL, 'FAILED', '184.170.253.246', 0, NULL, 0, NULL, '2018-05-29 09:06:15', '2018-05-29 09:06:16', NULL),
(152758505980, 'PARSIAN', '52300.00', NULL, NULL, NULL, 'INIT', '184.170.253.246', 0, NULL, 0, NULL, '2018-05-29 09:10:59', '2018-05-29 09:10:59', NULL),
(152758648947, 'PARSIAN', '52300.00', NULL, NULL, NULL, 'INIT', '184.170.253.246', 0, NULL, 0, NULL, '2018-05-29 09:34:49', '2018-05-29 09:34:49', NULL),
(152758721892, 'PARSIAN', '52300.00', NULL, NULL, NULL, 'INIT', '184.170.253.246', 0, NULL, 0, NULL, '2018-05-29 09:46:58', '2018-05-29 09:46:58', NULL),
(152758886544, 'SAMAN', '100000.00', NULL, NULL, NULL, 'INIT', '184.170.253.246', 0, NULL, 0, NULL, '2018-05-29 10:14:25', '2018-05-29 10:14:25', NULL),
(152758898776, 'SAMAN', '100000.00', NULL, NULL, NULL, 'INIT', '184.170.253.246', 0, NULL, 0, NULL, '2018-05-29 10:16:27', '2018-05-29 10:16:27', NULL),
(152758905536, 'SAMAN', '100000.00', NULL, NULL, NULL, 'INIT', '184.170.253.246', 0, NULL, 0, NULL, '2018-05-29 10:17:35', '2018-05-29 10:17:35', NULL),
(152806502257, 'SAMAN', '91150.00', NULL, NULL, NULL, 'INIT', '198.7.58.230', 0, NULL, 0, NULL, '2018-06-03 22:30:22', '2018-06-03 22:30:22', NULL),
(152806503119, 'SAMAN', '91150.00', NULL, NULL, NULL, 'INIT', '198.7.58.230', 0, NULL, 0, NULL, '2018-06-03 22:30:31', '2018-06-03 22:30:31', NULL),
(152836165138, 'SAMAN', '244300.00', NULL, NULL, NULL, 'INIT', '5.215.46.249', 0, NULL, 0, NULL, '2018-06-07 08:54:11', '2018-06-07 08:54:11', NULL),
(152846054117, 'SAMAN', '15000.00', NULL, NULL, NULL, 'INIT', '5.214.56.121', 0, NULL, 0, NULL, '2018-06-08 12:22:21', '2018-06-08 12:22:21', NULL),
(152861780241, 'MELLAT', '25000.00', NULL, NULL, NULL, 'FAILED', '5.218.234.246', 0, NULL, 0, NULL, '2018-06-10 08:03:22', '2018-06-10 08:03:22', NULL),
(152861781967, 'MELLAT', '25000.00', NULL, NULL, NULL, 'FAILED', '5.218.234.246', 0, NULL, 0, NULL, '2018-06-10 08:03:39', '2018-06-10 08:03:39', NULL),
(152861794911, 'MELLAT', '25000.00', NULL, NULL, NULL, 'FAILED', '5.218.234.246', 0, NULL, 0, NULL, '2018-06-10 08:05:49', '2018-06-10 08:06:01', NULL),
(152861799613, 'MELLAT', '25000.00', NULL, NULL, NULL, 'FAILED', '5.218.234.246', 0, NULL, 0, NULL, '2018-06-10 08:06:36', '2018-06-10 08:06:36', NULL),
(152861802459, 'MELLAT', '25000.00', NULL, NULL, NULL, 'FAILED', '5.218.234.246', 0, NULL, 0, NULL, '2018-06-10 08:07:04', '2018-06-10 08:07:05', NULL),
(152861824525, 'MELLAT', '25000.00', NULL, NULL, NULL, 'FAILED', '151.245.149.47', 0, NULL, 0, NULL, '2018-06-10 08:10:45', '2018-06-10 08:10:46', NULL),
(152861828709, 'MELLAT', '25000.00', NULL, NULL, NULL, 'FAILED', '151.245.149.47', 0, NULL, 0, NULL, '2018-06-10 08:11:27', '2018-06-10 08:11:28', NULL),
(152861910296, 'ZARINPAL', '2500.00', '000000000000000000000000000000000199', NULL, NULL, 'INIT', '5.218.185.195', 0, NULL, 0, NULL, '2018-06-10 08:25:02', '2018-06-10 08:25:08', NULL),
(152861929623, 'ZARINPAL', '10000.00', '000000000000000000000000000000000200', NULL, NULL, 'INIT', '5.218.185.195', 0, NULL, 0, NULL, '2018-06-10 08:28:16', '2018-06-10 08:28:20', NULL),
(152861934381, 'ZARINPAL', '10000.00', '000000000000000000000000000000000201', NULL, NULL, 'INIT', '5.218.185.195', 0, NULL, 0, NULL, '2018-06-10 08:29:03', '2018-06-10 08:29:04', NULL),
(152862298362, 'ZARINPAL', '10000.00', '000000000000000000000000000000000202', NULL, NULL, 'INIT', '5.218.95.68', 0, NULL, 0, NULL, '2018-06-10 09:29:43', '2018-06-10 09:29:45', NULL),
(152862586683, 'ZARINPAL', '1630.00', '000000000000000000000000000000000203', NULL, NULL, 'INIT', '5.218.61.103', 0, NULL, 0, NULL, '2018-06-10 10:17:46', '2018-06-10 10:17:48', NULL),
(152862588746, 'ZARINPAL', '2500.00', '000000000000000000000000000000000204', NULL, NULL, 'INIT', '5.218.61.103', 0, NULL, 0, NULL, '2018-06-10 10:18:07', '2018-06-10 10:18:08', NULL),
(152869560910, 'ZARINPAL', '20200.00', '000000000000000000000000000000000205', NULL, NULL, 'INIT', '5.213.125.135', 0, NULL, 0, NULL, '2018-06-11 05:40:09', '2018-06-11 05:40:09', NULL),
(152869625420, 'ZARINPAL', '2500.00', '000000000000000000000000000000000206', NULL, NULL, 'INIT', '5.213.125.135', 0, NULL, 0, NULL, '2018-06-11 05:50:54', '2018-06-11 05:50:54', NULL),
(152869773069, 'ZARINPAL', '1150.00', '000000000000000000000000000000000207', NULL, NULL, 'INIT', '5.213.161.56', 0, NULL, 0, NULL, '2018-06-11 06:15:30', '2018-06-11 06:15:31', NULL),
(152869778330, 'ZARINPAL', '0.00', NULL, NULL, NULL, 'FAILED', '5.213.161.56', 0, NULL, 0, NULL, '2018-06-11 06:16:23', '2018-06-11 06:16:23', NULL),
(152869780720, 'ZARINPAL', '2500.00', '000000000000000000000000000000000208', NULL, NULL, 'INIT', '5.213.161.56', 0, NULL, 0, NULL, '2018-06-11 06:16:47', '2018-06-11 06:16:47', NULL),
(152869803534, 'ZARINPAL', '2500.00', '000000000000000000000000000000000209', NULL, NULL, 'INIT', '5.213.161.56', 0, NULL, 0, NULL, '2018-06-11 06:20:35', '2018-06-11 06:20:36', NULL),
(152869806702, 'ZARINPAL', '2500.00', '000000000000000000000000000000000210', NULL, NULL, 'INIT', '5.213.161.56', 0, NULL, 0, NULL, '2018-06-11 06:21:07', '2018-06-11 06:21:07', NULL),
(152869826064, 'ZARINPAL', '2500.00', '000000000000000000000000000000000211', NULL, NULL, 'INIT', '5.213.161.56', 0, NULL, 0, NULL, '2018-06-11 06:24:20', '2018-06-11 06:24:20', NULL),
(152869926611, 'ZARINPAL', '10000.00', '000000000000000000000000000000000212', NULL, NULL, 'INIT', '5.213.31.235', 0, NULL, 0, NULL, '2018-06-11 06:41:06', '2018-06-11 06:41:06', NULL),
(152870003759, 'ZARINPAL', '2500.00', '000000000000000000000000000000000213', NULL, NULL, 'INIT', '5.213.31.235', 0, NULL, 0, NULL, '2018-06-11 06:53:57', '2018-06-11 06:54:02', NULL),
(152870012945, 'ZARINPAL', '2500.00', '000000000000000000000000000000000214', NULL, NULL, 'INIT', '5.213.31.235', 0, NULL, 0, NULL, '2018-06-11 06:55:29', '2018-06-11 06:55:30', NULL),
(152870052503, 'ZARINPAL', '0.00', NULL, NULL, NULL, 'FAILED', '5.213.31.235', 0, NULL, 0, NULL, '2018-06-11 07:02:05', '2018-06-11 07:02:06', NULL),
(152870082403, 'ZARINPAL', '1150.00', NULL, NULL, NULL, 'INIT', '5.213.33.162', 0, NULL, 0, NULL, '2018-06-11 07:07:04', '2018-06-11 07:07:04', NULL),
(152870087746, 'ZARINPAL', '1150.00', '000000000000000000000000000000000215', NULL, NULL, 'INIT', '5.213.33.162', 0, NULL, 0, NULL, '2018-06-11 07:07:57', '2018-06-11 07:07:57', NULL),
(152870189053, 'ZARINPAL', '2500.00', '000000000000000000000000000000000216', NULL, NULL, 'INIT', '5.213.33.162', 0, NULL, 0, NULL, '2018-06-11 07:24:50', '2018-06-11 07:24:51', NULL),
(152870294855, 'ZARINPAL', '2500.00', '000000000000000000000000000000000217', NULL, NULL, 'INIT', '5.213.177.31', 0, NULL, 0, NULL, '2018-06-11 07:42:28', '2018-06-11 07:42:37', NULL),
(152870330295, 'ZARINPAL', '2500.00', '000000000000000000000000000000000218', NULL, NULL, 'INIT', '5.213.177.31', 0, NULL, 0, NULL, '2018-06-11 07:48:22', '2018-06-11 07:48:23', NULL),
(152879677798, 'ZARINPAL', '1500.00', '000000000000000000000000000000000219', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 09:46:17', '2018-06-12 09:46:18', NULL),
(152879701515, 'ZARINPAL', '1500.00', '000000000000000000000000000000000220', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 09:50:15', '2018-06-12 09:50:15', NULL),
(152879705737, 'ZARINPAL', '2500.00', '000000000000000000000000000000000221', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 09:50:57', '2018-06-12 09:50:57', NULL),
(152879741583, 'ZARINPAL', '2500.00', '000000000000000000000000000000000222', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 09:56:55', '2018-06-12 09:56:56', NULL),
(152879796802, 'ZARINPAL', '2500.00', '000000000000000000000000000000000223', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:06:08', '2018-06-12 10:06:08', NULL),
(152879801934, 'ZARINPAL', '2500.00', '000000000000000000000000000000000224', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:06:59', '2018-06-12 10:06:59', NULL),
(152879818232, 'ZARINPAL', '2500.00', '000000000000000000000000000000000225', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:09:42', '2018-06-12 10:09:42', NULL),
(152879841961, 'ZARINPAL', '2500.00', '000000000000000000000000000000000226', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:13:39', '2018-06-12 10:13:40', NULL),
(152879857768, 'ZARINPAL', '2500.00', '000000000000000000000000000000000227', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:16:17', '2018-06-12 10:16:17', NULL),
(152879860839, 'ZARINPAL', '2500.00', '000000000000000000000000000000000228', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:16:48', '2018-06-12 10:16:48', NULL),
(152879866198, 'ZARINPAL', '1150.00', '000000000000000000000000000000000229', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:17:41', '2018-06-12 10:17:42', NULL),
(152879871470, 'ZARINPAL', '1150.00', '000000000000000000000000000000000230', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:18:34', '2018-06-12 10:18:34', NULL),
(152879967289, 'ZARINPAL', '2500.00', '000000000000000000000000000000000231', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:34:32', '2018-06-12 10:34:33', NULL),
(152879984412, 'ZARINPAL', '1150.00', '000000000000000000000000000000000232', NULL, NULL, 'INIT', '5.213.160.164', 0, NULL, 0, NULL, '2018-06-12 10:37:24', '2018-06-12 10:37:24', NULL),
(152888168333, 'ZARINPAL', '2500.00', '000000000000000000000000000000000234', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-13 09:21:23', '2018-06-13 09:21:23', NULL),
(152888388476, 'ZARINPAL', '2500.00', '000000000000000000000000000000000235', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-13 09:58:04', '2018-06-13 09:58:06', NULL),
(152888611376, 'ZARINPAL', '2500.00', '000000000000000000000000000000000236', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-13 10:35:13', '2018-06-13 10:35:15', NULL),
(152888630177, 'ZARINPAL', '2500.00', '000000000000000000000000000000000237', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-13 10:38:21', '2018-06-13 10:38:21', NULL),
(152888709630, 'ZARINPAL', '2500.00', '000000000000000000000000000000000238', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-13 10:51:36', '2018-06-13 10:51:36', NULL),
(152888954281, 'ZARINPAL', '2500.00', '000000000000000000000000000000000239', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-13 11:32:22', '2018-06-13 11:32:23', NULL),
(152895706768, 'ZARINPAL', '2500.00', '000000000000000000000000000000000240', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-14 06:17:47', '2018-06-14 06:17:48', NULL),
(152895786310, 'ZARINPAL', '2500.00', '000000000000000000000000000000000241', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-14 06:31:03', '2018-06-14 06:31:03', NULL),
(152896577148, 'ZARINPAL', '3053.00', '000000000000000000000000000000000242', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-14 08:42:51', '2018-06-14 08:42:51', NULL),
(152896973628, 'ZARINPAL', '2953.00', '000000000000000000000000000000000243', NULL, NULL, 'INIT', '151.245.170.161', 0, NULL, 0, NULL, '2018-06-14 09:48:56', '2018-06-14 09:48:56', NULL),
(152930632335, 'ZARINPAL', '2500.00', '000000000000000000000000000000000244', NULL, NULL, 'INIT', '107.181.165.204', 0, NULL, 0, NULL, '2018-06-18 07:18:43', '2018-06-18 07:18:43', NULL),
(152930635680, 'ZARINPAL', '2500.00', '000000000000000000000000000000000245', NULL, NULL, 'INIT', '107.181.165.204', 0, NULL, 0, NULL, '2018-06-18 07:19:16', '2018-06-18 07:19:17', NULL),
(152930769689, 'ZARINPAL', '2500.00', '000000000000000000000000000000000246', NULL, NULL, 'INIT', '5.213.170.108', 0, NULL, 0, NULL, '2018-06-18 07:41:36', '2018-06-18 07:41:37', NULL),
(152930787679, 'ZARINPAL', '2500.00', '000000000000000000000000000000000247', NULL, NULL, 'INIT', '5.213.170.108', 0, NULL, 0, NULL, '2018-06-18 07:44:36', '2018-06-18 07:44:37', NULL),
(152930791358, 'ZARINPAL', '2500.00', '000000000000000000000000000000000248', NULL, NULL, 'INIT', '5.213.170.108', 0, NULL, 0, NULL, '2018-06-18 07:45:13', '2018-06-18 07:45:13', NULL),
(152931009025, 'ZARINPAL', '2500.00', '000000000000000000000000000000000249', NULL, NULL, 'INIT', '151.245.167.113', 0, NULL, 0, NULL, '2018-06-18 08:21:30', '2018-06-18 08:21:30', NULL),
(152931025112, 'ZARINPAL', '2500.00', '000000000000000000000000000000000250', NULL, NULL, 'INIT', '151.245.167.113', 0, NULL, 0, NULL, '2018-06-18 08:24:11', '2018-06-18 08:24:11', NULL),
(152931036848, 'ZARINPAL', '2500.00', '000000000000000000000000000000000251', NULL, NULL, 'INIT', '151.245.167.113', 0, NULL, 0, NULL, '2018-06-18 08:26:08', '2018-06-18 08:26:08', NULL),
(152931133079, 'ZARINPAL', '0.00', NULL, NULL, NULL, 'FAILED', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-18 08:42:10', '2018-06-18 08:42:11', NULL),
(152931141535, 'ZARINPAL', '2000.00', '000000000000000000000000000000000252', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-18 08:43:35', '2018-06-18 08:43:35', NULL),
(152931183796, 'ZARINPAL', '2000.00', '000000000000000000000000000000000253', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-18 08:50:37', '2018-06-18 08:50:38', NULL),
(152931275098, 'ZARINPAL', '200.00', '000000000000000000000000000000000254', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-18 09:05:50', '2018-06-18 09:05:51', NULL),
(152931288522, 'ZARINPAL', '3053.00', '000000000000000000000000000000000255', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-18 09:08:05', '2018-06-18 09:08:05', NULL),
(152931293002, 'ZARINPAL', '3053.00', '000000000000000000000000000000000256', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-18 09:08:50', '2018-06-18 09:08:50', NULL),
(152938637431, 'ZARINPAL', '200.00', '000000000000000000000000000000000257', NULL, NULL, 'INIT', '107.152.104.131', 0, NULL, 0, NULL, '2018-06-19 05:32:54', '2018-06-19 05:32:54', NULL),
(152938649711, 'ZARINPAL', '300.00', '000000000000000000000000000000000258', NULL, NULL, 'INIT', '107.152.104.131', 0, NULL, 0, NULL, '2018-06-19 05:34:57', '2018-06-19 05:34:57', NULL),
(152938661713, 'ZARINPAL', '4000.00', '000000000000000000000000000000000259', NULL, NULL, 'INIT', '107.152.104.131', 0, NULL, 0, NULL, '2018-06-19 05:36:57', '2018-06-19 05:36:57', NULL),
(152938905602, 'ZARINPAL', '300.00', '000000000000000000000000000000000260', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-19 06:17:36', '2018-06-19 06:17:36', NULL),
(152938916266, 'ZARINPAL', '200.00', '000000000000000000000000000000000261', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-19 06:19:22', '2018-06-19 06:19:22', NULL),
(152938923940, 'ZARINPAL', '2000.00', '000000000000000000000000000000000262', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-19 06:20:39', '2018-06-19 06:20:39', NULL),
(152938942345, 'ZARINPAL', '200.00', '000000000000000000000000000000000263', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-19 06:23:43', '2018-06-19 06:23:43', NULL),
(152938949516, 'ZARINPAL', '200.00', '000000000000000000000000000000000264', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-19 06:24:55', '2018-06-19 06:24:55', NULL),
(152938958886, 'ZARINPAL', '2000.00', '000000000000000000000000000000000265', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-19 06:26:28', '2018-06-19 06:26:29', NULL),
(152939128637, 'ZARINPAL', '200.00', '000000000000000000000000000000000266', NULL, NULL, 'INIT', '185.93.183.22', 0, NULL, 0, NULL, '2018-06-19 06:54:46', '2018-06-19 06:54:46', NULL),
(152940047633, 'ZARINPAL', '3053.00', '000000000000000000000000000000000267', NULL, NULL, 'INIT', '185.93.183.22', 1, NULL, 0, NULL, '2018-06-19 09:27:56', '2018-06-19 09:27:56', NULL),
(152940182621, 'ZARINPAL', '14966.00', '000000000000000000000000000000000268', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, 0, '2018-06-19 09:55:44', '2018-06-19 09:50:26', '2018-06-19 09:55:44', NULL),
(152940218927, 'ZARINPAL', '14966.00', '000000000000000000000000000000000269', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, 0, '2018-06-19 09:56:44', '2018-06-19 09:56:29', '2018-06-19 09:56:44', NULL),
(152940220263, 'ZARINPAL', '3053.00', '000000000000000000000000000000000270', NULL, '', 'SUCCEED', '185.93.183.22', 1, NULL, 0, '2018-06-19 09:56:58', '2018-06-19 09:56:42', '2018-06-19 09:56:58', NULL),
(152940227212, 'ZARINPAL', '14966.00', '000000000000000000000000000000000271', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, 0, '2018-06-19 09:59:27', '2018-06-19 09:57:52', '2018-06-19 09:59:27', NULL),
(152940894470, 'ZARINPAL', '14966.00', '000000000000000000000000000000000272', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, 0, '2018-06-19 11:49:23', '2018-06-19 11:49:04', '2018-06-19 11:49:23', NULL),
(152940952294, 'ZARINPAL', '14966.00', '000000000000000000000000000000000273', NULL, NULL, 'INIT', '151.245.167.113', 5, NULL, 0, NULL, '2018-06-19 11:58:42', '2018-06-19 11:58:43', NULL),
(152940963366, 'ZARINPAL', '14966.00', '000000000000000000000000000000000274', NULL, NULL, 'INIT', '151.245.167.113', 5, 200, 0, NULL, '2018-06-19 12:00:33', '2018-06-19 12:00:33', NULL),
(152948769116, 'ZARINPAL', '14966.00', '000000000000000000000000000000000275', NULL, NULL, 'INIT', '151.245.167.113', 5, 209, 184, NULL, '2018-06-20 09:41:31', '2018-06-20 09:41:32', NULL),
(152948778387, 'ZARINPAL', '14966.00', '000000000000000000000000000000000276', NULL, '', 'SUCCEED', '151.245.167.113', 5, 210, 185, '2018-06-20 09:43:17', '2018-06-20 09:43:03', '2018-06-20 09:43:17', NULL),
(152948824762, 'ZARINPAL', '14966.00', '000000000000000000000000000000000277', NULL, '', 'SUCCEED', '151.245.167.113', 5, 211, 186, '2018-06-20 09:51:04', '2018-06-20 09:50:47', '2018-06-20 09:51:04', NULL),
(152948847976, 'ZARINPAL', '14966.00', '000000000000000000000000000000000278', NULL, '', 'SUCCEED', '151.245.167.113', 5, 212, 187, '2018-06-20 09:55:16', '2018-06-20 09:54:39', '2018-06-20 09:55:16', NULL),
(152948861253, 'ZARINPAL', '14966.00', '000000000000000000000000000000000279', NULL, '', 'SUCCEED', '151.245.167.113', 5, 213, 188, '2018-06-20 09:57:28', '2018-06-20 09:56:52', '2018-06-20 09:57:28', NULL),
(152948870681, 'ZARINPAL', '14966.00', '000000000000000000000000000000000280', NULL, '', 'SUCCEED', '151.245.167.113', 5, 214, 189, '2018-06-20 09:58:42', '2018-06-20 09:58:26', '2018-06-20 09:58:42', NULL),
(152948878004, 'ZARINPAL', '1568.00', '000000000000000000000000000000000281', NULL, '', 'SUCCEED', '151.245.167.113', 5, 215, 190, '2018-06-20 09:59:56', '2018-06-20 09:59:40', '2018-06-20 09:59:56', NULL),
(152949007643, 'ZARINPAL', '2000.00', '000000000000000000000000000000000282', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, NULL, '2018-06-20 10:21:34', '2018-06-20 10:21:16', '2018-06-20 10:21:34', NULL),
(152949020094, 'ZARINPAL', '2000.00', '000000000000000000000000000000000283', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, NULL, '2018-06-20 10:23:40', '2018-06-20 10:23:20', '2018-06-20 10:23:40', NULL),
(152949030069, 'ZARINPAL', '2000.00', '000000000000000000000000000000000284', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, NULL, '2018-06-20 10:25:17', '2018-06-20 10:25:00', '2018-06-20 10:25:17', NULL),
(152949039420, 'ZARINPAL', '20000.00', '000000000000000000000000000000000285', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, NULL, '2018-06-20 10:26:46', '2018-06-20 10:26:34', '2018-06-20 10:26:46', NULL),
(152949180993, 'ZARINPAL', '5000.00', '000000000000000000000000000000000286', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, NULL, '2018-06-20 10:50:24', '2018-06-20 10:50:09', '2018-06-20 10:50:24', NULL),
(152957214696, 'ZARINPAL', '5000.00', '000000000000000000000000000000000287', NULL, '', 'SUCCEED', '151.245.167.113', 5, NULL, NULL, '2018-06-21 09:09:22', '2018-06-21 09:09:06', '2018-06-21 09:09:22', NULL),
(152957269385, 'ZARINPAL', '50.00', NULL, NULL, NULL, 'FAILED', '151.245.167.113', 7, NULL, NULL, NULL, '2018-06-21 09:18:13', '2018-06-21 09:18:14', NULL),
(152957269952, 'ZARINPAL', '50.00', NULL, NULL, NULL, 'FAILED', '151.245.167.113', 7, NULL, NULL, NULL, '2018-06-21 09:18:19', '2018-06-21 09:18:19', NULL),
(152957271277, 'ZARINPAL', '500.00', '000000000000000000000000000000000288', NULL, NULL, 'INIT', '151.245.167.113', 7, NULL, NULL, NULL, '2018-06-21 09:18:32', '2018-06-21 09:18:33', NULL),
(152957274765, 'ZARINPAL', '100.00', '000000000000000000000000000000000289', NULL, '', 'SUCCEED', '151.245.167.113', 7, NULL, NULL, '2018-06-21 09:19:19', '2018-06-21 09:19:07', '2018-06-21 09:19:19', NULL),
(152957611374, 'ZARINPAL', '25636.00', '000000000000000000000000000000000290', NULL, '', 'SUCCEED', '151.245.167.113', 4, 216, 191, '2018-06-21 10:15:21', '2018-06-21 10:15:13', '2018-06-21 10:15:21', NULL),
(152957635836, 'ZARINPAL', '5561.00', '000000000000000000000000000000000291', NULL, '', 'SUCCEED', '151.245.167.113', 4, 217, 192, '2018-06-21 10:19:24', '2018-06-21 10:19:18', '2018-06-21 10:19:24', NULL),
(152973462488, 'ZARINPAL', '869.90', '000000000000000000000000000000000292', NULL, NULL, 'INIT', '107.152.104.129', 4, NULL, NULL, NULL, '2018-06-23 06:17:04', '2018-06-23 06:17:05', NULL),
(152973568200, 'ZARINPAL', '1000.00', '000000000000000000000000000000000293', NULL, '', 'SUCCEED', '107.152.104.129', 4, NULL, NULL, '2018-06-23 06:34:58', '2018-06-23 06:34:42', '2018-06-23 06:34:58', NULL),
(152973574188, 'ZARINPAL', '464.60', '000000000000000000000000000000000294', NULL, NULL, 'INIT', '107.152.104.129', 4, NULL, NULL, NULL, '2018-06-23 06:35:41', '2018-06-23 06:35:42', NULL),
(152973763829, 'ZARINPAL', '22500.00', '000000000000000000000000000000000295', NULL, '', 'SUCCEED', '151.245.167.113', 4, 218, 193, '2018-06-23 07:07:28', '2018-06-23 07:07:18', '2018-06-23 07:07:28', NULL),
(152974732651, 'ZARINPAL', '14053.00', '000000000000000000000000000000000296', NULL, '', 'SUCCEED', '107.152.104.45', 1, 223, 198, '2018-06-23 09:49:00', '2018-06-23 09:48:46', '2018-06-23 09:49:00', NULL),
(152974998283, 'ZARINPAL', '2000.00', '000000000000000000000000000000000297', NULL, '', 'SUCCEED', '185.93.183.22', 1, NULL, NULL, '2018-06-23 10:33:21', '2018-06-23 10:33:02', '2018-06-23 10:33:21', NULL),
(152991477826, 'ZARINPAL', '2467.00', '000000000000000000000000000000000298', NULL, '', 'SUCCEED', '5.217.184.114', 1, 230, 205, '2018-06-25 08:20:10', '2018-06-25 08:19:38', '2018-06-25 08:20:10', NULL),
(152991557074, 'ZARINPAL', '500.00', '000000000000000000000000000000000299', NULL, '', 'SUCCEED', '5.217.184.114', 1, NULL, NULL, '2018-06-25 08:33:02', '2018-06-25 08:32:50', '2018-06-25 08:33:02', NULL),
(152991728647, 'ZARINPAL', '1288.00', '000000000000000000000000000000000300', NULL, '', 'SUCCEED', '5.217.244.73', 1, 231, 206, '2018-06-25 09:01:41', '2018-06-25 09:01:26', '2018-06-25 09:01:41', NULL),
(152991794799, 'ZARINPAL', '11477.00', '000000000000000000000000000000000301', NULL, NULL, 'INIT', '151.245.216.66', 4, 233, 208, NULL, '2018-06-25 09:12:27', '2018-06-25 09:12:28', NULL),
(152991804438, 'ZARINPAL', '11477.00', '000000000000000000000000000000000302', NULL, NULL, 'INIT', '151.245.216.66', 4, 234, 209, NULL, '2018-06-25 09:14:04', '2018-06-25 09:14:04', NULL),
(152991807096, 'ZARINPAL', '11477.00', '000000000000000000000000000000000303', NULL, NULL, 'INIT', '151.245.216.66', 4, 235, 210, NULL, '2018-06-25 09:14:30', '2018-06-25 09:14:31', NULL),
(152991822788, 'ZARINPAL', '11477.00', '000000000000000000000000000000000304', NULL, NULL, 'INIT', '151.245.216.66', 4, 236, 211, NULL, '2018-06-25 09:17:07', '2018-06-25 09:17:08', NULL),
(152992247752, 'ZARINPAL', '200.00', '000000000000000000000000000000000305', NULL, '', 'SUCCEED', '88.208.221.101', 5, NULL, NULL, '2018-06-25 10:28:13', '2018-06-25 10:27:57', '2018-06-25 10:28:13', NULL),
(152992783945, 'ZARINPAL', '2468.00', '000000000000000000000000000000000306', NULL, '', 'SUCCEED', '151.245.216.66', 4, 240, 215, '2018-06-25 11:57:59', '2018-06-25 11:57:19', '2018-06-25 11:57:59', NULL),
(152992812484, 'ZARINPAL', '2468.00', '000000000000000000000000000000000307', NULL, '', 'SUCCEED', '151.245.216.66', 4, 241, 216, '2018-06-25 12:02:10', '2018-06-25 12:02:04', '2018-06-25 12:02:10', NULL),
(152992820568, 'ZARINPAL', '2468.00', '000000000000000000000000000000000308', NULL, '', 'SUCCEED', '151.245.216.66', 4, 242, 217, '2018-06-25 12:03:34', '2018-06-25 12:03:25', '2018-06-25 12:03:34', NULL),
(152992906311, 'ZARINPAL', '56286.00', '000000000000000000000000000000000309', NULL, '', 'SUCCEED', '151.245.216.66', 4, 244, 219, '2018-06-25 12:17:52', '2018-06-25 12:17:43', '2018-06-25 12:17:52', NULL),
(152992912661, 'ZARINPAL', '56286.00', '000000000000000000000000000000000310', NULL, '', 'SUCCEED', '151.245.216.66', 4, 245, 220, '2018-06-25 12:18:53', '2018-06-25 12:18:46', '2018-06-25 12:18:53', NULL),
(152992933470, 'ZARINPAL', '56286.00', '000000000000000000000000000000000311', NULL, '', 'SUCCEED', '151.245.216.66', 4, 246, 221, '2018-06-25 12:22:20', '2018-06-25 12:22:14', '2018-06-25 12:22:20', NULL),
(152992936455, 'ZARINPAL', '56286.00', '000000000000000000000000000000000312', NULL, '', 'SUCCEED', '151.245.216.66', 4, 247, 222, '2018-06-25 12:22:50', '2018-06-25 12:22:44', '2018-06-25 12:22:50', NULL),
(152992945196, 'ZARINPAL', '56286.00', '000000000000000000000000000000000313', NULL, '', 'SUCCEED', '151.245.216.66', 4, 248, 223, '2018-06-25 12:24:18', '2018-06-25 12:24:11', '2018-06-25 12:24:18', NULL),
(152992952442, 'ZARINPAL', '56286.00', '000000000000000000000000000000000314', NULL, '', 'SUCCEED', '151.245.216.66', 4, 249, 224, '2018-06-25 12:25:32', '2018-06-25 12:25:24', '2018-06-25 12:25:32', NULL),
(152992961348, 'ZARINPAL', '56286.00', '000000000000000000000000000000000315', NULL, '', 'SUCCEED', '151.245.216.66', 4, 250, 225, '2018-06-25 12:27:00', '2018-06-25 12:26:53', '2018-06-25 12:27:00', NULL),
(152992964161, 'ZARINPAL', '56286.00', '000000000000000000000000000000000316', NULL, '', 'SUCCEED', '151.245.216.66', 4, 251, 226, '2018-06-25 12:27:28', '2018-06-25 12:27:21', '2018-06-25 12:27:28', NULL),
(152992977965, 'ZARINPAL', '67589.00', '000000000000000000000000000000000317', NULL, '', 'SUCCEED', '151.245.216.66', 4, 253, 228, '2018-06-25 12:29:45', '2018-06-25 12:29:39', '2018-06-25 12:29:45', NULL),
(152992994329, 'ZARINPAL', '67589.00', '000000000000000000000000000000000318', NULL, '', 'SUCCEED', '151.245.216.66', 4, 254, 229, '2018-06-25 12:32:30', '2018-06-25 12:32:23', '2018-06-25 12:32:30', NULL),
(152993003414, 'ZARINPAL', '89589.00', '000000000000000000000000000000000319', NULL, '', 'SUCCEED', '151.245.216.66', 4, 255, 230, '2018-06-25 12:34:01', '2018-06-25 12:33:54', '2018-06-25 12:34:01', NULL),
(152993010074, 'ZARINPAL', '23589.00', '000000000000000000000000000000000320', NULL, '', 'SUCCEED', '151.245.216.66', 4, 256, 231, '2018-06-25 12:35:08', '2018-06-25 12:35:00', '2018-06-25 12:35:08', NULL),
(152993014776, 'ZARINPAL', '23589.00', '000000000000000000000000000000000321', NULL, '', 'SUCCEED', '151.245.216.66', 4, 257, 232, '2018-06-25 12:35:54', '2018-06-25 12:35:47', '2018-06-25 12:35:54', NULL),
(153003277051, 'ZARINPAL', '100.00', '000000000000000000000000000000000322', NULL, NULL, 'INIT', '5.119.73.198', 9, NULL, NULL, NULL, '2018-06-26 17:06:10', '2018-06-26 17:06:11', NULL),
(153017884515, 'ZARINPAL', '250.00', '000000000000000000000000000000000323', NULL, '', 'SUCCEED', '151.245.216.66', 1, NULL, NULL, '2018-06-28 09:43:04', '2018-06-28 09:40:45', '2018-06-28 09:43:04', NULL),
(153035471821, 'ZARINPAL', '2000.00', '000000000000000000000000000000000324', NULL, '', 'SUCCEED', '62.151.181.60', 5, NULL, NULL, '2018-06-30 10:32:26', '2018-06-30 10:31:58', '2018-06-30 10:32:26', NULL),
(153043079778, 'ZARINPAL', '20000.00', '000000000000000000000000000000000325', NULL, '', 'SUCCEED', '151.245.142.37', 4, NULL, NULL, '2018-07-01 07:40:15', '2018-07-01 07:39:57', '2018-07-01 07:40:15', NULL),
(153051050794, 'ZARINPAL', '2000.00', '000000000000000000000000000000000326', NULL, '', 'SUCCEED', '151.245.186.57', 4, NULL, NULL, '2018-07-02 05:48:39', '2018-07-02 05:48:27', '2018-07-02 05:48:39', NULL),
(153051769504, 'ZARINPAL', '2000.00', '000000000000000000000000000000000327', NULL, '', 'SUCCEED', '151.245.186.57', 4, NULL, NULL, '2018-07-02 07:48:25', '2018-07-02 07:48:15', '2018-07-02 07:48:25', NULL),
(153051802692, 'ZARINPAL', '2000.00', '000000000000000000000000000000000328', NULL, '', 'SUCCEED', '151.245.186.57', 4, NULL, NULL, '2018-07-02 07:53:56', '2018-07-02 07:53:46', '2018-07-02 07:53:56', NULL),
(153051833540, 'ZARINPAL', '2000.00', '000000000000000000000000000000000329', NULL, '', 'SUCCEED', '151.245.186.57', 4, NULL, NULL, '2018-07-02 07:59:04', '2018-07-02 07:58:55', '2018-07-02 07:59:04', NULL),
(153051846180, 'ZARINPAL', '20000.00', '000000000000000000000000000000000330', NULL, '', 'SUCCEED', '151.245.186.57', 4, NULL, NULL, '2018-07-02 08:01:09', '2018-07-02 08:01:01', '2018-07-02 08:01:09', NULL),
(153095352862, 'ZARINPAL', '28025.00', '000000000000000000000000000000000331', NULL, '', 'SUCCEED', '151.245.156.25', 4, 270, 245, '2018-07-07 08:52:25', '2018-07-07 08:52:08', '2018-07-07 08:52:25', NULL),
(153156886682, 'ZARINPAL', '15997.00', NULL, NULL, NULL, 'FAILED', '151.245.136.99', 1, 271, 246, '2018-07-14 11:47:46', '2018-07-14 11:47:46', '2018-07-14 11:47:47', NULL),
(153190560007, 'ZARINPAL', '76070.00', NULL, NULL, NULL, 'FAILED', '151.245.142.248', 5, 274, 249, '2018-07-18 09:20:00', '2018-07-18 09:20:00', '2018-07-18 09:20:01', NULL),
(153215953077, 'ZARINPAL', '2000.00', NULL, NULL, NULL, 'FAILED', '95.64.85.117', 5, NULL, NULL, '2018-07-21 07:52:10', '2018-07-21 07:52:10', '2018-07-21 07:52:13', NULL),
(153218654999, 'ZARINPAL', '2800.00', NULL, NULL, NULL, 'FAILED', '37.63.255.149', 5, NULL, NULL, '2018-07-21 15:22:29', '2018-07-21 15:22:29', '2018-07-21 15:22:31', NULL),
(153310281268, 'ZARINPAL', '4989.00', NULL, NULL, NULL, 'FAILED', '151.245.138.136', 4, 277, 252, '2018-08-01 05:53:32', '2018-08-01 05:53:32', '2018-08-01 05:53:33', NULL),
(153400435929, 'ZARINPAL', '40885.00', NULL, NULL, NULL, 'FAILED', '104.200.140.134', 1, 282, 257, '2018-08-11 16:19:19', '2018-08-11 16:19:19', '2018-08-11 16:19:19', NULL),
(153400436583, 'ZARINPAL', '40885.00', NULL, NULL, NULL, 'FAILED', '104.200.140.134', 1, 283, 258, '2018-08-11 16:19:25', '2018-08-11 16:19:25', '2018-08-11 16:19:26', NULL),
(153400437620, 'ZARINPAL', '40885.00', NULL, NULL, NULL, 'FAILED', '104.200.140.134', 1, 284, 259, '2018-08-11 16:19:36', '2018-08-11 16:19:36', '2018-08-11 16:19:36', NULL),
(153400438474, 'ZARINPAL', '40885.00', NULL, NULL, NULL, 'FAILED', '104.200.140.134', 1, 285, 260, '2018-08-11 16:19:44', '2018-08-11 16:19:44', '2018-08-11 16:19:44', NULL),
(153632559984, 'ZARINPAL', '10250.00', '000000000000000000000000000000000346', NULL, NULL, 'INIT', '188.253.43.92', 14, 286, 261, '2018-09-07 13:06:39', '2018-09-07 13:06:39', '2018-09-07 13:06:40', NULL),
(153632562389, 'ZARINPAL', '10250.00', '000000000000000000000000000000000347', NULL, NULL, 'INIT', '188.253.43.92', 14, 287, 262, '2018-09-07 13:07:03', '2018-09-07 13:07:03', '2018-09-07 13:07:04', NULL),
(153632575157, 'ZARINPAL', '32250.00', '000000000000000000000000000000000348', NULL, NULL, 'INIT', '188.253.43.92', 14, 288, 263, '2018-09-07 13:09:11', '2018-09-07 13:09:11', '2018-09-07 13:09:11', NULL),
(153655868030, 'ZARINPAL', '11303.00', '000000000000000000000000000000000349', NULL, NULL, 'INIT', '151.233.202.193', 12, 289, 264, '2018-09-10 05:51:20', '2018-09-10 05:51:20', '2018-09-10 05:51:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gateway_transactions_logs`
--

CREATE TABLE `gateway_transactions_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_id` bigint(20) UNSIGNED NOT NULL,
  `result_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gateway_transactions_logs`
--

INSERT INTO `gateway_transactions_logs` (`id`, `transaction_id`, `result_code`, `result_message`, `log_date`) VALUES
(1, 152758477557, 'SoapFault', 'Server was unable to read request. ---> There is an error in XML document (2, 258). ---> Value was either too large or too small for an Int32.', '2018-05-29 09:06:16'),
(2, 152861780241, '21', 'پذیرنده نامعتبر است', '2018-06-10 08:03:22'),
(3, 152861781967, '21', 'پذیرنده نامعتبر است', '2018-06-10 08:03:39'),
(4, 152861794911, '21', 'پذیرنده نامعتبر است', '2018-06-10 08:06:01'),
(5, 152861799613, '21', 'پذیرنده نامعتبر است', '2018-06-10 08:06:36'),
(6, 152861802459, '21', 'پذیرنده نامعتبر است', '2018-06-10 08:07:05'),
(7, 152861824525, 'SoapFault', 'SOAP-ERROR: Parsing WSDL: Couldn\'t load from \'http://aadasdsbanktesadt.ir/gateway/mellanbmbt/ws?wsdl\' : failed to load external entity \"http://aadasdsbanktesadt.ir/gateway/mellanbmbt/ws?wsdl\"\n', '2018-06-10 08:10:46'),
(8, 152861828709, '21', 'پذیرنده نامعتبر است', '2018-06-10 08:11:28'),
(9, 152869778330, '-1', 'اطلاعات ارسال شده ناقص است.', '2018-06-11 06:16:23'),
(10, 152870052503, '-1', 'اطلاعات ارسال شده ناقص است.', '2018-06-11 07:02:06'),
(11, 152931133079, '-1', 'اطلاعات ارسال شده ناقص است.', '2018-06-18 08:42:11'),
(12, 152940182621, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-19 09:55:44'),
(13, 152940218927, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-19 09:56:44'),
(14, 152940220263, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-19 09:56:58'),
(15, 152940227212, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-19 09:59:27'),
(16, 152940894470, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-19 11:49:23'),
(17, 152948778387, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 09:43:17'),
(18, 152948824762, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 09:51:04'),
(19, 152948847976, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 09:55:16'),
(20, 152948861253, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 09:57:28'),
(21, 152948870681, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 09:58:42'),
(22, 152948878004, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 09:59:56'),
(23, 152949007643, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 10:21:34'),
(24, 152949020094, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 10:23:40'),
(25, 152949030069, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 10:25:17'),
(26, 152949039420, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 10:26:46'),
(27, 152949180993, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-20 10:50:24'),
(28, 152957214696, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-21 09:09:22'),
(29, 152957269385, '-3', 'رقم باید بالای 100 تومان باشد', '2018-06-21 09:18:14'),
(30, 152957269952, '-3', 'رقم باید بالای 100 تومان باشد', '2018-06-21 09:18:19'),
(31, 152957274765, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-21 09:19:19'),
(32, 152957611374, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-21 10:15:21'),
(33, 152957635836, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-21 10:19:24'),
(34, 152973568200, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-23 06:34:58'),
(35, 152973763829, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-23 07:07:28'),
(36, 152974732651, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-23 09:49:00'),
(37, 152974998283, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-23 10:33:21'),
(38, 152991477826, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 08:20:10'),
(39, 152991557074, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 08:33:02'),
(40, 152991728647, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 09:01:41'),
(41, 152992247752, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 10:28:13'),
(42, 152992783945, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 11:57:59'),
(43, 152992812484, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:02:10'),
(44, 152992820568, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:03:34'),
(45, 152992906311, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:17:52'),
(46, 152992912661, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:18:53'),
(47, 152992933470, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:22:20'),
(48, 152992936455, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:22:50'),
(49, 152992945196, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:24:18'),
(50, 152992952442, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:25:32'),
(51, 152992961348, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:27:00'),
(52, 152992964161, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:27:28'),
(53, 152992977965, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:29:45'),
(54, 152992994329, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:32:30'),
(55, 152993003414, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:34:01'),
(56, 152993010074, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:35:08'),
(57, 152993014776, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-25 12:35:54'),
(58, 153017884515, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-28 09:43:04'),
(59, 153035471821, '100', 'پرداخت با موفقیت انجام شد.', '2018-06-30 10:32:26'),
(60, 153043079778, '100', 'پرداخت با موفقیت انجام شد.', '2018-07-01 07:40:15'),
(61, 153051050794, '100', 'پرداخت با موفقیت انجام شد.', '2018-07-02 05:48:39'),
(62, 153051769504, '100', 'پرداخت با موفقیت انجام شد.', '2018-07-02 07:48:25'),
(63, 153051802692, '100', 'پرداخت با موفقیت انجام شد.', '2018-07-02 07:53:56'),
(64, 153051833540, '100', 'پرداخت با موفقیت انجام شد.', '2018-07-02 07:59:04'),
(65, 153051846180, '100', 'پرداخت با موفقیت انجام شد.', '2018-07-02 08:01:09'),
(66, 153095352862, '100', 'پرداخت با موفقیت انجام شد.', '2018-07-07 08:52:25'),
(67, 153156886682, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-07-14 11:47:47'),
(68, 153190560007, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-07-18 09:20:01'),
(69, 153215953077, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-07-21 07:52:13'),
(70, 153218654999, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-07-21 15:22:31'),
(71, 153310281268, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-08-01 05:53:33'),
(72, 153400435929, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-08-11 16:19:19'),
(73, 153400436583, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-08-11 16:19:26'),
(74, 153400437620, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-08-11 16:19:36'),
(75, 153400438474, '-2', 'IP و یا مرچنت کد پذیرنده صحیح نیست', '2018-08-11 16:19:44');

-- --------------------------------------------------------

--
-- Table structure for table `index_categories`
--

CREATE TABLE `index_categories` (
  `id` int(11) NOT NULL,
  `title_en` varchar(200) NOT NULL,
  `desc_en` varchar(200) NOT NULL,
  `title_ar` varchar(500) DEFAULT NULL,
  `desc_ar` varchar(500) DEFAULT NULL,
  `file_name` varchar(200) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` int(1) NOT NULL COMMENT '0 = col / 1= row',
  `url` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `index_categories`
--

INSERT INTO `index_categories` (`id`, `title_en`, `desc_en`, `title_ar`, `desc_ar`, `file_name`, `category_id`, `parent_id`, `type`, `url`, `created_at`, `updated_at`) VALUES
(5, 'Title', 'CATCH BIG <strong>DEALS</strong> ON THE CAMERAS', 'عنوان', 'احصل على <strong> صفقات كبيرة </strong> على الكاميرات', 'img1.png', 1, NULL, 0, NULL, '2021-08-26 14:49:22', '2021-08-26 14:49:22'),
(6, 'Title', 'CATCH BIG <strong>DEALS</strong> ON THE CAMERAS', 'عنوان', 'احصل على <strong> صفقات كبيرة </strong> على الكاميرات', 'img2.png', 1, NULL, 0, NULL, '2021-08-26 14:49:22', '2021-08-26 14:49:22'),
(7, 'Title', 'CATCH BIG <strong>DEALS</strong> ON THE CAMERAS', 'عنوان', 'احصل على <strong> صفقات كبيرة </strong> على الكاميرات', 'img3.png', 1, NULL, 0, NULL, '2021-08-26 14:49:22', '2021-08-26 14:49:22'),
(8, 'Title', 'CATCH BIG <strong>DEALS</strong> ON THE CAMERAS', 'عنوان', 'احصل على <strong> صفقات كبيرة </strong> على الكاميرات', 'img4.png', 1, NULL, 0, NULL, '2021-08-26 14:49:22', '2021-08-26 14:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `url` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `title` varchar(500) COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `category_id`, `tag_id`, `url`, `title`, `created_at`, `updated_at`) VALUES
(11, NULL, NULL, '/products', 'عبايات', '2021-03-23 00:00:00', '2021-03-23 00:00:00'),
(18, NULL, NULL, '/products', 'مغربي', '2021-04-01 00:00:00', '2021-04-01 00:00:00'),
(19, NULL, NULL, '/products', 'شيلة', '2021-04-01 00:00:00', '2021-04-01 00:00:00'),
(20, NULL, NULL, '/products', 'عطور', '2021-04-01 00:00:00', '2021-04-01 00:00:00'),
(21, NULL, NULL, '/products', 'لوزان أوتليت', '2021-04-01 00:00:00', '2021-04-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2014_10_12_000000_create_users_table', 1),
(9, '2014_10_12_100000_create_password_resets_table', 1),
(10, '2019_08_19_000000_create_failed_jobs_table', 1),
(11, '2021_08_23_125114_create_shoppingcart_table', 1),
(12, '2021_08_25_093410_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `percent` int(11) NOT NULL,
  `amount` double NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `type`, `product_id`, `category_id`, `percent`, `amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 5, NULL, 2, 40000, '2018-05-29 07:59:18', '2018-05-29 08:02:25', NULL),
(2, 0, NULL, 6, 15, 100000, '2018-05-29 09:10:28', '2018-05-29 09:10:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `tracking_code` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `recieve_date` datetime DEFAULT NULL,
  `desc` text COLLATE utf8_bin DEFAULT NULL,
  `payment_type` int(1) NOT NULL,
  `paied_at` datetime DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `user_id`, `courier_id`, `status_id`, `tracking_code`, `coupon_id`, `recieve_date`, `desc`, `payment_type`, `paied_at`, `address_id`, `shipping_address_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, 0, '12345', NULL, NULL, NULL, 0, NULL, 1, NULL, '2020-12-09 20:27:39', NULL, NULL),
(2, 27, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 192, 193, '2021-09-12 12:56:11', '2021-09-12 17:26:11', NULL),
(3, 29, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 196, 197, '2021-09-12 12:56:55', '2021-09-12 17:26:55', NULL),
(4, 30, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 198, 199, '2021-09-12 12:57:05', '2021-09-12 17:27:05', NULL),
(5, 31, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 200, 201, '2021-09-12 12:57:26', '2021-09-12 17:27:26', NULL),
(6, 32, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 202, 203, '2021-09-12 12:57:38', '2021-09-12 17:27:38', NULL),
(7, 33, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 204, 205, '2021-09-12 12:58:49', '2021-09-12 17:28:49', NULL),
(8, 34, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 206, 207, '2021-09-12 13:00:20', '2021-09-12 17:30:20', NULL),
(9, 35, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 208, 209, '2021-09-12 13:09:14', '2021-09-12 17:39:14', NULL),
(10, 36, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 210, 211, '2021-09-12 13:09:23', '2021-09-12 17:39:23', NULL),
(11, 37, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 212, 213, '2021-09-12 13:10:28', '2021-09-12 17:40:28', NULL),
(12, 38, NULL, NULL, NULL, NULL, NULL, 'asdasdasdasdasd', 1, NULL, 214, 215, '2021-09-12 12:17:44', '2021-09-12 16:47:44', NULL),
(13, 39, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 219, NULL, '2021-09-12 12:20:01', '2021-09-12 16:50:01', NULL),
(14, 39, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 220, NULL, '2021-09-12 12:20:37', '2021-09-12 16:50:37', NULL),
(15, 39, NULL, NULL, NULL, NULL, NULL, 'dfsgsdfgdsfg', 1, NULL, 221, NULL, '2021-09-12 12:24:36', '2021-09-12 16:54:36', NULL),
(16, 39, NULL, NULL, NULL, NULL, NULL, 'dfsgsdfgdsfg', 1, NULL, 222, NULL, '2021-09-12 12:36:08', '2021-09-12 17:06:08', NULL),
(17, 39, NULL, NULL, NULL, NULL, NULL, 'dfsgsdfgdsfg', 1, NULL, 223, NULL, '2021-09-12 12:36:27', '2021-09-12 17:06:27', NULL),
(18, 39, NULL, NULL, NULL, NULL, NULL, 'dfsgsdfgdsfg', 1, NULL, 224, NULL, '2021-09-12 12:37:21', '2021-09-12 17:07:21', NULL),
(19, 39, NULL, NULL, NULL, NULL, NULL, 'dfsgsdfgdsfg', 1, NULL, 225, NULL, '2021-09-12 12:38:03', '2021-09-12 17:08:03', NULL),
(20, 39, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 226, NULL, '2021-09-12 12:38:33', '2021-09-12 17:08:33', NULL),
(21, 39, NULL, NULL, NULL, NULL, NULL, 'asdasd', 1, NULL, 227, NULL, '2021-09-12 12:53:34', '2021-09-12 17:23:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_has_rate`
--

CREATE TABLE `order_has_rate` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `personnel` int(2) NOT NULL,
  `quality` int(2) NOT NULL,
  `taste` int(2) NOT NULL,
  `delivery` int(2) NOT NULL,
  `desc` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('y.t.15132@gmail.com', '$2y$10$/n7n/Byb77ZWofll3W/YQu4D91c4.3E0zvTSAGhJ2K9SS6lkZ7G9S', '2021-09-12 13:04:13');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `name`, `route`) VALUES
(1, 'مدیریت مدیران', 'admin/admins'),
(2, 'مدیریت آدرس های کاربر', 'admin/addresses'),
(3, 'مدیریت دسته ها', 'admin/categories'),
(4, 'مدیریت نان ها', 'admin/breads'),
(5, 'مدیریت پیک ها', 'admin/couriers'),
(6, 'مدیریت محصول', 'admin/products'),
(7, 'مدیریت کاربران', 'admin/users'),
(8, 'مدیریت مناطق', 'admin/zones'),
(9, 'مدیریت سفارشات', 'admin/orders'),
(10, 'مدیریت کوپن ها', 'admin/coupons'),
(11, 'مدیریت دیوایس ها', 'admin/devices'),
(12, 'مدیریت نظرها', 'admin/rates'),
(13, 'مدیریت پیشنهادهای ویژه', 'admin/special-offers'),
(14, 'مدیریت تراکنش ها', 'admin/transactions');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title_en` varchar(500) COLLATE utf8_bin NOT NULL,
  `title_ar` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `price` double DEFAULT NULL,
  `barcode` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fixed_price` double DEFAULT NULL,
  `order` int(2) DEFAULT 0,
  `desc_en` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `desc_ar` text COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `view` int(11) DEFAULT NULL,
  `sold` int(100) DEFAULT NULL,
  `total` int(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `title_en`, `title_ar`, `price`, `barcode`, `fixed_price`, `order`, `desc_en`, `desc_ar`, `active`, `view`, `sold`, `total`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, 28, 30, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(2, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL),
(3, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(4, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL),
(5, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(6, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL),
(7, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(23, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL),
(24, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(25, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL),
(26, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(27, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL),
(28, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(29, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL),
(30, 1, 'Tablet White EliteBook Revolve 810 G2', 'Tablet White EliteBook Revolve 810 G2', 99, NULL, 98, 1, 'عباية بقصة ضيقة', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 20:17:18', '2020-12-09 20:17:18', NULL),
(31, 2, 'Purple Solo 2 Wireless', 'Purple Solo 2 Wireless', 100, NULL, 99, 1, 'عطر', NULL, 1, NULL, NULL, NULL, NULL, '2020-12-09 21:19:03', '2021-03-28 09:52:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_has_image`
--

CREATE TABLE `product_has_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `order` int(11) DEFAULT NULL,
  `default` int(11) DEFAULT 0,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product_has_image`
--

INSERT INTO `product_has_image` (`id`, `product_id`, `file_name`, `order`, `default`, `created_at`, `updated_at`) VALUES
(2, 1, 'img1.png', 1, 1, NULL, NULL),
(3, 2, 'img2.png', 1, 1, NULL, NULL),
(4, 3, 'img3.png', 1, 1, NULL, NULL),
(5, 4, 'img4.png', 1, 1, NULL, NULL),
(6, 5, 'img5.png', 1, 1, NULL, NULL),
(7, 6, 'img6.png', 1, 1, NULL, NULL),
(9, 10, '81iAxw9H.jpg', 1, 1, NULL, NULL),
(10, 11, 'tsbsPRC4.jpg', 1, 1, NULL, NULL),
(11, 12, 'T7KZphP2.jpg', 1, 1, NULL, NULL),
(12, 13, 'Tlw9UKdh.jpg', 1, 1, NULL, NULL),
(13, 14, 'bie9B7fB.jpg', 1, 1, NULL, NULL),
(14, 15, 'z0UiB5Fi.jpg', 1, 1, NULL, NULL),
(15, 15, 'HQfswdak.jpg', 2, 0, NULL, NULL),
(16, 15, 'qTNncO5P.jpg', 3, 0, NULL, NULL),
(17, 16, 'PFJoBw8c.jpg', 1, 1, NULL, NULL),
(18, 16, 'HE4BjCjE.jpg', 2, 0, NULL, NULL),
(19, 16, '81RH1YzF.jpg', 3, 0, NULL, NULL),
(21, 17, 'tGGtu1gb.jpg', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_size_items`
--

CREATE TABLE `product_size_items` (
  `id` int(11) NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `title` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `product_size_items`
--

INSERT INTO `product_size_items` (`id`, `name`, `title`, `type`, `created_at`, `updated_at`) VALUES
(1, 'across_shoulder', 'Across Shoulder', 1, NULL, NULL),
(2, 'neck_size', 'Neck Size', 1, NULL, NULL),
(3, 'sleeves_loose', 'Sleeves Loose', 1, NULL, NULL),
(4, 'sleeves_length', 'Sleeves Length', 1, NULL, NULL),
(5, 'sleeves_open', 'Sleeves Open', 1, NULL, NULL),
(6, 'bust', 'Bust', 1, NULL, NULL),
(7, 'waist', 'Waist', 1, NULL, NULL),
(8, 'hip', 'Hip', 1, NULL, NULL),
(9, 'full_length', 'Full Length', 1, NULL, NULL),
(10, 'bottom', 'Bottom', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_tags`
--

CREATE TABLE `product_tags` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_tags`
--

INSERT INTO `product_tags` (`id`, `product_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(2, 1, 2, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(3, 2, 1, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(4, 2, 2, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(5, 3, 1, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(6, 3, 2, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(7, 4, 1, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(8, 4, 2, '2021-08-26 16:30:27', '2021-08-26 16:30:27'),
(9, 5, 2, '2021-08-26 16:31:35', '2021-08-26 16:31:35'),
(10, 6, 2, '2021-08-26 16:31:35', '2021-08-26 16:31:35');

-- --------------------------------------------------------

--
-- Table structure for table `referral`
--

CREATE TABLE `referral` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `phone` varchar(11) COLLATE utf8_bin NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('kPSr6lPTry2IexGWouKxQaX6ysG4fhxqCYBRyTwF', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoieEczMzd4Mjg5Y0J5ZE9TTWVvWkZzQXFOSHhXYldHbkE2aENUUGlGWiI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozODoiaHR0cDovLzEyNy4wLjAuMTo4MDAwL2FkbWluL2NhdGVnb3JpZXMiO31zOjk6Il9wcmV2aW91cyI7YToxOntzOjM6InVybCI7czoyMToiaHR0cDovLzEyNy4wLjAuMTo4MDAwIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MjoibG9naW5fYWRtaW5fNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aTozO30=', 1631724498);

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `desc_en` text NOT NULL,
  `desc_ar` longtext DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  `file_name` varchar(200) NOT NULL,
  `background_file_name` varchar(500) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `desc_en`, `desc_ar`, `enabled`, `file_name`, `background_file_name`, `product_id`, `created_at`, `updated_at`) VALUES
(3, '1', ' <h1 class=\"font-size-64 text-lh-57 font-weight-light\"\r\n                                data-scs-animation-in=\"fadeInUp\">\r\n                                THE NEW <span class=\"d-block font-size-55\">STANDARD</span>\r\n                            </h1>\r\n                            <h6 class=\"font-size-15 font-weight-bold mb-3\"\r\n                                data-scs-animation-in=\"fadeInUp\"\r\n                                data-scs-animation-delay=\"200\">UNDER FAVORABLE SMARTWATCHES\r\n                            </h6>\r\n                            <div class=\"mb-4\"\r\n                                 data-scs-animation-in=\"fadeInUp\"\r\n                                 data-scs-animation-delay=\"300\">\r\n                                <span class=\"font-size-13\">FROM</span>\r\n                                <div class=\"font-size-50 font-weight-bold text-lh-45\">\r\n                                    <sup class=\"\">$</sup>749<sup class=\"\">99</sup>\r\n                                </div>\r\n                            </div>', ' <h1 class=\"font-size-64 text-lh-57 font-weight-light\"\r\n                                data-scs-animation-in=\"fadeInUp\">\r\n                                الجديد <span class=\"d-block font-size-55\">استاندارد</span>\r\n                            </h1>\r\n                            <h6 class=\"font-size-15 font-weight-bold mb-3\"\r\n                                data-scs-animation-in=\"fadeInUp\"\r\n                                data-scs-animation-delay=\"200\">UNDER الساعات الذكية المفضلة\r\n                            </h6>\r\n                            <div class=\"mb-4\"\r\n                                 data-scs-animation-in=\"fadeInUp\"\r\n                                 data-scs-animation-delay=\"300\">\r\n                                <span class=\"font-size-13\">من عند</span>\r\n                                <div class=\"font-size-50 font-weight-bold text-lh-45\">\r\n                                    <sup class=\"\">AED</sup>749<sup class=\"\">99</sup>\r\n                                </div>\r\n                            </div>', 1, 'watches.png', NULL, NULL, '2021-08-26 09:09:25', '2021-08-26 09:09:25');

-- --------------------------------------------------------

--
-- Table structure for table `special_offer`
--

CREATE TABLE `special_offer` (
  `id` int(11) NOT NULL,
  `title_en` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `desc_en` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `title_ar` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `desc_ar` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `debate_value` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sold` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `exp_date` datetime DEFAULT NULL,
  `image` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `special_offer`
--

INSERT INTO `special_offer` (`id`, `title_en`, `desc_en`, `title_ar`, `desc_ar`, `debate_value`, `url`, `category_id`, `product_id`, `sold`, `total`, `exp_date`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(10, 'Special Offer', 'Game Console Controller + USB 3.0 Cable', 'عرض خاص', 'وحدة تحكم الألعاب + كابل USB 3.0', '120', NULL, NULL, 1, 28, 100, '2021-09-30 16:53:28', 'special_offer1.png', '2021-08-26 14:23:28', '2021-08-26 14:23:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supported_zones_cordinates`
--

CREATE TABLE `supported_zones_cordinates` (
  `id` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `supported_zones_cordinates`
--

INSERT INTO `supported_zones_cordinates` (`id`, `lat`, `lon`) VALUES
(1, 29.597017, 52.570845),
(2, 29.592472, 52.597766),
(3, 29.576408, 52.606787),
(4, 29.547281, 52.578423),
(5, 29.571288, 52.538948);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title_en` varchar(500) COLLATE utf8mb4_bin NOT NULL,
  `title_ar` varchar(500) COLLATE utf8mb4_bin NOT NULL,
  `show_index` int(11) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title_en`, `title_ar`, `show_index`, `created_at`, `updated_at`) VALUES
(1, 'Featured', 'متمیز', 1, '2021-08-26 15:16:03', '2021-08-26 15:16:03'),
(2, 'On Sale', 'للبيع', 1, '2021-08-26 15:16:03', '2021-08-26 15:16:03'),
(3, 'Top Rated', 'أعلى التقييمات', 1, '2021-08-26 15:16:03', '2021-08-26 15:16:03');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` double(8,3) NOT NULL,
  `order_id` int(11) NOT NULL,
  `factor_id` int(11) NOT NULL,
  `status_code` int(11) NOT NULL,
  `res_bank` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `refrence_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `family` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `validation_code` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `balance` double DEFAULT 0,
  `point` int(11) DEFAULT 0,
  `banned` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT NULL,
  `validation_date` datetime DEFAULT NULL,
  `provider_id` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `provider` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `family`, `mobile`, `email`, `password`, `validation_code`, `last_login`, `balance`, `point`, `banned`, `active`, `validation_date`, `provider_id`, `provider`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'اشکان', 'قدرت', '09171380449', 'Ashezpower@gmail.com', NULL, '88770', '2018-06-02 11:33:04', 30530, 0, 0, 1, '2018-09-23 22:15:32', NULL, NULL, NULL, '2018-06-02 11:30:54', '2018-09-23 22:15:32', NULL),
(2, 'سید میثم', 'حدیقه', '09397835865', NULL, NULL, '16494', '2018-06-06 11:31:16', 1000, 0, 0, 1, '2018-06-06 00:00:00', NULL, NULL, NULL, '2018-06-06 11:30:59', '2018-06-06 11:31:55', NULL),
(3, 'مهدی', 'شاه امیریان', '09173202395', 'm.shahamirian@gmail.com', NULL, '28304', '2018-06-13 15:31:12', 0, 0, 0, 1, '2018-06-14 00:00:00', NULL, NULL, NULL, '2018-06-13 15:30:53', '2018-06-14 10:04:49', NULL),
(5, 'میثم', 'حدیقه', '09171392756', 'does@do.com', NULL, '48349', '2018-06-19 08:32:27', 149660, 0, 0, 1, '2018-08-23 09:53:00', NULL, NULL, NULL, '2018-06-19 08:32:11', '2018-08-23 09:53:00', NULL),
(6, 'یحیی', 'تاشک', '09173383797', NULL, NULL, '59436', '2018-06-27 06:42:07', 0, 0, 0, 1, '2020-09-29 19:39:29', NULL, NULL, NULL, '2018-06-21 07:20:48', '2020-09-29 19:39:29', NULL),
(7, 'yahya', 'test', '09398165531', NULL, NULL, '74634', '2018-06-21 09:17:48', 0, 0, 0, 1, '2018-06-21 00:00:00', NULL, NULL, NULL, '2018-06-21 07:21:09', '2018-06-21 09:18:02', NULL),
(8, 'kambiz', 'dehghani', '09022251020', 'kambizvb6@gmail.com', NULL, '48886', '2018-06-25 11:01:55', 0, 0, 0, 1, '2018-06-25 11:01:44', NULL, NULL, NULL, '2018-06-25 11:01:44', '2018-06-25 11:02:32', NULL),
(9, 'شهریار', 'ar\nar\nعرب پور', '09173153213', NULL, NULL, '55944', '2018-06-26 10:59:16', 0, 0, 0, 1, '2018-06-26 16:53:05', NULL, NULL, NULL, '2018-06-26 10:57:26', '2018-07-27 15:12:48', '2018-07-27 15:12:48'),
(10, 'شهریار', 'عرب پور', '09173153212', NULL, NULL, '85207', '2018-06-28 09:12:52', 0, 0, 0, 1, '2018-09-30 12:05:22', NULL, NULL, NULL, '2018-06-28 09:11:14', '2018-09-30 12:05:22', NULL),
(11, 'ابوذر', 'شکیبا', '09125843808', 'shakibafard@gmail.com', NULL, '48580', '2018-08-09 16:45:07', 0, 0, 0, 1, '2018-08-09 16:43:35', NULL, NULL, NULL, '2018-08-09 16:43:35', '2018-08-09 17:12:16', NULL),
(12, 'یحیی', 'تاشک', '09398165521', NULL, NULL, '63926', '2018-08-25 09:50:34', 0, 0, 0, 1, '2018-08-29 13:36:56', NULL, NULL, NULL, '2018-08-25 09:47:57', '2018-08-29 13:36:56', NULL),
(13, 'سعید', 'للللل', '09177382520', NULL, NULL, '57590', '2018-08-29 13:34:33', 0, 0, 0, 1, '2018-08-29 13:34:09', NULL, NULL, NULL, '2018-08-29 13:34:09', '2018-08-29 13:34:51', NULL),
(14, 'Test', 'test', '09122981019', 'test@gmail.com', NULL, '27806', '2018-09-07 13:03:15', 0, 0, 0, 1, '2018-09-07 13:02:26', NULL, NULL, NULL, '2018-09-07 13:02:26', '2018-09-07 13:19:15', NULL),
(15, 'ngtt', 'fghjb', '09365016900', NULL, NULL, '64064', '2018-09-08 18:45:48', 0, 0, 0, 1, '2018-09-08 18:45:34', NULL, NULL, NULL, '2018-09-08 18:45:34', '2018-09-08 18:46:23', NULL),
(16, 'naser', 'khadem', '09302494490', NULL, NULL, '24465', '2018-09-10 07:05:41', 0, 0, 0, 1, '2018-09-10 07:05:23', NULL, NULL, NULL, '2018-09-10 07:05:23', '2018-09-10 07:15:55', NULL),
(17, 'علیرضا', 'نعمت الهی', '09376399957', NULL, NULL, '63740', '2018-09-10 14:21:50', 0, 0, 0, 1, '2018-09-10 14:21:30', NULL, NULL, NULL, '2018-09-10 14:21:30', '2018-09-10 14:22:18', NULL),
(18, NULL, NULL, '05666553635', NULL, NULL, '29171', NULL, 0, 0, 0, 0, '2018-09-22 06:36:20', NULL, NULL, NULL, '2018-09-22 06:36:20', '2018-09-22 06:36:20', NULL),
(19, NULL, NULL, '09124931242', NULL, NULL, '92380', NULL, 0, 0, 0, 0, '2018-10-19 08:34:42', NULL, NULL, NULL, '2018-10-19 08:34:42', '2018-10-19 08:34:42', NULL),
(20, 'Yah Ya', NULL, NULL, 'y.t.15132@gmail.com', NULL, NULL, NULL, 0, 0, 0, NULL, NULL, '3847481251999600', 'facebook', 'https://graph.facebook.com/v3.3/3847481251999600/picture?type=normal', '2021-04-03 15:56:46', '2021-04-03 16:05:46', NULL),
(21, 'Yahya Tashk', NULL, NULL, 'hell.lord.1988.ch@gmail.com', NULL, NULL, NULL, 0, 0, 0, NULL, NULL, '101806689349440949864', 'google', 'https://lh3.googleusercontent.com/a-/AOh14GixEyZIXFefWCiBIZDG0qGHr6E699uj2wYhQm2ZGg=s96-c', '2021-04-03 16:39:28', '2021-04-03 16:40:08', NULL),
(22, 'yahhh', NULL, NULL, 'y.t.15132@gmail.com', '$2y$10$5HxLyu/etw5UuYfbWqeQPuP5va4Kv.tepLvnmBnb8vMa1/Cjvu8L2', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-11 13:58:04', '2021-09-11 13:58:04', NULL),
(23, '1111', '1111', NULL, '1111', '$2y$10$gPu.nkzDT.AjymJ6L9saD.rGtvLfKrVR7wpDKtp6GzNK7TUctVhGC', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:24:20', '2021-09-12 17:24:20', NULL),
(24, '1111', '1111', NULL, '1111', '$2y$10$qjcV4i.dsX2icF2sUN9U/e6ptKzvSPTq8m6SjlwauF4lgvNO0z996', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:24:41', '2021-09-12 17:24:41', NULL),
(25, '1111', '1111', NULL, '1111', '$2y$10$eYuEJWQ741qfL.BnnmEOkuV5bL5iHg8p/Z6A7etvSCPQ5mN8jh7KK', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:24:55', '2021-09-12 17:24:55', NULL),
(26, '1111', '1111', NULL, '1111', '$2y$10$8xzmvKxIP448XlSRLIGwq.vJ.1mcQ717iU7KzCNHN2VmDtlpCeHRG', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:25:45', '2021-09-12 17:25:45', NULL),
(27, '1111', '1111', NULL, '1111', '$2y$10$8xPLPWj22fbb0rW7g4CKreN0YKDGFAVPbphT87tsJk0B4w8161fvG', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:26:11', '2021-09-12 17:26:11', NULL),
(28, '1111', '1111', NULL, '1111', '$2y$10$7sSjyzlclkcaoOpE5l5y9OgDHBuyTlP4l7ZbDQb8FJVAjKF.aEmea', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:26:24', '2021-09-12 17:26:24', NULL),
(29, '1111', '1111', NULL, '1111', '$2y$10$mfwfPPf84zlo9tflT1NSquwPr/LARvXA9.vIIZ9nGPgIgJG3.irTS', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:26:55', '2021-09-12 17:26:55', NULL),
(30, '1111', '1111', NULL, '1111', '$2y$10$I3I3xiXEoXvXlOG9WbsYYOajQg5.7yI1uddzmEzBRAxWFQ9nfpEHG', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:27:05', '2021-09-12 17:27:05', NULL),
(31, '1111', '1111', NULL, '1111', '$2y$10$31d/Ms365F6Rxdog2sQkzueymFhWgQ/62nDQkoF6dDTztpyd11zyi', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:27:26', '2021-09-12 17:27:26', NULL),
(32, '1111', '1111', NULL, '1111', '$2y$10$Ih3ZpyBRqO03HXB5VADWP.mbk35xdJDdHvXGcy3t4A0WbQHGj3DKy', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:27:38', '2021-09-12 17:27:38', NULL),
(33, '1111', '1111', NULL, '1111', '$2y$10$ZVQkY6pZWGMbbYd6/TJiXeVZ/GNbNESrLHbwyvShqHDRYhu7NBIsS', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:28:48', '2021-09-12 17:28:48', NULL),
(34, '1111', '1111', NULL, '1111', '$2y$10$bCsFaFjDWt9iyMV9a5C7suCkocg/jjTeKU1iuO/cUnIkWpDu7TH4i', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:30:20', '2021-09-12 17:30:20', NULL),
(35, '1111', '1111', NULL, '1111', '$2y$10$YMrHao7nCikSUSsTcifrxO3UTz3cAL/ZX2NeUIcWMQgkCYYKcORYe', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:39:14', '2021-09-12 17:39:14', NULL),
(36, '1111', '1111', NULL, '1111', '$2y$10$vdKzabxdacjLM6W5PWmPuuQzEw9s1NaBoz6XodnEdCiwpko4l8Rqu', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:39:22', '2021-09-12 17:39:22', NULL),
(37, '1111', '1111', NULL, '1111', '$2y$10$S8J65olVp/PU7jkOl4v16.inMu9xRLK4s/EjVUar6CAxBF20Wmc0u', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:40:28', '2021-09-12 17:40:28', NULL),
(38, '1111', '1111', NULL, '1111', '$2y$10$Cu/ulPr1VwIoiMcQdDYf/eBf2zQ.swxM/ctSQpbKZzzyHJgmXr1G6', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 16:47:44', '2021-09-12 16:47:44', NULL),
(39, 'yahyya', NULL, NULL, 'test@test.com', '$2y$10$FfJcCfGfK7zodScnQ3ylwOC3a/yjdrwMwcYC9NUEyX4qTFSGSTnxW', NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '2021-09-12 17:36:20', '2021-09-12 17:36:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_addresses`
--

CREATE TABLE `user_has_addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_has_addresses`
--

INSERT INTO `user_has_addresses` (`id`, `user_id`, `address_id`) VALUES
(2, 4, 2),
(4, 2, 4),
(6, 15, 6),
(7, 15, 7),
(47, 4, 8),
(48, 4, 48),
(51, 3, 51),
(52, 3, 52),
(53, 4, 53),
(54, 4, 54),
(55, 4, 55),
(56, 4, 56),
(57, 4, 57),
(58, 4, 58),
(59, 4, 59),
(60, 4, 60),
(61, 4, 61),
(62, 4, 62),
(63, 4, 63),
(64, 4, 64),
(65, 4, 65),
(66, 4, 66),
(67, 4, 67),
(68, 4, 68),
(69, 4, 69),
(70, 4, 70),
(71, 4, 71),
(72, 4, 72),
(73, 4, 73),
(74, 4, 74),
(75, 4, 75),
(76, 4, 76),
(77, 4, 77),
(78, 4, 78),
(79, 4, 79),
(80, 4, 80),
(81, 4, 81),
(82, 4, 82),
(83, 4, 83),
(84, 4, 84),
(85, 4, 85),
(86, 4, 86),
(87, 4, 87),
(88, 4, 88),
(89, 4, 89),
(90, 4, 90),
(91, 4, 91),
(92, 4, 92),
(93, 4, 93),
(94, 4, 94),
(95, 4, 95),
(96, 4, 96),
(97, 4, 97),
(98, 4, 98),
(99, 4, 99),
(100, 4, 100),
(101, 2, 101),
(102, 2, 102),
(103, 2, 103),
(104, 2, 104),
(105, 2, 105),
(106, 1, 106),
(107, 1, 107),
(108, 1, 108),
(109, 1, 109),
(110, 1, 110),
(111, 1, 111),
(112, 1, 112),
(114, 1, 114),
(118, 1, 118),
(119, 2, 119),
(139, 1, 139),
(140, 3, 140),
(141, 1, 141),
(142, 1, 142),
(143, 1, 143),
(144, 1, 144),
(145, 1, 145),
(146, 1, 146),
(147, 1, 147),
(149, 1, 149),
(153, 1, 153),
(154, 5, 154),
(156, 1, 156),
(159, 1, 159),
(160, 1, 160),
(161, 5, 161),
(163, 1, 163),
(164, 4, 164),
(169, 4, 169),
(170, 4, 170),
(171, 4, 171),
(174, 4, 174),
(176, 5, 176),
(177, 5, 177),
(178, 5, 178),
(179, 9, 179),
(180, 4, 180),
(181, 10, 181),
(182, 10, 182),
(183, 4, 183),
(184, 1, 184),
(185, 11, 185),
(186, 12, 186),
(187, 14, 187);

-- --------------------------------------------------------

--
-- Table structure for table `winners`
--

CREATE TABLE `winners` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `winners`
--

INSERT INTO `winners` (`id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2021-09-10 21:03:21', '2021-09-10 21:03:21');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `id` int(11) NOT NULL,
  `title` varchar(55) COLLATE utf8_bin DEFAULT NULL,
  `price` double(8,3) DEFAULT 0.000,
  `long` double(10,8) DEFAULT 0.00000000,
  `lat` double(10,8) DEFAULT 0.00000000,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id`, `title`, `price`, `long`, `lat`, `created_at`, `updated_at`) VALUES
(1, 'ارم', 30.000, 1.43200000, 2.56700000, '2018-01-21 10:06:52', '2018-01-21 10:10:27'),
(2, 'ستارخان', 2000.000, 2.66660000, 34.56456450, '2018-01-27 08:28:50', '2018-02-19 07:07:22'),
(3, 'معالی آباد', 50000.000, 0.00000000, 0.00000000, '2018-06-24 10:15:21', '2018-06-24 10:15:21'),
(4, 'صنایع', 50000.000, 0.00000000, 0.00000000, '2018-06-24 10:15:28', '2018-06-24 10:15:28'),
(5, 'زرگری', 50000.000, 0.00000000, 0.00000000, '2018-06-24 10:15:45', '2018-06-24 10:15:45'),
(6, 'قدوسی', 50000.000, 0.00000000, 0.00000000, '2018-06-24 10:15:51', '2018-06-24 10:15:51'),
(7, 'چمران', 50000.000, 0.00000000, 0.00000000, '2018-06-24 10:15:55', '2018-06-24 10:15:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_email_uindex` (`email`);

--
-- Indexes for table `admin_has_permission`
--
ALTER TABLE `admin_has_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `config_id_uindex` (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupon_code_uindex` (`code`);

--
-- Indexes for table `courier`
--
ALTER TABLE `courier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `courier_email_uindex` (`email`);

--
-- Indexes for table `credit`
--
ALTER TABLE `credit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_category`
--
ALTER TABLE `custom_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_item`
--
ALTER TABLE `custom_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_product`
--
ALTER TABLE `custom_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_product_has_item`
--
ALTER TABLE `custom_product_has_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_rule`
--
ALTER TABLE `custom_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_sub_item`
--
ALTER TABLE `custom_sub_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `factors`
--
ALTER TABLE `factors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `factor_has_items`
--
ALTER TABLE `factor_has_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gateway_transactions`
--
ALTER TABLE `gateway_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gateway_transactions_logs`
--
ALTER TABLE `gateway_transactions_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gateway_transactions_logs_transaction_id_foreign` (`transaction_id`);

--
-- Indexes for table `index_categories`
--
ALTER TABLE `index_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order_courier1_idx` (`courier_id`);

--
-- Indexes for table `order_has_rate`
--
ALTER TABLE `order_has_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_category1_idx` (`category_id`);

--
-- Indexes for table `product_has_image`
--
ALTER TABLE `product_has_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_size_items`
--
ALTER TABLE `product_size_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tags`
--
ALTER TABLE `product_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral`
--
ALTER TABLE `referral`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `refferal_id_uindex` (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `special_offer`
--
ALTER TABLE `special_offer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `special_offer_int_uindex` (`id`);

--
-- Indexes for table `supported_zones_cordinates`
--
ALTER TABLE `supported_zones_cordinates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_has_addresses`
--
ALTER TABLE `user_has_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `winners`
--
ALTER TABLE `winners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=228;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_has_permission`
--
ALTER TABLE `admin_has_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `courier`
--
ALTER TABLE `courier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `credit`
--
ALTER TABLE `credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `custom_category`
--
ALTER TABLE `custom_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `custom_item`
--
ALTER TABLE `custom_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `custom_product`
--
ALTER TABLE `custom_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_product_has_item`
--
ALTER TABLE `custom_product_has_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_rule`
--
ALTER TABLE `custom_rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `custom_sub_item`
--
ALTER TABLE `custom_sub_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `factors`
--
ALTER TABLE `factors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `factor_has_items`
--
ALTER TABLE `factor_has_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gateway_transactions`
--
ALTER TABLE `gateway_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153655868031;

--
-- AUTO_INCREMENT for table `gateway_transactions_logs`
--
ALTER TABLE `gateway_transactions_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `index_categories`
--
ALTER TABLE `index_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `order_has_rate`
--
ALTER TABLE `order_has_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `product_has_image`
--
ALTER TABLE `product_has_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `product_size_items`
--
ALTER TABLE `product_size_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_tags`
--
ALTER TABLE `product_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `referral`
--
ALTER TABLE `referral`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `special_offer`
--
ALTER TABLE `special_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `supported_zones_cordinates`
--
ALTER TABLE `supported_zones_cordinates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_has_addresses`
--
ALTER TABLE `user_has_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `winners`
--
ALTER TABLE `winners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gateway_transactions_logs`
--
ALTER TABLE `gateway_transactions_logs`
  ADD CONSTRAINT `gateway_transactions_logs_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `gateway_transactions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_courier1` FOREIGN KEY (`courier_id`) REFERENCES `courier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
