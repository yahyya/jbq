<?php
return [
    'uploadFilePath'=>public_path('upload/temp/'),
    'uploadBreadsPath'=>public_path('upload/images/breads/'),
    'uploadProductsPath'=>public_path('upload/images/products/'),
    'uploadCategoriesPath'=>public_path('upload/images/categories/'),
    'uploadIndexCategoriesPath'=>public_path('upload/index_category/'),
    'uploadCouriersPath'=>public_path('upload/images/couriers/'),
    'uploadSubItemsPath'=>public_path('upload/images/sub-items/'),
    'uploadSpecialOffersPath'=>public_path('upload/special_offer/'),
    'uploadSliderPath'=>public_path('upload/slider/'),
];
