﻿// check for hash(#) in url to activate tab
var hash = window.location.hash;
hash && $('ul.nav a[href="' + hash + '"]').tab('show');

// scroll to top on tab change
$('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
});