<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = \App\Models\Product::where('active',1)->get();
    return view('welcome',['products'=>$products]);
});
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);


Route::get('/cart',[\App\Http\Controllers\CartController::class,'view']);


Route::post('/wishlist/add',[\App\Http\Controllers\WishlistController::class,'add']);
Route::get('/wishlist',[\App\Http\Controllers\WishlistController::class,'view']);
Route::post('/wishlist/remove',[\App\Http\Controllers\WishlistController::class,'remove']);
Route::get('/terms-and-services',function(\Illuminate\Http\Request $req){
   return view('terms');
});
Route::get('/about-us',function(\Illuminate\Http\Request $req){
    return view('aboutUs');
});
Route::get('/contact-us',function(\Illuminate\Http\Request $req){
    return view('contactUs');
});


Route::post('/cart/add',[\App\Http\Controllers\CartController::class,'add']);
Route::post('/cart/remove',[\App\Http\Controllers\CartController::class,'remove']);
Route::get('/product/{product}',[\App\Http\Controllers\ProductController::class,'view'])->where('product','^[a-zA-Z0-9-_\/]+$');
Route::get('/product/{id}/customize',[\App\Http\Controllers\ProductController::class,'customize']);
Route::post('/review/save',[\App\Http\Controllers\ProductController::class,'saveReview']);
Route::get('/products',[\App\Http\Controllers\ProductController::class,'index']);
Route::get('category/{category}',[\App\Http\Controllers\CategoryController::class,'view'])->where('category','^[a-zA-Z0-9-_\/]+$');
Route::get('/special-offers',function(){
   $cat = \App\Models\Category::find(8);
   return view('category',['cat'=>$cat]);
});
Route::get('search',function(\Illuminate\Http\Request $req){
    $products = \App\Models\Product::whereRaw(\Illuminate\Support\Facades\DB::raw('LOWER(title_'.\Illuminate\Support\Facades\App::getLocale() .') LIKE "%'.strtolower($req->txt).'%"'))->get();
    return view('search',['products'=>$products]);

})->name('search');
Route::group(['middleware'=>'auth:web'],function(){
    Route::delete('user/products/pic/{id}/delete', 'App\Http\Controllers\Admin\ProductController@destroyPic');
    Route::get('user/orders', function () {
        return view('user.orders',['user'=>\Illuminate\Support\Facades\Auth::user()]);
    })->name('user.orders');
    Route::get('user/order/{id}',[\App\Http\Controllers\UserController::class,'showOrder'])->name('user.order.show');
    Route::get('/checkout',[\App\Http\Controllers\CartController::class,'checkout']);
    Route::post('/checkout',[\App\Http\Controllers\CartController::class,'saveCheckout']);
    Route::any('/checkout/failed',[\App\Http\Controllers\CartController::class,'failed']);
    Route::any('/checkout/success',[\App\Http\Controllers\CartController::class,'success']);
    Route::get('user/dashboard', function () {
        return view('user.dashboard',['user'=>\Illuminate\Support\Facades\Auth::user()]);
    })->name('user.dashboard');

    Route::get('basket',function(){

    })->name('basket');


    Route::get('user/orders', function () {
        return view('user.orders',['user'=>\Illuminate\Support\Facades\Auth::user()]);
    })->name('user.orders');

    Route::get('user/products', function () {
        return view('user.products',['user'=>\Illuminate\Support\Facades\Auth::user()]);
    })->name('user.products');
//    Route::get('user/order/{id}',[\App\Http\Controllers\UserController::class,'showOrder'])->name('user.order.show');
//    Route::get('user/return/{id}',[\App\Http\Controllers\UserController::class,'showReturn'])->name('user.return.show');
//    Route::get('user/return/create',[\App\Http\Controllers\UserController::class,'createReturn'])->name('user.return.create');
    Route::post('user/order/{id}',[\App\Http\Controllers\UserController::class,'showOrderItems'])->name('user.order.items');
//    Route::post('user/return/{id}',[\App\Http\Controllers\UserController::class,'showReturnItems'])->name('user.return.items');

    Route::get('user/factures', function () {
        return view('user.factures',['user'=>\Illuminate\Support\Facades\Auth::user()]);
    })->name('user.factures');

//    Route::post('user/factures/save',[\App\Http\Controllers\UserController::class,'saveFacture'])->name('user.factures.store');
    Route::get('user/returns', function () {
        return view('user.returns',['user'=>\Illuminate\Support\Facades\Auth::user()]);
    })->name('user.returns');

    Route::get('/user/{id}/orders/list', 'App\Http\Controllers\UserController@ordersList')->name('user.orders.list');
    Route::get('/user/order/{id}/', 'App\Http\Controllers\UserController@order')->name('user.orders.view');
    Route::get('/user/order/{id}/items',[\App\Http\Controllers\UserController::class,'orderItems'])->name('user.order.items');
    Route::get('/user/order/{id}/transactions',[\App\Http\Controllers\UserController::class,'orderTransactions'])->name('user.orders.transactions');
    Route::get('/user/{id}/products/list', 'App\Http\Controllers\UserController@productsList')->name('user.products.list');

    Route::get('/user/products/add',[\App\Http\Controllers\UserController::class,'addProduct']);
    Route::post('/user/products/add',[\App\Http\Controllers\UserController::class,'saveProduct'])->name('user.products.store');
    Route::get('/user/products/{id}/edit',[\App\Http\Controllers\UserController::class,'editProduct'])->name('user.products.edit');
    Route::put('/user/products/{id}/update',[\App\Http\Controllers\UserController::class,'updateProduct'])->name('user.products.update');
//    Route::post('/user/order/item/save', 'App\Http\Controllers\UserController@saveItem')->name('user.order.item.store');
//    Route::delete('/user/order/item/{id}',[App\Http\Controllers\UserController::class,'deleteItem']);
//    Route::get('/user/{id}/returns/list', 'App\Http\Controllers\UserController@returnsList')->name('user.returns.list');
//    Route::post('/user/returns/save', 'App\Http\Controllers\UserController@returnsSave')->name('order.returns.save');
//    Route::get('/user/{id}/factures/list', 'App\Http\Controllers\Admin\FacturesController@index')->name('user.factures.list');

    Route::post('/user/fileUpload',[\App\Http\Controllers\Admin\FileUploadController::class,'fileUpload'])->name('user.fileUpload');
});

Route::any('admin/login', 'App\Http\Controllers\Admin\AuthController@login')->name('admin.login');
Route::any('admin/logout', 'App\Http\Controllers\Admin\AuthController@logout')->name('admin.logout');
Route::group(['prefix'=>'admin', 'middleware' => ['auth:admin']], function (){
    Route::post('products/pic/{id}/default', 'App\Http\Controllers\Admin\ProductController@updateDefaultPic');
    Route::post('fileUpload', 'App\Http\Controllers\Admin\FileUploadController@fileUpload')->name('fileUpload');

    Route::resource('indexCategories', 'App\Http\Controllers\Admin\IndexCategoryController', ['names' => [
        'index' => 'admin.indexCategories.index',
        'create' => 'admin.indexCategories.create',
        'store' => 'admin.indexCategories.store',
        'show' => 'admin.indexCategories.show',
        'edit' => 'admin.indexCategories.edit',
        'update' => 'admin.indexCategories.update',
        'destroy' => 'admin.indexCategories.delete'
    ]]);

    Route::resource('tag', 'App\Http\Controllers\Admin\TagController', ['names' => [
        'index' => 'admin.tag.index',
        'create' => 'admin.tag.create',
        'store' => 'admin.tag.store',
        'show' => 'admin.tag.show',
        'edit' => 'admin.tag.edit',
        'update' => 'admin.tag.update',
        'destroy' => 'admin.tag.delete'
    ]]);
    Route::post('token','App\Http\Controllers\Admin\AdminController@saveFCMToken')->name('admin.token');
    Route::get('dashboard','App\Http\Controllers\Admin\AdminController@dashboard')->name('admin.dashboard');
    Route::post('updateSalesStatus','App\Http\Controllers\Admin\AdminController@salesStatus')->name('admin.salesStatus');
    Route::post('updateCustomProductSalesStatus','App\Http\Controllers\Admin\AdminController@customProductSalesStatus')->name('admin.customProductSalesStatus');
    Route::get('orderCounts','App\Http\Controllers\Admin\AdminController@orderCounts')->name('admin.dashboard.orderCounts');
    Route::get('db/fullBackup','App\Http\Controllers\Admin\AdminController@dbFullBackup')->name('admin.db.fullBackup');

    Route::group(['prefix'=>'custom-product'], function () {
        Route::post('categories/setOrder','App\Http\Controllers\Admin\CustomCategoryController@setOrder')->name('admin.custom.categories.setOrder');
        Route::resource('categories', 'App\Http\Controllers\Admin\CustomCategoryController', ['names' => [
            'index' => 'admin.custom.categories.index',
            'create' => 'admin.custom.categories.create',
            'store' => 'admin.custom.categories.store',
            'show' => 'admin.custom.categories.show',
            'edit' => 'admin.custom.categories.edit',
            'update' => 'admin.custom.categories.update',
            'destroy' => 'admin.custom.categories.delete'
        ]]);



        Route::post('items/setOrder','App\Http\Controllers\Admin\CustomItemController@setOrder')->name('admin.custom.items.setOrder');
        Route::get('items/{id}/rules','App\Http\Controllers\Admin\CustomItemController@rules')->name('admin.custom.items.rules');
        Route::post('items/{id}/rules/create','App\Http\Controllers\Admin\CustomItemController@newRule')->name('admin.custom.items.rules.create');
        Route::delete('items/rules/delete', 'App\Http\Controllers\Admin\CustomItemController@destroyRule')->name('admin.custom.items.rules.delete');
        Route::resource('items', 'App\Http\Controllers\Admin\CustomItemController', ['names' => [
            'index' => 'admin.custom.items.index',
            'create' => 'admin.custom.items.create',
            'store' => 'admin.custom.items.store',
            'show' => 'admin.custom.items.show',
            'edit' => 'admin.custom.items.edit',
            'update' => 'admin.custom.items.update',
            'destroy' => 'admin.custom.items.delete'
        ]]);
        Route::post('sub-items/setOrder','App\Http\Controllers\Admin\CustomSubItemController@setOrder')->name('admin.custom.subItems.setOrder');
        Route::resource('sub-items', 'App\Http\Controllers\Admin\CustomSubItemController', ['names' => [
            'index' => 'admin.custom.subItems.index',
            'create' => 'admin.custom.subItems.create',
            'store' => 'admin.custom.subItems.store',
            'show' => 'admin.custom.subItems.show',
            'edit' => 'admin.custom.subItems.edit',
            'update' => 'admin.custom.subItems.update',
            'destroy' => 'admin.custom.subItems.delete'
        ]]);
    });

    Route::resource('admins','App\Http\Controllers\Admin\AdminsController', ['names' => [
        'index' => 'admin.admins.index',
        'create' => 'admin.admins.create',
        'store' => 'admin.admins.store',
        'show' => 'admin.admins.show',
        'edit' => 'admin.admins.edit',
        'update' => 'admin.admins.update',
        'destroy' => 'admin.admins.delete'
    ]]);
    Route::resource('stats',\App\Http\Controllers\Admin\StatsController::class, ['names' => [
        'index' => 'admin.stats.index',
        'create' => 'admin.stats.create',
        'store' => 'admin.stats.store',
        'show' => 'admin.stats.show',
        'edit' => 'admin.stats.edit',
        'update' => 'admin.stats.update',
        'destroy' => 'admin.stats.delete'
    ]]);
    Route::resource('addresses','App\Http\Controllers\Admin\AddressController', ['names' => [
        'index' => 'admin.addresses.index',
        'create' => 'admin.addresses.create',
        'store' => 'admin.addresses.store',
        'show' => 'admin.addresses.show',
        'edit' => 'admin.addresses.edit',
        'update' => 'admin.addresses.update',
        'destroy' => 'admin.addresses.delete'
    ]]);

    Route::post('categories/setOrder','App\Http\Controllers\Admin\CategoryController@setOrder')->name('admin.categories.setOrder');
    Route::resource('categories','App\Http\Controllers\Admin\CategoryController', ['names' => [
        'index' => 'admin.categories.index',
        'create' => 'admin.categories.create',
        'store' => 'admin.categories.store',
        'show' => 'admin.categories.show',
        'edit' => 'admin.categories.edit',
        'update' => 'admin.categories.update',
        'destroy' => 'admin.categories.delete'
    ]]);

    Route::resource('sliders','App\Http\Controllers\Admin\SliderController', ['names' => [
        'index' => 'admin.sliders.index',
        'create' => 'admin.sliders.create',
        'store' => 'admin.sliders.store',
        'show' => 'admin.sliders.show',
        'edit' => 'admin.sliders.edit',
        'update' => 'admin.sliders.update',
        'destroy' => 'admin.sliders.delete'
    ]]);

    Route::post('couriers/{id}/orders/filter',[\App\Http\Controllers\Admin\CourierController::class,'filterByDates'])->name('admin.couriers.orders.filter');
    Route::get('couriers/{id}/orders',[\App\Http\Controllers\Admin\CourierController::class,'orders'])->name('admin.couriers.orders');
    Route::resource('couriers',\App\Http\Controllers\Admin\CourierController::class, ['names' => [
        'index' => 'admin.couriers.index',
        'create' => 'admin.couriers.create',
        'store' => 'admin.couriers.store',
        'show' => 'admin.couriers.show',
        'edit' => 'admin.couriers.edit',
        'update' => 'admin.couriers.update',
        'destroy' => 'admin.couriers.delete'
    ]]);
    Route::resource('coupons',\App\Http\Controllers\Admin\CouponController::class, ['names' => [
        'index' => 'admin.coupons.index',
        'create' => 'admin.coupons.create',
        'store' => 'admin.coupons.store',
        'show' => 'admin.coupons.show',
        'edit' => 'admin.coupons.edit',
        'update' => 'admin.coupons.update',
        'destroy' => 'admin.coupons.delete'
    ]]);
    Route::post('devices/notify',[ \App\Http\Controllers\Admin\DeviceController::class,'notify'])->name('admin.devices.notify');
    Route::resource('devices', \App\Http\Controllers\Admin\DeviceController::class, ['names' => [
        'index' => 'admin.devices.index',
        'create' => 'admin.devices.create',
        'store' => 'admin.devices.store',
        'show' => 'admin.devices.show',
        'edit' => 'admin.devices.edit',
        'update' => 'admin.devices.update',
        'destroy' => 'admin.devices.delete',
    ]]);
    Route::resource('config', \App\Http\Controllers\Admin\ConfigController::class, ['names' => [
        'index' => 'admin.config.index',
        'create' => 'admin.config.create',
        'store' => 'admin.config.store',
        'show' => 'admin.config.show',
        'edit' => 'admin.config.edit',
        'update' => 'admin.config.update',
        'destroy' => 'admin.config.delete',
    ]]);
    Route::get('orders/{id}/print', 'App\Http\Controllers\Admin\OrderController@printInvoice')->name('admin.orders.print');
    Route::post('orders/{id}/set-courier', 'App\Http\Controllers\Admin\OrderController@setCourier')->name('admin.orders.set.courier');
    Route::post('orders/{id}/set-status', 'App\Http\Controllers\Admin\OrderController@setStatus')->name('admin.orders.set.status');
    Route::post('orders/{id}/set-name', 'App\Http\Controllers\Admin\OrderController@setName')->name('admin.orders.set.name');
    Route::delete('orders/item/delete', 'App\Http\Controllers\Admin\OrderController@destroyItem')->name('admin.orders.item.delete');
    Route::get('orders/{id}/rates','App\Http\Controllers\Admin\OrderController@rates')->name('admin.orders.rates');
    Route::get('orders/{id}/transactions','App\Http\Controllers\Admin\OrderController@transactions')->name('admin.orders.transactions');
    Route::get('orders/total','App\Http\Controllers\Admin\OrderController@getTotalAmounts')->name('admin.orders.index.total');
    Route::get('orders/check_new','App\Http\Controllers\Admin\OrderController@checkNew')->name('admin.orders.check_new');

    Route::resource('orders', 'App\Http\Controllers\Admin\OrderController', ['names' => [
        'index' => 'admin.orders.index',
//        'create' => 'admin.orders.create',
//        'store' => 'admin.orders.store',
        'show' => 'admin.orders.show',
        'edit' => 'admin.orders.edit',
        'update' => 'admin.orders.update',
        'destroy' => 'admin.orders.delete'
    ]]);
    Route::post('products/setOrder','App\Http\Controllers\Admin\ProductController@setOrder')->name('admin.products.setOrder');
    Route::delete('products/pic/{id}/delete', 'App\Http\Controllers\Admin\ProductController@destroyPic')->name('admin.products.delete.pic');
    Route::resource('products','App\Http\Controllers\Admin\ProductController', ['names' => [
        'index' => 'admin.products.index',
        'create' => 'admin.products.create',
        'store' => 'admin.products.store',
        'show' => 'admin.products.show',
        'edit' => 'admin.products.edit',
        'update' => 'admin.products.update',
        'destroy' => 'admin.products.delete'
    ]]);
    Route::resource('reviews','App\Http\Controllers\Admin\ReviewController', ['names' => [
        'index' => 'admin.reviews.index',
        'create' => 'admin.reviews.create',
        'store' => 'admin.reviews.store',
        'show' => 'admin.reviews.show',
        'edit' => 'admin.reviews.edit',
        'update' => 'admin.reviews.update',
        'destroy' => 'admin.reviews.delete'
    ]]);
    Route::resource('offers','App\Http\Controllers\Admin\OfferController', ['names' => [
        'index' => 'admin.offers.index',
        'create' => 'admin.offers.create',
        'store' => 'admin.offers.store',
        'show' => 'admin.offers.show',
        'edit' => 'admin.offers.edit',
        'update' => 'admin.offers.update',
        'destroy' => 'admin.offers.delete'
    ]]);
    Route::post('special-offers/categoryProducts','App\Http\Controllers\Admin\SpecialOfferController@categoryProducts')->name('admin.specialOffers.categoryProducts');
    Route::resource('special-offers','App\Http\Controllers\Admin\SpecialOfferController', ['names' => [
        'index' => 'admin.specialOffers.index',
        'create' => 'admin.specialOffers.create',
        'store' => 'admin.specialOffers.store',
        'show' => 'admin.specialOffers.show',
        'edit' => 'admin.specialOffers.edit',
        'update' => 'admin.specialOffers.update',
        'destroy' => 'admin.specialOffers.delete'
    ]]);
    Route::resource('transactions','App\Http\Controllers\Admin\TransactionController', ['names' => [
        'index' => 'admin.transactions.index',
//        'create' => 'admin.transactions.create',
//        'store' => 'admin.transactions.store',
//        'show' => 'admin.transactions.show',
//        'edit' => 'admin.transactions.edit',
//        'update' => 'admin.transactions.update',
//        'destroy' => 'admin.transactions.delete'
    ]]);
    Route::post('/users/notify/{id}','App\Http\Controllers\Admin\UserController@notify');
    Route::post('users/toggleBanStatus','App\Http\Controllers\Admin\UserController@toggleBanStatus')->name('admin.users.toggleBanStatus');
    Route::get('users/{id}/addresses','App\Http\Controllers\Admin\UserController@addresses')->name('admin.users.addresses');
    Route::get('users/{id}/credits','App\Http\Controllers\Admin\UserController@credits')->name('admin.users.credits');
    Route::get('users/{id}/transactions','App\Http\Controllers\Admin\UserController@transactions')->name('admin.users.transactions');
    Route::get('users/{id}/orders','App\Http\Controllers\Admin\UserController@orders')->name('admin.users.orders');
    Route::get('users/{id}/referrals','App\Http\Controllers\Admin\UserController@referrals')->name('admin.users.referrals');
    Route::post('users/{id}/setBalance','App\Http\Controllers\Admin\UserController@setBalance')->name('admin.users.setBalance');
//    Route::match(['get', 'post'],'users/{id}/addresses','Admin\UserController@address')->name('admin.users.address');
    Route::resource('users','App\Http\Controllers\Admin\UserController', ['names' => [
        'index' => 'admin.users.index',
        'create' => 'admin.users.create',
        'store' => 'admin.users.store',
        'show' => 'admin.users.show',
        'edit' => 'admin.users.edit',
        'update' => 'admin.users.update',
        'destroy' => 'admin.users.delete',
    ]]);
    Route::post('group/{id}/winner',function($id){
        $winner = \App\Models\Winner::where('product_id',$id)->first();
        if($winner){
            return response()->json(['res'=>\App\Models\User::find( $winner->user_id)]);
        }
        $facturesItems = \App\Models\FactorHasItem::where('product_id',$id)->get();
        $users = [];
        foreach($facturesItems as $items){
            $order = \App\Models\Order::find( @\App\Models\Factor::find($items->factor_id)->order_id);
if($order)
//            if(!empty($order->paied_at)){
                $users[] = $order->user_id;
//            }
        }
        $user = \App\Models\User::find($users[array_rand($users)]);

        $winner = new \App\Models\Winner();
        $winner->user_id = $user->id;
        $winner->product_id = $id;

        $product = \App\Models\Product::find($id);
        $product->active = 0;
        $product->save();

        $winner->save();
        return response()->json(['res'=>$user]);
    })->name('admin.group.winner');
    Route::resource('group','App\Http\Controllers\Admin\GroupController', ['names' => [
        'index' => 'admin.group.index',
        'create' => 'admin.group.create',
        'store' => 'admin.group.store',
        'show' => 'admin.group.show',
        'edit' => 'admin.group.edit',
        'update' => 'admin.group.update',
        'destroy' => 'admin.group.delete',
    ]]);
    Route::resource('zones','App\Http\Controllers\Admin\ZoneController', ['names' => [
        'index' => 'admin.zones.index',
        'create' => 'admin.zones.create',
        'store' => 'admin.zones.store',
        'show' => 'admin.zones.show',
        'edit' => 'admin.zones.edit',
        'update' => 'admin.zones.update',
        'destroy' => 'admin.zones.delete',
    ]]);

    Route::resource('redirects','App\Http\Controllers\Admin\RedirectsController', ['names' => [
        'index' => 'admin.redirects.index',
        'create' => 'admin.redirects.create',
        'store' => 'admin.redirects.store',
        'show' => 'admin.redirects.show',
        'edit' => 'admin.redirects.edit',
        'update' => 'admin.redirects.update',
        'destroy' => 'admin.redirects.delete',
    ]]);

});


Route::get('product/{w}x{h}/{address?}', function( $w=null,$h=null,$address=null)
{
    ini_set('memory_limit','256M');
    ini_set('display_errors',1);
    error_reporting(E_ALL);

    if(!file_exists(public_path('upload') . '/images/products/' . $address)){
        \Illuminate\Support\Facades\Log::info('image not find'. public_path('upload') . '/images/products/' . $address);
        return asset('images/house.svg');
    }
    $img = \Intervention\Image\Facades\Image::make(public_path('upload') . '/images/products/' . $address)->resize($w, $h, function ($constraint) {
        $constraint->aspectRatio();
    });
    ob_get_clean();
    return $img->response();
})->where('address', '(.*)');

require __DIR__.'/auth.php';



Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('verified');
