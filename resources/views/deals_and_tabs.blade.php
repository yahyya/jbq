<div class="mb-5">
    <div class="row">
        <!-- Deal -->
        <div class="col-md-auto mb-6 mb-md-0">
            @foreach(\App\Models\SpecialOffer::whereRaw(\Illuminate\Support\Facades\DB::raw('unix_timestamp(exp_date) > '.time()))->get() as $so)
            <div class="p-3 border border-width-2 border-primary borders-radius-20 bg-white min-width-370">
                <div class="d-flex justify-content-between align-items-center m-1 @if(App::getLocale()=='ar') mr-2 @else ml-2 @endif">
                    <h3 class="font-size-22 mb-0 font-weight-normal text-lh-28 max-width-120">
                        @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                            {{$so->title_ar}}
                        @else
                            {{$so->title_en}}
                        @endif
                    </h3>
                    <div class="d-flex align-items-center flex-column justify-content-center bg-primary rounded-pill height-75 width-75 text-lh-1">
                        <span class="font-size-12">{{__('jbq.Save')}}</span>
                        <div class="font-size-20 font-weight-bold">{{$so->debate_value}}</div>
                    </div>
                </div>
                <div class="mb-4">
                    <a href="{{url('product')}}/{{$so->product_id}}" class="d-block text-center p-4"><img class="w-75" src="{{asset('upload/special_offer')}}/{{$so->image}}" alt="
@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                        {{$so->title_ar}}
                        @else
                        {{$so->title_en}}
                        @endif"></a>
                </div>
                <h5 class="mb-2 font-size-14 text-center mx-auto max-width-180 text-lh-18"><a href="{{url('product')}}/{{$so->product_id}}" class="text-blue font-weight-bold">{{$so->desc}}</a></h5>
                <div class="d-flex align-items-center justify-content-center mb-3">
                    <small style="font-size:11px;color:#ccc" class="m-2">AED</small>
                    <del class="font-size-18 @if(App::getLocale()=='ar') ml-2 @else mr-2 @endif text-gray-2">
                        {{$so->product->price}}</del>
                    <ins class="font-size-30 text-red text-decoration-none">{{$so->product->price - $so->debate_value}}</ins>
                </div>
                @if($so->product->total>0)
                <div class="mb-3 mx-2">

                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <span class="">{{__('jbq.Available')}}: <strong>{{$so->product->total}}</strong></span>
                        <span class="">{{__('jbq.Already Sold')}}: <strong>{{$so->product->sold}}</strong></span>
                    </div>

                    <div class="rounded-pill bg-gray-3 height-20 position-relative">
                        @php
                            $total = 100 - ($so->product->total - $so->product->sold);

                        @endphp
                        <span style="width:{{$total}}%" class="position-absolute @if(App::getLocale()=='ar') right-0 @else left-0 @endif top-0 bottom-0 rounded-pill bg-primary"></span>
                    </div>

                </div>
                @endif
            </div>
            @endforeach
        </div>
        <!-- End Deal -->
        <!-- Tab Prodcut -->
        <div class="col">
            <!-- Features Section -->
            <div class="">
                <!-- Nav Classic -->
                <div class="position-relative bg-white text-center z-index-2">
                    <ul class="nav nav-classic nav-tab justify-content-center" id="pills-tab" role="tablist">

                        @foreach(\App\Models\Tag::where('show_index',1)->get() as $tag)
                        <li class="nav-item">
                            <a class="nav-link @if($loop->iteration ==1) active @endif " id="pills-example{{$tag->id}}-tab" data-toggle="pill" href="#pills-example{{$tag->id}}" role="tab" aria-controls="pills-example{{$tag->id}}" @if($loop->iteration ==1)  aria-selected="true" @endif>
                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                    @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                        {{$tag->title_ar}}
                                    @else
                                        {{$tag->title_en}}
                                    @endif
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- End Nav Classic -->

                <!-- Tab Content -->
                <div class="tab-content" id="pills-tabContent">
                    @foreach(\App\Models\Tag::where('show_index',1)->get() as $tag)
                        <div class="tab-pane fade pt-2 @if($loop->iteration ==1)  show active @endif" id="pills-example{{$tag->id}}" role="tabpanel" aria-labelledby="pills-example{{$tag->id}}-tab">
                            <ul class="row list-unstyled products-group no-gutters">
                                @foreach($tag->products()->take(8)->get() as $pr)
                                    @include('product_box_tabs',['pr'=>$pr])
                                @endforeach
                            </ul>
                        </div>
                    @endforeach


                </div>
                <!-- End Tab Content -->
            </div>
            <!-- End Features Section -->
        </div>
        <!-- End Tab Prodcut -->
    </div>
</div>