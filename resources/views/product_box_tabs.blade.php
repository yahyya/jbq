<li class="col-6 col-wd-3 col-md-4 product-item">
    <div class="product-item__outer h-100">
        <div class="product-item__inner px-xl-4 p-3" @if(App::getLocale()=='ar') style="text-align:right" @endif>
            <div class="product-item__body pb-xl-2">
                <div class="mb-2"><a href="{{url('product/')}}/{{$pr->url}}" class="font-size-12 text-gray-5">
                        @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                            {{$pr->category->title_ar}}
                        @else
                            {{$pr->category->title_en}}
                        @endif
                    </a></div>
                <h3 class="mb-1 product-item__title"><a href="{{url('product/')}}/{{$pr->url}}" class="text-blue font-weight-bold">
                        @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                            {{$pr->title_ar}}
                        @else
                            {{$pr->title_en}}
                        @endif
                    </a></h3>
                <div class="mb-2">
                    <a href="{{url('product/')}}/{{$pr->url}}" class="d-block text-center">
                        <img class="img-fluid lazy" src="{{asset('images/circle-preloader.svg')}}" data-src="{{@$pr->image->file_name}}" alt="{{@$pr->image->alt}}"></a>
                </div>
                <div class="flex-center-between mb-1">
                    <div class="prodcut-price">

                        <div class="text-gray-100">
                            <small style="font-size:11px;color:#ccc">AED</small>
                            @if(!empty($pr->fixed_price))
                                <ins class="font-size-20 text-decoration-none">{{$pr->fixed_price}}</ins>
                                <del class="font-size-15 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-2 @else ml-2 @endif text-gray-6">{{$pr->price}}</del>
                            @else
                                <ins class="font-size-20 text-decoration-none">{{$pr->price}}</ins>
                            @endif</div>
                    </div>
                    <div class="d-none d-xl-block prodcut-add-cart">
                        <a href="{{url('product/')}}/{{$pr->url}}" class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>