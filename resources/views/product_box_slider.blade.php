@if(@$pr->id)
<li class="@if(@$classes) {{$classes}} @else col-md-4 product-item product-item__card pb-2 mb-2 pb-md-0 mb-md-0 border-bottom border-md-bottom-0 @endif">
    <div class="product-item__outer h-100">
        <div class="product-item__inner p-md-3 row no-gutters">
            <div class="col col-lg-auto product-media-left">
                <a href="{{url('/product')}}/{{$pr->url}}" class="max-width-150 d-block">
                    <img class="img-fluid" src="{{@$pr->image->file_name}}" alt="@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                    {{$pr->title_ar}}
                    @else
                    {{$pr->title_en}}
                    @endif"></a>
            </div>
            <div class="col product-item__body pl-2 pl-lg-3 mr-xl-2 mr-wd-1">
                <div class="mb-4">
                    <div class="mb-2"><a href="{{url('category/')}}/{{$pr->category->url}}" class="font-size-12 text-gray-5">
                            @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                {{$pr->category->title_ar}}
                            @else
                                {{$pr->category->title_en}}
                            @endif
                        </a></div>
                    <h3 class="product-item__title"><a href="{{url('/product')}}/{{$pr->url}}" class="text-blue font-weight-bold">
                            @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                {{$pr->title_ar}}
                            @else
                                {{$pr->title_en}}
                            @endif
                        </a></h3>
                </div>
                <div class="flex-center-between mb-3">
                    <div class="prodcut-price">
                        <div class="text-gray-100">
                            <small style="font-size:11px;color:#ccc">AED</small>
                            @if(!empty($pr->fixed_price))
                                <ins class="font-size-25 text-decoration-none">{{$pr->fixed_price}}</ins>
                                <del class="font-size-20 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-2 @else ml-2 @endif text-gray-6">{{$pr->price}}</del>
                            @else
                                <ins class="font-size-25 text-decoration-none">{{$pr->price}}</ins>
                            @endif
                        </div>
                    </div>
                    <div class="d-none d-xl-block prodcut-add-cart">
                        <a href="{{url('/product')}}/{{$pr->url}}" class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>
@endif