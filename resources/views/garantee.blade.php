@extends('header')
@section('content')



    <section class="container mt-5 pt-3">
        <div class="row">
            <div class="col-md-6 col-sm-12 text-center">
                <img src="{{asset('image/title.jpg')}}"  width="70%"/>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="text-center">


                    <p style="font-size: 14px;color:#393939;text-align:right;line-height:30px;">
                       در رابطه با گارانتی
                    </p>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p>لطفا کد درج شده بر روی محصول را وارد کنید.</p>
                        <div>
                            <div class="form-group">
                                <label>
                                    سریال محصول :
                                </label>
                                <input type="text" id="tbGaranteeSerial2" class="form-control" />
                            </div>
                            <div class="form-group mt-2 text-center">
                                <div id="garanteeTime2">
                                </div>
                                <div style="display:none" class="text-center" id="garanteeLoading2">
                                    <img width="50" src="{{asset('/image/loading.gif')}}" />
                                </div>

                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-block btn-success" id="check-garantee2" >
                                    بررسی
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#check-garantee2').on('click',function(){
                var serial = $('#tbGaranteeSerial2').val();
                checkGarantee(serial);
            });
        });
        function checkGarantee(serial){
            $('#check-garantee2').hide();
            $('#garanteeLoading2').show();

            $.ajax(
                {
                    type: 'POST',
                    url: '{{\Illuminate\Support\Facades\URL::to('api/garantee/')}}/',
                    data:{serial:serial},
                    dataType: 'JSON',
                    beforeSend: function () {


                    },
                    success: function (res) {
                        console.log(res);
                        if(res.res) {
                            $('#garanteeTime2').html('تاریخ انقضای گارانتی : ' +
                                res.date);
                        } else {
                            $('#garanteeTime2').html('سریال وارد شده صحیح نمیباشد!');
                        }
                    },
                    complete:function(){
                        $('#check-garantee2').show();
                        $('#garanteeLoading2').hide();

                    }
                });
        }
    </script>

@endsection
