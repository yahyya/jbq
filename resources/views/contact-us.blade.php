@extends('header')
@section('content')



<section class="container m-5">

    <div class="row text-right p5 mb-3" style="direction:rtl">
        <h3>
            تماس با ما
        </h3>
    </div>
    <div class="row " style="min-height:500px;direction: rtl">

        <div class="col-md-6 col-sm-12">
            <div id="mapNew" style="width:100%;height:500px;direction:ltr"></div>
        </div>

        <div class="col-md-6 col-sm-12" style="text-align: right">
            <div class="row p-3 justify-content-center">
                <h5>
                    اطلاعات تماس
                </h5>
            </div>
            <div>
                <ul>
                    <li>
                        آدرس : شیراز - بلوار قدوسی غربی
                    </li>
                    <li style="">
                        <div style="direction:ltr">
                            تلفن :
                            ۳۷۷۳۲۵۶۰-۰۷۱
                            <span style="color:#878787">|</span>
                            ۳۶۳۱۳۷۵۰-۰۷۱
                        </div>
                    </li>
                    <li>

                        ایمیل :
                        <a href="email:info@iranmaple.com">
                            info@amiranled.com
                        </a>
                    </li>
                </ul>
            </div>

            <div>
                <hr/>
                <div class="row p-3 justify-content-center">
                    <h5>
                        فرم ارسال پیام
                    </h5>
                </div>
                <form action="{{route('web.contactUs')}}" method="post">
                    @csrf
                    <div class="form-group validate-check">
                        <label for="tbName">نام و نام خانوادگی</label>
                        <div class="form-line">
                            <input id="tbName" type="text" class="form-control dynamic-form" name="tbName">
                        </div>
                    </div>
                    <div class="form-group validate-check">
                        <label for="tbEmail">ایمیل</label>
                        <div class="form-line">
                            <input id="tbEmail" type="text" class="form-control dynamic-form" name="tbEmail">
                        </div>
                    </div>
                    <div class="form-group validate-check">
                        <label for="tbTitle">موضوع</label>
                        <div class="form-line">
                            <input id="tbTitle" type="text" class="form-control dynamic-form" name="موضوع">
                        </div>
                    </div>
                    <div class="form-group validate-check">
                        <label for="tbTxt">متن</label>
                        <div class="form-line">
                            <textarea id="tbTxt" type="text" class="form-control dynamic-form"
                                      name="tbTxt"></textarea>
                        </div>
                    </div>
                    <div class="form-group mt-2">
                        @if(@$sent)
                            نظر شما با موفقیت ارسال شد. با تشکر از شما
                        @endif
                        <button @if(@$sent) disabled="disabled" @endif class="btn btn-primary">
                            ارسال
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
    <script src='{{asset('js/mapbox/mapbox-gl.js')}}'></script>
    <link href='{{asset('js/mapbox/mapbox-gl.css')}}' rel='stylesheet'/>

    <script src='{{asset('js/mapbox/mapbox-gl-geocoder.min.js')}}'></script>
    <link rel='stylesheet'
          href='{{asset('js/mapbox/mapbox-gl-geocoder.css')}}'
          type='text/css'/>
    <script>
        mapboxgl.accessToken = 'pk.eyJ1IjoiYmF0YWJhcHAiLCJhIjoiY2pvYnZ6aHlhMG4xdzNxcndqOTZ5cWZpeiJ9.m2eKos4wTVoqHYmKPgSjnA';
        var map = new mapboxgl.Map({
            container: 'mapNew',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [52.470099, 29.644372],
            zoom: 17
        });
        circleMarker = new mapboxgl.Marker().setLngLat([52.503687, 29.624322]).addTo(map);
        mapboxgl.setRTLTextPlugin("{{asset('js/mapbox/mapbox-gl-rtl-text.js')}}");
        map.addControl(new mapboxgl.NavigationControl());


    </script>
    <script>
        $(document).ready(function () {
            $('#check-garantee2').on('click', function () {
                var serial = $('#tbGaranteeSerial2').val();
                checkGarantee(serial);
            });
        });

        function checkGarantee(serial) {
            $('#check-garantee2').hide();
            $('#garanteeLoading2').show();

            $.ajax(
                {
                    type: 'POST',
                    url: '{{\Illuminate\Support\Facades\URL::to('api/garantee/')}}/',
                    data: {serial: serial},
                    dataType: 'JSON',
                    beforeSend: function () {


                    },
                    success: function (res) {
                        console.log(res);
                        if (res.res) {
                            $('#garanteeTime2').html('تاریخ انقضای گارانتی : ' +
                                res.date);
                        } else {
                            $('#garanteeTime2').html('سریال وارد شده صحیح نمیباشد!');
                        }
                    },
                    complete: function () {
                        $('#check-garantee2').show();
                        $('#garanteeLoading2').hide();

                    }
                });
        }
    </script>

@endsection
