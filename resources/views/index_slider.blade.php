<div class="mb-5">
    <div class="bg-img-hero" style="background-image: url({{asset('images/bg_big.png')}});">
        <div class="container min-height-420 overflow-hidden">
            @php
                $allSliders = \App\Models\Slider::where('enabled',1)->get();
            @endphp


            <div class="js-slick-carousel u-slick"
                 data-pagi-classes="text-center position-absolute right-0 bottom-0 left-0 u-slick__pagination u-slick__pagination--long justify-content-start mb-3 mb-md-4 offset-xl-3 @if(App::getLocale()=='ar') pr-2 @else pl-2 @endif pb-1">
                @foreach(\App\Models\Slider::where('enabled',1)->get() as $slider)
                <div class="js-slide bg-img-hero-center">
                    <div class="row min-height-420 py-7 py-md-0">
                        <div class="offset-xl-3 col-xl-4 col-6 mt-md-8">


                            @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                {!!@$slider->desc_ar!!}
                            @else
                                {!!@$slider->desc_en!!}
                            @endif
                            <a target="_blank" href="{{url('/')}}{{@\App\Models\Product::find($slider->product_id)->url}}" class="btn btn-primary transition-3d-hover rounded-lg font-weight-normal py-2 px-md-7 px-3 font-size-16"
                               data-scs-animation-in="fadeInUp"
                               data-scs-animation-delay="400">
                                {{__('jbq.Start Buying')}}
                            </a>
                        </div>
                        <div class="col-xl-5 col-6  d-flex align-items-center"
                             data-scs-animation-in="zoomIn"
                             data-scs-animation-delay="500">
                            <img class="img-fluid" src="{{asset('/upload/slider/')}}/{{$slider->file_name}}" alt="{{$slider->title}}">
                        </div>
                    </div>
                </div>

                @endforeach
            </div>
        </div>
    </div>
</div>

