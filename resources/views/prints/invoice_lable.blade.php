<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'HivaTeck') }}</title>

    <!-- Favicon-->
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{URL::asset('css/fontiran.css')}}">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin-asset/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Bootstrap Rtl -->
    <link href="{{asset('admin-asset/plugins/bootstrap-rtl/dist/css/bootstrap-rtl.min.css')}}" rel="stylesheet">

    <!-- Custom Styles -->
    <style>
        *{
            font-family: IRANSans;
        }
        table.table{
            margin-bottom: 0;

        }
        thead{
            background-color: #333;
            color: #fff;
        }
        @media print { body { -webkit-print-color-adjust: exact; } }

    </style>
</head>
<body>
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="panel panel-default">--}}
            {{--<div class="panel-heading">Panel heading without title</div>--}}
            {{--<div class="panel-body">--}}
                {{--Basic panel example--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div> <!-- /row -->--}}
{{--</div> <!-- /container -->--}}

<iframe>
<table width="245px !important" >
    <tr style="border-bottom:solid 1px #333">

        <td class="text-center" width="70%"  >
            <div style="padding:5px 0 5px 0">
                <div class="text-right">
                شماره سفارش :

                <span style="font-size:16px;font-weight: bold">
                    {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian(153128012)}}
                </span>


                <div>
                    تاریخ :
                   {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian( \Morilog\Jalali\Jalalian::forge($order->created_at)->format('Y/m/d'))}}
                </div>
                <div>
                    ساعت :
                    {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($order->created_at->format('H:i'))}}
                </div>
            </div>
            </div>
        </td>
        <td class="text-center" width="30%" >
            <img width="80%" src="{{asset('images/grizzly-logo-farsi-min.png')}}" />
        </td>
    </tr>

    <tr>
        <td colspan="2">
           <div style="padding:5px 0 5px 0">
            <div>
            نام خریدار :
            {{$order->user->name}} {{$order->user->family}}
            </div>
            <div>
            شماره تماس :
            {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian( $order->user->mobile)}}
            </div>
            <div>
                آدرس :
                {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian( $order->address->address)}}
            </div>
           </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table " style="border:solid 1px #000">
                <thead>
                    <tr >
                        {{--<td align="center">ردیف</td>--}}
                        <td align="center" style="border-bottom:solid 1px #000">عنوان</td>
                        <td align="center" style="border-bottom:solid 1px #000">تعداد</td>
                        <td align="center" style="border-bottom:solid 1px #000">قیمت</td>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty(@$order->factor()->items))
                    @foreach(@$order->factor()->items as $key=>$item)
                    <tr>
                        {{--<td align="center">{{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($key+1)}}</td>--}}
                        <?php
                         $theProduct = @$item->product;
                            if (!$theProduct)
                                $theProduct = @$item->customProduct;
                        ?>
                        <td align="center">{{@$theProduct->title}}</td>
                        <td align="center">{{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($item->count)}}</td>
                        <td align="center">{{\App\Http\Controllers\Api\ApiController::priceFormat($item->price,false)}}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </td>
    </tr>
    <tr >
        <td class="text-right" style="padding:5px 0 0 0">
            قیمت کل خوراک
        </td>
        <td class="text-left" style="padding:5px 0 0 0">
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->amount,false)}}
        </td>
    </tr>
    <tr>
        <td class="text-right" >
            هزینه بسته بندی
        </td>
        <td class="text-left" >
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->boxing_price,false)}}
        </td>
    </tr>
    <tr>
        <td class="text-right" >
            هزینه ارسال
        </td>
        <td class="text-left" >
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->delivery_price,false)}}
        </td>
    </tr>
    <tr>
        <td class="text-right" >
            مالیات
        </td>
        <td class="text-left" >
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->tax_price,false)}}
        </td>
    </tr>
    <tr >
        <td class="text-right" style="padding:0 0 5px 0">
            تخفیف
        </td>
        <td class="text-left" style="padding:0 0 5px 0">
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->discount_price,false)}}
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;" >
        <td class="text-right" width="50%" colspan="2" style="padding:5px 0 5px 0">
            <div style="width:50%;float:right">
            قابل پرداخت نهایی :
            </div>
            <div style="width:50%;float:right;text-align: left" >
                <span style="font-size:16px;font-weight: bold">
                    {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->total,false)}}
                </span>
                تومان

            </div>
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;">
        <td class="text-right" width="50%" style="padding:5px 0 5px 0" colspan="2">
            نحوه پرداخت :
            <span style="font-size:16px;font-weight: bold">
            @if($order->payment_type=='0')
                    آنلاین تسویه شد
            @endif
            @if($order->payment_type=='2')
                    از اعتبار تسویه شد
            @endif
            @if($order->payment_type=='1')
                    پرداخت در محل
            @endif
            </span>
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;border-bottom:dotted 1px #000">
        <td class="text-center" colspan="2" style="padding:5px 0 5px 0" >
            گریزلی، اپلیکیشن سفارش غذا
            <br />
            www.grizzlyfood.com

        </td>

    </tr>

</table>
</iframe>

<iframe>
<table width="245px !important" >
    <tr style="border-bottom:solid 1px #333">

        <td class="text-center" width="70%"  >
            <div style="padding:5px 0 5px 0">
                <div class="text-right">
                شماره سفارش :

                <span style="font-size:16px;font-weight: bold">
                    {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian(153128012)}}
                </span>


                <div>
                    تاریخ :
                   {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian( \Morilog\Jalali\Jalalian::forge($order->created_at)->format('Y/m/d'))}}
                </div>
                <div>
                    ساعت :
                    {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($order->created_at->format('H:i'))}}
                </div>
            </div>
            </div>
        </td>
        <td class="text-center" width="30%" >
            <img width="80%" src="{{asset('images/grizzly-logo-farsi-min.png')}}" />
        </td>
    </tr>

    <tr>
        <td colspan="2">
           <div style="padding:5px 0 5px 0">
            <div>
            نام خریدار :
            {{$order->user->name}} {{$order->user->family}}
            </div>
            <div>
            شماره تماس :
            {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian( $order->user->mobile)}}
            </div>
            <div>
                آدرس :
                {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian( $order->address->address)}}
            </div>
           </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table " style="border:solid 1px #000">
                <thead>
                    <tr >
                        {{--<td align="center">ردیف</td>--}}
                        <td align="center" style="border-bottom:solid 1px #000">عنوان</td>
                        <td align="center" style="border-bottom:solid 1px #000">تعداد</td>
                        <td align="center" style="border-bottom:solid 1px #000">قیمت</td>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty(@$order->factor()->items))
                    @foreach(@$order->factor()->items as $key=>$item)
                    <tr>
                        {{--<td align="center">{{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($key+1)}}</td>--}}
                        <?php
                         $theProduct = @$item->product;
                            if (!$theProduct)
                                $theProduct = @$item->customProduct;
                        ?>
                        <td align="center">{{@$theProduct->title}}</td>
                        <td align="center">{{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($item->count)}}</td>
                        <td align="center">{{\App\Http\Controllers\Api\ApiController::priceFormat($item->price,false)}}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </td>
    </tr>
    <tr >
        <td class="text-right" style="padding:5px 0 0 0">
            قیمت کل خوراک
        </td>
        <td class="text-left" style="padding:5px 0 0 0">
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->amount,false)}}
        </td>
    </tr>
    <tr>
        <td class="text-right" >
            هزینه بسته بندی
        </td>
        <td class="text-left" >
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->boxing_price,false)}}
        </td>
    </tr>
    <tr>
        <td class="text-right" >
            هزینه ارسال
        </td>
        <td class="text-left" >
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->delivery_price,false)}}
        </td>
    </tr>
    <tr>
        <td class="text-right" >
            مالیات
        </td>
        <td class="text-left" >
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->tax_price,false)}}
        </td>
    </tr>
    <tr >
        <td class="text-right" style="padding:0 0 5px 0">
            تخفیف
        </td>
        <td class="text-left" style="padding:0 0 5px 0">
            {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->discount_price,false)}}
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;" >
        <td class="text-right" width="50%" colspan="2" style="padding:5px 0 5px 0">
            <div style="width:50%;float:right">
            قابل پرداخت نهایی :
            </div>
            <div style="width:50%;float:right;text-align: left" >
                <span style="font-size:16px;font-weight: bold">
                    {{\App\Http\Controllers\Api\ApiController::priceFormat(@$order->factor()->total,false)}}
                </span>
                تومان

            </div>
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;">
        <td class="text-right" width="50%" style="padding:5px 0 5px 0" colspan="2">
            نحوه پرداخت :
            <span style="font-size:16px;font-weight: bold">
            @if($order->payment_type=='0')
                    آنلاین تسویه شد
            @endif
            @if($order->payment_type=='2')
                    از اعتبار تسویه شد
            @endif
            @if($order->payment_type=='1')
                    پرداخت در محل
            @endif
            </span>
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;border-bottom:dotted 1px #000">
        <td class="text-center" colspan="2" style="padding:5px 0 5px 0" >
            گریزلی، اپلیکیشن سفارش غذا
            <br />
            www.grizzlyfood.com

        </td>

    </tr>

</table>
</iframe>
</body>
<script>
    window.print();
</script>
</html>