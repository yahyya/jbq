<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'HivaTeck') }}</title>

    <!-- Favicon-->
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{URL::asset('css/fontiran.css')}}">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin-asset/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Bootstrap Rtl -->
    <link href="{{asset('admin-asset/plugins/bootstrap-rtl/dist/css/bootstrap-rtl.min.css')}}" rel="stylesheet">

    <!-- Custom Styles -->
    <style>
        *{
            font-family: IRANSans;
        }
        table.table{
            margin-bottom: 0;

        }
        thead{
            background-color: #333;
            color: #fff;
        }
        @media print { body { -webkit-print-color-adjust: exact; } }

    </style>
    <script>
        function printAll() {
            window.open("{{url('/admin/orders/')}}/{{$order->id}}/print?type=1");
            window.open("{{url('/admin/orders/')}}/{{$order->id}}/print?type=2");
            window.open("{{url('/admin/orders/')}}/{{$order->id}}/print?type=3");
        }
    </script>
</head>
<body>

<button type="button" onclick="printAll()" >Make A Print</button>
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="panel panel-default">--}}
            {{--<div class="panel-heading">Panel heading without title</div>--}}
            {{--<div class="panel-body">--}}
                {{--Basic panel example--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div> <!-- /row -->--}}
{{--</div> <!-- /container -->--}}


</body>
</html>