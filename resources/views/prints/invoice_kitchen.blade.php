<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'HivaTeck') }}</title>

    <!-- Favicon-->
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{URL::asset('css/fontiran.css')}}">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin-asset/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Bootstrap Rtl -->
    <link href="{{asset('admin-asset/plugins/bootstrap-rtl/dist/css/bootstrap-rtl.min.css')}}" rel="stylesheet">

    <!-- Custom Styles -->
    <style>
        * {
            font-family: IRANSans;
        }

        table.table {
            margin-bottom: 0;

        }

        thead {
            background-color: #333;
            color: #fff;
        }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

    </style>
</head>
<body>
{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="panel panel-default">--}}
{{--<div class="panel-heading">Panel heading without title</div>--}}
{{--<div class="panel-body">--}}
{{--Basic panel example--}}
{{--</div>--}}
{{--</div>--}}
{{--</div> <!-- /row -->--}}
{{--</div> <!-- /container -->--}}


    <table width="245px !important">
        <tr >

            <td class="text-center" >
                <div style="padding:5px 0 5px 0">
                    <div class="text-center">
                        شماره سفارش

                        <span style="font-size:18px;font-weight: bold">
                            {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian(153128012)}}
                        </span>
                    </div>
                    <div class="text-right">

                        <div>
                            تاریخ :
                            {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian( \Morilog\Jalali\Jalalian::forge($order->created_at)->format('Y/m/d'))}}
                        </div>
                        <div>
                            ساعت :
                            {{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($order->created_at->format('H:i'))}}
                        </div>
                    </div>

                </div>
            </td>

        </tr>

        <tr>
            <td colspan="2">
                <div style="padding:5px 0 5px 0">
                    <div>
                        نام خریدار :
                        {{$order->user->name}} {{$order->user->family}}
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="table " style="border:solid 1px #000">
                    <thead>
                    <tr>
                        <td align="center">ردیف</td>
                        <td align="center" style="border-bottom:solid 1px #000">عنوان</td>
                        <td align="center" style="border-bottom:solid 1px #000">تعداد</td>

                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty(@$order->factor()->items))
                        @foreach(@$order->factor()->items as $key=>$item)
                            <tr>
                                <td align="center">{{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($key+1)}}</td>
                                <?php
                                $theProduct = @$item->product;
                                $items=[];
                                if (!$theProduct){
                                    $theProduct = @$item->customProduct;

                                }
                                ?>
                                <td align="center">{{@$theProduct->title}}
                                    <br />

                                    @if(!empty($theProduct->items))
                                        {{@$theProduct}}
                                        @foreach ($theProduct->items as $cpItem)
                                            {{@$cpItem}}
                                            {{@$cpItem->title}} {{@$cpItem->count}}
                                            <br />
                                        @endforeach
                                    @endif
                                </td>
                                <td align="center">{{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($item->count)}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </td>
        </tr>

        <tr>
            <td class="text-right" style="padding:0 0 5px 0" colspan="2">
                توضیحات خریدار :

                {{@$order->desc}}
            </td>
        </tr>



    </table>


</body>
<script>
    window.print();
</script>
</html>