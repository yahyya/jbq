<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'HivaTeck') }}</title>

    <!-- Favicon-->
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{URL::asset('css/fontiran.css')}}">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('admin-asset/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Bootstrap Rtl -->
    <link href="{{asset('admin-asset/plugins/bootstrap-rtl/dist/css/bootstrap-rtl.min.css')}}" rel="stylesheet">

    <!-- Custom Styles -->
    <style>
        *{
            font-family: IRANSans;
        }
        table.table{
            margin-bottom: 0;

        }
        thead{
            background-color: #333;
            color: #fff;
        }
        @media print { body { -webkit-print-color-adjust: exact; } }

    </style>
</head>
<body>
{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="panel panel-default">--}}
            {{--<div class="panel-heading">Panel heading without title</div>--}}
            {{--<div class="panel-body">--}}
                {{--Basic panel example--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div> <!-- /row -->--}}
{{--</div> <!-- /container -->--}}

<table width="245px !important" >
    <tr style="border-bottom:solid 1px #333">

        <td class="text-center" width="70%"  >
            <div style="padding:5px 0 5px 0">
                <div class="text-right">


                <span style="font-size:16px;font-weight: bold">
                    {{153128012}}
                </span>


                <div>
                    Date :
                   {{ \Carbon\Carbon::make($order->created_at)->format('Y/m/d')}}
                </div>
                <div>
                    Hour :
                    {{$order->created_at->format('H:i')}}
                </div>
            </div>
            </div>
        </td>
        <td class="text-center" width="30%" >
            <img width="80%" src="{{asset('images/logo.png')}}" />
        </td>
    </tr>

    <tr>
        <td colspan="2">
           <div style="padding:5px 0 5px 0">
            <div>
            Name :
            {{$order->user->name}} {{$order->user->family}}
            </div>
            <div>
            Phone :
            {{ $order->user->mobile}}
            </div>
            <div>
                @if($order->address)
                    <h4>
                        Order Address
                    </h4>
                    <ul>
                        <li>
                            {{__('jbq.First name')}} : {{{$order->address->first_name}}}
                        </li>
                        <li>
                            {{__('jbq.Last name')}}  : {{{$order->address->last_name}}}
                        </li>
                        <li>
                            {{__('jbq.Country')}}  : {{{$order->address->country}}}
                        </li>
                        <li>
                            {{__('jbq.City')}} : {{{$order->address->city}}}
                        </li>
                        <li>
                            {{__('jbq.State')}} : {{{$order->address->state}}}
                        </li>
                        <li>
                            {{__('jbq.Postcode/Zip')}} : {{{$order->address->postcode}}}
                        </li>
                        <li>
                            {{__('jbq.Phone')}} : {{{$order->address->phone}}}
                        </li>
                        <li>
                            {{__('jbq.Email address')}} : {{{$order->address->email}}}
                        </li>
                        <li>
                            {{__('jbq.Company name')}} : {{{$order->address->company_name}}}
                        </li>
                        <li>
                            {{__('jbq.Street address')}} : {{{$order->address->street_address}}}
                        </li>
                    </ul>
                @endif
                @if($order->shippingAddress)
                    <h4>
                        Order Shipping Address
                    </h4>
                    <ul>
                        <li>
                            {{__('jbq.First name')}} : {{{$order->shippingAddress->first_name}}}
                        </li>
                        <li>
                            {{__('jbq.Last name')}}  : {{{$order->shippingAddress->last_name}}}
                        </li>
                        <li>
                            {{__('jbq.Country')}}  : {{{$order->shippingAddress->country}}}
                        </li>
                        <li>
                            {{__('jbq.City')}} : {{{$order->shippingAddress->city}}}
                        </li>
                        <li>
                            {{__('jbq.State')}} : {{{$order->shippingAddress->state}}}
                        </li>
                        <li>
                            {{__('jbq.Postcode/Zip')}} : {{{$order->shippingAddress->postcode}}}
                        </li>
                        <li>
                            {{__('jbq.Phone')}} : {{{$order->shippingAddress->phone}}}
                        </li>
                        <li>
                            {{__('jbq.Email address')}} : {{{$order->shippingAddress->email}}}
                        </li>
                        <li>
                            {{__('jbq.Company name')}} : {{{$order->shippingAddress->company_name}}}
                        </li>
                        <li>
                            {{__('jbq.Street address')}} : {{{$order->shippingAddress->street_address}}}
                        </li>
                    </ul>
                @endif
            </div>
           </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table " style="border:solid 1px #000">
                <thead>
                    <tr >
                        {{--<td align="center">ردیف</td>--}}
                        <td align="center" style="border-bottom:solid 1px #000">Title</td>
                        <td align="center" style="border-bottom:solid 1px #000">Count</td>
                        <td align="center" style="border-bottom:solid 1px #000">Price</td>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty(@$order->factor()->items))
                    @foreach(@$order->factor()->items as $key=>$item)
                    <tr>
                        {{--<td align="center">{{\App\Http\Controllers\Api\ApiController::convertNumbersToPersian($key+1)}}</td>--}}
                        <?php
                         $theProduct = @$item->product;
                        ?>
                        <td align="center">{{@$theProduct->title_en}}</td>
                        <td align="center">{{$item->count}}</td>
                        <td align="center">{{$item->price}}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </td>
    </tr>
    <tr >
        <td class="text-left" style="padding:5px 0 0 0">
            Total Price
        </td>
        <td class="text-right" style="padding:5px 0 0 0">
            {{@$order->factor()->amount}}
        </td>
    </tr>
    <tr>
        <td class="text-left" >
           Packing
        </td>
        <td class="text-right" >
            {{@$order->factor()->boxing_price}}
        </td>
    </tr>
    <tr>
        <td class="text-left" >
            Delivery Price
        </td>
        <td class="text-right" >
            {{@$order->factor()->delivery_price}}
        </td>
    </tr>
    <tr>
        <td class="text-left" >
            Tax
        </td>
        <td class="text-right" >
            {{@$order->factor()->tax_price}}
        </td>
    </tr>
    <tr >
        <td class="text-left" style="padding:0 0 5px 0">
            Discount
        </td>
        <td class="text-right" style="padding:0 0 5px 0">
            {{@$order->factor()->discount_price}}
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;" >
        <td class="text-left" width="50%" colspan="2" style="padding:5px 0 5px 0">
            <div style="width:50%;float:left">
            Final Price :
            </div>
            <div style="width:50%;float:left;text-align: right" >
                <span style="font-size:16px;font-weight: bold">
                    {{@$order->factor()->total}}
                </span>


            </div>
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;">
        <td class="text-left" width="50%" style="padding:5px 0 5px 0" colspan="2">
            Payment Type :
            <span style="font-size:16px;font-weight: bold;text-align: right">
            @if($order->payment_type=='0')
                   Online
            @endif
            @if($order->payment_type=='2')
                    Credit
            @endif
            @if($order->payment_type=='1')
                    In Place
            @endif
            </span>
        </td>
    </tr>
    <tr style="border-top:solid 1px #000;border-bottom:dotted 1px #000">
        <td class="text-center" colspan="2" style="padding:5px 0 5px 0" >
            JBQ
            <br />
            www.jbq.ae

        </td>

    </tr>

</table>
</body>
<script>
    window.print();
</script>
</html>