<div class="container">
    <!-- Prodcut-cards-carousel -->
    <div class="space-top-2" >

        <div  class=" d-flex justify-content-between border-bottom border-color-1 flex-md-nowrap flex-wrap border-sm-bottom-0">
            <h3 class="section-title mb-0 pb-2 font-size-22">{{__('jbq.Bestsellers')}}</h3>
            <ul class="nav nav-pills mb-2 pt-3 pt-md-0 mb-0 border-top border-color-1 border-md-top-0 align-items-center font-size-15 font-size-15-md flex-nowrap flex-md-wrap overflow-auto overflow-md-visble">
                <li class="nav-item flex-shrink-0 flex-md-shrink-1">
                    <a class="text-gray-90 btn btn-outline-primary border-width-2 rounded-pill py-1 px-4 font-size-15 text-lh-19 font-size-15-md" href="#">{{__('jbq.Top 20')}}</a>
                </li>
                @foreach(\App\Models\Category::where('show_index',1)->get() as $cat)
                <li class="nav-item flex-shrink-0 flex-md-shrink-1">
                    <a class="nav-link text-gray-8" href="{{url('category')}}/{{$cat->id}}">
                        @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                            {{$cat->title_ar}}
                        @else
                            {{$cat->title_en}}
                        @endif
                    </a>
                </li>
                @endforeach
{{--                <li class="nav-item flex-shrink-0 flex-md-shrink-1">--}}
{{--                    <a class="nav-link text-gray-8" href="#">Phones & Tablets</a>--}}
{{--                </li>--}}
{{--                <li class="nav-item flex-shrink-0 flex-md-shrink-1">--}}
{{--                    <a class="nav-link text-gray-8" href="#">Laptops & Computers</a>--}}
{{--                </li>--}}
{{--                <li class="nav-item flex-shrink-0 flex-md-shrink-1">--}}
{{--                    <a class="nav-link text-gray-8" href="#"> Video Cameras</a>--}}
{{--                </li>--}}
            </ul>
        </div>
        <div @if(App::getLocale()=='ar') style="direction:ltr" @endif class="js-slick-carousel u-slick u-slick--gutters-2 overflow-hidden u-slick-overflow-visble pt-3 pb-6"
             data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-4">
            <?php
            $mostView = \App\Models\Product::take(20)->orderBy('view','DESC')->get();
            $total = count($mostView);
            $totalSlides = round($total/8);

            ?>
            @for($i=0;$i<$totalSlides;$i++)
            <div class="js-slide">
                <ul class="row list-unstyled products-group no-gutters mb-0 overflow-visible">
                    @foreach(\App\Models\Product::take(8)->skip(8*$i)->orderBy('view','DESC')->get() as $pr)
                        @include('product_box_small',['pr'=>$pr])
                    @endforeach
                </ul>
            </div>
            @endfor


        </div>
    </div>
    <!-- End Prodcut-cards-carousel -->
    <!-- Full banner -->
{{--    <div class="mb-6">--}}
{{--        <a href="../shop/shop.html" class="d-block text-gray-90">--}}
{{--            <div class="" style="background-image: url(../../assets/img/1400X206/img1.jpg);">--}}
{{--                <div class="space-top-2-md p-4 pt-6 pt-md-8 pt-lg-6 pt-xl-8 pb-lg-4 px-xl-8 px-lg-6">--}}
{{--                    <div class="flex-horizontal-center mt-lg-3 mt-xl-0 overflow-auto overflow-md-visble">--}}
{{--                        <h1 class="text-lh-38 font-size-32 font-weight-light mb-0 flex-shrink-0 flex-md-shrink-1">SHOP AND <strong>SAVE BIG</strong> ON HOTTEST TABLETS</h1>--}}
{{--                        <div class="ml-5 flex-content-center flex-shrink-0">--}}
{{--                            <div class="bg-primary rounded-lg px-6 py-2">--}}
{{--                                <em class="font-size-14 font-weight-light">STARTING AT</em>--}}
{{--                                <div class="font-size-30 font-weight-bold text-lh-1">--}}
{{--                                    <sup class="">$</sup>79<sup class="">99</sup>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </a>--}}
{{--    </div>--}}
    <!-- End Full banner -->
    @if (\Illuminate\Support\Facades\Request::session()->exists('product_view'))
    <!-- Recently viewed -->
    <div class="mb-6">
        <div class="position-relative">
            <div class="border-bottom border-color-1 mb-2">
                <h3 class="section-title mb-0 pb-2 font-size-22">__({{'Recently Viewed'}})</h3>
            </div>
            <div class="js-slick-carousel u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1"
                 data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0"
                 data-slides-show="7"
                 data-slides-scroll="1"
                 data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                 data-arrow-left-classes="fa fa-angle-left right-1"
                 data-arrow-right-classes="fa fa-angle-right right-0"
                 data-responsive='[{
                              "breakpoint": 1400,
                              "settings": {
                                "slidesToShow": 6
                              }
                            }, {
                                "breakpoint": 1200,
                                "settings": {
                                  "slidesToShow": 4
                                }
                            }, {
                              "breakpoint": 992,
                              "settings": {
                                "slidesToShow": 3
                              }
                            }, {
                              "breakpoint": 768,
                              "settings": {
                                "slidesToShow": 2
                              }
                            }, {
                              "breakpoint": 554,
                              "settings": {
                                "slidesToShow": 2
                              }
                            }]'>
                @foreach(\Illuminate\Support\Facades\Request::session()->get('product_view') as $prId)
                    <?php
                        $pr = \App\Models\Product::find($prId);
                    ?>
                <div class="js-slide products-group">
                    <div class="product-item">
                        <div class="product-item__outer h-100">
                            <div class="product-item__inner p-md-3 row no-gutters">
                                <div class="col col-lg-auto product-media-left">
                                    <a href="{{url('/')}}/product/{{$pr->url}}" class="max-width-150 d-block">
                                        <img class="img-fluid" src="{{@$pr->image->file_name}}" alt="@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                        {{$pr->title_ar}}
                                        @else
                                        {{$pr->title_en}}
                                        @endif"></a>
                                </div>
                                <div class="col product-item__body pl-2 pl-lg-3 mr-xl-2 mr-wd-1">
                                    <div class="mb-4">
                                        <div class="mb-2"><a href="{{url('category/')}}/{{$pr->category_id}}" class="font-size-12 text-gray-5"> @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                    {{$pr->category->title_ar}}
                                                @else
                                                    {{$pr->category->title_en}}
                                                @endif</a></div>
                                        <h5 class="product-item__title"><a href="{{url('/')}}/product/{{$pr->url}}" class="text-blue font-weight-bold">@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                    {{$pr->title_ar}}
                                                @else
                                                    {{$pr->title_en}}
                                                @endif</a></h5>
                                    </div>
                                    <div class="flex-center-between mb-3">
                                        <div class="prodcut-price">
                                            <div class="text-gray-100">
                                                <small style="font-size:11px;color:#ccc">AED</small>
                                                @if(!empty($pr->fixed_price))
                                                    <ins class="font-size-25 text-decoration-none">{{$pr->fixed_price}}</ins>
                                                    <del class="font-size-20 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-2 @else ml-2 @endif text-gray-6">{{$pr->price}}</del>
                                                @else
                                                    <ins class="font-size-25 text-decoration-none">{{$pr->price}}</ins>
                                                @endif</div>
                                        </div>
                                        <div class="d-none d-xl-block prodcut-add-cart">
                                            <a href="{{url('/')}}/product/{{$pr->url}}" class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Recently viewed -->
    @endif
    <!-- Brand Carousel -->
{{--    <div class="mb-8">--}}
{{--        <div class="py-2 border-top border-bottom">--}}
{{--            <div class="js-slick-carousel u-slick my-1"--}}
{{--                 data-slides-show="5"--}}
{{--                 data-slides-scroll="1"--}}
{{--                 data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"--}}
{{--                 data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"--}}
{{--                 data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"--}}
{{--                 data-responsive='[{--}}
{{--                                "breakpoint": 992,--}}
{{--                                "settings": {--}}
{{--                                    "slidesToShow": 2--}}
{{--                                }--}}
{{--                            }, {--}}
{{--                                "breakpoint": 768,--}}
{{--                                "settings": {--}}
{{--                                    "slidesToShow": 1--}}
{{--                                }--}}
{{--                            }, {--}}
{{--                                "breakpoint": 554,--}}
{{--                                "settings": {--}}
{{--                                    "slidesToShow": 1--}}
{{--                                }--}}
{{--                            }]'>--}}
{{--                <div class="js-slide">--}}
{{--                    <a href="#" class="link-hover__brand">--}}
{{--                        <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img1.png" alt="Image Description">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="js-slide">--}}
{{--                    <a href="#" class="link-hover__brand">--}}
{{--                        <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img2.png" alt="Image Description">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="js-slide">--}}
{{--                    <a href="#" class="link-hover__brand">--}}
{{--                        <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img3.png" alt="Image Description">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="js-slide">--}}
{{--                    <a href="#" class="link-hover__brand">--}}
{{--                        <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img4.png" alt="Image Description">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="js-slide">--}}
{{--                    <a href="#" class="link-hover__brand">--}}
{{--                        <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img5.png" alt="Image Description">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="js-slide">--}}
{{--                    <a href="#" class="link-hover__brand">--}}
{{--                        <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img6.png" alt="Image Description">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- End Brand Carousel -->
</div>