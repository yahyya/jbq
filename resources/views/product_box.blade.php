<li class="mb-4">
    <div class="row" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
        <div class="col-auto">
            <a href="{{url('product')}}/{{$pr->id}}" class="d-block width-75">
                <img class="img-fluid" src="{{asset('images/placeholder.gif')}}" data-src="{{@$pr->image->file_name}}" alt="@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                {{$pr->title_ar}}
                @else
                {{$pr->title_en}}
                @endif">
            </a>
        </div>
        <div class="col">
            <h3 class="text-lh-1dot2 font-size-14 mb-0"><a href="{{url('product')}}/{{$pr->id}}">
                    @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                        {{$pr->title_ar}}
                    @else
                        {{$pr->title_en}}
                    @endif
                </a>
            </h3>
            <div class="font-weight-bold">
                <small style="font-size:11px;color:#4a4a4a">AED</small>
                @if(!empty($pr->fixed_price))
                    <ins class="font-size-15 text-decoration-none">{{$pr->fixed_price}}</ins>
                    <del class="font-size-10 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-2 @else ml-2 @endif text-gray-6">{{$pr->price}}</del>
                @else
                    <ins class="font-size-15 text-decoration-none">{{$pr->price}}</ins>
                @endif
            </div>
        </div>
    </div>
</li>