<li class="col d-none d-xl-block text-center"><a href="{{url('/wishlist')}}" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="Favorites"><i class="font-size-22 ec ec-favorites"></i><br /><span class="font-size-1" >{{__('jbq.Wish List')}}</span></a></li>
<li class="col d-xl-none px-2 px-sm-3"><a href="{{url('/user/profile')}}" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="My Account"><i class="font-size-22 ec ec-user"></i></a></li>
<li class="col pr-xl-0 px-2 px-sm-3 d-xl-none">
    <a href="{{url('cart')}}" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Cart">
        <i class="font-size-22 ec ec-shopping-bag"></i>
        <span class="cartQty bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12">{{Cart::instance('shopping')->content()->count()}}</span>
        <span class="cartSubtotal d-none d-xl-block font-weight-bold font-size-16 text-gray-90 ml-3">${{Cart::instance('shopping')->subtotal()}}</span>
    </a>
</li>
<li class="col pr-xl-0 px-2 px-sm-3 d-none d-xl-block">
    <div id="basicDropdownHoverInvoker" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Cart"
         aria-controls="basicDropdownHover"
         aria-haspopup="true"
         aria-expanded="false"
         data-unfold-event="click"
         data-unfold-target="#basicDropdownHover"
         data-unfold-type="css-animation"
         data-unfold-duration="300"
         data-unfold-delay="300"
         data-unfold-hide-on-scroll="true"
         data-unfold-animation-in="slideInUp"
         data-unfold-animation-out="fadeOut">
        <i class="font-size-22 ec ec-shopping-bag"></i>
        <span class="cartQty bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle @if(App::getLocale()=='ar') right-12 @else left-12 @endif top-8 font-weight-bold font-size-12">{{Cart::instance('shopping')->content()->count()}}</span>
        <span class="cartSubtotal d-none d-xl-block font-weight-bold font-size-16 text-gray-90 @if(App::getLocale()=='ar') mr-3 @else ml-3 @endif">{{Cart::instance('shopping')->subtotal()}}</span>
    </div>
    <div id="basicDropdownHover" class="cart-dropdown dropdown-menu dropdown-unfold border-top border-top-primary mt-3 border-width-2 border-left-0 border-right-0 border-bottom-0 @if(App::getLocale()=='ar') right-auto left-0 @else left-auto right-0 @endif" aria-labelledby="basicDropdownHoverInvoker">
        <ul id="basketItemsList" class="list-unstyled px-3 pt-3">
            @foreach(\App\Http\Controllers\CartController::getCartProducts() as $cartPr)
                <li data-id="{{$cartPr['rowId']}}" class="border-bottom pb-3 mb-3">
                    <div> <ul class="list-unstyled row mx-n2"><li class="px-2 col-auto"><img width="100" class="img-fluid" src="{{$cartPr['image']}}"></li>
                            <li class="px-2 col">
                                <h5 class="text-blue font-size-14 font-weight-bold">{{$cartPr['title']}}</h5><span class="font-size-14">{{$cartPr['qty']}} x {{$cartPr['price']}}</span></li>
                            <li class="px-2 col-auto">
                                <a href="#" onclick="removeFromBasket('{{$cartPr['rowId']}}')" class="text-gray-90"><i class="ec ec-close-remove"></i></a>
                            </li></ul></div></li>
            @endforeach
        </ul>
        <div class="flex-center-between px-4 pt-2">
            <a href="{{url('/cart')}}" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5">{{__('jbq.View cart')}}</a>
            <a href="{{url('/checkout')}}" class="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5">{{__('jbq.Checkout')}}</a>
        </div>
    </div>
</li>