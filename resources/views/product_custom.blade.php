@extends('header')
@section('content')
    <style>
        .product-thumb{
            width:100%;
            height:100px;
            overflow:hidden;
            margin-bottom:10px;
        }
        .header-bar{
            border-bottom:solid 1px #efefef;
        }
        .sizes-wrapper .row{
            display:none;
        }
        .sizes-wrapper .row:first-child{
            display:block;
        }
        .sizes-wrapper{
            max-height:90vh;
            overflow-y:scroll;
        }

        .product-count{
            border:solid 1px #efefef;
            border-radius: 30px;
            padding:10px 30px;
            margin-left:10px;
        }
        .product-count-btn{
            cursor:pointer;
            color: #656565;
            transform: scale(1);
            user-select: none;
        }
        .product-count-btn:hover{
            color:#000;
            transform: scale(1.2);
        }
        .scaleUp{
            transform: scale(1.5)!important;
        }
        .btn-orange{
            background:#FBB03B;
            padding:15px 30px;
            color:#000;
            border-radius: 30px;
            font-size:0.7rem;
            border:0;
        }
        .btn-orange:hover{
            background: #ffc015;
        }
        .btn-outline-orange{
            padding:15px 30px;
            color:#000;
            border-radius: 30px;
            font-size:0.7rem;
            border:solid 1px #FBB03B;
        }
        .btn-outline-orange:hover{
            background:#FBB03B;
        }
        .custom-item{
            border-radius: 10px;
            width: 50px;
            height:50px;
            overflow:hidden;
            cursor:pointer;
            opacity:0.7;
        }
        .custom-item:hover{
            opacity:0.9;
        }
        .custom-item.selected{
            opacity:1;
        }
        .custom-img{
            position:absolute;
            left:0;
            top:0;
            z-index:99;
        }
        .custom-img img{
            width:100%;
        }
        .product-img-wrapper{
            position:relative;
        }
    </style>


    <div id="sizeHelpModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">كيفية اختيار القياس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="{{asset('images/help.jpeg')}}" width="100%" />
                </div>
            </div>
        </div>
    </div>


    <section class="container " style="padding-top:100px;border-top:solid 1px #efefef;position:relative">
        <div class="row d-flex justify-content-between mt-5">
            <div class="ml-2">
                <ion-icon  style="opacity:0.5" class="ml-2" name="home-outline"></ion-icon>
                <span  style="opacity:0.5" class="ml-2 mr-2">/</span>

                <span  style="opacity:0.5" class="ml-2">
                        {{$product->category->title}}
                        </span>

                <span style="opacity:0.5" class="ml-2">/
                </span>
                <span style="opacity:0.5" class="ml-2">
                    {{$product->title}}
                </span>
                <span style="opacity:0.5" class="ml-2">/
                </span>
<span>
                التخصيص
</span>
            </div>

        </div>
        <div class="row mt-5" data-sticky-container>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="product-img-wrapper sticky" data-margin-top="20">
                            <div >

                                <img alt="{{$product->title}}" src="{{url('product')}}/1000x1000/{{@$product->image->file_name}}" width="100%"/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7 text-right">
                <div class="row mb-5">
                    <div class="col-12">
                        <h3>
                            {{$product->title}}
                        </h3>
                        <span style="color:red">
                            {{$product->price}} درهم
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">

                            @foreach(\App\Models\CustomCategory::orderBy('order','desc')->get() as $ps)
                                <div class="row mb-5">
                                    <div class="col-12">
                                        <h6 style="margin:0">
                                            {{$ps->title}}
                                        </h6>
                                        <span style="font-size:0.6rem">
                                            {{$ps->desc}}
                                        </span>
                                        <div class="row d-flex">
                                            @foreach(\App\Models\CustomItem::where('custom_category_id',$ps->id)->get() as $cp)

                                                @foreach(\App\Models\CustomSubItem::where('custom_item_id',$cp->id)->where('product_id',$product->id)->get() as $cp2)
                                                    <div class="ml-2 custom-item" data-cat-id="{{$ps->id}}" data-index="{{$ps->order}}" data-main-pic="{{asset('upload')}}/images/sub-items/{{$cp2->main_pic}}">
                                                        <img width="100%" src="{{asset('upload')}}/images/sub-items/{{$cp2->stack_pic}}" />

                                                    </div>
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                    </div>

                </div>
                <div class="product-footer-buttons d-flex row">
                    <div class="product-count d-flex">
                        <span class="product-count-btn product-count-plus m-1 ml-2" style="height:16px;">
                            <ion-icon name="add-outline"></ion-icon>
                        </span>
                        <span class="product-count-total m-1" style="height:16px;">
                            1
                        </span>
                        <span class="product-count-btn product-count-minus m-1 mr-2" style="height:16px;">
                            <ion-icon name="remove-outline"></ion-icon>
                        </span>
                    </div>

                    <div class="ml-3">
                        <button class="btn btn-orange">
                            أضف إلى السلة
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('script')

    <script>
        var total=1;
        function showMoreSizes(){
            $('.sizes-wrapper .row').show();
            $('#show-more-size').hide();
            $('#show-less-size').show();
        }
        function showLessSizes(){
            $('#show-more-size').show();
            $('#show-less-size').hide();
            $('.sizes-wrapper .row:not(:first-child)').hide();
        }
        var easyzoom = null;
        $(document).ready(function(){
            $('.custom-item').on('click',function(){

                $(this).addClass('selected');
                var catId = $(this).data('cat-id');
                var mainPic = $(this).data('main-pic');
                $('.custom-item[data-cat-id='+catId+']').removeClass('selected');
                $(this).addClass('selected');
                $('.custom-img[data-cat-id='+catId+']').remove();
                $('.product-img-wrapper').append('<div data-cat-id="'+catId+'" class="custom-img" style="z-index:'+$(this).data('index')+'"><img src="'+mainPic+'" /></div>');
            });
            $('.product-next').click(function() {
                $('.owl-carousel-product').trigger('next.owl.carousel');
            });

            $('.product-prev').click(function() {
                $('.owl-carousel-product').trigger('prev.owl.carousel');
            });

            $('.product-count-btn').on('click',function(){
                if($(this).hasClass('product-count-plus')){
                    total++;

                } else {
                    if(total-1>0)
                        total--;
                }

                $('.product-count-total').html(total);
            });

            $('.product-count-btn').on('mousedown',function(){
                $(this).addClass('scaleUp');
            });
            $('.product-count-btn').on('mouseup',function(){
                $(this).removeClass('scaleUp');
            });
            $('.product-thumb-change').on('click',function(){
                console.log('items');
                var fileName = $(this).data('file-name');
                $('.product-img-wrapper a').attr('href','{{url('product')}}/2000x2000/'+fileName);
                $('.product-img-wrapper img').attr('src','{{url('product')}}/1000x1000/'+fileName);

                easyzoom = $('.easyzoom').swap('{{url('product')}}/1000x1000/'+fileName,'{{url('product')}}/2000x2000/'+fileName);


            });
            easyzoom = $('.easyzoom').easyZoom();

            $('.product-dec-slider').owlCarousel({
                loop: true,
                autoplay: false,
                autoplayTimeout: 5000,
                navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
                nav: true,
                item: 4,
                margin: 12,
                responsive: {
                    0: {
                        items: 2
                    },
                    768: {
                        items: 4
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        })

    </script>
@endsection
