@extends('header')
@section('content')


    @include('index_slider')

    <div class="container">
        @include('index_categories')

        @include('deals_and_tabs')
    </div>

    @include('index_category_products')
    @include('index_products')


@endsection
@section('script')
    <script>
        $(window).on('load', function () {
            // initialization of HSMegaMenu component
            // initialization of svg injector module
            // $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
        });

       $(function () {

            // initialization of header
            $.HSCore.components.HSHeader.init($('#header'));

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });


            // initialization of slick carousel
            $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel',{
                autoplay:true
            });


            // initialization of hamburgers
            $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                beforeClose: function () {
                    $('#hamburgerTrigger').removeClass('is-active');
                },
                afterClose: function() {
                    $('#headerSidebarList .collapse.show').collapse('hide');
                }
            });

            $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                e.preventDefault();

                var target = $(this).data('target');

                if($(this).attr('aria-expanded') === "true") {
                    $(target).collapse('hide');
                } else {
                    $(target).collapse('show');
                }
            });

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

        });
    </script>
@endsection
