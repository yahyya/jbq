@extends('header',['dontShowBasket'=>true])
@section('content')

    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url('/')}}">{{__('jbq.Home')}}</a></li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                                {{__('jbq.Dashboard')}}
                            </li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
           <div class="row mb-8">

               <div class="d-none d-xl-block col-xl-3 col-wd-2gdot5">
                   <ul class="list-unstyled mb-0 sidebar-navbar">
                       <li @if(\Illuminate\Support\Facades\App::getLocale()=='ar') class="text-right" @endif>
                           <a class="dropdown-current active" href="{{route('home')}}">
                               {{__('jbq.orders')}}
                           </a>
                       </li>

                       @if(auth()->user()->type=='1')
                           <li>
                               <a class="dropdown-current" href="{{url('user/products')}}">
                                   {{__('jbq.products')}}
                               </a>
                           </li>
                       @endif
                       <li>
                           <a class="dropdown-current" href="{{route('logout')}}" >
                               {{__('jbq.logout')}}
                           </a>
                       </li>
                   </ul>
               </div>
               <div class="col-xl-9 col-wd-9gdot5">
                   <div class="row">
                       <div class="col-12">
                           <table id="orders-table"  class="table table-striped table-bordered pt-3" >
                               <thead>
                               <tr>
                                   <td>
                                       #
                                   </td>
                                   <td>
                                      {{__('jbq.total')}}
                                   </td>
                                   <td>
                                       {{__('jbq.discount')}}
                                   </td>
                                   <td>
                                       {{__('jbq.paid')}}
                                   </td>
                                   <td>
                                       {{__('jbq.date')}}
                                   </td>
                                   <td>
                                       {{__('jbq.details')}}
                                   </td>

                               </tr>
                               </thead>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </main>
@endsection
@section('script')
    <link href="{{ asset('js/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            orders = $('#orders-table').DataTable({
                processing: true,
                serverSide: true,
                dom: 'lBfrtip',
                responsive: true,
                autowidth: false,
                buttons: [
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o fa-fw"></i> خروجی اکسل',
                        exportOptions: {
                            modifier: {
                                page: 'current'
                            }
                        }
                    }
                ],
                ajax: '{{route('user.orders.list',auth()->user()->id)}}',
                "order": [[4, "asc"]],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'amount', name: 'total_amount'},
                    {data: 'discount_price', name: 'discount_price'},
                    {data: 'total', name: 'total'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action',orderable:false,searchable:false},

                ]
            });

        })
    </script>
@endsection
