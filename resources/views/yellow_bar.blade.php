<div class="d-none d-xl-block bg-primary">
    <div class="container">
        <div class="row align-items-stretch min-height-50">
            <!-- Vertical Menu -->
            <div class="col-md-auto d-none d-xl-flex align-items-end">
                <div class="max-width-270 min-width-270">
                    <!-- Basics Accordion -->
                    <div id="basicsAccordion">
                        <!-- Card -->
                        <div class="card border-0 rounded-0">
                            <div  class="@if(\Illuminate\Support\Facades\App::getLocale()=='ar') flex-row-reverse @endif card-header bg-primary rounded-0 card-collapse border-0" id="basicsHeadingOne">
                                <button type="button" class="btn-link btn-remove-focus btn-block d-flex card-btn py-3 text-lh-1 px-4 shadow-none btn-primary rounded-top-lg border-0 font-weight-bold text-gray-90"
                                        data-toggle="collapse"
                                        data-target="#basicsCollapseOne"
                                        aria-expanded="true"
                                        aria-controls="basicsCollapseOne">
                                    <span class="@if(\Illuminate\Support\Facades\App::getLocale()=='ar') pr-1 @else pl-1 @endif text-gray-90">{{__('jbq.Shop By Category')}}</span>
                                    <span class="text-gray-90 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-3 @else ml-3 @endif ">
                                                        <span class="ec ec-arrow-down-search"></span>
                                                    </span>
                                </button>
                            </div>
                            <div id="basicsCollapseOne" class="collapse vertical-menu v1"
                                 aria-labelledby="basicsHeadingOne"
                                 data-parent="#basicsAccordion">
                                <div class="card-body p-0">
                                    <nav class="js-mega-menu navbar navbar-expand-xl u-header__navbar u-header__navbar--no-space hs-menu-initialized">
                                        <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                            <ul class="navbar-nav u-header__navbar-nav">

                                                @foreach(\App\Models\Category::where('show_sidebar',1)->get() as $cat)
                                                    <li class="nav-item u-header__nav-item"
                                                        data-event="hover"
                                                        data-position="left">
                                                        <a href="{{url('category/')}}/{{$cat->url}}" class="nav-link u-header__nav-link">
                                                            @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                                {{$cat->title_ar}}
                                                            @else
                                                                {{$cat->title_en}}
                                                            @endif
                                                        </a>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                    <!-- End Basics Accordion -->
                </div>
            </div>
            <!-- End Vertical Menu -->
            <!-- Search bar -->
            <div class="col align-self-center">
                <!-- Search-Form -->
                <form class="js-focus-state" method="get" action="{{url('search')}}" >
                    <label class="sr-only" for="searchProduct">{{__('jbq.Search')}}</label>
                    <div class="input-group">
                        <input type="text" class="form-control py-2 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') pr-5 @else pl-5 @endif font-size-15 border-0 height-40 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') rounded-right-pill @else rounded-left-pill @endif" name="txt" id="searchProduct" placeholder="{{__('jbq.Search for Products')}}" aria-label="{{__('jbq.Search for Products')}}" aria-describedby="searchProduct1" required>
                        <div class="input-group-append">

                            <button class="btn btn-dark height-40 py-2 px-3 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') rounded-left-pill @else rounded-right-pill @endif" type="submit" id="searchProduct1">
                                <span class="ec ec-search font-size-24"></span>
                            </button>
                        </div>
                    </div>
                </form>
                <!-- End Search-Form -->
            </div>
            <!-- End Search bar -->
            <!-- Header Icons -->
            <div class="col-md-auto align-self-center">
                <div class="d-flex">
                    <ul class="d-flex list-unstyled mb-0">
                        <li class="col d-none d-xl-block text-center"><a href="{{url('/wishlist')}}" class="text-gray-90" data-toggle="tooltip" data-placement="top" title="{{__('jbq.Favorites')}}"><i class="font-size-22 ec ec-favorites"></i><br /><span class="font-size-1" >{{__('jbq.Wish List')}}</span></a></li>
                        <li class="col pr-xl-0 px-2 px-sm-3 d-xl-none">
                            <a href="{{url('cart')}}" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Cart">
                                <i class="font-size-22 ec ec-shopping-bag"></i>
                                <span class="cartQty bg-lg-down-black width-22 height-22 bg-white position-absolute d-flex align-items-center justify-content-center rounded-circle @if(\Illuminate\Support\Facades\App::getLocale()=='ar') right-12 @else left-12 @endif top-8 font-weight-bold font-size-12">{{Cart::instance('shopping')->content()->count()}}</span>
                                <span class="cartSubtotal d-none d-xl-block font-weight-bold font-size-16 text-gray-90 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-3 @else ml-3 @endif">{{Cart::instance('shopping')->subtotal()}}</span>
                            </a>
                        </li>
                        <li class="col pr-xl-0 px-2 px-sm-3 d-none d-xl-block">
                            <div id="basicDropdownHoverInvoker" class="text-gray-90 position-relative d-flex " data-toggle="tooltip" data-placement="top" title="Cart"
                                 aria-controls="basicDropdownHover"
                                 aria-haspopup="true"
                                 aria-expanded="false"
                                 data-unfold-event="click"
                                 data-unfold-target="#basicDropdownHover"
                                 data-unfold-type="css-animation"
                                 data-unfold-duration="300"
                                 data-unfold-delay="300"
                                 data-unfold-hide-on-scroll="true"
                                 data-unfold-animation-in="slideInUp"
                                 data-unfold-animation-out="fadeOut">
                                <i class="font-size-22 ec ec-shopping-bag"></i>
                                <span class="cartQty bg-lg-down-black width-22 height-22 bg-white position-absolute d-flex align-items-center justify-content-center rounded-circle @if(\Illuminate\Support\Facades\App::getLocale()=='ar') right-12 @else left-12 @endif top-8 font-weight-bold font-size-12">{{Cart::instance('shopping')->content()->count()}}</span>
                                <span class="cartSubtotal d-none d-xl-block font-weight-bold font-size-16 text-gray-90 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-3 @else ml-3 @endif">{{Cart::instance('shopping')->subtotal()}}</span>
                            </div>
                            <div id="basicDropdownHover" class="cart-dropdown dropdown-menu dropdown-unfold border-top border-top-primary mt-3 border-width-2 border-left-0 border-right-0 border-bottom-0 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') right-auto left-0 @else left-auto right-0 @endif" aria-labelledby="basicDropdownHoverInvoker">
                                <ul id="basketItemsList" class="list-unstyled px-3 pt-3">
                                    @foreach(\App\Http\Controllers\CartController::getCartProducts() as $cartPr)
                                        <li data-id="{{$cartPr['rowId']}}" class="border-bottom pb-3 mb-3">
                                            <div> <ul class="list-unstyled row mx-n2"><li class="px-2 col-auto"><img width="100" class="img-fluid" src="{{$cartPr['image']}}"></li>
                                                    <li class="px-2 col">
                                                        <h5 class="text-blue font-size-14 font-weight-bold">{{$cartPr['title']}}</h5><span class="font-size-14">{{$cartPr['qty']}} x {{$cartPr['price']}}</span></li>
                                                    <li class="px-2 col-auto">
                                                        <a href="#" onclick="removeFromBasket('{{$cartPr['rowId']}}')" class="text-gray-90"><i class="ec ec-close-remove"></i></a>
                                                    </li></ul></div></li>
                                    @endforeach
                                </ul>
                                <div class="flex-center-between px-4 pt-2">
                                    <a href="{{url('/cart')}}" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5">{{__('jbq.View cart')}}</a>
                                    <a href="{{url('/checkout')}}" class="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5">{{__('jbq.Checkout')}}</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- End Header Icons -->
        </div>
    </div>
</div>