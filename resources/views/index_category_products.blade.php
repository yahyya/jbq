<div class="products-group-4-1-4 space-1 bg-gray-7">
    <div class="container">
        <!-- Nav Classic -->
        <div class="position-relative text-center z-index-2 mb-3">
            <ul class="nav nav-classic nav-tab nav-tab-sm px-md-3 justify-content-start justify-content-lg-center flex-nowrap flex-lg-wrap overflow-auto overflow-lg-visble border-md-down-bottom-0 pb-1 pb-lg-0 mb-n1 mb-lg-0" id="pills-tab-1" role="tablist">
                @foreach(\App\Models\Category::all() as $cat)
                <li class="nav-item flex-shrink-0 flex-lg-shrink-1">
                    <a class="nav-link @if($loop->iteration=='1') active @endif " id="Tpills-example{{$cat->id}}-tab" data-toggle="pill" href="#Tpills-example{{$cat->id}}" role="tab" aria-controls="Tpills-example{{$cat->id}}" @if($loop->iteration=='1') aria-selected="true" @endif>
                        <div class="d-md-flex justify-content-md-center align-items-md-center">
                            @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                {{$cat->title_ar}}
                            @else
                                {{$cat->title_en}}
                            @endif
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        <!-- End Nav Classic -->

        <div class="tab-content" id="Tpills-tabContent">
            @foreach(\App\Models\Category::where('show_index',1)->where('active',1)->get() as $cat)
                <?php

                    $theCenterProduct = $cat->products()->first();


                ?>

                <div class="tab-pane fade pt-2 @if($loop->iteration=='1') show active @endif" id="Tpills-example{{$cat->id}}" role="tabpanel" aria-labelledby="Tpills-example{{$cat->id}}-tab">
                <div class="row no-gutters">
                    <div class="col-md-12 col-wd-12 d-md-flex d-wd-block">
                        <ul class="row list-unstyled products-group no-gutters mb-0">
                            @foreach($cat->products()->take(12)->get() as $pr)
                               @include('product_box_tabs',['pr'=>$pr])
                            @endforeach

                        </ul>
                    </div>

                </div>
            </div>
            @endforeach

        </div>
    </div>

    <!-- End Features Section -->
</div>