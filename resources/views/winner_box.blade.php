@if($winner->user)
<li class="@if(@$classes) {{$classes}} @else col-md-3 product-item product-item__card pb-2 mb-2 pb-md-0 mb-md-0 border-bottom border-md-bottom-0 @endif">
    <div class="product-item__outer " >
        <div class="product-item__inner row no-gutters">
            <div class="col col-lg-auto product-media-left p-2" style="background: #fff;height:400px;overflow:hidden">
                    <img class="img-fluid" style="height: 40%;
    margin: 0 auto;" src="{{@$winner->product->image->file_name}}" alt="@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                    {{$winner->product->title_ar}}
                    @else
                    {{$winner->product->title_en}}
                    @endif">

                <div style="position:relative;margin:-50px auto;text-align: center">
                        <img style="margin:0 auto" src="{{asset('images/user_mask.svg')}}" />
                                            <h5 style="color:#d5b549;font-weight: bold">
                                                Congratulations
                                            </h5>
                        <h4>
                            {{@$winner->user->name}}
                        </h4>
                        <p class="mb-0">
                            On Winning
                        </p>
                        <h5 class="pb-5">
                            2021 land cruiser prado
                        </h5>
                </div>
            </div>

        </div>
    </div>
</li>
@endif