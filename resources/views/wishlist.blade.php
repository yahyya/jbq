@extends('header',['dontShowBasket'=>true])
@section('content')

    @extends('header',['dontShowBasket'=>true])
@section('content')

    <main id="content" role="main" class="cart-page">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url('/')}}">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">{{__('Wishlist')}}</li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
            <div class="my-6">
                <h1 class="text-center">{{__('jbq.My wishlist')}}</h1>
            </div>
            <div class="mb-16 wishlist-table">
                @if(empty($products))
                    <div >
                        <h3>
                            {{__('jbq.empty_wishlist')}}
                        </h3>
                    </div>
                @else
                    <form class="mb-4" action="#" method="post">
                        <div class="table-responsive">
                            <table class="table" cellspacing="0">
                                <thead>
                                <tr>
                                    <th class="product-remove">&nbsp;</th>
                                    <th class="product-thumbnail">&nbsp;</th>
                                    <th class="product-name">{{__('jbq.Product')}}</th>
                                    <th class="product-price">{{__('jbq.Unit Price')}}</th>
                                    <th class="product-subtotal min-width-200-md-lg">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $pr)
                                    <?php
                                        $theProduct = \App\Models\Product::find($pr['id']);
                                    ?>
                                <tr id="w-{{$pr['rowId']}}">
                                    <td class="text-center">
                                        <a href="#" onclick="removeWishList( '{{$pr['rowId']}}')" class="text-gray-32 font-size-26">×</a>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <a href="{{url('/product')}}/{{$theProduct->url}}">
                                            <img class="img-fluid max-width-100 p-1 border border-color-1" src="{{$pr['image']}}" ></a>
                                    </td>

                                    <td data-title="Product">
                                        <a href="{{url('/product')}}/{{$theProduct->url}}" class="text-gray-90">{{$pr['title']}}</a>
                                    </td>

                                    <td data-title="Unit Price">
                                        <span class="">{{$pr['price']}}</span>
                                    </td>

                                    <td>
                                        <button onclick="addToCart({{$pr['id']}})" type="button" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">{{__('Add to Cart')}}</button>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </main>
@endsection

@section('script')
    <script>
        $(window).on('load', function () {
            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                direction: 'horizontal',
                pageContainer: $('.container'),
                breakpoint: 767.98,
                hideTimeOut: 0
            });

            // initialization of svg injector module
            // $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
        });

        $(function () {
            console.log('ready');
            // initialization of header
            $.HSCore.components.HSHeader.init($('#header'));

            // initialization of animation
            $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of popups
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of countdowns
            var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
                yearsElSelector: '.js-cd-years',
                monthsElSelector: '.js-cd-months',
                daysElSelector: '.js-cd-days',
                hoursElSelector: '.js-cd-hours',
                minutesElSelector: '.js-cd-minutes',
                secondsElSelector: '.js-cd-seconds'
            });


            // initialization of forms
            $.HSCore.components.HSFocusState.init();

            // initialization of form validation
            // $.HSCore.components.HSValidation.init('.js-validate', {
            //     rules: {
            //         confirmPassword: {
            //             equalTo: '#signupPassword'
            //         }
            //     }
            // });

            // initialization of show animations
            $.HSCore.components.HSShowAnimation.init('.js-animation-link');

            // initialization of fancybox
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of slick carousel
            $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of hamburgers
            $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                beforeClose: function () {
                    $('#hamburgerTrigger').removeClass('is-active');
                },
                afterClose: function() {
                    $('#headerSidebarList .collapse.show').collapse('hide');
                }
            });

            $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                e.preventDefault();

                var target = $(this).data('target');

                if($(this).attr('aria-expanded') === "true") {
                    $(target).collapse('hide');
                } else {
                    $(target).collapse('show');
                }
            });

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

            // initialization of select picker
            $.HSCore.components.HSSelectPicker.init('.js-select');
        });

        function removeWishList(id){
            $.ajax(
                {
                    url: '{{url('/wishlist/remove')}}',
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "rowId": id,
                    },
                    success: function (res) {
                        $('#w-'+id).remove();
                    },
                    error: function (xhr, stat) {
                        alert('Error');
                    }
                });
        }
    </script>
@endsection
