@extends('header')

@section('content')
<style>
    #orders-table{
        font-size:12px;
    }
    .bootstrap-tagsinput .tag{
        background:red;
    }
    .bootstrap-tagsinput{
        width:100%;
    }
</style>
    <div class="container-fluid" style="background: #fff;">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                @include('user.side',['user'=>$user])
            </div>
            <div class="col-md-9 col-sm-12" style="box-shadow: 0px 10px 30px 0px rgb(82 63 105 / 8%)">
                <div class="row pt-5 pb-5">
                    <div class="col-12">
                        <h5 class="bd-title">
                            برگشت از فروش
                        </h5>
                    </div>
                    <a href="{{route('user.return.create')}}" >
                        ثبت جدید
                    </a>
                </div>

                <div class="row">
                    <div class="col-12">
                        <table id="orders-table"  class="table table-striped table-bordered pt-3" >
                            <thead>
                            <tr>
                                <td>
                                    #
                                </td>
                                <td>
                                    وضعیت
                                </td>
                                <td>
                                    ساعت و تاریخ
                                </td>
                                <td>
                                    جزییات
                                </td>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <link href="{{ asset('plugins/custom/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('css/bootstrap-tagsinput.min.js')}}" type="text/javascript"></script>
    <link href="{{ asset('css/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"/>

    <script>
var orders;
function del(id){
    var con = window.confirm('آیا مطمین هستید');
    if(!con) return;

    $.ajax(
        {
            url: '{{\Illuminate\Support\Facades\URL::to('/user/returns/')}}/' + id,
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
            type:'DELETE',
            dataType: "json",
            // contentType: "application/json; charset=utf-8",
            success:function (res) {
                if(res.res) {
                    orders.draw();
                } else {
                    $('.card-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطا!<br>' + res.message + '</div>');
                }
            },
            error: function (xhr, stat) {

                $('.card-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + (xhr.msg==undefined ? xhr.responseText : xhr.msg) + '</div>');
            }
        });
}
        function saveReturnItem(){
            $.ajax(
                {
                    url: '{{route('order.returns.save')}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type:'post',
                    dataType: "json",
                    data:{items:$('#numbers').val()},
                    success:function (res) {

                        if(res.res) {
                            orders.draw();
                            $('#numbers').tagsinput('removeAll');
                        }
                    },
                    error: function (xhr, stat) {

                        $('.card-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + (xhr.msg==undefined ? xhr.responseText : xhr.msg) + '</div>');
                    }
                });
        }
        $(document).ready(function () {
            orders = $('#orders-table').DataTable({
                "language": {
                    "url": "{{asset('plugins/custom/datatables/Persian.json')}}"
                },
                processing: true,
                serverSide: true,
                dom: 'lBfrtip',
                responsive: true,
                autowidth: false,
                buttons: [
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o fa-fw"></i> خروجی اکسل',
                        exportOptions: {
                            modifier: {
                                page: 'current'
                            }
                        }
                    }
                ],
                ajax: '{{route('user.returns.list',@$user->id)}}',
                "order": [[0, "desc"]],
                columns: [
                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    {data: 'status', name: 'status'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action',orderable:false,searchable:false},

                ]
            });

        })

    </script>
@endsection
