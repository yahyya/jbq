@extends('header',['dontShowBasket'=>true])
@section('content')

    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url('/')}}">{{__('jbq.Home')}}</a></li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                                <a href="{{url('/home')}}">{{__('jbq.orders')}}</a>
                            </li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                               {{__('jbq.View Order')}}
                            </li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="pull-right">
                                Order Details
                            </h2>
                            <div class="align-left">

                            </div>
                            {{--<ul class="header-dropdown m-r--5">--}}
                            {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                            {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                            {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                            {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                            {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="section-title section-title__full mb-0 pb-2 font-size-22 active mr-5">
                                    <a href="#order-details" data-toggle="tab" aria-expanded="false">
                                        Order Info
                                    </a>
                                </li>
                                @if(auth()->user()->type=='1')
                                <li role="presentation" class="section-title section-title__full mb-0 pb-2 font-size-22">
                                    <a href="#order-transactions" data-toggle="tab" aria-expanded="true">
                                        Order Transactions
                                    </a>
                                </li>
                                @endif
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active show" id="order-details">
                                    {{--<fieldset>--}}
                                    {{--<legend>اطلاعات سفارش:</legend>--}}
                                    <div class="row p-5">
                                        <div class="col-sm-12">
                                            @if(auth()->user()->type=='1')
                                                <strong>User : </strong>{{@$order->user->name.' '.@$order->user->family}}</div>
                                            <div class="col-sm-6">
                                                <div class="form-group clearfix">
                                                    <label for="sbStatus"><strong>Order Status : </strong></label>
                                                    <select name="category" id="sbStatus" class="selectpicker show-tick">
                                                        <option value="0" @if($order->status_id == 0) selected @endif><span
                                                                    class="text-muted">Pending</span></option>
                                                        <option value="1" @if($order->status_id == 1) selected @endif><span
                                                                    class="text-warning">Preparing</span></option>
                                                        <option value="2" @if($order->status_id == 2) selected @endif><span
                                                                    class="text-primary">Sent</span></option>
                                                        <option value="3" @if($order->status_id == 3) selected @endif><span
                                                                    class="text-success">Done</span></option>
                                                        <option value="4" @if($order->status_id == 4) selected @endif><span
                                                                    class="text-danger">Canceled</span></option>
                                                        <option value="5" @if($order->status_id == 5) selected @endif><span
                                                                    class="text-danger">Returned</span></option>
                                                    </select>


                                                    <button id="btnStatus" class="btn btn-primary btn-xs" type="button">Save
                                                    </button>
                                                </div>
                                                <div>
                                                    @if($order->status_id=='4')
                                                        Cancelation Reason :
                                                        {{$order->reason}}

                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                            <div class="col-sm-6">
                                                {{--<div class="col-sm-4">--}}
                                                {{--                                        <div class="form-group {{ $errors->has('courier_id') ? ' has-error' : '' }}">--}}
                                                {{--                                            <label for="email_address"><strong>پیک: </strong></label>--}}
                                                {{--                                            --}}{{--<div class="form-line">--}}
                                                {{--                                            <select id="sbCourier" class="selectpicker show-tick"--}}
                                                {{--                                                    data-live-search="true" title="انتخاب پیک..." required>--}}
                                                {{--                                                @foreach(@$couriers as $courier)--}}
                                                {{--                                                    <option value="{{$courier->id}}"--}}
                                                {{--                                                            @if($courier->id == @$order->courier_id) selected @endif>{{$courier->name.' '.$courier->family}}</option>--}}
                                                {{--                                                @endforeach--}}
                                                {{--                                            </select>--}}
                                                {{--                                            @if ($errors->has('courier_id'))--}}
                                                {{--                                                <span class="help-block">--}}
                                                {{--                                                            <strong>{{ $errors->first('courier_id') }}</strong>--}}
                                                {{--                                                        </span>--}}
                                                {{--                                            @endif--}}
                                                {{--                                            <botton id="btnCourier" class="btn btn-primary btn-xs" type="botton">ذخیره--}}
                                                {{--                                            </botton>--}}
                                                {{--                                        </div>--}}
                                                {{--</div>--}}
                                            </div>
                                            <div class="col-sm-6"><strong>Payment Type : </strong>{{$order->fapaymentType}}</div>
                                            <div class="col-sm-6"><strong>Payment Status :</strong>@if(empty($order->paied_at))
                                                    Not Payed
                                                @else
                                                    {{$order->paied_at}}
                                                @endif  </div>
                                            <div class="col-sm-12"><strong>Invoice Number : </strong>{{$order->tracking_code}}</div>
                                            <div class="col-sm-6"><strong>
                                                    Order : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->amount,true )}}
                                            </div>
                                            <div class="col-sm-6"><strong>
                                                    Tax : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->tax_price,true )}}
                                            </div>
                                            <div class="col-sm-6"><strong>
                                                    Delivery : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->delivery_price,true )}}
                                            </div>
                                            <div class="col-sm-6"><strong>
                                                    Discount : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->discount_price,true )}}
                                            </div>
                                            <div class="col-sm-6"><strong>
                                                    Boxing Price: </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->boxing_price,true )}}
                                            </div>
                                            <div class="col-sm-6"><strong>
                                                    Total : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->total,true )}}
                                            </div>
                                            <div class="col-sm-6"><strong>
                                                    Discount Percent : </strong>{{ !empty($order->coupon_id) ? $order->coupon->value : '-' }}
                                            </div>
                                            <div class="col-sm-12"><strong>
                                                    Delivered At :
                                                </strong>{{!empty($order->recieve_date) ? $order->recieve_date : '-'}}
                                            </div>

                                            <div class="col-sm-12"><strong>Desc : </strong><br>{{$order->desc}}</div>

                                    </div>
                                    <div class="row p-5">
                                        @if($order->address)
                                            <h4>
                                                Order Address
                                            </h4>
                                            <ul>
                                                <li>
                                                    {{__('jbq.First name')}} : {{{$order->address->first_name}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Last name')}}  : {{{$order->address->last_name}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Country')}}  : {{{$order->address->country}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.City')}} : {{{$order->address->city}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.State')}} : {{{$order->address->state}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Postcode/Zip')}} : {{{$order->address->postcode}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Phone')}} : {{{$order->address->phone}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Email address')}} : {{{$order->address->email}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Company name')}} : {{{$order->address->company_name}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Street address')}} : {{{$order->address->street_address}}}
                                                </li>
                                            </ul>
                                        @endif
                                    </div>
                                    <div class="row p-5">
                                        @if($order->shipping_address_id)
                                            <h4>
                                                Order Shipping Address
                                            </h4>
                                            <ul>
                                                <li>
                                                    {{__('jbq.First name')}} : {{{$order->shippingAddress->first_name}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Last name')}}  : {{{$order->shippingAddress->last_name}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Country')}}  : {{{$order->shippingAddress->country}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.City')}} : {{{$order->shippingAddress->city}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.State')}} : {{{$order->shippingAddress->state}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Postcode/Zip')}} : {{{$order->shippingAddress->postcode}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Phone')}} : {{{$order->shippingAddress->phone}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Email address')}} : {{{$order->shippingAddress->email}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Company name')}} : {{{$order->shippingAddress->company_name}}}
                                                </li>
                                                <li>
                                                    {{__('jbq.Street address')}} : {{{$order->shippingAddress->street_address}}}
                                                </li>
                                            </ul>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table id="orderItems" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>Title</th>
                                                        <th>Count</th>
                                                        <th>Price</th>
                                                    </tr>
                                                    </thead>
                                                    <tfoot>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>Title</th>
                                                        <th>Count</th>
                                                        <th>Price</th>
                                                    </tr>
                                                    </tfoot>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="order-rates">
                                    <div class="table-responsive">
                                        <table id="ratesDatatable" class="table table-bordered table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Date</th>
                                                <th>User</th>
                                                {{--<th>کد سفارش</th>--}}
                                                <th>Delivery</th>
                                                <th>Quality</th>
                                                <th>Size</th>
                                                <th>Deliver Time</th>
                                                <th>Desc.</th>
                                                {{--<th>عملیات</th>--}}
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>id</th>
                                                <th>Date</th>
                                                <th>User</th>
                                                {{--<th>کد سفارش</th>--}}
                                                <th>Delivery</th>
                                                <th>Quality</th>
                                                <th>Size</th>
                                                <th>Deliver Time</th>
                                                <th>Desc.</th>
                                                {{--<th>عملیات</th>--}}
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="order-transactions">
                                    <div class="table-responsive">
                                        <table id="transactionsDatatable"
                                               class="table table-bordered table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Facture </th>
                                                <th>Port</th>
                                                <th>Price</th>
                                                <th>Recieved Code</th>
                                                <th>Refrence Code</th>
                                                <th>Card</th>
                                                <th>ip</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>id</th>
                                                <th>Facture </th>
                                                <th>Port</th>
                                                <th>Price</th>
                                                <th>Recieved Code</th>
                                                <th>Refrence Code</th>
                                                <th>Card</th>
                                                <th>ip</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('script')
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
    <link href="{{ asset('js/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script>
        Dropzone.autoDiscover = false;

        var fileName = [];
        var breads = [];
        $(function () {
//            $('#form').parsley({successClass: '', errorClass: 'alert-error'}).on('field:validated', function () {
//                var ok = $('.parsley-error').length === 0;
//                $('.info').toggleClass('hidden', !ok);
//                $('.warning').toggleClass('hidden', ok);
//            })
//                .on('form:submit', function () {
//                    return false; // Don't submit form for this demo
//                });
            $('#form').on('form:submit',function () {
                return false;
            });

            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                saveItem();
            });

            $("#fileUpload").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 3,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = responseText ;
                        fileName.push(t.name);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $('.checkbox-bread:checked').each(function(i){
                breads[i] = $(this).val();
            });
            $.ajax(
                {
                    url: '@if(@$product){{route('user.products.update',$product->id)}} @else {{route('user.products.store')}}@endif',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: @if(@$product)'put'@else'post' @endif,
                    dataType: "JSON",
                    data: {
                        @if(@$product)"id": {{$product->id}},@endif
                        "_method": 'post',
                        "title_en": $('#tbTitleEn').val(),
                        "title_ar": $('#tbTitleAr').val(),
                        "total": $('#tbTotal').val(),
                        "category_id": $('#sbCategory').val(),
                        "price": $('#tbPrice').val(),
                        "fixed_price": $('#tbfixedPrice').val(),
//                        "order": $('#tbOrder').val(),
                        "desc_en": $('#taDescEn').val(),
                        "desc_ar": $('#taDescAr').val(),
                        "active": ($('#chbActive:checked').length)? 1 : 0,
                        "images": fileName,
                        "breads": breads,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$product) Item Edited @else Item Added @endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            window.location = '{{url('user/products')}}';
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error<br>' + res.msg + '</div>');
                    }
                });
        }

        tableOrderItems = $('#orderItems').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Persian.json"
            },
            pageLength: 100,
            responsive: true,
            processing: true,
            serverSide: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: '{{route('user.order.items',  $order->id)}}',
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                method: 'GET'
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'product_id', name: 'product_id'},
                {data: 'count', name: 'count'},
                {data: 'price', name: 'price'},
            ]
        });

        $('#transactionsDatatable').DataTable({
            language: {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Persian.json"
            },
            responsive: true,
            processing: true,
            serverSide: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: '{{route('user.orders.transactions', $order->id)}}',
                method: 'GET'
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'factor_id', name: 'factor_id'},
                {data: 'port', name: 'port'},
                {data: 'price', name: 'price'},
                {data: 'ref_id', name: 'ref_id'},
                {data: 'tracking_code', name: 'tracking_code'},
                {data: 'card_number', name: 'card_number'},
                {data: 'ip', name: 'ip'},
                {data: 'status', name: 'status'},
                {data: 'payment_date', name: 'payment_date', orderable: false, searchable: false}
            ]
        });

        function setStatus() {
            $('#btnStatus').attr('disabled', 'disabled');
            var reason = '';
            if ($('#sbStatus').val()=='4'){
                reason = prompt('Cancelation Reason');
                if (reason ==''){
                    return;
                }
            }
            $.ajax(
                {
                    url: '{{route('admin.orders.set.status',$order->id)}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "id": {{$order->id}},
                        "_method": 'post',
                        "status_id": $('#sbStatus').val(),
                        "notification": $('#cbSendNotify').is(':checked'),
                        "sms": $('#cbSendSms').is(':checked'),
                        'reason':reason
                    },
                    success: function (res) {
                        $('#btnStatus').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Order Status Updated!</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function () {
                            $('.alert').fadeOut('slow', function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('#btnStatus').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }

        function deletePic(id) {
            var confirm = window.confirm('Are You Sure ?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{@url('user/products/')}}/pic/'+id+'/delete',
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                location.reload();
                            }
                            $('.product-pics').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.product-pics').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>
@endsection
