@extends('header',['dontShowBasket'=>true])
@section('content')

    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url('/')}}">{{__('jbq.Home')}}</a></li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                                {{__('jbq.Dashboard')}}
                            </li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
            <div class="row mb-8">

                <div class="d-none d-xl-block col-xl-3 col-wd-2gdot5">
                    <ul class="list-unstyled mb-0 sidebar-navbar">
                        <li>
                            <a class="dropdown-current " href="{{route('home')}}">
                                {{__('jbq.orders')}}
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-current active" href="{{url('user/products')}}">
                                {{__('jbq.products')}}
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-current" href="{{route('logout')}}" >
                                {{__('jbq.logout')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xl-9 col-wd-9gdot5 body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="email_address">Title En</label>
                            <div class="form-line">
                                <input type="text" id="tbTitleEn" class="form-control" value="@if(@$product){{$product->title_en}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('title_ar') ? ' has-error' : '' }}">
                            <label for="email_address">Title Ar</label>
                            <div class="form-line">
                                <input type="text" id="tbTitleAr" class="form-control" value="@if(@$product){{$product->title_ar}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="email_address">Category </label>
                            <div class="form-line">
                                <select id="sbCategory" class="form-control " required>
                                    @foreach(@\App\Models\Category::all() as $category)
                                        <option value="{{$category->id}}" @if($category->id == @$product->category_id) selected @endif>{{$category->title_en}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="tbPrice">Price</label>
                            <div class="form-line">
                                <input type="text" id="tbPrice" class="form-control" value="@if(@$product){{$product->price}}@endif" placeholder="" required>
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('fixed_price') ? ' has-error' : '' }}">
                            <label for="tbfixedPrice">Final Price</label>
                            <div class="form-line">
                                <input type="text" id="tbfixedPrice" class="form-control" value="@if(@$product){{$product->fixed_price}}@endif" placeholder="" required>
                                @if ($errors->has('fixed_price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fixed_price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('desc_en') ? ' has-error' : '' }}">
                            <label for="taDesc">Description En</label>
                            <div class="form-line">
                                <textarea rows="4" id="taDescEn" class="form-control no-resize" placeholder="Description ....">@if(@$product){{$product->desc_en}}@endif</textarea>
                                @if ($errors->has('desc_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('desc_ar') ? ' has-error' : '' }}">
                            <label for="taDesc">Description Ar</label>
                            <div class="form-line">
                                <textarea rows="4" id="taDescAr" class="form-control no-resize" placeholder="Description ....">@if(@$product){{$product->desc_ar}}@endif</textarea>
                                @if ($errors->has('desc_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(isset($product->images[0]))
                            <div class="form-group{{ $errors->has('images') ? ' has-error' : '' }}">
                                <label for="email_address"> Image</label>
                                <div class="row product-pics">
                                    @foreach(@$product->images as $image)
                                        <div class="col-xs-6 col-md-3">
                                            <img src="{{$image->file_name}}" class="img-responsive">
                                            <button type="button" onclick="deletePic({{$image->id}})" class="btn btn-danger m-t-15 waves-effect">Remove Image</button>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </form>
                    <form class="dropzone" id="fileUpload"></form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$product)Save @else Save @endif</button>

                </div>
            </div>
        </div>
    </main>
@endsection
@section('script')
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
    <link href="{{ asset('js/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script>
        Dropzone.autoDiscover = false;

        var fileName = [];
        var breads = [];
        $(function () {
//            $('#form').parsley({successClass: '', errorClass: 'alert-error'}).on('field:validated', function () {
//                var ok = $('.parsley-error').length === 0;
//                $('.info').toggleClass('hidden', !ok);
//                $('.warning').toggleClass('hidden', ok);
//            })
//                .on('form:submit', function () {
//                    return false; // Don't submit form for this demo
//                });
            $('#form').on('form:submit',function () {
                return false;
            });

            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                saveItem();
            });

            $("#fileUpload").dropzone({
                url: "{{route('user.fileUpload')}}",
                maxFilesize: 3,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = responseText ;
                        fileName.push(t.name);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $('.checkbox-bread:checked').each(function(i){
                breads[i] = $(this).val();
            });
            $.ajax(
                {
                    url: '@if(@$product){{route('user.products.update',$product->id)}} @else {{route('user.products.store')}}@endif',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: @if(@$product)'put'@else'post' @endif,
                    dataType: "JSON",
                    data: {
                        @if(@$product)"id": {{$product->id}},@endif
                        "_method": 'post',
                        "title_en": $('#tbTitleEn').val(),
                        "title_ar": $('#tbTitleAr').val(),
                        "total": $('#tbTotal').val(),
                        "category_id": $('#sbCategory').val(),
                        "price": $('#tbPrice').val(),
                        "fixed_price": $('#tbfixedPrice').val(),
//                        "order": $('#tbOrder').val(),
                        "desc_en": $('#taDescEn').val(),
                        "desc_ar": $('#taDescAr').val(),
                        "active": ($('#chbActive:checked').length)? 1 : 0,
                        "images": fileName,
                        "breads": breads,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$product) Item Edited @else Item Added @endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            window.location = '{{url('user/products')}}';
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error<br>' + res.msg + '</div>');
                    }
                });
        }

        function deletePic(id) {
            var confirm = window.confirm('Are You Sure ?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{@url('user/products/')}}/pic/'+id+'/delete',
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                location.reload();
                            }
                            $('.product-pics').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.product-pics').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>
@endsection
