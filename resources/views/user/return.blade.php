@extends('header')

@section('content')
<style>
    #orders-table{
        font-size:12px;
    }
</style>
    <div class="container-fluid" style="background: #fff;">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                @include('user.side',['user'=>$user])
            </div>
            <div class="col-md-9 col-sm-12" style="box-shadow: 0px 10px 30px 0px rgb(82 63 105 / 8%)">
                <div class="row pt-5 pb-5">
                    <div class="col-12">
                        <h5 class="bd-title">
                             برگشت از فروش
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label>
                                سریال
                            </label>
                            <input id="tbSerial" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label>
                                محصول
                            </label>
                            <select id="sbProduct" class="form-control">
                                @foreach(\App\Models\Product::all() as $pr)
                                    <option data-wat="{{$pr->wat}}" value="{{$pr->id}}" >
                                        {{$pr->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>
                                نوع
                            </label>
                            <select id="sbType" class="form-control">
                                <option value="0" data-type="1">
                                    آفتابی
                                </option>
                                <option value="1"data-type="0" >
                                    مهتابی
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label>
                                تعداد
                            </label>
                            <input id="tbTotal" type="text" class="form-control" />
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label>
                                توضیحات
                            </label>
                            <input id="tbDesc" type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mb-5">
                        <div class="form-group mt-2">
                            <button class="btn btn-primary" onclick="saveReturnItem();" >
                                افزودن به لیست
                            </button>

                            <button class="btn btn-success" onclick="saveReturn();" >
                                ثبت نهایی برگشت از فروش
                            </button>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-12">
                        <table id="orders-table"  class="table table-striped table-bordered pt-3" >

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

    <link href="{{ asset('plugins/custom/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script>
        var dataSet = @if(@$order)

        [
            @foreach(\App\Models\OrderItems::where('order_id',$order->id)->get() as $oi)
            {
                "serial": "{{$oi->serial}}",
                "productId": {{$oi->product_id}},
                "type": {{$oi->color}},
                "total":{{$oi->total??0}},
                "desc": "{{$oi->desc}}"

            },
            @endforeach
        ];
        @else
            [];
        @endif


        var dataSet2 =
            @if(@$order)
            [

                @foreach(\App\Models\OrderItems::where('order_id',$order->id)->get() as $oi)
                [
                    "{{$oi->serial}}"
                    ,
                    <?php
                        $productName = \App\Models\Product::find($oi->product_id);
                        $productName = @$productName->title;
                    ?>
                    "{{$productName}}"
                    ,
                    "{{  $oi->color==0 ? 'آفتابی' : 'مهتابی' }}"
                    ,
                "{{$oi->total??0}}",
                        "{{$oi->desc}}"
                    ,
                    '<a href="javascript:void(0)"><i style="color:red" onclick="removeItem({{$loop->index}})" class="fa fa-times" ></i></a>'
                    ,
                ],
                @endforeach
            ]
            @else
             [];

             @endif
        var orders = null;
        function saveReturn(){
            $.ajax(
                {
                    url: @if(!@$order)'{{route('order.returns.save')}}'@else '{{route('order.returns.update',$order->id)}}' @endif,
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "json",
                    data: {
                        items:dataSet
                    },
                    // contentType: "application/json; charset=utf-8",
                    success: function (res) {

                        if (res.res) {
                            window.location = "{{url('user/returns')}}";
                            $('.card-body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>با موفقیت ثبت شد</div>');
                        } else {
                            $('.card-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطا!<br>' + res.msg + '</div>');
                        }
                    },
                    error: function (xhr, stat) {

                        $('.card-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + (xhr.msg == undefined ? xhr.responseText : xhr.msg) + '</div>');
                    }
                });
        }

        function saveReturnItem(){
            var str = $('#tbSerial').val();
            var productType = null;
            var productName = null;

            if(str!='') {
                if (str.length < 15) {
                    alert('شماره سریال صحیح نمیباشد');
                    return;
                }
                var wat = parseInt(str.slice(0, 3));
                var type = parseInt(str.slice(3, 4)) - 1;
                productType = $('#sbType option[data-type=' + type + ']').text();
                productName = $('#sbProduct option[data-wat=' + wat + ']').text();
                if (productName == '' || productType == '') {
                    alert('شماره سریال صحیح نمیباشد.');
                    return
                }
                $('#sbType option').removeAttr('selected');
                $('#sbProduct option').removeAttr('selected');
                $('#sbType option[data-type='+type+']').attr('selected','selected');
                $('#sbProduct option[data-wat='+wat+']').attr('selected','selected');
            } else {
                productName = $('#sbProduct option:selected').text();
                productType = $('#sbType option:selected').text();
            }

            setTimeout(function(){

                dataSet.push({
                    "serial":function(){
                        return $('#tbSerial').val()
                    },
                    "productId":function(){
                        return $('#sbProduct').val()
                    },
                    "type":function(){
                        return $('#sbType').val()
                    },
                    "total":function(){
                        return $('#tbTotal').val()
                    },
                    "desc": $('#tbDesc').val()
                    ,
                });
                dataSet2.push([
                    $('#tbSerial').val()
                    ,
                    productName
                    ,
                    productType
                    ,
                    $('#tbTotal').val(),
                    $('#tbDesc').val()
                    ,
                    '<a href="javascript:void(0)"><i style="color:red" onclick="removeItem('+(dataSet2.length)+')" class="fa fa-times" ></i></a>'
                    ,
                ]);
                orders.clear();
                orders.rows.add(dataSet2);
                orders.draw();
                $('#tbSerial,#tbDesc').val('');
            },500);

        }

        function removeItem(num){
            dataSet2.splice(num, 1);
            dataSet.splice(num, 1);
            orders.clear();
            orders.rows.add(dataSet2);
            orders.draw();
        }
        $(document).ready(function () {
            orders = $('#orders-table').DataTable({
                "language": {
                    "url": "{{asset('plugins/custom/datatables/Persian.json')}}"
                },

                dom: 'lBfrtip',
                responsive: true,
                autowidth: false,
                buttons: [
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o fa-fw"></i> خروجی اکسل',
                        exportOptions: {
                            modifier: {
                                page: 'current'
                            }
                        }
                    }
                ],
                data: dataSet2,
                columns: [
                    { title: "سریال" },
                    { title: "محصول" },
                    { title: "نوع" },
                    { title: "تعداد" },
                    { title: "توضیحات" },
                    { title: "عملیات" },
                ]


            });

        })

    </script>
@endsection
