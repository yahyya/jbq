<style>
    .side-items li{
        margin:10px 0;
        padding:10px 0;
        list-style: none;
        border-bottom:solid 1px #efefef;

    }
    .side-items li a{
        color: #435217;
    }
    .side-items li a:hover{
        color: #1a2006;
    }
    li i{
        color: #82a12c;
        margin-left:10px;
    }
</style>
            <div class="row mt-5">
                <div class="col-12 text-center mb-2" style="border-radius: 100%;overflow:hidden">
                    <img width="50%" style="opacity:0.5" src="{{asset('image/male.svg')}}" />
                </div>
                <div class="text-center row mb-2">
                    <div class="col-12">
                        {{$user->name}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="progress" style="height:5px;">
                            <div class="progress-bar" role="progressbar" style="width: 25%;color:#efefef" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-6 text-center" style="font-size:11px;color:#606060">
                        سقف خرید :
                        {{\App\Http\Controllers\Controller::priceFormat($user->min_credit)}}
                    </div>
                    <div class="col-6 text-center" style="font-size:11px;color:#606060">
                        خرید :
                        {{\App\Http\Controllers\Controller::priceFormat($user->buys())}}
                    </div>
                </div>
            </div>


            <div class="mt-5"  >
                <ul class="side-items">
                    <li>

                        <a href="{{route('user.dashboard')}}" class="active">
                            <i class="fas fa-tachometer-alt"></i>
                            داشبورد
                        </a>
                    </li>
                    <li>
                        <a href="{{route('user.orders')}}">
                            <i class="fas fa-shopping-basket"></i>
                            سفارشات من
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{route('user.factures')}}">--}}
{{--                            فاکتور های من--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li>
                        <a href="{{route('user.returns')}}">
                            <i class="fas fa-undo-alt"></i>
                            برگشت از فروش
                        </a>
                    </li>
                    <li>
                        <a href="{{route('user.logout')}}">
                            <i class="fas fa-sign-out-alt"></i>
                            خروج
                        </a>
                    </li>
                </ul>
            </div>

