@extends('header',['dontShowBasket'=>true])
@section('content')

    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url('/')}}">{{__('jbq.Home')}}</a></li>
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                                {{__('jbq.dashboard')}}
                            </li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
            <div class="row">
                <div class="d-none d-xl-block col-xl-3 col-wd-2gdot5">
                    <div class="mb-8 border border-width-2 border-color-3 borders-radius-6" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align:right" @endif>
                        <!-- List -->
                        <ul id="sidebarNav" class="list-unstyled mb-0 sidebar-navbar" >
                            <li>
                                <a class="dropdown-toggle dropdown-toggle-collapse dropdown-title" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav1Collapse" data-target="#sidebarNav1Collapse">
                                    {{__('jbq.Show_All_Categories')}}
                                </a>

                                <div id="sidebarNav1Collapse" class="collapse" data-parent="#sidebarNav">
                                    <ul id="sidebarNav1" class="list-unstyled dropdown-list">
                                        <!-- Menu List -->
                                        @foreach(\App\Models\Category::all() as $theCat)
                                            <li><a class="dropdown-item" href="{{url('/category/')}}/{{$theCat->id}}">
                                                    @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                        {{$theCat->title_ar}}
                                                    @else
                                                        {{$theCat->title_en}}
                                                    @endif
                                                    <span class="text-gray-25 font-size-12 font-weight-normal"> </span></a></li>
                                        @endforeach
                                        <!-- End Menu List -->
                                    </ul>
                                </div>
                            </li>
                            <li>


                            </li>
                        </ul>
                        <!-- End List -->
                    </div>
                    <div class="mb-8">
                        <div class="border-bottom border-color-1 mb-5" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                            <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">{{__('jbq.Latest Products')}}</h3>
                        </div>
                        <ul class="list-unstyled">
                            @foreach(\App\Models\Product::take(5)->get() as $pr)
                            <li class="mb-4">
                                <div class="row" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                                    <div class="col-auto">
                                        <a href="{{url('product')}}/{{$pr->id}}" class="d-block width-75">
                                            <img class="img-fluid" src="{{@$pr->image->file_name}}" alt="@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                            {{$pr->title_ar}}
                                            @else
                                            {{$pr->title_en}}
                                            @endif">
                                        </a>
                                    </div>
                                    <div class="col">
                                        <h3 class="text-lh-1dot2 font-size-14 mb-0"><a href="{{url('product')}}/{{$pr->id}}">
                                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                    {{$pr->title_ar}}
                                                @else
                                                    {{$pr->title_en}}
                                                @endif
                                            </a></h3>
                                        <div class="text-warning text-ls-n2 font-size-16 mb-1" style="width: 80px;">
                                            <small class="fas fa-star"></small>
                                            <small class="fas fa-star"></small>
                                            <small class="fas fa-star"></small>
                                            <small class="fas fa-star"></small>
                                            <small class="far fa-star text-muted"></small>
                                        </div>
                                        <div class="font-weight-bold">
{{--                                            <del class="font-size-11 text-gray-9 d-block">{{$pr->price}}</del>--}}
                                            <ins class="font-size-15 text-red text-decoration-none d-block">{{$pr->price}}</ins>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xl-9 col-wd-9gdot5">
                    <div class="d-flex justify-content-between align-items-center border-bottom border-color-1 flex-lg-nowrap flex-wrap mb-4">
                        <h3 class="section-title section-title__full mb-0 pb-2 font-size-22">
                          {{__('jbq.dashboard')}}
                        </h3>
                    </div>

                    <!-- Best Sellers -->
                    <div class="mb-6 position-relative">
                        <div class="d-flex justify-content-between border-bottom border-color-1 flex-md-nowrap flex-wrap border-sm-bottom-0">
                            <h3 class="section-title section-title__full mb-0 pb-2 font-size-22">{{__('jbq.Best Sellers')}}</h3>
                        </div>
                        <div @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="direction: ltr" @endif class="js-slick-carousel position-static u-slick u-slick--gutters-1 overflow-hidden u-slick-overflow-visble pt-3 pb-3"
                             data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                             data-arrow-left-classes="fa fa-angle-left @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-0 @else right-1 @endif"
                             data-arrow-right-classes="fa fa-angle-right @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-3 @else right-0 @endif"
                             data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-4">

                        </div>
                    </div>
                    <!-- End Best Sellers -->


                    <div class="mb-8 position-relative">
                        <div class="d-flex justify-content-between border-bottom border-color-1 flex-md-nowrap flex-wrap border-sm-bottom-0">
                            <h3 class="section-title section-title__full mb-0 pb-2 font-size-22">{{__('jbq.Top rated in this category')}}</h3>
                        </div>
                        <div  @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="direction: ltr" @endif class="js-slick-carousel position-static u-slick u-slick--gutters-1 overflow-hidden u-slick-overflow-visble pt-3 pb-3"
                             data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                             data-arrow-left-classes="fa fa-angle-left @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-0 @else right-1 @endif"
                             data-arrow-right-classes="fa fa-angle-right @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-3 @else right-0 @endif"
                             data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-4">




                        </div>
                    </div>
                    <!-- End Top rated in this category -->
                </div>
            </div>


            <!-- End Brand Carousel -->
        </div>
    </main>
@endsection
@section('script')
    <script>
        $(window).on('load', function () {
            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                direction: 'horizontal',
                pageContainer: $('.container'),
                breakpoint: 767.98,
                hideTimeOut: 0
            });

            // initialization of svg injector module
            // $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
        });

       $(function () {
            console.log('ready');
            // initialization of header
            $.HSCore.components.HSHeader.init($('#header'));

            // initialization of animation
            $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of popups
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of countdowns
            var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
                yearsElSelector: '.js-cd-years',
                monthsElSelector: '.js-cd-months',
                daysElSelector: '.js-cd-days',
                hoursElSelector: '.js-cd-hours',
                minutesElSelector: '.js-cd-minutes',
                secondsElSelector: '.js-cd-seconds'
            });


            // initialization of forms
            $.HSCore.components.HSFocusState.init();

            // initialization of form validation
            // $.HSCore.components.HSValidation.init('.js-validate', {
            //     rules: {
            //         confirmPassword: {
            //             equalTo: '#signupPassword'
            //         }
            //     }
            // });

            // initialization of show animations
            $.HSCore.components.HSShowAnimation.init('.js-animation-link');

            // initialization of fancybox
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of slick carousel
            $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of hamburgers
            $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                beforeClose: function () {
                    $('#hamburgerTrigger').removeClass('is-active');
                },
                afterClose: function() {
                    $('#headerSidebarList .collapse.show').collapse('hide');
                }
            });

            $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                e.preventDefault();

                var target = $(this).data('target');

                if($(this).attr('aria-expanded') === "true") {
                    $(target).collapse('hide');
                } else {
                    $(target).collapse('show');
                }
            });

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

            // initialization of select picker
            $.HSCore.components.HSSelectPicker.init('.js-select');
        });
    </script>
@endsection
