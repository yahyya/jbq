@extends('header',['dontShowBasket'=>true])
@section('content')

    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url('/')}}">{{__('jbq.Home')}}</a></li>

                            <?php
                                $parents = $cat->getParents($cat->id);
                            ?>
                            @foreach($parents as $catSub)
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                                    <a href="{{url('/category')}}/{{$catSub->url}}">
                                    @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                        {{$catSub->title_ar}}
                                    @else
                                        {{$catSub->title_en}}
                                    @endif
                                    </a>
                                </li>
                            @endforeach
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->

        <div class="container">
            <div class="row">
                <div class="d-none d-xl-block col-xl-3 col-wd-2gdot5">
                    <div class="mb-8 border border-width-2 border-color-3 borders-radius-6" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align:right" @endif>
                        <!-- List -->
                        <ul id="sidebarNav" class="list-unstyled mb-0 sidebar-navbar" >
                            <li>
                                <a class="dropdown-toggle dropdown-toggle-collapse dropdown-title" href="javascript:;" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="sidebarNav1Collapse" data-target="#sidebarNav1Collapse">
                                    {{__('jbq.Show_All_Categories')}}
                                </a>

                                <div id="sidebarNav1Collapse" class="collapse" data-parent="#sidebarNav">
                                    <ul id="sidebarNav1" class="list-unstyled dropdown-list">
                                        <!-- Menu List -->
                                        @foreach(\App\Models\Category::all() as $theCat)
                                            <li><a class="dropdown-item" href="{{url('/category/')}}/{{$theCat->url}}">
                                                    @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                        {{$theCat->title_ar}}
                                                    @else
                                                        {{$theCat->title_en}}
                                                    @endif
                                                    <span class="text-gray-25 font-size-12 font-weight-normal"> </span></a></li>
                                        @endforeach
                                        <!-- End Menu List -->
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="dropdown-current active" href="#">@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                        {{$cat->title_ar}}
                                    @else
                                        {{$cat->title_en}}
                                    @endif</a>

                                <ul class="list-unstyled dropdown-list">
                                    <!-- Menu List -->
                                    @foreach(\App\Models\Category::where('parent_id',$cat->id)->get() as $theCat)
                                        <li><a class="dropdown-item" href="{{url('/category/')}}/{{$theCat->url}}">
                                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                    {{$theCat->title_ar}}
                                                @else
                                                    {{$theCat->title_en}}
                                                @endif
                                                <span class="text-gray-25 font-size-12 font-weight-normal"> </span></a></li>
                                @endforeach
                                    <!-- End Menu List -->
                                </ul>
                            </li>
                        </ul>
                        <!-- End List -->
                    </div>
                    <div class="mb-8">
                        <div class="border-bottom border-color-1 mb-5" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                            <h2 class="section-title section-title__sm mb-0 pb-2 font-size-18">{{__('jbq.Latest Products')}}</h2>
                        </div>
                        <ul class="list-unstyled">
                            @foreach(\App\Models\Product::take(5)->get() as $pr)
                                @include('product_box',['pr'=>$pr])
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xl-9 col-wd-9gdot5">
                    <div class="d-flex justify-content-between align-items-center border-bottom border-color-1 flex-lg-nowrap flex-wrap mb-4">
                        <h1 class="section-title section-title__full mb-0 pb-2 font-size-22">
                            @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                {{$cat->title_ar}}
                            @else
                                {{$cat->title_en}}
                            @endif
                        </h1>
                    </div>
                    <ul  class="@if(\Illuminate\Support\Facades\App::getLocale()=='ar') flex-row-reverse @endif row list-unstyled products-group no-gutters mb-6">

                        @foreach($cat->products as $pr)
                        <li class="col-6 col-md-3 product-item">
                            <div class="product-item__outer h-100 w-100">
                                <div class="product-item__inner px-xl-4 p-3">
                                    <div class="product-item__body pb-xl-2">
                                        <div class="mb-2">
                                            <a href="{{url('/')}}/product/{{$pr->url}}" class="d-block text-center">
                                                <img class="img-fluid" src="{{@$pr->image->file_name}}" alt="@if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                {{$pr->title_ar}}
                                                @else
                                                {{$pr->title_en}}
                                                @endif">
                                            </a>
                                        </div>
                                        <h3 class="text-center mb-1 product-item__title">
                                            <a href="{{url('/')}}/product/{{$pr->url}}" class="font-size-15 text-gray-90">
                                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                    {{$pr->title_ar}}
                                                @else
                                                    {{$pr->title_en}}
                                                @endif
                                            </a>
                                        </h3>
                                        @if(!empty($pr->total))
                                            <div class="mt-3 mb-1" style="font-size:9px;text-align: center" >
                                                @php
                                                    $pr->sold = empty($pr->sold) ? 0 : $pr->sold;
                                                @endphp
                                                <b>{{$pr->sold}}</b> {{__('jbq.Sold out of')}} <b>{{$pr->total}}</b>
                                            </div>
                                            <div class="rounded-pill bg-gray-3 position-relative" style="height:5px;">
                                                @php
                                                    $total = $pr->total - ($pr->total - $pr->sold);

                                                @endphp
                                                <span style="width:{{$total}}%" class="position-absolute @if(App::getLocale()=='ar') right-0 @else left-0 @endif top-0 bottom-0 rounded-pill bg-primary"></span>
                                            </div>
                                        @endif
                                        <div class="mt-2">
                                            <small style="font-size:11px;color:#4a4a4a">AED</small>
                                            @if(!empty($pr->fixed_price))
                                                <ins class="font-size-25 text-decoration-none">{{$pr->fixed_price}}</ins>
                                                <del class="font-size-20 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-2 @else ml-2 @endif text-gray-6">{{$pr->price}}</del>
                                            @else
                                                <ins class="font-size-25 text-decoration-none">{{$pr->price}}</ins>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach

                    </ul>
                    <!-- Best Sellers -->
                    <div class="mb-6 position-relative">
                        <div class="d-flex justify-content-between border-bottom border-color-1 flex-md-nowrap flex-wrap border-sm-bottom-0">
                            <h2 class="section-title section-title__full mb-0 pb-2 font-size-22">{{__('jbq.Best Sellers')}}</h2>
                        </div>
                        <div @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="direction: ltr" @endif class="js-slick-carousel position-static u-slick u-slick--gutters-1 overflow-hidden u-slick-overflow-visble pt-3 pb-3"
                             data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                             data-arrow-left-classes="fa fa-angle-left @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-0 @else right-1 @endif"
                             data-arrow-right-classes="fa fa-angle-right @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-3 @else right-0 @endif"
                             data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-4">
                            <?php
                            $mostView = \App\Models\FactorHasItem::groupBy('product_id')->select(\Illuminate\Support\Facades\DB::raw('count(*) as total'),'product_id')->orderBy('total','DESC')->take(20)->get();
                            $total = count($mostView);
                            $totalSlides = round($total/3);

                            ?>
                            @for($i=0;$i<$totalSlides;$i++)
                            <div class="js-slide" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                                <ul class="row list-unstyled products-group no-gutters mb-0 overflow-visible">
                                    <?php
                                        $products = \App\Models\FactorHasItem::groupBy('product_id')->select(\Illuminate\Support\Facades\DB::raw('count(*) as total'),'product_id')->orderBy('total','DESC')->take(3)->skip(3*$i)->get();
                                        $productsList = [];
                                        foreach($products as $pr){
                                            $productsList[] = \App\Models\Product::find($pr->product_id);
                                        }
                                    ?>
                                    @foreach($productsList as $pr)
                                        @include('product_box_slider',['pr'=>$pr])
                                    @endforeach
                                </ul>
                            </div>
                            @endfor
                        </div>
                    </div>
                    <!-- End Best Sellers -->
                    @if($cat->type!='1')
                    <!-- Top rated in this category -->

                    <?php
                    $mostView = $cat->products()->orderBy('view','desc')->take(20)->get();
                    $total = count($mostView);
                    $totalSlides = round($total/3);

                    ?>

                    <div class="mb-8 position-relative">
                        <div class="d-flex justify-content-between border-bottom border-color-1 flex-md-nowrap flex-wrap border-sm-bottom-0">
                            <h2 class="section-title section-title__full mb-0 pb-2 font-size-22">{{__('jbq.Top rated in this category')}}</h2>
                        </div>
                        <div  @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="direction: ltr" @endif class="js-slick-carousel position-static u-slick u-slick--gutters-1 overflow-hidden u-slick-overflow-visble pt-3 pb-3"
                             data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                             data-arrow-left-classes="fa fa-angle-left @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-0 @else right-1 @endif"
                             data-arrow-right-classes="fa fa-angle-right @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-3 @else right-0 @endif"
                             data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-4">

                            @for($i=0;$i<$totalSlides;$i++)
                                <?php
                                $products = $cat->products()->orderBy('view','desc')->take(3)->skip(3*$i)->get();
                                ?>
                            <div class="js-slide"  @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                                <ul class="row list-unstyled products-group no-gutters mb-0 overflow-visible">
                                    @foreach($products as $pr)
                                        @include('product_box_slider',['pr'=>$pr])
                                    @endforeach
                                </ul>
                            </div>
                            @endfor


                        </div>
                    </div>



                    @endif
                    <!-- End Top rated in this category -->
                </div>
            </div>

            @if($cat->type=='1')
            <div class="mb-6 position-relative p-5" style="background-color:#f8edca">
                <div class="d-flex justify-content-between border-bottom border-color-1 flex-md-nowrap flex-wrap border-sm-bottom-0">
                    <h2 class="section-title section-title__full mb-0 pb-2 font-size-22">{{__('jbq.Winners')}}</h2>
                </div>
                <div @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="direction: ltr" @endif class="js-slick-carousel position-static u-slick u-slick--gutters-1 overflow-hidden u-slick-overflow-visble pt-3 pb-3"
                     data-arrows-classes="position-absolute top-0 font-size-17 u-slick__arrow-normal top-10"
                     data-arrow-left-classes="fa fa-angle-left @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-0 @else right-1 @endif"
                     data-arrow-right-classes="fa fa-angle-right @if(\Illuminate\Support\Facades\App::getLocale()=='ar') left-3 @else right-0 @endif"
                     data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-4">
                    <?php
                    $winners = \App\Models\Winner::take(20)->get();
                    $total = count($mostView);
                    $totalSlides = round($total/3);

                    ?>
                    @for($i=0;$i<$totalSlides;$i++)
                        <div  class="js-slide" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                            <ul class="row list-unstyled products-group no-gutters mb-0 overflow-visible">
                                @php
                                    $productsList = \App\Models\Winner::take(8)->skip($i*8)->get();
                                @endphp
                                @foreach($productsList as $pr)
                                    @include('winner_box',['winner'=>$pr])
                                @endforeach
                            </ul>
                        </div>
                    @endfor
                </div>
            </div>
            @endif
            <!-- Brand Carousel -->
{{--            <div class="mb-8">--}}
{{--                <div class="py-2 border-top border-bottom">--}}
{{--                    <div class="js-slick-carousel u-slick my-1"--}}
{{--                         data-slides-show="5"--}}
{{--                         data-slides-scroll="1"--}}
{{--                         data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"--}}
{{--                         data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"--}}
{{--                         data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"--}}
{{--                         data-responsive='[{--}}
{{--                                "breakpoint": 992,--}}
{{--                                "settings": {--}}
{{--                                    "slidesToShow": 2--}}
{{--                                }--}}
{{--                            }, {--}}
{{--                                "breakpoint": 768,--}}
{{--                                "settings": {--}}
{{--                                    "slidesToShow": 1--}}
{{--                                }--}}
{{--                            }, {--}}
{{--                                "breakpoint": 554,--}}
{{--                                "settings": {--}}
{{--                                    "slidesToShow": 1--}}
{{--                                }--}}
{{--                            }]'>--}}
{{--                        <div class="js-slide">--}}
{{--                            <a href="#" class="link-hover__brand">--}}
{{--                                <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img1.png" alt="Image Description">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="js-slide">--}}
{{--                            <a href="#" class="link-hover__brand">--}}
{{--                                <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img2.png" alt="Image Description">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="js-slide">--}}
{{--                            <a href="#" class="link-hover__brand">--}}
{{--                                <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img3.png" alt="Image Description">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="js-slide">--}}
{{--                            <a href="#" class="link-hover__brand">--}}
{{--                                <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img4.png" alt="Image Description">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="js-slide">--}}
{{--                            <a href="#" class="link-hover__brand">--}}
{{--                                <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img5.png" alt="Image Description">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="js-slide">--}}
{{--                            <a href="#" class="link-hover__brand">--}}
{{--                                <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img6.png" alt="Image Description">--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <!-- End Brand Carousel -->
        </div>
    </main>
@endsection
@section('script')
    <script>
        $(window).on('load', function () {
            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                direction: 'horizontal',
                pageContainer: $('.container'),
                breakpoint: 767.98,
                hideTimeOut: 0
            });

            // initialization of svg injector module
            // $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
        });

       $(function () {
            console.log('ready');
            // initialization of header
            $.HSCore.components.HSHeader.init($('#header'));

            // initialization of animation
            $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of popups
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of countdowns
            var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
                yearsElSelector: '.js-cd-years',
                monthsElSelector: '.js-cd-months',
                daysElSelector: '.js-cd-days',
                hoursElSelector: '.js-cd-hours',
                minutesElSelector: '.js-cd-minutes',
                secondsElSelector: '.js-cd-seconds'
            });


            // initialization of forms
            $.HSCore.components.HSFocusState.init();

            // initialization of form validation
            // $.HSCore.components.HSValidation.init('.js-validate', {
            //     rules: {
            //         confirmPassword: {
            //             equalTo: '#signupPassword'
            //         }
            //     }
            // });

            // initialization of show animations
            $.HSCore.components.HSShowAnimation.init('.js-animation-link');

            // initialization of fancybox
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of slick carousel
            $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of hamburgers
            $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                beforeClose: function () {
                    $('#hamburgerTrigger').removeClass('is-active');
                },
                afterClose: function() {
                    $('#headerSidebarList .collapse.show').collapse('hide');
                }
            });

            $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                e.preventDefault();

                var target = $(this).data('target');

                if($(this).attr('aria-expanded') === "true") {
                    $(target).collapse('hide');
                } else {
                    $(target).collapse('show');
                }
            });

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

            // initialization of select picker
            $.HSCore.components.HSSelectPicker.init('.js-select');
        });
    </script>
@endsection
