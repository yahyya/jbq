@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Rtl -->
    <link href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet">
@endsection

@section('title','Orders')
@section('subtitle','Manage Orders By Users')

@section('content')

    <!-- Basic Examples -->
    <div class="modal fade" id="modal-desc" role="dialog" aria-labelledby="showDesc">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Order Description</h4>
                </div>
                <div id="modal-desc-value" class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
                        Order Details
                    </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.orders.edit',$order->id) }}"
                           class="btn btn-primary waves-effect">Edit Order Items</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#order-details" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">view_list</i>Order Info
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#order-rates" data-toggle="tab" aria-expanded="true">
                                <i class="material-icons">comment</i>Order Comments
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#order-transactions" data-toggle="tab" aria-expanded="true">
                                <i class="material-icons">swap_horiz</i>Order Transactions
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active show" id="order-details">
                            {{--<fieldset>--}}
                            {{--<legend>اطلاعات سفارش:</legend>--}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <strong>User : </strong>{{@$order->user->name.' '.@$order->user->family}}</div>
                                    <div class="col-sm-6">
                                        <div class="form-group clearfix">
                                            <label for="sbStatus"><strong>Order Status : </strong></label>
                                            <select name="category" id="sbStatus" class="select show-tick">
                                                <option value="0" @if($order->status_id == 0) selected @endif><span
                                                            class="text-muted">Pending</span></option>
                                                <option value="1" @if($order->status_id == 1) selected @endif><span
                                                            class="text-warning">Preparing</span></option>
                                                <option value="2" @if($order->status_id == 2) selected @endif><span
                                                            class="text-primary">Sent</span></option>
                                                <option value="3" @if($order->status_id == 3) selected @endif><span
                                                            class="text-success">Done</span></option>
                                                <option value="4" @if($order->status_id == 4) selected @endif><span
                                                            class="text-danger">Canceled</span></option>
                                                <option value="5" @if($order->status_id == 5) selected @endif><span
                                                            class="text-danger">Returned</span></option>
                                            </select>

                                            <button id="btnStatus" class="btn btn-primary btn-xs" type="button">Save
                                            </button>
                                        </div>
                                        <div>
                                            @if($order->status_id=='4')
                                                Cancelation Reason :
                                                {{$order->reason}}

                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row p-5">
                                            @if($order->address)
                                                <h4>
                                                    Order Address
                                                </h4>
                                                <ul>
                                                    <li>
                                                        {{__('jbq.First name')}} : {{{$order->address->first_name}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Last name')}}  : {{{$order->address->last_name}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Country')}}  : {{{$order->address->country}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.City')}} : {{{$order->address->city}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.State')}} : {{{$order->address->state}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Postcode/Zip')}} : {{{$order->address->postcode}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Phone')}} : {{{$order->address->phone}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Email address')}} : {{{$order->address->email}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Company name')}} : {{{$order->address->company_name}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Street address')}} : {{{$order->address->street_address}}}
                                                    </li>
                                                </ul>
                                            @endif
                                        </div>
                                        <div class="row p-5">
                                            @if($order->shipping_address_id)
                                                <h4>
                                                    Order Shipping Address
                                                </h4>
                                                <ul>
                                                    <li>
                                                        {{__('jbq.First name')}} : {{{$order->shippingAddress->first_name}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Last name')}}  : {{{$order->shippingAddress->last_name}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Country')}}  : {{{$order->shippingAddress->country}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.City')}} : {{{$order->shippingAddress->city}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.State')}} : {{{$order->shippingAddress->state}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Postcode/Zip')}} : {{{$order->shippingAddress->postcode}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Phone')}} : {{{$order->shippingAddress->phone}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Email address')}} : {{{$order->shippingAddress->email}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Company name')}} : {{{$order->shippingAddress->company_name}}}
                                                    </li>
                                                    <li>
                                                        {{__('jbq.Street address')}} : {{{$order->shippingAddress->street_address}}}
                                                    </li>
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-6"><strong>Payment Type : </strong>{{$order->fapaymentType}}</div>
                                    <div class="col-sm-6"><strong>Payment Status :</strong>@if(empty($order->paied_at))
                                            Not Payed
                                        @else
                                            {{$order->paied_at}}
                                        @endif  </div>
                                    <div class="col-sm-12"><strong>Invoice Number : </strong>{{$order->tracking_code}}</div>
                                    <div class="col-sm-6"><strong>
                                            Order : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->amount,true )}}
                                    </div>
                                    <div class="col-sm-6"><strong>
                                            Tax : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->tax_price,true )}}
                                    </div>
                                    <div class="col-sm-6"><strong>
                                            Delivery : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->delivery_price,true )}}
                                    </div>
                                    <div class="col-sm-6"><strong>
                                            Discount : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->discount_price,true )}}
                                    </div>
                                    <div class="col-sm-6"><strong>
                                            Boxing Price: </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->boxing_price,true )}}
                                    </div>
                                    <div class="col-sm-6"><strong>
                                            Total : </strong>{{\App\Http\Controllers\Controller::priceFormat( @$order->factor()->total,true )}}
                                    </div>
                                    <div class="col-sm-6"><strong>
                                            Discount Percent : </strong>{{ !empty($order->coupon_id) ? $order->coupon->value : '-' }}
                                    </div>
                                    <div class="col-sm-12"><strong>
                                            Delivered At :
                                        </strong>{{!empty($order->recieve_date) ? $order->recieve_date : '-'}}
                                    </div>
                                    <div class="col-sm-12"><strong>Desc : </strong><br>{{$order->desc}}</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table id="orderItems" class="table table-bordered table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Title</th>
                                                <th>Count</th>
                                                <th>Price</th>

                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>id</th>
                                                <th>Title</th>
                                                <th>Count</th>
                                                <th>Price</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="order-rates">
                            <div class="table-responsive">
                                <table id="ratesDatatable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Date</th>
                                        <th>User</th>
                                        {{--<th>کد سفارش</th>--}}
                                        <th>Delivery</th>
                                        <th>Quality</th>
                                        <th>Size</th>
                                        <th>Deliver Time</th>
                                        <th>Desc.</th>
                                        {{--<th>عملیات</th>--}}
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Date</th>
                                        <th>User</th>
                                        {{--<th>کد سفارش</th>--}}
                                        <th>Delivery</th>
                                        <th>Quality</th>
                                        <th>Size</th>
                                        <th>Deliver Time</th>
                                        <th>Desc.</th>
                                        {{--<th>عملیات</th>--}}
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="order-transactions">
                            <div class="table-responsive">
                                <table id="transactionsDatatable"
                                       class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Facture </th>
                                        <th>Port</th>
                                        <th>Price</th>
                                        <th>Recieved Code</th>
                                        <th>Refrence Code</th>
                                        <th>Card</th>
                                        <th>ip</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Facture </th>
                                        <th>Port</th>
                                        <th>Price</th>
                                        <th>Recieved Code</th>
                                        <th>Refrence Code</th>
                                        <th>Card</th>
                                        <th>ip</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <!-- Custom Js -->
    {{--    <script src="{{ URL::asset('admin-asset/js/pages/tables/jquery-datatable.js') }}"></script>--}}

    <script>
        var tableOrderItems;
        function showDesc(desc){
            $('#modal-desc-value').html(desc);
            $('#modal-desc').modal('show');
        }
        function changeNamePrompt(id){
            var name = prompt('Type the new name');
            $.ajax(
                {
                    url: '{{route('admin.orders.set.name',$order->id)}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "id": id,
                        "_method": 'post',
                        "name": name,

                    },
                    success: function (res) {
                        tableOrderItems.draw();
                    },
                    error: function (xhr, stat) {
                        $('#btnStatus').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error !<br>' + res.msg + '</div>');
                    }
                });

        }
        $(function () {
            tableOrderItems = $('#orderItems').DataTable({

                pageLength: 100,
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.orders.show',  $order->id)}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'product_id', name: 'product_id'},
                    {data: 'count', name: 'count'},
                    {data: 'price', name: 'price'},
                ]
            });

            $('#ratesDatatable').DataTable({

                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.orders.rates', $order->id)}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'user_id', name: 'user_id'},
//                    {data: 'order_id', name: 'order_id'},
                    {data: 'personnel', name: 'personnel'},
                    {data: 'quality', name: 'quality'},
                    {data: 'taste', name: 'taste'},
                    {data: 'delivery', name: 'delivery'},
                    {data: 'desc', name: 'desc'},
//                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            $('#transactionsDatatable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.orders.transactions', $order->id)}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'factor_id', name: 'factor_id'},
                    {data: 'port', name: 'port'},
                    {data: 'price', name: 'price'},
                    {data: 'ref_id', name: 'ref_id'},
                    {data: 'tracking_code', name: 'tracking_code'},
                    {data: 'card_number', name: 'card_number'},
                    {data: 'ip', name: 'ip'},
                    {data: 'status', name: 'status'},
                    {data: 'payment_date', name: 'payment_date', orderable: false, searchable: false}
                ]
            });

            $('#btnStatus').on('click', function () {
                setStatus();
            });
            $('#btnCourier').on('click', function () {
                setCourier();
            });

        });


        function setStatus() {

            $('#btnStatus').attr('disabled', 'disabled');

            var reason = '';
              if ($('#sbStatus').val()=='4'){
                   reason = prompt('Cancelation Reason');
                   if (reason ==''){
                       return;
                   }
               }
            $.ajax(
                {
                    url: '{{route('admin.orders.set.status',$order->id)}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "id": {{$order->id}},
                        "_method": 'post',
                        "status_id": $('#sbStatus').val(),
                        "notification": $('#cbSendNotify').is(':checked'),
                        "sms": $('#cbSendSms').is(':checked'),
                        'reason':reason
                    },
                    success: function (res) {
                        $('#btnStatus').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Order Status Updated!</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function () {
                            $('.alert').fadeOut('slow', function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('#btnStatus').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }

    </script>

@endsection
