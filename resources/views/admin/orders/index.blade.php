@extends('admin.layouts.panel')

@section('title','Orders')
@section('subtitle','Orders Sent by users')
@section('style')

@endsection
@section('content')
    <div class="modal fade" id="modal-desc" role="dialog" aria-labelledby="showDesc">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Order Description</h4>
                </div>
                <div id="modal-desc-value" class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
                        Orders List
                    </h2>
                    <div class="align-left">

                        {{--<a href="{{ route('admin.orders.create') }}" class="btn btn-primary waves-effect">افزودن سفارش</a>--}}
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class=" row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="sbStatus"><strong>Order Status : </strong></label>
                                <div class="form-line">
                                    <select name="category" id="sbStatus" required>
                                        <option value="-2">All</option>
                                        <option value="0">Pending</option>
                                        <option value="1">Preparing</option>
                                        <option value="2">Sent</option>
                                        <option value="3">Done</option>
                                        <option value="4">Canceled</option>
                                        <option value="5">Returned</option>
                                        <option value="-1">Suspended</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>
                                        Date Range :
                                    </h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tb-date-from">From </label>
                                        <input id="tb-date-from" type="text" class="datepicker"
                                               value="{{date('Y-m-d')}}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tb-date-from">To </label>
                                        <input id="tb-date-to" type="text" class="datepicker"
                                               value="{{date('Y-m-d',strtotime('+1 day'))}}"/>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <input type="button" class="btn btn-danger btn-search" value="Search" />
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">

                            <h5>
                                 Total Orders :
                                <span id="txt-total"></span>
                            </h5>

                            <h5  >
                                Total Amount :
                                <span id="txt-sum-prices"></span>
                            </h5>
                        </div>
                    </div>
                    <div class="table-responsive row">


                        <table id="datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>User</th>

                                <th>Invoice Number</th>
                                <th>Status</th>

                                <th>Payment Type</th>

                                <th>Total Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>User</th>

                                <th>Invoice Number</th>
                                <th>Status</th>

                                <th>Payment Type</th>

                                <th>Total Price</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        var table;

        function showDesc(desc) {
            $('#modal-desc-value').html(desc);
            $('#modal-desc').modal('show');
        }
        $('.btn-search').on('click',function (btn) {
            table.draw();
            getTotal();
        });
        function getTotal(){
            $.ajax(
            {
                url: '{{route('admin.orders.index.total')}}',
                type: 'get',
                dataType: "JSON",
                data: {
                    status: function () {
                            return $('#sbStatus').val()
                        },
                        dateFrom:function (){ return $('#tb-date-from').val();} ,
                        dateTo:function (){ return $('#tb-date-to').val();}
                },
                success: function (res) {
                    $('#txt-total').html(res.total);
                    $('#txt-sum-prices').html(res.sum);
                },
                error: function (xhr, stat) {

                    console.log(stat);
                }
            });
        }
        $(function () {
            $('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
            $('#sbStatus').on('change', function () {
                table.draw();
                getTotal();
            });
            getTotal();
            table = $('#datatable').DataTable({
                pageLength: 100,
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.orders.index')}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    data: {
                        status: function () {
                            return $('#sbStatus').val()
                        },
                        dateFrom:function (){ return $('#tb-date-from').val();} ,
                        dateTo:function (){ return $('#tb-date-to').val();}
                    },
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'created_at_time', name: 'created_at'},
                    {data: 'user_id', name: 'user_id'},
                    {data: 'tracking_code', name: 'tracking_code'},
                    {data: 'status_id', name: 'status_id'},
                    {data: 'payment_type', name: 'payment_type'},
                    {data: 'total', name: 'total'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

        function deleteItem(id) {
            var confirm = window.confirm('Are You sure you want to delete this?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{route('admin.orders.index')}}/' + id,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                table.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed Successfully!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>

@endsection
