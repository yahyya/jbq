@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Select -->
    <link href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet">
@endsection

@section('title','Orders')
@section('subtitle','Edit order items')

@section('content')

    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
View Order Items
                    </h2>
                    <div class="align-left">
                        <a href="#" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#newItemModal">Add Item</a>
                        <a href="#" class="btn btn-success waves-effect saveBtn">Save Changes</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>title</th>
                                <th>count</th>
                                <th>amount</th>
                                <th>action</th>
                            </tr>
                            </thead>
                            <tbody>
                                {{--@foreach($orderItems as $item)--}}
                                    {{--<tr>--}}
                                        {{--<td>{{$item->product_id}}</td>--}}
                                        {{--<td>{{$item->product_name}}</td>--}}
                                        {{--<td>{{$item->bread_name}}</td>--}}
                                        {{--<td>{{$item->count}}</td>--}}
                                        {{--<td>{{$item->price}}</td>--}}
                                        {{--<td>{!! htmlspecialchars_decode($item->action) !!}</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>title</th>
                                <th>count</th>
                                <th>amount</th>
                                <th>action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')
    <!-- Modal -->
    <div class="modal fade" id="newItemModal" tabindex="-1" role="dialog" aria-labelledby="addItemModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">select item</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('product_id') ? ' has-error' : '' }}">
                        {{--<label for="email_address">دسته محصول</label>--}}
                        {{--<div class="form-line">--}}
                        <select id="sbProduct" class="form-control show-tick" data-live-search="true" title="select product ..." required>
                            @foreach(@$products as $product)
                                <option value="{{$product->id}}" data-price="{{$product->price}}">{{$product->title_en}} - {{$product->price}}  </option>
                            @endforeach
                        </select>
                        @if ($errors->has('product_id'))
                            <span class="help-block">
                                                <strong>{{ $errors->first('product_id') }}</strong>
                                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('count') ? ' has-error' : '' }}">
                        <label for="tbCount">count</label>
                        <div class="form-line">
                            <input type="number" id="tbCount" class="form-control" value="1" required>
                            @if ($errors->has('count'))
                                <span class="help-block">
                            <strong>{{ $errors->first('count') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                    <button type="button" id="btnAddItem" class="btn btn-primary">save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editCountModal" tabindex="-1" role="dialog" aria-labelledby="editCountModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit count</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group{{ $errors->has('count') ? ' has-error' : '' }}">
                        <label for="tbEditCount">count</label>
                        <div class="form-line">
                            <input type="number" id="tbEditCount" class="form-control" required>
                            @if ($errors->has('count'))
                                <span class="help-block">
                            <strong>{{ $errors->first('count') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                    <button type="button" id="btnAddItem" class="btn btn-primary">save</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap Select Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>


    <!-- Custom Js -->

    <script>
        var table,orderItems = [];
        @foreach($orderItems as $item)
            orderItems.push({!! $item !!});
        @endforeach
        $(function () {

            $('.saveBtn').on('click', function () {
                saveItem();
            });

            table = $('#datatable').DataTable({

                pageLength:100,
                responsive: true,
                processing: true,
                serverSide: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                data: orderItems,
                columns: [
                    {data: 'product_id' },
                    {data: 'product_name' },
                    {data: 'count' },
                    {data: 'price' },
                    {data: 'action' }
                ]
            });

            $('#btnAddItem').on('click', function () {
//                if ($('#form').parsley().validate())
                addItem();
            });


        });

        function addItem() {
            if ($('#sbProduct').val()) {
                var count = parseInt($('#tbCount').val()),
                    productId = parseInt($('#sbProduct').val());
                var item = {
                    "product_id": productId,
                    "product_name": $('#sbProduct :selected').text(),
                    "count": count,
                    "price": $('#sbProduct :selected').data('price') * count,
                    "action": ' <button onclick="editCount('+productId+')" class="btn btn-xs btn-primary waves-effect">Edit Total</button> <button onclick="deleteItem('+productId+')" class="btn btn-xs btn-danger btn-delete waves-effect">Delete</button>',
                };
                var result = $.grep(orderItems, function(item){
                    if(item.product_id == productId)
                        return item;
                });

                if (result.length == 0) {
                    orderItems.push(item);
                    table.row.add(item).draw();
                } else if (result.length == 1) {
                    // access the foo property using result[0].foo
                    var index = orderItems.indexOf(result[0]);
                    result[0].count += item.count;
                    console.log(item.price);
                    console.log(result[0].price);
                    result[0].price += item.price;
                    table.clear().rows.add(orderItems).draw().nodes();
                }
//                orderItems.push(item);

//                table.row.add(item).draw();
//                $('#newItemModal').modal('hide');
//                $('#newItemModal .modal-backdrop').css('display','none');
            } else {
                $('.modal-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>No product selected!</div>');
            }
        }

        function editCount(productId) {
            $('#editCountModal').modal('show');
            var result = $.grep(orderItems, function(item){
                if(item.product_id == productId )
                    return item;
            });
            console.log(result[0].count);
            $('#tbEditCount').val(result[0].count);
        }

        function deleteItem(btn,productId) {
            var confirm = window.confirm('Are you sure?');
            if (confirm) {
                var result = $.grep(orderItems, function(item){
                    if(item.product_id == productId )
                        return item;
                });
                console.log(result);
                if (result.length == 0) {
                    window.alert('Item not found!');
                } else if (result.length == 1) {
                    // access the foo property using result[0].foo
                    var index = orderItems.indexOf(result[0]);
                    console.log('index of item '+ index);
                    (index !== -1) ? orderItems.splice(index, 1) : '';
                } else {
                    // multiple items found
                }
                table.row($(btn).closest('tr')).remove().draw();
//                table.draw();
                console.log('row maybe has been deleted');
            }
        }

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');

            $.ajax(
                {
                    url: '{{route('admin.orders.update', $id)}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'put',
                    dataType: "JSON",
                    data: {
                        "id": {{$id}},
                        "_method": 'post',
                        "orderItems": orderItems
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Edited!</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
