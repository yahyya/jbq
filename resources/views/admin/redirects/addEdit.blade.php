@extends('admin.layouts.panel')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title',' Category')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Redirect
                    </h2>
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group">
                            <label for="tbTitle">Title </label>
                            <div class="form-line">
                                <input type="text" id="tbTitle" class="form-control" value="" placeholder="" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tbAddress">Address </label>
                            <div class="form-line">
                                <input type="text" id="tbAddress" class="form-control" value="" placeholder="" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tbRedirect">Redirect </label>
                            <div class="form-line">
                                <input type="text" id="tbRedirect" class="form-control" value="" placeholder="" required autofocus>
                            </div>
                        </div>


                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->

    <script>
        $(function () {


            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '{{route('admin.redirects.store')}}',
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "title": $('#tbTitle').val(),
                        "address" :$('#tbAddress').val(),
                        "redirect": $('#tbRedirect').val(),
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$category)Saved!@else Saved!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
