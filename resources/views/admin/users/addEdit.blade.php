@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Select Css -->
    <link  rel="stylesheet" href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title','Products')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="{{ @$user ? ' pull-right' : '' }}">
                        @if(@$user)
                            Edit User
                        @else
                            Add User
                        @endif
                    </h2>

                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#user-profile" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">face</i>User Info
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#user-addresses" data-toggle="tab" aria-expanded="true">
                                <i class="material-icons">map</i> Addresses
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#user-transactions" data-toggle="tab">
                                <i class="material-icons">account_balance_wallet</i>
                                Transactions
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#user-orders" data-toggle="tab">
                                <i class="material-icons">shopping_basket</i>
                                Orders
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active show" id="user-profile">
                            <form id="form">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="tbName">Name</label>
                                    <div class="form-line">
                                        <input type="text" id="tbName" class="form-control" value="@if(@$user) {{$user->name}} @endif" placeholder="" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('family') ? ' has-error' : '' }}">
                                    <label for="tbFamily">Last Name</label>
                                    <div class="form-line">
                                        <input type="text" id="tbFamily" class="form-control" value="@if(@$user) {{$user->family}} @endif" placeholder="" required>
                                        @if ($errors->has('family'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('family') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                    <label for="tbMobile">Mobile</label>
                                    <div class="form-line">
                                        <input type="text" id="tbMobile" class="form-control" value="@if(@$user) {{$user->mobile}} @endif" placeholder="" required>
                                        @if ($errors->has('mobile'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="tbEmail">Email</label>
                                    <div class="form-line">
                                        <input type="email" id="tbEmail" class="form-control" value="@if(@$user) {{$user->email}} @endif" placeholder="" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label for="sbType">User Type</label>
                                    <div class="form-line">
                                        <select id="sbType" class="form-control" >
                                            <option value="0" @if(@$user) @if($user->type=='0') selected="selected" @endif @endif >
                                                Normal User
                                            </option>
                                            <option value="1" @if(@$user) @if($user->type=='1') selected="selected" @endif @endif >
                                                Seller User
                                            </option>
                                        </select>
                                        @if ($errors->has('validation_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('validation_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </form>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$user) Save @else Update @endif</button>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="user-addresses">
                            <div class="table-responsive">
                                <table id="addressesDatatable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="user-wallet">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Wallet Amount:</label>
                                    <div class="col-sm-10">
                                        {{--<h3 class="form-control-static">{{@$user->balance}}</h3>--}}
                                        <p class="form-control-static">@if(@$user) {{@$user->balance}} @else {{ '-' }} @endif</p>
                                    </div>
                                </div>
                            </form>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Wallet Stats</div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table id="creditsDatatable" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>Amount</th>
                                                        <th>Transaction Id</th>
                                                        <th>Admin</th>
                                                    </tr>
                                                    </thead>
                                                    <tfoot>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>Amount</th>
                                                        <th>Transaction Id</th>
                                                        <th>Admin</th>
                                                    </tr>
                                                    </tfoot>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="user-transactions">
                            <div class="table-responsive">
                                <table id="transactionsDatatable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Amount</th>
                                        <th>Transaction Id</th>
                                        <th>Order Invoice Id</th>
                                        <th>Status code</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Amount</th>
                                        <th>Transaction Id</th>
                                        <th>Order Invoice Id</th>
                                        <th>Status code</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in active" id="user-orders">
                            <div class="table-responsive">
                                <table id="ordersDatatable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Order At</th>
{{--                                        <th>پیک</th>--}}
                                        <th>Order Status</th>
                                        <th>Delivery at</th>
                                        <th>Payment Type</th>
                                        <th>Desc.</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Order At</th>
                                        {{--                                        <th>پیک</th>--}}
                                        <th>Order Status</th>
                                        <th>Delivery at</th>
                                        <th>Payment Type</th>
                                        <th>Desc.</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in active" id="user-referrals">
                            <div class="table-responsive">
                                <table id="referralsDatatable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Phone</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Phone</th>
                                        <th>Date</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- modal -->
    <div class="modal fade" id="userCreditModal" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="smallModalLabel">Edit Users Wallet Amont</h4>
                </div>
                <div class="modal-body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="tbAmount">Amount</label>
                            <div class="form-line">
                                <input type="text" id="tbAmount" class="form-control" placeholder="2000" required>
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('transaction_id') ? ' has-error' : '' }}">
                            <label for="tbTransactionId">Transaction Id</label>
                            <div class="form-line">
                                <input type="text" id="tbTransactionId" class="form-control">
                                @if ($errors->has('transaction_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('transaction_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('family') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input name="balanceType" type="radio" class="with-gap radio-col-green" value="increase" id="increase">
                                <label for="increase">Ascend</label>
                            </div>
                            <div class="col-xs-12">
                                <input name="balanceType" type="radio" class="with-gap radio-col-red" value="decrease" id="decrease">
                                <label for="decrease">Descend</label>
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect setBalance">Save</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <script>
        @if(@$user)var userId = {{@$user->id}};@endif
        var creditTable;
        $(function () {
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });

            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
            $('.setBalance').on('click', function () {
//                if ($('#form').parsley().validate())
                    setBalance();
            });

            @if(@$user)
            $('#addressesDatatable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.users.addresses',['id' => $user->id])}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'address', name: 'address'},
                    {data: 'phone', name: 'phone'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            creditTable = $('#creditsDatatable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.users.credits',['id' => $user->id])}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'amount', name: 'amount'},
                    {data: 'transaction_id', name: 'transaction_id'},
                    {data: 'admin_id', name: 'admin_id'},
                ]
            });

            $('#transactionsDatatable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.users.transactions',['id' => $user->id])}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'price', name: 'price'},
                    {data: 'tracking_code', name: 'tracking_code'},
                    {data: 'order_tracking_code', name: 'order.tracking_code'},
                    {data: 'status', name: 'status'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'created_at_time', name: 'created_at'},
                ]
            });

            $('#ordersDatatable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.users.orders',['id' => $user->id])}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'created_at', name: 'created_at'},
                    // {data: 'courier_id', name: 'courier_id'},
                    {data: 'status_id', name: 'status_id'},
                    {data: 'recieve_date', name: 'recieve_date'},
                    {data: 'payment_type', name: 'payment_type'},
                    {data: 'desc', name: 'desc'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            $('#referralsDatatable').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.users.referrals',['id' => $user->id])}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'phone', name: 'phone'},
                    {data: 'created_at', name: 'created_at'},
                ]
            });
            @endif
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$user){{route('admin.users.update',$user->id)}} @else {{route('admin.users.store')}}@endif',
                    type: @if(@$user)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$user)"id": {{$user->id}},@endif
                        "_method": 'post',
                        "name": $('#tbName').val(),
                        "family": $('#tbFamily').val(),
                        "mobile": $('#tbMobile').val(),
                        "email": $('#tbEmail').val(),
                        "type":$('#sbType').val(),
                        "validation_code": $('#tbValidationCode').val(),
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$user)Updated Successfully!@else Added Successfully @endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
        @if(@$user)
        function setBalance() {
            $('.setBalance').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '{{route('admin.users.setBalance',['id'=>$user->id])}}',
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "id": userId,
                        "amount": $('#tbAmount').val(),
                        "transaction_id": $('#tbTransactionId').val(),
                        "type": $('input[name=balanceType]:checked').val(),
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.modal-body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Item Added Successfully!</div>');
                            creditTable.draw();
                        } else {
                            $('.alert').remove();
                            $('.modal-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
        @endif

        function deleteAddress(id) {
            console.log('delete');
            var confirm = window.confirm('Are you sure?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{route('admin.addresses.index')}}/'+id ,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                table.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>

@endsection
