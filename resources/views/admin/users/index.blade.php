@extends('admin.layouts.panel')

@section('title','Users')
@section('subtitle','All users')

@section('content')

    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
Users List
                    </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.users.create') }}" class="btn btn-primary waves-effect">Add User</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Last Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Score</th>
                                <th>Total Orders</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Last Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Score</th>
                                <th>Total Orders</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Send Notification</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="url">
                            Title
                        </label>
                        <input type="text" id="tbTitle" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea id="message" name="message" class="form-control" cols="" rows=""></textarea>
                    </div>
                    <div class="form-group">
                        <label for="url">
                            Image URL
                        </label>
                        <input type="text" id="url" class="form-control" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="btn-nofity">Send</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.Modal -->

@endsection

@section('script')

    <script>
        var table;
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });

            table = $('#datatable').DataTable({
                pageLength: 100,
                responsive: true,
                processing: true,
                serverSide: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.users.index')}}',
                    method: 'GET'
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'family', name: 'family'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'email', name: 'email'},
                    {data: 'point', name: 'point'},
                    {data: 'ordersCount', name: 'ordersCount'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

        function deleteItem(id) {
            console.log('delete');
            var confirm = window.confirm('Are you sure you want to remove this?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{route('admin.users.index')}}/' + id,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                table.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>RemoveD1</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }

        function toggleBanStatus(id) {
            var confirm = window.confirm('Are You sure you want to ban this user?');
            if (confirm) {
                console.log(id);
                $.ajax(
                    {
                        url: '{{route('admin.users.toggleBanStatus')}}',
                        type:"POST",
                        dataType: "JSON",

                        data: {
                            "_method":"post",
                            "id": id
                        },
                        success: function (res) {
                            if (res.res) {
                                table.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>User is not ban anymore!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
//                            console.log(stat);
                        }
                    });
            }
        }

        function notifyModal(id) {
            $('#btn-nofity').attr('onclick', 'notifyUser(' + id + ')');
            $('#notificationModal').modal('show');
        }

        function notifyUser(id) {
            var confirm = window.confirm('Are You sure ?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{URL::to('/admin/users/notify')}}/' + id,
                        type: 'post',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'POST',
                            "message": $('#message').val(),
                            "url":$('#url').val(),
                            'title':$('#tbTitle').val()
                        },
                        success: function (res) {
                            console.log(res);
                            $('#message').val('');
                            $('#notificationModal').modal('hide');
                            if (res.res){
                                $('.box-header').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Sent Successfully!</div>');
                            } else {
                                $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Token not found!</div>');
                            }
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            console.log(stat);
                            $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>error!</div>');
//                        window.setTimeout(function () {
//                            $(".alert").alert('close');
//                        }, 5000);
                        }
                    });
            }

        };
    </script>

@endsection
