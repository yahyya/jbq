@extends('admin.layouts.panel')

@section('title','  مدیران')
@section('subtitle','زیر عنوان')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$admin)
                            ویرایش مدیر
                        @else
                            افزودن مدیر
                        @endif
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="tbName">نام</label>
                            <div class="form-line">
                                <input type="text" id="tbName" class="form-control" value="@if(@$admin){{$admin->name}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('family') ? ' has-error' : '' }}">
                            <label for="tbFamily">نام نام خانوادگی</label>
                            <div class="form-line">
                                <input type="text" id="tbFamily" class="form-control" value="@if(@$admin){{$admin->family}}@endif" placeholder="" required>
                                @if ($errors->has('family'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('family') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="tbMobile">موبایل</label>
                            <div class="form-line">
                                <input type="text" id="tbMobile" class="form-control" value="@if(@$admin){{$admin->mobile}}@endif" placeholder="" required>
                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="tbEmail">ایمیل</label>
                            <div class="form-line">
                                <input type="email" id="tbEmail" class="form-control" value="@if(@$admin){{$admin->email}}@endif" placeholder="" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="tbPassword">رمز عبور</label>
                            <div class="form-line">
                                <input type="password" id="tbPassword" class="form-control" value="" placeholder="">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="tbConfirmPassword">رمز عبور</label>
                            <div class="form-line">
                                <input type="password" id="tbConfirmPassword" class="form-control" value="" placeholder="">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="permissions form-group{{ $errors->has('permissions') ? ' has-error' : '' }} clearfix">
                            <label for="tbPrice">سطوح دسترسی</label>
                            <div class="checkbox-group">
                                @foreach($permissions as $permission)
                                    <div class="">
                                        <input type="checkbox" id="permission_{{$permission->id}}" class="filled-in checkbox-permission" value="{{$permission->id}}" @if(!empty(@$admin->permissions[0])) @if(@$admin->permissions->contains('permission_id',$permission->id)) checked @endif @endif>
                                        <label for="permission_{{$permission->id}}">{{$permission->name}}</label>
                                    </div>
                                @endforeach
                                @if ($errors->has('permissions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('permissions') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$admin)ذخیره@elseثبت@endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <script>
        var fileName;
        var permissions = [];
        $(function () {
            var fileName;
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            permissions = [];
            $('.checkbox-permission:checked').each(function(i){
                permissions[i] = $(this).val();
            });
            $.ajax(
                {
                    url: '@if(@$admin){{route('admin.admins.update',['id'=>$admin->id])}} @else {{route('admin.admins.store')}}@endif',
                    type: @if(@$admin)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$admin)"id": {{$admin->id}},@endif
                        "_method": 'post',
                        "name": $('#tbName').val(),
                        "family": $('#tbFamily').val(),
                        "mobile": $('#tbMobile').val(),
                        "email": $('#tbEmail').val(),
                        "password": $('#tbPassword').val(),
                        "password_confirmation": $('#tbConfirmPassword').val(),
                        "permissions": permissions,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$admin)آیتم با موفقیت ویرایش شد!@elseآیتم با موفقیت افزوده شد!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>خطایی رخ داد، آیتم ذخیره نشد!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection