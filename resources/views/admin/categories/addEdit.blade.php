@extends('admin.layouts.panel')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title',' Category')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$category)
                            Edit Category
                        @else
                            Add Category
                        @endif
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('title_en') ? ' has-error' : '' }}">
                            <label for="tbTitle">Title En</label>
                            <div class="form-line">
                                <input type="text" id="tbTitleEn" class="form-control" value="@if(@$category){{$category->title_en}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('title_ar') ? ' has-error' : '' }}">
                            <label for="tbTitle">Title Ar</label>
                            <div class="form-line">
                                <input type="text" id="tbTitleAr" class="form-control" value="@if(@$category){{$category->title_ar}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="sbParent">Parent Category</label>
                            <div class="form-line">
                                <select class="form-control" id="sbParent" >
                                    <option value="0"> No Parent</option>
                                    @foreach(\App\Models\Category::where('active',1)->get() as $cat)
                                        <option value="{{$cat->id}}" @if(@$category) @if($category->parent_id == $cat->id) selected="selected" @endif @endif >
                                            {{$cat->title_en}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                            <label for="tbSlug">Slug</label>
                            <div class="form-line">
                                <input type="text" id="tbSlug" class="form-control" value="@if(@$product){{$product->slug}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('slug'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('meta_keywords') ? ' has-error' : '' }}">
                            <label for="tbMeta">Meta Keywords</label>
                            <div class="form-line">
                                <input type="text" id="tbMeta" class="form-control" value="@if(@$product){{$product->meta_keywords}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('meta_keywords'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('meta_keywords') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="tbDesc">Description Meta</label>
                            <div class="form-line">
                                <input type="text" id="tbDesc" class="form-control" value="@if(@$product){{$product->description}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }} clearfix">
                            <label>Status</label>
                            <div>
                                <input type="checkbox" id="chbActive" class="filled-in" @if(@$category->active) checked @endif>
                                <label for="chbActive">Active</label>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('show_sidebar') ? ' has-error' : '' }} clearfix">
                            <label>Show in sidebar</label>
                            <div>
                                <input type="checkbox" id="chShowSidebar" class="filled-in" @if(@$category->show_sidebar) checked @endif>
                                <label for="chShowSidebar">Show</label>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('show_index') ? ' has-error' : '' }} clearfix">
                            <label>Show index</label>
                            <div>
                                <input type="checkbox" id="cbShowIndex" class="filled-in" @if(@$category->show_index) checked @endif>
                                <label for="cbShowIndex">Show</label>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }} clearfix">
                            <label>Is this cat for Group buy?</label>
                            <div>
                                <input type="checkbox" id="chType" class="filled-in" @if(@$category->type) checked @endif>
                                <label for="chType">Group buy</label>
                            </div>
                        </div>
                        @if(isset($category->image))
                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label> Image</label>
                                <div class="row">
                                    <div class="col-xs-6 col-md-3">
                                        <a href="javascript:void(0);" class="thumbnail">
                                            <img src="{{asset('uploads/images/categories/').'/'.$category->image}}" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                    <form class="dropzone" id="fileUpload"></form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$category)Save @else Save @endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <script>
        var fileName;
        Dropzone.autoDiscover = false;
        $(function () {
            var fileName;
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
            $(".dropzone").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 1,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = $.parseJSON( responseText );
                        window.fileName = t.name;
                        console.log(fileName);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$category){{route('admin.categories.update',$category->id)}} @else {{route('admin.categories.store')}}@endif',
                    type: @if(@$category)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$category)"id": {{$category->id}},@endif
                        "_method": 'post',
                        "title_en": $('#tbTitleEn').val(),
                        "title_ar": $('#tbTitleAr').val(),
                        "show_sidebar":$('#chShowSidebar').length>0 ? 1:0,
                        "show_index":$('#cbShowIndex').length>0? 1:0,
                        "meta": $('#tbMeta').val(),
                        "desc": $('#tbDesc').val(),
                        "slug": $('#tbSlug').val(),
                        "parent_id":$('#sbParent').val(),
                        "image": fileName,
                        "active": ($('#chbActive:checked').length)? 1 : 0,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$category)Saved!@else Saved!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
