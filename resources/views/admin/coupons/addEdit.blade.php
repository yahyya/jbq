@extends('admin.layouts.panel')

@section('style')
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('admin-asset/plugins/datepicker/datepicker3.css')}}">

    <!-- Dropzone -->
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title','کوپن ها')
@section('subtitle','زیر عنوان')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$coupon)
                            ویرایش کوپن
                        @else
                            افزودن کوپن
                        @endif
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="tbCode">کد تخفیف</label>
                            <div class="form-line">
                                <input type="text" id="tbCode" class="form-control" value="@if(@$coupon){{$coupon->code}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                            <label for="tbValue">مقدار</label>
                            <div class="form-line">
                                <input type="text" id="tbValue" class="form-control" value="@if(@$coupon){{$coupon->value}}@endif" placeholder="" required>
                                @if ($errors->has('value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="tbValue">نوع کوپن</label>
                            <div class="switch">
                                <label>درصد<input type="checkbox" id="chbType" @if(@$coupon->type) checked @endif><span class="lever"></span>وجه نقد</label>
                            </div>
                            @if ($errors->has('type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                            <label>تاریخ شروع</label>
                            <div class="form-line">
                                <input type="text" id="startDate" class="form-control" value="@if(@$coupon){{\Morilog\Jalali\Jalalian::forge($coupon->start_date)->format('Y-m-d')}}@endif" placeholder="" required>
                                {{--<input type="hidden" id="tbType" value="@if(@$delivery){{$delivery->type}}@else{{$flag}}@endif">--}}
                                @if ($errors->has('start_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                            <label>تاریخ پایان</label>
                            <div class="form-line">
                                <input type="text" id="endDate" class="form-control" value="@if(@$coupon){{\Morilog\Jalali\Jalalian::forge($coupon->end_date)->format('Y-m-d')}}@endif" placeholder="" required>
                                {{--<input type="hidden" id="tbType" value="@if(@$delivery){{$delivery->type}}@else{{$flag}}@endif">--}}
                                @if ($errors->has('end_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('count') ? ' has-error' : '' }}">
                            <label for="tbCount">تعداد</label>
                            <div class="form-line">
                                <input type="text" id="tbCount" class="form-control" value="@if(@$coupon){{$coupon->count}}@endif" placeholder="" required>
                                @if ($errors->has('count'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('count') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$coupon)ذخیره@elseثبت@endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Date Picker -->
    <script src="{{URL::asset('js/datepicker/jquery.ui.datepicker-cc.js')}}"></script>
    <script src="{{URL::asset('js/datepicker/calendar.js')}}"></script>
    <script src="{{URL::asset('js/datepicker/jquery.ui.datepicker-cc-fa.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <script>
        var table;
        $(function () {
            $('#datePicker').datepicker({dateFormat: 'yy-mm-dd'});
            $('#startDate').datepicker({dateFormat: 'yy-mm-dd'});
            $('#endDate').datepicker({dateFormat: 'yy-mm-dd'});
            $('#form').on('form:submit',function () {
                return false;
            });

            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });

            $('.filterDateRangeBtn').on('click',function () {
                filterDateRange();
            });

            $(".dropzone").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 1,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = $.parseJSON( responseText );
                        window.fileName = t.name;
                        console.log(fileName);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });

        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$coupon){{route('admin.coupons.update',['id'=>$coupon->id])}} @else {{route('admin.coupons.store')}}@endif',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: @if(@$coupon)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$coupon)"id": {{$coupon->id}},@endif
                        "_method": 'post',
                        "code": $('#tbCode').val(),
                        "value": $('#tbValue').val(),
                        "type": ($('#chbType:checked').length)? 1 : 0,
                        "start_date": $('#startDate').val(),
                        "end_date": $('#endDate').val(),
                        "count": $('#tbCount').val(),

                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$coupon)آیتم با موفقیت ویرایش شد!@elseآیتم با موفقیت افزوده شد!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>خطایی رخ داد، آیتم ذخیره نشد!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection