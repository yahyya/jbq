@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Select -->
    <link href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <!-- Datatables RowReorder -->
    <link href="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.css') }}" rel="stylesheet">
@endsection

@section('title','Products')
@section('subtitle',' ')

@section('content')

    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
                        Products
                    </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.products.create') }}" class="btn btn-primary waves-effect">Add Product</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="sbCategory">Category : </label>
                                <select name="category" id="sbCategory" class="form-control">
                                    <option value="">All</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title_en}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="sbCategory">Status : </label>
                                <select name="status" id="sbStatus" class="form-control">
                                    <option value="">All</option>
                                    <option value="1">Active</option>
                                    <option value="0">Not Active</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive clearfix">
                        <table id="datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Total</th>
                                <th>sold</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Total</th>
                                <th>sold</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <script>
        var table;
        $(function () {
            $('.selectpicker').selectpicker();
            $('#sbCategory').on('change', function(e) {
                table.draw();
            });
            $('#sbStatus').on('change', function(e) {
                table.draw();
            });

            table = $('#datatable').DataTable({

                pageLength:100,
                responsive: true,
                processing: true,
                serverSide: true,
                rowReorder: true,
                columnDefs: [
                    { orderable: true, className: 'reorder', targets: 0 },
                    { orderable: false, targets: '_all' }
                ],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.group.index')}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    data:{
                        categoryId : function(){return $('#sbCategory').val();},
                        status:function(){return $('#sbStatus').val()}
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'image', name: 'image'},
                    {data: 'title_en', name: 'title_en'},
                    {data: 'price', name: 'price'},
                    {data: 'category.title_en', name: 'category.title_en'},
                    {data: 'total', name: 'total'},
                    {data: 'sold', name: 'sold'},
                    {data: 'active', name: 'active'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });


            table.on( 'row-reorder', function ( e, diff, edit ) {
                var products = [];
                for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                    var product = {};
                    var rowData = table.row( diff[i].node ).data();
                    product.id = parseInt(rowData.id);
                    product.newOrder = parseInt(rowData.order) + (parseInt(diff[i].oldPosition) - parseInt(diff[i].newPosition));
                    products.push(product);
                }


            });
        });

        function deleteItem(id) {
            console.log('delete');
            var confirm = window.confirm('Are you sure?');
            if (confirm) {
                $.ajax({
                    url: '{{route('admin.group.index')}}/' + id,
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'delete',
                    dataType: "JSON",
                    data: {
                        "id": id,
                        "_method": 'DELETE'
                    },
                    success: function (res) {
                        if (res.res) {
                            table.draw();
                        }
                        $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed!</div>');
                        window.setTimeout(function () {
                            $(".alert").alert('close');
                        }, 3000);
                    },
                    error: function (xhr, stat) {
                        $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                        window.setTimeout(function () {
                            $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                $(this).remove();
                            });
                        }, 3000);
                        console.log(stat);
                    }
                });
            }
        }
    </script>

@endsection
