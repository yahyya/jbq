@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Select Css -->
    <link  rel="stylesheet" href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title','Products')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$product)
                            Edit Product
                        @else
                            Add Product
                        @endif
                    </h2>

                </div>
                <div class="body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#details" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">view_list</i>product Info
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#winner" data-toggle="tab" aria-expanded="true">
                                <i class="material-icons">comment</i>winner
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active show" id="details">
                            <form id="form">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="email_address">Title En</label>
                                    <div class="form-line">
                                        <input type="text" id="tbTitleEn" class="form-control" value="@if(@$product){{$product->title_en}}@endif" placeholder="" required autofocus>
                                        @if ($errors->has('title_en'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title_en') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('title_ar') ? ' has-error' : '' }}">
                                    <label for="email_address">Title Ar</label>
                                    <div class="form-line">
                                        <input type="text" id="tbTitleAr" class="form-control" value="@if(@$product){{$product->title_ar}}@endif" placeholder="" required autofocus>
                                        @if ($errors->has('title_ar'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title_ar') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                    <label for="email_address">Category </label>
                                    <div class="form-line">
                                        <select id="sbCategory" class="form-control " required>
                                            @foreach(@$categories as $category)
                                            <option value="{{$category->id}}" @if($category->id == @$product->category_id) selected @endif>{{$category->title_en}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }} clearfix">
                                    <label>Product Status</label>
                                    <div>
                                        <input type="checkbox" id="chbActive" class="filled-in" @if(@$product->active) checked @endif>
                                        <label for="chbActive">Active</label>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label for="tbPrice">Price</label>
                                    <div class="form-line">
                                        <input type="text" id="tbPrice" class="form-control" value="@if(@$product){{$product->price}}@endif" placeholder="" required>
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('fixed_price') ? ' has-error' : '' }}">
                                    <label for="tbfixedPrice">Final Price</label>
                                    <div class="form-line">
                                        <input type="text" id="tbfixedPrice" class="form-control" value="@if(@$product){{$product->fixed_price}}@endif" placeholder="" required>
                                        @if ($errors->has('fixed_price'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('fixed_price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('total') ? ' has-error' : '' }}">
                                    <label for="tbTotal">If Group buy , please insert the total number to buy</label>
                                    <div class="form-line">
                                        <input type="text" id="tbTotal" class="form-control" value="@if(@$product){{$product->total}}@endif" placeholder="" required>
                                        @if ($errors->has('total'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('total') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

        {{--                        <div class="form-group{{ $errors->has('breads') ? ' has-error' : '' }} clearfix">--}}
        {{--                            <label for="tbBread">نان ها</label>--}}
        {{--                            <div class="checkbox-group">--}}
        {{--                                @foreach($breads as $bread)--}}
        {{--                                    <div class="col-xs-6 col-md-2">--}}
        {{--                                        <input type="checkbox" id="bread_{{$bread->id}}" class="filled-in checkbox-bread" value="{{$bread->id}}" @if(!empty(@$product->breads[0])) @if(@$product->breads->contains('bread_id',$bread->id)) checked @endif @endif>--}}
        {{--                                        <label for="bread_{{$bread->id}}">{{$bread->title}}<img src="{{asset('uploads/images/breads/').'/'.$bread->image}}" class="img-responsive"></label>--}}
        {{--                                    </div>--}}
        {{--                                @endforeach--}}
        {{--                                @if ($errors->has('breads'))--}}
        {{--                                    <span class="help-block">--}}
        {{--                                        <strong>{{ $errors->first('breads') }}</strong>--}}
        {{--                                    </span>--}}
        {{--                                @endif--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
                                {{--<div class="form-group{{ $errors->has('order') ? ' has-error' : '' }}">--}}
                                    {{--<label for="tbOrder">ترتیب / اولویت نمایش</label>--}}
                                    {{--<div class="form-line">--}}
                                        {{--<input type="number" id="tbOrder" class="form-control" value="@if(@$product){{$product->order}}@else{{1}}@endif" placeholder="" required>--}}
                                        {{--@if ($errors->has('order'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--<strong>{{ $errors->first('order') }}</strong>--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group{{ $errors->has('desc_en') ? ' has-error' : '' }}">
                                    <label for="taDesc">Description En</label>
                                    <div class="form-line">
                                        <textarea rows="4" id="taDescEn" class="form-control no-resize" placeholder="Description ....">@if(@$product){{$product->desc_en}}@endif</textarea>
                                        @if ($errors->has('desc_en'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('desc_en') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('desc_ar') ? ' has-error' : '' }}">
                                    <label for="taDesc">Description Ar</label>
                                    <div class="form-line">
                                        <textarea rows="4" id="taDescAr" class="form-control no-resize" placeholder="Description ....">@if(@$product){{$product->desc_ar}}@endif</textarea>
                                        @if ($errors->has('desc_ar'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('desc_ar') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                @if(isset($product->images[0]))
                                    <div class="form-group{{ $errors->has('images') ? ' has-error' : '' }}">
                                        <label for="email_address"> Image</label>
                                        <div class="row product-pics">
                                            @foreach(@$product->images as $image)
                                            <div class="col-xs-6 col-md-3">
                                                <img src="{{$image->file_name}}" class="img-responsive">
                                                <button type="button" onclick="deletePic({{$image->id}})" class="btn btn-danger m-t-15 waves-effect">Remove Image</button>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </form>
                            <form class="dropzone" id="fileUpload"></form>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$product)Save @else Save @endif</button>
                        </div>
                        <div role="tabpanel" class="tab-pane fade " id="winner">

                            @if($product->sold<$product->total)
                                <h2>
                                    You can not select winner because sold numbers are lower than total needed
                                </h2>

                            @else
                                <button onclick="selectWinner()">Select Winner</button>
                            @endif

                            <div id="winnerData" >

                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <!-- Select Plugin Js -->
{{--    <script src="{{asset('admin-asset/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>--}}

    <!-- Custom Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;

        var fileName = [];
        var breads = [];

        function selectWinner(){
            $.ajax(
                {
                    url: '{{route('admin.group.winner',$product->id)}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "JSON",
                    data: {
                    },
                    success: function (res) {
                        console.log(res);
                       $('#winnerData').html('<a href="{{url('/admin/user/')}}/'+res.id+'" >'+res.name + ' ' + res.family+'</a>');
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error<br>' + res.msg + '</div>');
                    }
                });
        }
        $(function () {
//            $('#form').parsley({successClass: '', errorClass: 'alert-error'}).on('field:validated', function () {
//                var ok = $('.parsley-error').length === 0;
//                $('.info').toggleClass('hidden', !ok);
//                $('.warning').toggleClass('hidden', ok);
//            })
//                .on('form:submit', function () {
//                    return false; // Don't submit form for this demo
//                });
            $('#form').on('form:submit',function () {
                return false;
            });

            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });

            $("#fileUpload").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 3,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = responseText ;
                        fileName.push(t.name);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $('.checkbox-bread:checked').each(function(i){
                breads[i] = $(this).val();
            });
            $.ajax(
                {
                    url: '@if(@$product){{route('admin.products.update',$product->id)}} @else {{route('admin.products.store')}}@endif',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: @if(@$product)'put'@else'post' @endif,
                    dataType: "JSON",
                    data: {
                        @if(@$product)"id": {{$product->id}},@endif
                        "_method": 'post',
                        "title_en": $('#tbTitleEn').val(),
                        "title_ar": $('#tbTitleAr').val(),
                        "total": $('#tbTotal').val(),
                        "category_id": $('#sbCategory').val(),
                        "price": $('#tbPrice').val(),
                        "fixed_price": $('#tbfixedPrice').val(),
//                        "order": $('#tbOrder').val(),
                        "desc_en": $('#taDescEn').val(),
                        "desc_ar": $('#taDescAr').val(),
                        "active": ($('#chbActive:checked').length)? 1 : 0,
                        "images": fileName,
                        "breads": breads,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$product) Item Edited @else Item Added @endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error<br>' + res.msg + '</div>');
                    }
                });
        }

        function deletePic(id) {
            var confirm = window.confirm('Are You Sure ?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{@route('admin.products.index')}}/pic/'+id+'/delete',
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                location.reload();
                            }
                            $('.product-pics').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.product-pics').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>

@endsection
