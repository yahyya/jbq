@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Select -->
    <link href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet">
@endsection

@section('title','پیشنهادهای ویژه')
@section('subtitle','زیر عنوان')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$offer)ویرایش پیشنهاد ویژه@elseافزودن پیشنهادهای ویژه@endif
                    </h2>
                </div>
                <div class="body">
                    <form id="form">

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="tbType">نوع</label>
                            {{--<div class="form-line">--}}
                                {{--<input type="text" id="tbType" class="form-control" value="@if(@$offer){{$offer->type}}@endif" placeholder="" required autofocus>--}}
                                {{--@if ($errors->has('type'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('type') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            <div class="switch">
                                <label>کالا<input type="checkbox" id="chbType" @if(@$offer) @if(@$offer->type) checked @endif @else checked @endif autofocus><span class="lever"></span>دسته</label>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                                    <label for="sbProduct">کالا</label>
                                    <select id="sbProduct" class="form-control selectpicker show-tick" data-live-search="true" title="انتخاب محصول..." @if(@$offer) @if(!@$offer->type) disabled @endif @endif>
                                        @if(@$products)
                                            @foreach($products as $product)
                                                <option value="{{$product->id}}" @if($product->id == @$offer->product_id) selected @endif>{{$product->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('product_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('product_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                    <label for="sbCategory">دسته</label>
                                    <select id="sbCategory" class="form-control selectpicker show-tick" data-live-search="true" title="انتخاب دسته..." @if(@$offer) @if(@$offer->type) disabled @endif @else disabled @endif>
                                        @if(@$categories)
                                            @foreach(@$categories as $category)
                                                <option value="{{$category->id}}" @if($category->id == @$offer->category_id) selected @endif>{{$category->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('category_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="tbAmount">حداقل خرید</label>
                            <div class="form-line">
                                <input type="text" id="tbAmount" class="form-control" value="@if(@$offer){{$offer->amount}}@endif" placeholder="" required>
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('percent') ? ' has-error' : '' }}">
                            <label for="tbPercent">درصد</label>
                            <div class="form-line">
                                <input type="text" id="tbPercent" class="form-control" value="@if(@$offer){{$offer->percent}}@endif" placeholder="" required>
                                @if ($errors->has('percent'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('percent') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$offer)ذخیره@elseثبت@endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Bootstrap Select Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <!-- Custom Js -->
    <script>
        $(function () {
//            if($('#chbType').is(':checked')){
//                $("#sbProduct").removeAttr('disabled');
//                $("#sbCategory").prop('disabled', false);
//            } else {
//                $("#sbProduct").prop('disabled', false);
//                $("#sbCategory").removeAttr('disabled');
//            }

            $('#chbType').on('change',function () {
                if($('#chbType').is(':checked')){
                    console.log('checked');
                    $("#sbProduct").prop('disabled', false);
                    $("#sbCategory").prop('disabled', true);
                    $('#sbProduct').selectpicker('refresh');
                    $('#sbCategory').selectpicker('refresh');
                } else {
                    console.log('unchecked');
                    $("#sbProduct").prop('disabled', true);
                    $("#sbCategory").prop('disabled', false);
                    $('#sbProduct').selectpicker('refresh');
                    $('#sbCategory').selectpicker('refresh');
                }
            });
            $('#form').on('form:submit',function () {
                return false;
            });
            $('.saveBtn').on('click', function () {
                    saveItem();
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$offer){{route('admin.offers.update',['id'=>$offer->id])}} @else {{route('admin.offers.store')}}@endif',
                    type: @if(@$offer)'put'@else'post'@endif,
                    dataType: "JSON",
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    data: {
                        @if(@$offer)"id": {{$offer->id}},@endif
                        "_method": 'post',
                        "type": ($('#chbType:checked').length)? 1 : 0,
                        "product_id": $('#sbProduct').val(),
                        "category_id": $('#sbCategory').val(),
                        "amount": $('#tbAmount').val(),
                        "percent": $('#tbPercent').val(),
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$offer)آیتم با موفقیت ویرایش شد!@elseآیتم با موفقیت افزوده شد!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>خطایی رخ داد، آیتم ذخیره نشد!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection