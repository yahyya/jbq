@extends('admin.layouts.panel')

@section('title','Dashboard')
@section('subtitle','Subtitle')

@section('style')
    <!-- Morris Chart Css-->
    <link href="{{ URL::asset('admin-asset/plugins/morrisjs/morris.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="modal fade" id="modal-desc" role="dialog" aria-labelledby="showDesc">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Order Desc.</h4>
                </div>
                <div id="modal-desc-value" class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Widgets -->
{{--    <div class="row clearfix">--}}
{{--        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
{{--            <div class="info-box bg-pink hover-expand-effect">--}}
{{--                <div class="icon">--}}
{{--                    <i class="material-icons">playlist_add_check</i>--}}
{{--                </div>--}}
{{--                <div class="content">--}}
{{--                    <div class="text">NEW TASKS</div>--}}
{{--                    <div class="number count-to" data-from="0" data-to="125" data-speed="15"--}}
{{--                         data-fresh-interval="20"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
{{--            <div class="info-box bg-cyan hover-expand-effect">--}}
{{--                <div class="icon">--}}
{{--                    <i class="material-icons">help</i>--}}
{{--                </div>--}}
{{--                <div class="content">--}}
{{--                    <div class="text">NEW TICKETS</div>--}}
{{--                    <div class="number count-to" data-from="0" data-to="257" data-speed="1000"--}}
{{--                         data-fresh-interval="20"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
{{--            <div class="info-box bg-light-green hover-expand-effect">--}}
{{--                <div class="icon">--}}
{{--                    <i class="material-icons">forum</i>--}}
{{--                </div>--}}
{{--                <div class="content">--}}
{{--                    <div class="text">NEW COMMENTS</div>--}}
{{--                    <div class="number count-to" data-from="0" data-to="243" data-speed="1000"--}}
{{--                         data-fresh-interval="20"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
{{--            <div class="info-box bg-orange hover-expand-effect">--}}
{{--                <div class="icon">--}}
{{--                    <i class="material-icons">person_add</i>--}}
{{--                </div>--}}
{{--                <div class="content">--}}
{{--                    <div class="text">NEW VISITORS</div>--}}
{{--                    <div class="number count-to" data-from="0" data-to="1225" data-speed="1000"--}}
{{--                         data-fresh-interval="20"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- #END# Widgets -->
    <!-- Custom Product Status -->
    <div class="row clearfix">


        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="header">
                    <h2>Database</h2>
                </div>
                <div class="body">
                    <div class="form-group">
                        <a href="{{route('admin.db.fullBackup')}}" class="btn btn-primary">Backup Database</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Custom Product Status -->
    <!-- CPU Usage -->
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                            <h2>Orders</h2>
                        </div>
                        {{--<div class="col-xs-12 col-sm-6 align-left">--}}
                        {{--<div class="switch panel-switch-btn">--}}
                        {{--<span class="m-r-10 font-12">REAL TIME</span>--}}
                        {{--<label>OFF<input type="checkbox" id="realtime" checked><span class="lever switch-col-cyan"></span>ON</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="nav-item">
                            <a class="nav-link active" href="#new" aria-controls="new" role="tab" data-toggle="tab">New <span id="newOrdersCount" class="badge">0</span></a></li>
                        <li role="presentation" class="nav-item ">
                            <a class="nav-link" href="#preparing" aria-controls="preparing" role="tab" data-toggle="tab">Preparing <span id="preparingOrdersCount" class="badge">0</span></a></li>
                        <li role="presentation" class="nav-item ">
                            <a class="nav-link" href="#sent" aria-controls="sent" role="tab" data-toggle="tab">Sent<span id="sentOrdersCount" class="badge">0</span></a></li>
                        <li role="presentation" class="nav-item ">
                            <a class="nav-link" href="#delivered" aria-controls="delivered" role="tab" data-toggle="tab">Delivered<span id="deliveredOrdersCount" class="badge">0</span></a></li>
                        <li role="presentation" class="nav-item ">
                            <a class="nav-link" href="#canceled" aria-controls="canceled" role="tab" data-toggle="tab">Canceled<span id="canceledOrdersCount" class="badge">0</span></a></li>
                        <li role="presentation" class="nav-item ">
                            <a class="nav-link" href="#leaved" aria-controls="leaved" role="tab" data-toggle="tab">Not checked<span id="leavedOrdersCount" class="badge">0</span></a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="new">
                            <div class="table-responsive">
                                <table id="newOrders" data-status="0" class="orders-table table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Data</th>
                                            <th>Time</th>
                                            <th>Invoice No.</th>
                                            <th>User</th>
                                            {{--<th>پیک</th>--}}
                                            {{--<th>مبلغ</th>--}}
                                            {{--<th>تاریخ دریافت</th>--}}
                                            <th>Payment</th>
                                            <th>Total</th>
                                            {{--<th>توضیحات</th>--}}
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>id</th>
                                            <th>Data</th>
                                            <th>Time</th>
                                            <th>Invoice No.</th>
                                            <th>User</th>
                                            {{--<th>پیک</th>--}}
                                            {{--<th>مبلغ</th>--}}
                                            {{--<th>تاریخ دریافت</th>--}}
                                            <th>Payment</th>
                                            <th>Total</th>
                                            {{--<th>توضیحات</th>--}}
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="preparing">
                            <div class="table-responsive">
                                <table id="preparingOrders" data-status="1" class="orders-table table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="sent">
                            <div class="table-responsive">
                                <table id="sentOrders" data-status="2" class="orders-table table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="delivered">
                            <div class="table-responsive">
                                <table id="deliveredOrders" data-status="3" class="orders-table table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="canceled">
                            <div class="table-responsive">
                                <table id="canceledOrders" data-status="4" class="orders-table table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="leaved">
                            <div class="table-responsive">
                                <table id="leavedOrders" data-status="-1" class="orders-table table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Data</th>
                                        <th>Time</th>
                                        <th>Invoice No.</th>
                                        <th>User</th>
                                        {{--<th>پیک</th>--}}
                                        {{--<th>مبلغ</th>--}}
                                        {{--<th>تاریخ دریافت</th>--}}
                                        <th>Payment</th>
                                        <th>Total</th>
                                        {{--<th>توضیحات</th>--}}
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script>
        // Notificaiton :
        //
        //
        document.addEventListener('DOMContentLoaded', function () {
          if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.');
            return;
          }

          if (Notification.permission !== "granted")
            Notification.requestPermission();
        });

        function notifyMe() {
          if (Notification.permission !== "granted")
            Notification.requestPermission();
          else {
            var notification = new Notification('Notification title', {
              icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
              body: "Hey there! You've been notified!",
            });

            notification.onclick = function () {
              window.open("http://stackoverflow.com/a/13328397/1269037");
            };

          }

        }

        {{----}}

        function showDesc(desc){
            $('#modal-desc-value').html(desc);
            $('#modal-desc').modal('show');
        }
        var dataTables = [];
        $(function () {

            $('.nav-tabs li').on('click',function () {
               $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            });
            $('#chbCustomProductSalesStatus').change(function () {
                updateCustomProductSalesStatus();
            });

            $('#chbSalesStatus').change(function () {
                updateSalesStatus();
            });
            orderCounts();

            $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            } );

            $('.orders-table').each(function () {
                var status = $(this).attr("data-status");
                var obj = $(this).DataTable({

                    pageLength: 100,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    ajax: {
                        url: '{{route('admin.orders.index')}}',
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        method: 'GET',
                        data:{status:status}
                    },
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'created_at_time', name: 'created_at_time'},
                        {data: 'tracking_code', name: 'tracking_code'},
                        {data: 'user_id', name: 'user_id'},
                        // {data: 'courier_id', name: 'courier_id'},
//                    {data: 'status_id', name: 'status_id'},
//                    {data: 'recieve_date', name: 'recieve_date'},
                        {data: 'payment_type', name: 'payment_type'},
                        {data: 'total', name: 'total'},
                        // {data: 'desc', name: 'desc'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                dataTables.push(obj);
            });


        });

        function updateSalesStatus() {
            $.ajax(
                {
                    url: '{{route('admin.salesStatus')}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "status": ($('#chbSalesStatus:checked').length)? 1 : 0,
                    },
                    success: function (res) {
                        if (res.res) {
                            $(this).closest('.alert').remove();
                            $(this).closest('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Updated Order Status!</div>');
                        } else {
                            $(this).closest('.alert').remove();
                            $(this).closest('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error On Updating Status!</div>');
                        }
                        setTimeout(function(){
                            $(this).closest('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error Accured!<br>' + res.msg + '</div>');
                    }
                });
        }

        function updateCustomProductSalesStatus() {
            $.ajax(
                {
                    url: '{{route('admin.customProductSalesStatus')}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "status": ($('#chbCustomProductSalesStatus:checked').length)? 1 : 0,
                    },
                    success: function (res) {
                        if (res.res) {
                            $(this).closest('.alert').remove();
                            $(this).closest('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Status of Custom Order Updated!</div>');
                        } else {
                            $(this).closest('.alert').remove();
                            $(this).closest('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Custom Order Not Updated!</div>');
                        }
                        setTimeout(function(){
                            $(this).closest('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error Accured!<br>' + res.msg + '</div>');
                    }
                });
        }

        function orderCounts() {
            $.ajax(
                {
                    url: '{{route('admin.dashboard.orderCounts')}}',
                    type: 'get',
                    dataType: "JSON",
                    data: {
                        "_method": 'get'
                    },
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    success: function (res) {
                        $("#newOrdersCount").text(res.newOrdersCount);
                        $("#preparingOrdersCount").text(res.preparingOrdersCount);
                        $("#sentOrdersCount").text(res.sentOrdersCount);
                        $("#deliveredOrdersCount").text(res.deliveredOrdersCount);
                        $("#canceledOrdersCount").text(res.canceledOrdersCount);
                        $("#leavedOrdersCount").text(res.leavedOrdersCount);
                    },
                    error: function (xhr, stat) {
                        $(this).closest('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error on getting total orders!</div>');
                        window.setTimeout(function () {
                            $(this).closest(".alert").fadeTo(500, 0).slideUp(500, function () {
                                $(this).remove();
                            });
                        }, 3000);
                        console.log(stat);
                    }
                });
        }
        function deleteItem(id) {
            var confirm = window.confirm('Are You sure you want to delete this?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{route('admin.orders.index')}}/' + id,
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        success: function (res) {
                            if (res.res) {
                                table.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed Successfully!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>
@endsection
