@extends('admin.layouts.panel')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title',' Category')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$slider)
                           Edit Slider
                        @else
                            Add Slider
                        @endif
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="tbTitle">Title </label>
                            <div class="form-line">
                                <input type="text" id="tbTitle" class="form-control" value="@if(@$slider){{$slider->title}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                            <label for="sbProduct">Product</label>
                            <div class="form-line">
                                <select class="form-control" id="sbProduct" >
                                    @foreach(\App\Models\Product::where('active',1)->get() as $pr)
                                        <option value="{{$pr->id}}" @if(@$slider) @if($slider->product_id == $pr->id) selected="selected" @endif @endif >
                                            {{$pr->title_en}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('desc_en') ? ' has-error' : '' }}">
                            <label for="tbDescEn">Desc en</label>
                            <div class="form-line">
                                <textarea id="tbDescEn" name="tbDescEn" class="form-control" value="@if(@$slider){{$slider->desc_en}}@endif" placeholder="" required autofocus>
                                    @if(@$slider) {{$slider->desc_en}} @endif
                                </textarea>
                                @if ($errors->has('desc_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('desc_ar') ? ' has-error' : '' }}">
                            <label for="tbDescAr">Desc Ar</label>
                            <div class="form-line">
                                <textarea id="tbDescAr" name="tbDescAr" class="form-control" value="@if(@$slider){{$slider->desc_ar}}@endif" placeholder="" required autofocus>
                                    @if(@$slider) {{$slider->desc_ar}} @endif
                                </textarea>
                                @if ($errors->has('desc_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(!empty($slider->file_name))
                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label> Image</label>
                                <div class="row">
                                    <div class="col-xs-6 col-md-3">
                                        <a href="javascript:void(0);" class="thumbnail">
                                            <img src="{{asset('upload/slider/').'/'.$slider->file_name}}" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                    <form class="dropzone" id="fileUpload"></form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$slider)Save @else Save @endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <script src="https://cdn.ckeditor.com/4.17.1/full/ckeditor.js"></script>
    <script>
        var fileName;
        Dropzone.autoDiscover = false;
        $(function () {
            CKEDITOR.replace( 'tbDescAr' );
            CKEDITOR.replace( 'tbDescEn' );
            var fileName;
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
            $(".dropzone").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 1,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        console.log(responseText);
                        var t =  responseText ;
                        window.fileName = t.name;
                        console.log(fileName);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$slider){{route('admin.sliders.update',$slider->id)}} @else {{route('admin.sliders.store')}}@endif',
                    type: @if(@$slider)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$slider)"id": {{$slider->id}},@endif
                        "_method": 'post',
                        "title": $('#tbTitle').val(),
                        "desc_ar": function(){ return CKEDITOR.instances.tbDescAr.getData();},
                        "desc_en": function(){ return CKEDITOR.instances.tbDescEn.getData();},
                        "product_id":$('#sbProduct').val(),
                        "image": fileName,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$category)Saved!@else Saved!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
