@extends('admin.layouts.header')

<body class="login-page">
<div class="login-box">
    <div class="logo">
        <img src="{{asset('images/logo.png')}}" />
    </div>
    <div class="card">
        <div class="body pb-0">
            <form id="sign_in" method="POST" action="{{route('admin.login')}}" style="margin-bottom:5px;">
                {{ csrf_field() }}
                <div class="form-group{{$errors->has('email')? 'has-error' : ''}}">
                    <div class="input-group">
                        <span class="input-group-addon" style="position: absolute;top:10px;z-index: 1">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line ml-auto" style="width:90%">
                            <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group{{$errors->has('password')? 'has-error' : ''}}">
                    <div class="input-group">
                        <span class="input-group-addon" style="position: absolute;top:10px;z-index: 1">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line ml-auto" style="width:90%">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="checkbox" name="remember" id="remember" class="filled-in chk-col-pink" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember">Remmember Me</label>
                    </div>

                </div>
                <div class="row">
                    <div class="w-100">
                        <button class="btn btn-block bg-pink waves-effect w-100" type="submit">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@extends('admin.layouts.footer')

@section('script')
    <script src="{{asset('admin-asset/js/pages/examples/sign-in.js')}}"></script>
@endsection
