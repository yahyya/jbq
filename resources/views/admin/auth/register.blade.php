@extends('layouts.header')

<body class="signup-page">
<div class="signup-box">
    <div class="logo">
        <a href="javascript:void(0);">همبر<b>آنلاین</b></a>
        <small>سفارش آنلاین خوراک</small>
    </div>
    <div class="card">
        <div class="body">
            <form id="sign_up" method="POST" action="{{ route('register') }}">
                <div class="msg">عضو شوید و همبر خودتان را سفارش دهید</div>
                {{ csrf_field() }}
                <div class="input-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input id="name" type="text" class="form-control" name="name" placeholder="نام" value="{{ old('name') }}" required autofocus>
                        {{--@if ($errors->has('name'))--}}
                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                        {{--@endif--}}
                    </div>
                </div>
                <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="ایمیل" required>
                        {{--@if ($errors->has('email'))--}}
                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                        {{--@endif--}}
                    </div>
                </div>
                <div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input id="password" type="password" class="form-control" name="password" minlength="6" placeholder="رمز عبور" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="6" placeholder="تایید رمز عبور" required>
                    </div>
                </div>
                {{--<div class="form-group">--}}
                    {{--<input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">--}}
                    {{--<label for="terms"><a href="javascript:void(0);">قوانین استفاده</a> را خوانده ام و می پذیرم.</label>--}}
                {{--</div>--}}

                <button class="btn btn-block btn-lg bg-black waves-effect" type="submit">ثبت نام</button>

                <div class="m-t-25 m-b--5 align-center">
                    <a href="{{ route('login') }}">عضو هستید؟</a>
                </div>
            </form>
        </div>
    </div>
</div>

@extends('layouts.footer')

@section('script')
    <script src="{{asset('admin-asset/js/pages/examples/sign-up.js')}}"></script>
@endsection

