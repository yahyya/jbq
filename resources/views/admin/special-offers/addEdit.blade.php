@extends('admin.layouts.panel')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title',' Category')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$offer)
                           Edit Offer
                        @else
                            Add Offer
                        @endif
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('title_en') ? ' has-error' : '' }}">
                            <label for="tbTitle">Title En</label>
                            <div class="form-line">
                                <input type="text" id="tbTitleEn" class="form-control" value="@if(@$category){{$category->title_en}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('title_ar') ? ' has-error' : '' }}">
                            <label for="tbTitle">Title Ar</label>
                            <div class="form-line">
                                <input type="text" id="tbTitleAr" class="form-control" value="@if(@$category){{$category->title_ar}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('exp_date') ? ' has-error' : '' }}">
                            <label for="tbTitle">Expire Date</label>
                            <div class="form-line">
                                <input type="text" id="tbExpDate" class="form-control" value="@if(@$offer){{$offer->exp_date}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('exp_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('exp_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                            <label for="sbProduct">Product</label>
                            <div class="form-line">
                                <select class="form-control" id="sbProduct" >
                                    @foreach(\App\Models\Product::where('active',1)->get() as $pr)
                                        <option value="{{$pr->id}}" @if(@$offer) @if($offer->product_id == $pr->id) selected="selected" @endif @endif >
                                            {{$pr->title_en}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('desc_en') ? ' has-error' : '' }}">
                            <label for="tbDescEn">Desc en</label>
                            <div class="form-line">
                                <textarea id="tbDescEn" class="form-control" value="@if(@$offer){{$offer->desc_en}}@endif" placeholder="" required autofocus>
                                    @if(@$offer) {{$offer->desc_en}} @endif
                                </textarea>
                                @if ($errors->has('desc_en'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_en') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('desc_ar') ? ' has-error' : '' }}">
                            <label for="tbDescAr">Desc Ar</label>
                            <div class="form-line">
                                <textarea id="tbDescAr" class="form-control" value="@if(@$category){{$category->desc_ar}}@endif" placeholder="" required autofocus>
                                    @if(@$category) {{$category->desc_ar}} @endif
                                </textarea>
                                @if ($errors->has('desc_ar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc_ar') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(isset($offer->image))
                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label> Image</label>
                                <div class="row">
                                    <div class="col-xs-6 col-md-3">
                                        <a href="javascript:void(0);" class="thumbnail">
                                            <img src="{{asset('upload/special_offer/').'/'.$offer->image}}" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </form>
                    <form class="dropzone" id="fileUpload"></form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$category)Save @else Save @endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <script>
        var fileName;
        Dropzone.autoDiscover = false;
        $(function () {
            var fileName;
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
            $(".dropzone").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 1,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        console.log(responseText);
                        var t =  responseText ;
                        window.fileName = t.name;
                        console.log(fileName);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$offer){{route('admin.specialOffers.update',$offer->id)}} @else {{route('admin.specialOffers.store')}}@endif',
                    type: @if(@$offer)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$category)"id": {{$offer->id}},@endif
                        "_method": 'post',
                        "title_en": $('#tbTitleEn').val(),
                        "title_ar": $('#tbTitleAr').val(),
                        "desc_ar":$('#tbDescAr').val(),
                        "desc_en":$('#tbDescAr').val(),
                        "product_id":$('#sbProduct').val(),
                        "exp_date":$('#tbExpDate').val(),
                        "image": fileName,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$category)Saved!@else Saved!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
