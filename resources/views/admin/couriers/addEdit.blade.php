@extends('admin.layouts.panel')

@section('style')
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('admin-asset/plugins/datepicker/datepicker3.css')}}">

    <!-- Dropzone -->
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title','پیک ها')
@section('subtitle','زیر عنوان')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$courier)
                            ویرایش پیک
                        @else
                            افزودن پیک
                        @endif
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#courier-profile" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">face</i> اطلاعات پیک
                            </a>
                        </li>
                        @if(@$courier)
                            <li role="presentation" class="">
                                <a href="#courier-orders" data-toggle="tab" aria-expanded="true">
                                    <i class="material-icons">view_headline</i> سفارش ها
                                </a>
                            </li>
                        @endif
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="courier-profile">
                            <form id="form">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="tbName">نام</label>
                                    <div class="form-line">
                                        <input type="text" id="tbName" class="form-control" value="@if(@$courier){{$courier->name}}@endif" placeholder="" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('family') ? ' has-error' : '' }}">
                                    <label for="tbFamily">نام خانوادگی</label>
                                    <div class="form-line">
                                        <input type="text" id="tbFamily" class="form-control" value="@if(@$courier){{$courier->family}}@endif" placeholder="" required>
                                        @if ($errors->has('family'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('family') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="tbEmail">ایمیل</label>
                                    <div class="form-line">
                                        <input type="email" id="tbEmail" class="form-control" value="@if(@$courier){{$courier->email}}@endif" placeholder="" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                    <label for="tbMobile">موبایل</label>
                                    <div class="form-line">
                                        <input type="text" id="tbMobile" class="form-control" value="@if(@$courier){{$courier->mobile}}@endif" placeholder="" required>
                                        @if ($errors->has('mobile'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('mobile') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label for="tbAddress">آدرس</label>
                                    <div class="form-line">
                                        <input type="text" id="tbAddress" class="form-control" value="@if(@$courier){{$courier->address}}@endif" placeholder="" required>
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                    <label>تاریخ آغاز به کار</label>
                                    <div class="form-line">
                                        <input type="text" id="datePicker" class="form-control" value="@if(@$courier){{\Morilog\Jalali\Jalalian::forge($courier->start_date)->format('Y-m-d')}}@endif" placeholder="" required>
                                        {{--<input type="hidden" id="tbType" value="@if(@$delivery){{$delivery->type}}@else{{$flag}}@endif">--}}
                                        @if ($errors->has('start_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('start_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="tbPassword">رمز عبور</label>
                                    <div class="form-line">
                                        <input type="password" id="tbPassword" class="form-control" value="" placeholder="" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                                    <label for="taDesc">توضیحات</label>
                                    <div class="form-line">
                                        <textarea rows="4" id="taDesc" class="form-control no-resize" placeholder="توضیحات ....">@if(@$courier){{$courier->desc}}@endif</textarea>
                                        @if ($errors->has('desc'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('desc') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                {{--<div class="form-group{{ $errors->has('active') ? ' has-error' : '' }} clearfix">--}}
                                    {{--<label>وضعیت محصول</label>--}}
                                    {{--<div>--}}
                                        {{--<input type="checkbox" id="chbActive" class="filled-in" @if(@$courier->active) checked @endif>--}}
                                        {{--<label for="chbActive">فعال/غیرفعال</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                @if(isset($courier->image))
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label> تصویر</label>
                                    <div class="row">
                                        <div class="col-xs-6 col-md-3">
                                            <a href="javascript:void(0);" class="thumbnail">
                                                <img src="{{asset('uploads/images/couriers/').'/'.$courier->image}}" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </form>
                            <form class="dropzone" id="fileUpload"></form>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$courier)ذخیره@elseثبت@endif</button>
                        </div>
                        @if(@$courier)
                            <div role="tabpanel" class="tab-pane fade in active" id="courier-orders">
                                <div class="row">
                                    <div id="col-sm-12 date-range">
                                        <div class="col-sm-5">
                                            <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                                <label>تاریخ آغاز به کار</label>
                                                <div class="form-line">
                                                    <input type="text" id="startDate" class="form-control">
                                                    {{--<input type="hidden" id="tbType" value="@if(@$delivery){{$delivery->type}}@else{{$flag}}@endif">--}}
                                                    @if ($errors->has('start_date'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('start_date') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                                                <label>تاریخ آغاز به کار</label>
                                                <div class="form-line">
                                                    <input type="text" id="endDate" class="form-control">
                                                    {{--<input type="hidden" id="tbType" value="@if(@$delivery){{$delivery->type}}@else{{$flag}}@endif">--}}
                                                    @if ($errors->has('end_date'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('end_date') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-primary m-t-15 waves-effect filterDateRangeBtn"><i class="material-icons">event</i> فیلتر</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table id="ordersDatatable" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>کاربر</th>
                                                        <th>پیک</th>
                                                        <th>وضعیت سفارش</th>
                                                        <th>تاریخ دریافت</th>
                                                        <th>توضیحات</th>
                                                        <th>عملیات</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th>id</th>
                                                        <th>کاربر</th>
                                                        <th>پیک</th>
                                                        <th>وضعیت سفارش</th>
                                                        <th>تاریخ دریافت</th>
                                                        <th>توضیحات</th>
                                                        <th>عملیات</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Date Picker -->
    <script src="{{URL::asset('js/datepicker/jquery.ui.datepicker-cc.js')}}"></script>
    <script src="{{URL::asset('js/datepicker/calendar.js')}}"></script>
    <script src="{{URL::asset('js/datepicker/jquery.ui.datepicker-cc-fa.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <script>
        var table, fileName, courierOrders = [];
        @if (@$courierOrderd)
            @foreach(@$courierOrders as $item)
                courierOrders.push({!! $item !!});
            @endforeach
        @endif
        $(function () {
            $('#datePicker').datepicker({dateFormat: 'yy-mm-dd'});
            $('#startDate').datepicker({dateFormat: 'yy-mm-dd'});
            $('#endDate').datepicker({dateFormat: 'yy-mm-dd'});
            $('#form').on('form:submit',function () {
                return false;
            });

            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });

            $('.filterDateRangeBtn').on('click',function () {
                filterDateRange();
            });

            $(".dropzone").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 1,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = $.parseJSON( responseText );
                        window.fileName = t.name;
                        console.log(fileName);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });

            @if(@$courier)
                table = $('#ordersDatatable').DataTable({
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Persian.json"
                    },
                    pageLength:100,
                    responsive: true,
                    processing: true,
                    serverSide: false,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    data:courierOrders ,
                    {{--ajax: {--}}
                        {{--url: '{{route('admin.couriers.orders',['id' => $courier->id])}}',--}}
                        {{--headers: {--}}
                            {{--'X-CSRF-TOKEN': "{{csrf_token()}}"--}}
                        {{--}--}}
                    {{--},--}}
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'user_name', name: 'user_id'},
                        {data: 'courier_name', name: 'courier_id'},
                        {data: 'status', name: 'status_id'},
                        {data: 'recieve_date', name: 'recieve_date'},
                        {data: 'desc', name: 'desc'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            @endif
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$courier){{route('admin.couriers.update',['id'=>$courier->id])}} @else {{route('admin.couriers.store')}}@endif',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: @if(@$courier)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$courier)"id": {{$courier->id}},@endif
                        "_method": 'post',
                        "name": $('#tbName').val(),
                        "family": $('#tbFamily').val(),
                        "email": $('#tbEmail').val(),
                        "mobile": $('#tbMobile').val(),
                        "address": $('#tbAddress').val(),
                        "start_date": $('#datePicker').val(),
                        "password": $('#tbPassword').val(),
                        "desc": $('#taDesc').val(),
                        "image": fileName,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$courier)آیتم با موفقیت ویرایش شد!@elseآیتم با موفقیت افزوده شد!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>خطایی رخ داد، آیتم ذخیره نشد!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + res.msg + '</div>');
                    }
                });
        }

        @if(@$courier)
        function filterDateRange(){
            $('.filterDateRangeBtn').attr('disabled','disabled');
            $.ajax(
                {
                    url: '{{route('admin.couriers.orders.filter',['id'=>$courier->id])}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "from_date": $('#startDate').val(),
                        "to_date": $('#endDate').val(),
                    },
                    success: function (res) {
                        $('.filterDateRangeBtn').removeAttr('disabled');
                        if (res) {
                            $('.alert').remove();
                            {{--$('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$courier)آیتم با موفقیت ویرایش شد!@elseآیتم با موفقیت افزوده شد!@endif</div>');--}}
                            courierOrders = res;
                            table.clear().rows.add(courierOrders).draw().nodes();
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>خطایی رخ داد، آیتم ذخیره نشد!<br>' + res + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + res.msg + '</div>');
                    }
                });
        }
        @endif
    </script>

@endsection