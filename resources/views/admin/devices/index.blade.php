@extends('admin.layouts.panel')

@section('style')
    {{--<link href="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.css') }}" rel="stylesheet">--}}
@endsection

@section('title','دیوایس ها')
@section('subtitle','زیر عنوان')

@section('content')

    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
لیست دیوایس ها
                    </h2>
                    <div class="align-left">
                        <button class="btn btn-info waves-effect btn-notify" onclick="notifyModal(-1)" style="font-size:14px;">
                            <i class="material-icons">notifications</i> ارسال نوتیفیکیشن همگانی</button>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>کاربر</th>
                                <th>موبایل</th>
                                <th>مدل دستگاه</th>
                                <th>سیستم عامل</th>
                                <th>نسخه سیستم عامل</th>
                                <th>build number</th>
                                <th>imei</th>
                                <th>تاریخ نصب</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>کاربر</th>
                                <th>موبایل</th>
                                <th>مدل دستگاه</th>
                                <th>سیستم عامل</th>
                                <th>نسخه سیستم عامل</th>
                                <th>build number</th>
                                <th>imei</th>
                                <th>تاریخ نصب</th>
                                <th>عملیات</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">ارسال اعلان</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message">پیام</label>
                        <textarea id="message" name="message" class="form-control" cols="" rows=""></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                    <button type="button" class="btn btn-primary" id="btn-nofity">ارسال اعلان</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.Modal -->

@endsection

@section('script')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
{{--    <script src="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.js') }}"></script>--}}
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
{{--    <script src="{{ asset('admin-asset/js/pages/tables/jquery-datatable.js') }}"></script>--}}

    <script>
        var table;
        $(function () {

            table = $('#datatable').DataTable({
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Persian.json"
                },
                pageLength:100,
                responsive: true,
                processing: true,
                serverSide: true,
                columnDefs: [
                    { orderable: true, className: 'reorder', targets: 0 },
                    { orderable: false, targets: '_all' }
                ],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.devices.index')}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    }
                },
                order: [[ 0, "desc" ]],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'fullname', name: 'fullname', searchable: false},
                    {data: 'mobile', name: 'user.mobile'},
                    {data: 'device_model', name: 'device_model'},
                    {data: 'os', name: 'os'},
                    {data: 'os_version', name: 'os_version'},
                    {data: 'build_number', name: 'build_number'},
                    {data: 'imei', name: 'imei'},
                    {data: 'created_at', name: 'created_at', searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

        function notifyModal(id) {
            $('#btn-nofity').attr('onclick', 'notifyUser(' + id + ')');
            $('#message').val('');
            $('#notificationModal').modal('show');
        }

        function notifyUser(id) {
            var confirm = window.confirm('آیا از ارسال پیام اطمینان کامل دارید؟');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{route('admin.devices.notify')}}',
                        type: 'post',
                        dataType: "JSON",
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        data: {
                            "id": id,
                            "_method": 'POST',
                            "message": $('#message').val(),
                        },
                        success: function (res) {
                            $('#message').val('');
                            $('#notificationModal').modal('hide');
                            if (res.res){
                                $('.box-header').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>اعلان با موفقیت ارسال شد!</div>');
                            } else {
                                $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>توکن یافت نشد!</div>');
                            }
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داده است!</div>');
                        }
                    });
            }

        }

    </script>

@endsection