@extends('admin.layouts.panel')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title','نان ها')
@section('subtitle','زیر عنوان')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$bread)
                            ویرایش نان
                        @else
                            افزودن نان
                        @endif
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="tbTitle">عنوان نان</label>
                            <div class="form-line">
                                <input type="text" id="tbTitle" class="form-control" value="@if(@$bread){{$bread->title}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }} clearfix">
                            <label>وضعیت محصول</label>
                            <div>
                                <input type="checkbox" id="chbActive" class="filled-in" @if(@$bread->active) checked @endif>
                                <label for="chbActive">فعال</label>
                            </div>
                        </div>
                        @if(isset($bread->image))
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label> تصویر</label>
                            <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="javascript:void(0);" class="thumbnail">
                                        <img src="{{asset('uploads/images/breads/').'/'.$bread->image}}" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endif
                    </form>
                    <form class="dropzone" id="fileUpload"></form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$bread)ذخیره@elseثبت@endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <script>
        var fileName;
        $(function () {
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });

            $(".dropzone").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 1,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = $.parseJSON( responseText );
                        window.fileName = t.name;
                        console.log(fileName);
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$bread){{route('admin.breads.update',['id'=>$bread->id])}} @else {{route('admin.breads.store')}}@endif',
                    type: @if(@$bread)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$bread)"id": {{$bread->id}},@endif
                        "_method": 'post',
                        "title": $('#tbTitle').val(),
                        "image": fileName,
                        "active": ($('#chbActive:checked').length)? 1 : 0,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$bread)آیتم با موفقیت ویرایش شد!@elseآیتم با موفقیت افزوده شد!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>خطایی رخ داد، آیتم ذخیره نشد!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection