@extends('admin.layouts.panel')

@section('style')
    <link href="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.css') }}" rel="stylesheet">
@endsection

@section('title','دسته بندی ها')
@section('subtitle','زیر عنوان')

@section('content')
<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
        تنظیمات
         </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.categories.create') }}" class="btn btn-primary waves-effect">افزودن دسته</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>

    <!-- Main content -->
   <div class="body">
                    <div class="table-responsive">
                <table id="status-table" class="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>عنوان</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                </table>

           </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin-asset/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
<script>

    var table;
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });

        table = $('#status-table').DataTable({
            "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Persian.json"
                },
            processing: true,
            serverSide: true,
            ajax: '{{URL::to('/')}}/admin/config',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });


  </script>
 @endsection
