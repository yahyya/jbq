@extends('admin.layouts.panel')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title','  ویرایش تنظیمات')
@section('subtitle','زیر عنوان')

@section('content')
    <div class="wrapper">
        <!-- Ready For Upload -->

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>

                            ویرایش کانفیگ

                        </h2>
                        {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                        {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                        {{--<i class="material-icons">more_vert</i>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu pull-right">--}}
                        {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                        {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                        {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </div>
                    <div class="body">

                        <form role="form" id="form">
                            <div class="form-group">
                                <label for="tbTitle">عنوان</label>
                                <input required type="text" class="form-control"
                                       @if(@$config) value="{{$config->title}}" @endif id="tbTitle" placeholder="عنوان">
                            </div>
                            <div class="form-group">

                                <textarea cols="80" id="editor1" name="editor1" rows="10">
                    @if($config)

                                        {{$config->value}}
                                    @endif
                  </textarea>
                            </div>

                        </form>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="saveBtn btn btn-primary">ثبت</button>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('script')

        <script>
            $(function () {
                $('#form')
                    .on('form:submit', function () {
                        return false; // Don't submit form for this demo
                    });
            });
            $(document).ready(function () {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    }
                });
                $('.saveBtn').on('click', function () {

                        saveItem();
                });


            });

            function saveItem() {
                $('.saveBtn').attr('disabled', 'disabled');
                var post = {
                    title: $('#tbTitle').val(),
                    value: $('#editor1').val()


                };
                $.ajax(
                {
                    url: '@if(@$config){{URL::to('admin/config/')}}/{{$config->id}} @else {{URL::to('admin/config')}}@endif',
                    type: @if(@$config)'PUT' @else 'POST' @endif,
                    dataType: "JSON",
                    data: post,
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        alert('با موفقیت ویرایش شد!');
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>خطایی رخ داد!<br>' + res.msg + '</div>');
                    }
                });
        }





        </script>
    @endsection
