<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Loading...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
{{--<!-- Search Bar -->--}}
{{--<div class="search-bar">--}}
    {{--<div class="search-icon">--}}
        {{--<i class="material-icons">search</i>--}}
    {{--</div>--}}
    {{--<input type="text" placeholder="START TYPING...">--}}
    {{--<div class="close-search">--}}
        {{--<i class="material-icons">close</i>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<!-- #END# Search Bar -->--}}
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="{{route('admin.dashboard')}}">{{ config('app.name') }}</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="{{route('admin.logout')}}"><i class="material-icons">input</i></a></li>
                {{--<!-- Call Search -->--}}
                {{--<li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>--}}
                {{--<!-- #END# Call Search -->--}}
                {{--<!-- Notifications -->--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">--}}
                        {{--<i class="material-icons">notifications</i>--}}
                        {{--<span class="label-count">7</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="header">NOTIFICATIONS</li>--}}
                        {{--<li class="body">--}}
                            {{--<ul class="menu">--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-light-green">--}}
                                            {{--<i class="material-icons">person_add</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4>12 new members joined</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> 14 mins ago--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-cyan">--}}
                                            {{--<i class="material-icons">add_shopping_cart</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4>4 sales made</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> 22 mins ago--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-red">--}}
                                            {{--<i class="material-icons">delete_forever</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4><b>Nancy Doe</b> deleted account</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> 3 hours ago--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-orange">--}}
                                            {{--<i class="material-icons">mode_edit</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4><b>Nancy</b> changed name</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> 2 hours ago--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-blue-grey">--}}
                                            {{--<i class="material-icons">comment</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4><b>John</b> commented your post</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> 4 hours ago--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-light-green">--}}
                                            {{--<i class="material-icons">cached</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4><b>John</b> updated status</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> 3 hours ago--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<div class="icon-circle bg-purple">--}}
                                            {{--<i class="material-icons">settings</i>--}}
                                        {{--</div>--}}
                                        {{--<div class="menu-info">--}}
                                            {{--<h4>Settings updated</h4>--}}
                                            {{--<p>--}}
                                                {{--<i class="material-icons">access_time</i> Yesterday--}}
                                            {{--</p>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li class="footer">--}}
                            {{--<a href="javascript:void(0);">View All Notifications</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<!-- #END# Notifications -->--}}
                {{--<!-- Tasks -->--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">--}}
                        {{--<i class="material-icons">flag</i>--}}
                        {{--<span class="label-count">9</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="header">TASKS</li>--}}
                        {{--<li class="body">--}}
                            {{--<ul class="menu tasks">--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<h4>--}}
                                            {{--Footer display issue--}}
                                            {{--<small>32%</small>--}}
                                        {{--</h4>--}}
                                        {{--<div class="progress">--}}
                                            {{--<div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<h4>--}}
                                            {{--Make new buttons--}}
                                            {{--<small>45%</small>--}}
                                        {{--</h4>--}}
                                        {{--<div class="progress">--}}
                                            {{--<div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<h4>--}}
                                            {{--Create new dashboard--}}
                                            {{--<small>54%</small>--}}
                                        {{--</h4>--}}
                                        {{--<div class="progress">--}}
                                            {{--<div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<h4>--}}
                                            {{--Solve transition issue--}}
                                            {{--<small>65%</small>--}}
                                        {{--</h4>--}}
                                        {{--<div class="progress">--}}
                                            {{--<div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);">--}}
                                        {{--<h4>--}}
                                            {{--Answer GitHub questions--}}
                                            {{--<small>92%</small>--}}
                                        {{--</h4>--}}
                                        {{--<div class="progress">--}}
                                            {{--<div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li class="footer">--}}
                            {{--<a href="javascript:void(0);">View All Tasks</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<!-- #END# Tasks -->--}}
                {{--<li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>--}}
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <?php $adminPermissions = \Illuminate\Support\Facades\Auth::guard('admin')->user()->permissions; ?>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="{{asset('images/user.png')}}" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{auth()->user()->name}} {{auth()->user()->family}}</div>
                <div class="email">{{auth()->user()->email}}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a ><i class="material-icons">person</i>Profile</a></li>
                        {{--<li role="seperator" class="divider"></li>--}}
                        {{--<li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>--}}
                        {{--<li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>--}}
                        {{--<li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>--}}
                        <li role="seperator" class="divider"></li>
                        <li><a href="{{route('admin.logout')}}"><i class="material-icons">input</i>LogOut</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="@if(Request::is('admin/dashboard/*') || Request::is('admin/dashboard')) active @endif">
                    <a href="{{route('admin.dashboard')}}">
                        <i class="material-icons">home</i>&nbsp;&nbsp;
                        <span>Dashboard</span>
                    </a>
                </li>
                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/orders')->first()->id)->first())
                    <li class="@if(Request::is('admin/orders/*') || Request::is('admin/orders')) active @endif">
                        <a href="{{route('admin.orders.index')}}">
                            <i class="material-icons">view_list</i>&nbsp;&nbsp;
                            <span>Orders</span>
                        </a>
                    </li>
                @endif
                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/users')->first()->id)->first())
                    <li class="@if(Request::is('admin/users/*') || Request::is('admin/users')) active @endif">
                        <a href="{{route('admin.users.index')}}">
                            <i class="material-icons">person</i>&nbsp;&nbsp;
                            <span>Users</span>
                        </a>
                    </li>
                @endif

                <li class="@if(Request::is('admin/sliders/*') || Request::is('admin/sliders')) active @endif">
                    <a href="{{route('admin.sliders.index')}}">
                        <i class="material-icons">view_list</i>&nbsp;&nbsp;
                        <span>Index Sliders</span>
                    </a>
                </li>

                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/transactions')->first()->id)->first())
                    <li class="@if(Request::is('admin/transactions/*') || Request::is('admin/transactions')) active @endif">
                        <a href="{{route('admin.transactions.index')}}">
                            <i class="material-icons">swap_horiz</i>&nbsp;&nbsp;
                            <span>Transactions</span>
                        </a>
                    </li>
                @endif
                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/categories')->first()->id)->first())
                <li class="@if(Request::is('admin/categories/*') || Request::is('admin/categories')) active @endif">
                    <a href="{{route('admin.categories.index')}}">
                        <i class="material-icons">view_module</i>&nbsp;&nbsp;
                        <span>Categories</span>
                    </a>
                </li>
                @endif
                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/products')->first()->id)->first())
                    <li class="@if(Request::is('admin/products/*') || Request::is('admin/products')) active @endif">
                        <a href="{{route('admin.products.index')}}">
                            <i class="material-icons">view_day</i>&nbsp;&nbsp;
                            <span>Products</span>
                        </a>
                    </li>
                @endif

                <li class="@if(Request::is('admin/indexCategories/*') || Request::is('admin/indexCategories')) active @endif">
                    <a href="{{route('admin.indexCategories.index')}}">
                        <i class="material-icons">view_module</i>&nbsp;&nbsp;
                        <span>Index Categories</span>
                    </a>
                </li>

                <li class="@if(Request::is('admin/tag/*') || Request::is('admin/tag')) active @endif">
                    <a href="{{route('admin.tag.index')}}">
                        <i class="material-icons">view_module</i>&nbsp;&nbsp;
                        <span>Tags</span>
                    </a>
                </li>

                <li class="@if(Request::is('admin/group/*') || Request::is('admin/group')) active @endif">
                    <a href="{{route('admin.group.index')}}">
                        <i class="material-icons">view_day</i>&nbsp;&nbsp;
                        <span>Group Buys</span>
                    </a>
                </li>

                <li class="@if(Request::is('admin/special-offers/*') || Request::is('admin/special-offers')) active @endif">
                    <a href="{{route('admin.specialOffers.index')}}">
                        <i class="material-icons">view_day</i>&nbsp;&nbsp;
                        <span>Offers</span>
                    </a>
                </li>

                <li class="@if(Request::is('admin/reviews/*') || Request::is('admin/reviews')) active @endif">
                    <a href="{{route('admin.reviews.index')}}">
                        <i class="material-icons">comment</i>&nbsp;&nbsp;
                        <span>Reviews</span>
                    </a>
                </li>
                <li class="@if(Request::is('admin/redirects/*') || Request::is('admin/redirects')) active @endif">
                    <a href="{{route('admin.redirects.index')}}">
                        <i class="material-icons">refresh</i>&nbsp;&nbsp;
                        <span>Redirects</span>
                    </a>
                </li>
{{--                @if($adminPermissions->where('permission_id', @\App\Permission::select('id')->where('route','admin/products')->first()->id)->first())--}}

{{--                @endif--}}


{{--                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/special-offers')->first()->id)->first() || $adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/offers')->first()->id)->first())--}}
{{--                    <li class="@if(Request::is('admin/offers/*') || Request::is('admin/offers') || Request::is('admin/special-offers/*') || Request::is('admin/special-offers')) active @endif">--}}
{{--                        <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                            <i class="material-icons">stars</i>&nbsp;&nbsp;--}}
{{--                            <span>Special Offers</span>--}}
{{--                        </a>--}}
{{--                        <ul class="ml-menu">--}}
{{--                            @if($adminPermissions->where('permission_id', @\App\Permission::select('id')->where('route','admin/offers')->first()->id)->first())--}}
{{--                            <li class="@if(Request::is('admin/offers/*') || Request::is('admin/offers')) active @endif">--}}
{{--                                <a href="{{route('admin.offers.index')}}">--}}
{{--                                    <span>Offers List</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            --}}{{--@endif--}}
{{--                            @if($adminPermissions->where('permission_id', @\App\Permission::select('id')->where('route','admin/special-offers')->first()->id)->first())--}}
{{--                            <li class="@if(Request::is('admin/special-offers/*') || Request::is('admin/special-offers')) active @endif">--}}
{{--                                <a href="{{route('admin.specialOffers.index')}}">--}}
{{--                                    <span>Special Banners</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            --}}{{--@endif--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                @endif--}}
{{--                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/zones')->first()->id)->first())--}}
{{--                <li class="@if(Request::is('admin/zones/*') || Request::is('admin/zones')) active @endif">--}}
{{--                    <a href="{{route('admin.zones.index')}}">--}}
{{--                        <i class="material-icons">location_searching</i>&nbsp;&nbsp;--}}
{{--                        <span>Districts</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}
{{--                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/coupons')->first()->id)->first())--}}
{{--                <li class="@if(Request::is('admin/coupons/*') || Request::is('admin/coupons')) active @endif">--}}
{{--                    <a href="{{route('admin.coupons.index')}}">--}}
{{--                        <i class="material-icons">style</i>&nbsp;&nbsp;--}}
{{--                        <span>Coupons</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}
{{--                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/couriers')->first()->id)->first())--}}
{{--                <li class="@if(Request::is('admin/couriers/*') || Request::is('admin/couriers')) active @endif">--}}
{{--                    <a href="{{route('admin.couriers.index')}}">--}}
{{--                        <i class="material-icons">directions_bike</i>&nbsp;&nbsp;--}}
{{--                        <span>پیک ها</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}
{{--                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/devices')->first()->id)->first())--}}
{{--                <li class="@if(Request::is('admin/devices/*') || Request::is('admin/devices')) active @endif">--}}
{{--                    <a href="{{route('admin.devices.index')}}">--}}
{{--                        <i class="material-icons">important_devices</i>&nbsp;&nbsp;--}}
{{--                        <span>Devices</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}
{{--                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/admins')->first()->id)->first())--}}
{{--                <li class="@if(Request::is('admin/admins/*') || Request::is('admin/admins')) active @endif">--}}
{{--                    <a href="{{route('admin.admins.index')}}">--}}
{{--                        <i class="material-icons">people</i>&nbsp;&nbsp;--}}
{{--                        <span>Admins</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}

{{--                 @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/stats')->first()->id)->first())--}}
{{--                <li class="@if(Request::is('admin/stats/*') || Request::is('admin/stats')) active @endif">--}}
{{--                    <a href="{{route('admin.stats.index')}}">--}}
{{--                        <i class="material-icons">people</i>&nbsp;&nbsp;--}}
{{--                        <span>Statistics</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}

{{--                @if($adminPermissions->where('permission_id', @\App\Models\Permission::select('id')->where('route','admin/config')->first()->id)->first())--}}
{{--                <li class="@if(Request::is('admin/config/*') || Request::is('admin/config')) active @endif">--}}
{{--                    <a href="{{route('admin.config.index')}}">--}}
{{--                        <i class="material-icons">location_searching</i>&nbsp;&nbsp;--}}
{{--                        <span>Settings</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                @endif--}}
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                <a href="javascript:void(0);"> {{ config('app.name') }}</a>.
            </div>
            <div class="version">
                <b>Ver: </b> 0.0.5
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
            <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                <ul class="demo-choose-skin">
                    <li data-theme="red" class="active">
                        <div class="red"></div>
                        <span>Red</span>
                    </li>
                    <li data-theme="pink">
                        <div class="pink"></div>
                        <span>Pink</span>
                    </li>
                    <li data-theme="purple">
                        <div class="purple"></div>
                        <span>Purple</span>
                    </li>
                    <li data-theme="deep-purple">
                        <div class="deep-purple"></div>
                        <span>Deep Purple</span>
                    </li>
                    <li data-theme="indigo">
                        <div class="indigo"></div>
                        <span>Indigo</span>
                    </li>
                    <li data-theme="blue">
                        <div class="blue"></div>
                        <span>Blue</span>
                    </li>
                    <li data-theme="light-blue">
                        <div class="light-blue"></div>
                        <span>Light Blue</span>
                    </li>
                    <li data-theme="cyan">
                        <div class="cyan"></div>
                        <span>Cyan</span>
                    </li>
                    <li data-theme="teal">
                        <div class="teal"></div>
                        <span>Teal</span>
                    </li>
                    <li data-theme="green">
                        <div class="green"></div>
                        <span>Green</span>
                    </li>
                    <li data-theme="light-green">
                        <div class="light-green"></div>
                        <span>Light Green</span>
                    </li>
                    <li data-theme="lime">
                        <div class="lime"></div>
                        <span>Lime</span>
                    </li>
                    <li data-theme="yellow">
                        <div class="yellow"></div>
                        <span>Yellow</span>
                    </li>
                    <li data-theme="amber">
                        <div class="amber"></div>
                        <span>Amber</span>
                    </li>
                    <li data-theme="orange">
                        <div class="orange"></div>
                        <span>Orange</span>
                    </li>
                    <li data-theme="deep-orange">
                        <div class="deep-orange"></div>
                        <span>Deep Orange</span>
                    </li>
                    <li data-theme="brown">
                        <div class="brown"></div>
                        <span>Brown</span>
                    </li>
                    <li data-theme="grey">
                        <div class="grey"></div>
                        <span>Grey</span>
                    </li>
                    <li data-theme="blue-grey">
                        <div class="blue-grey"></div>
                        <span>Blue Grey</span>
                    </li>
                    <li data-theme="black">
                        <div class="black"></div>
                        <span>Black</span>
                    </li>
                </ul>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="settings">
                <div class="demo-settings">
                    <p>GENERAL SETTINGS</p>
                    <ul class="setting-list">
                        <li>
                            <span>Report Panel Usage</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Email Redirect</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                    <p>SYSTEM SETTINGS</p>
                    <ul class="setting-list">
                        <li>
                            <span>Notifications</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Auto Updates</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                    <p>ACCOUNT SETTINGS</p>
                    <ul class="setting-list">
                        <li>
                            <span>Offline</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Location Permission</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
    <!-- #END# Right Sidebar -->
</section>
