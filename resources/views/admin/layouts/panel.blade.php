@include('admin.layouts.header')

@include('admin.layouts.menu')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>@yield('title')<small>@yield('subtitle')</small></h2>
        </div>
        @yield('content')
    </div>
</section>

@include('admin.layouts.footer')