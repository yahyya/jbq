
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('admin-asset/plugins/node-waves/waves.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ URL::asset('admin-asset/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ URL::asset('admin-asset/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ URL::asset('admin-asset/plugins/jquery-countto/jquery.countTo.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{asset('admin-asset/plugins/jquery-validation/jquery.validate.js')}}"></script>
    <!-- Custom Js -->
    <script src="{{asset('admin-asset/js/admin.js')}}"></script>
    <script src="{{asset('admin-asset/js/script.js')}}"></script>


    <script src="{{asset('admin-asset/js/firebase.js')}}"></script>
    <script>
      // Initialize Firebase

      function sendTokenToServer(token) {
          $.ajax(
            {
                url: '{{route('admin.token')}}',
                type: 'post',
                dataType: "JSON",
                data: {
                    token: token,

                },
                headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                success: function (res) {
                    $('#txt-total').html(res.total);
                    $('#txt-sum-prices').html(res.sum);
                },
                error: function (xhr, stat) {

                    console.log(stat);
                }
            });
      }


      // Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.


// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
var config = {
        apiKey: "AIzaSyDfcScgsvFptMRZSSfSeRlgLczNNj4E4fI",
        authDomain: "grizlly-2e8db.firebaseapp.com",
        databaseURL: "https://grizlly-2e8db.firebaseio.com",
        projectId: "grizlly-2e8db",
        storageBucket: "grizlly-2e8db.appspot.com",
        messagingSenderId: "992176186494"
      };
firebase.initializeApp(config);
const messaging = firebase.messaging();
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
function notifyMe(notify) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification("Hi there!");
  }

  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {

      // Whatever the user answers, we make sure we store the information
      if(!('permission' in Notification)) {
        Notification.permission = permission;
      }

      // If the user is okay, let's create a notification
      if (permission === "granted") {
        var notification = notify;
      }
    });
  }

  // At last, if the user already denied any notification, and you
  // want to be respectful there is no need to bother him any more.
}
console.log('sw');
messaging.requestPermission().then(function() {

          messaging.getToken().then(function(currentToken) {
          if (currentToken) {
            sendTokenToServer(currentToken);
            console.log('token sent');
          } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');

          }
        }).catch(function(err) {
          console.log('An error occurred while retrieving token. ', err);

        });


}).catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});
var pay=null;
      messaging.onMessage(function(payload) {
          pay = payload;
          var notification = new Notification(payload.data.title, {
      icon: '{{asset('images/grizzly-logo-farsi-min.png')}}',
      body: payload.data.body,
    });

    notification.onclick = function () {
      window.open("{{url('/')}}/admin/orders/"+pay.data.id);
    };
    notifyMe(notification);
    $('#newOrders tbody').prepend('<tr style="background:#ffa298;color:#000" role="row" class="odd"><td class="sorting_1">'+payload.data.id+'</td><td>'+payload.data.created_at+'</td><td>'+payload.data.created_at_time+'</td><td>'+payload.data.tracking_code+'</td><td>'+payload.data.user_id+'</td><td>'+payload.data.payment_type+'</td><td>'+payload.data.total+'</td><td><div style="text-align:right">'+payload.data.action+'</div></td></tr>')
          $('#newOrdersCount').html(parseInt($('#newOrdersCount').html())+1);
  console.log('Message received. ', payload);
  // ...
});




    </script>
    @yield('script')
</body>
</html>
