@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Select -->
    <link href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <!-- Datatables RowReorder -->
    <link href="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.css') }}" rel="stylesheet">
@endsection

@section('title','Custom Product')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$customItem)
                            Edit Item
                        @else
                            Add Item
                        @endif
                    </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.custom.categories.show',$catId) }}" class="btn btn-primary waves-effect">Back</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="nav-item">
                            <a href="#item-details" class="nav-link" data-toggle="tab" aria-expanded="true">
                                <i class="material-icons">face</i>Item Specifics
                            </a>
                        </li>
                        <li role="presentation" class="nav-item">
                            <a href="#item-rules" class="nav-link" data-toggle="tab" aria-expanded="false">
                                <i class="material-icons">map</i>Incompatible items
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="item-details">
                            <form id="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="sbCustomCategory">Category :</label>
                                            <select name="category" id="sbCustomCategory" class="form-control" @if(@$customItem) disabled @endif>
                                                <option value="">Select</option>
                                                @foreach($customCategories as $customCategory)
                                                    <option value="{{$customCategory->id}}" @if($customCategory->id == @$customItem->custom_category_id || $customCategory->id == $catId) selected @endif>{{$customCategory->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="tbTitle">Title :</label>
                                    <div class="form-line">
                                        <input type="text" id="tbTitle" class="form-control" value="@if(@$customItem){{$customItem->title}}@endif" placeholder="" required autofocus>
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }} clearfix">
                                    <label>Status :</label>
                                    <div>
                                        <input type="checkbox" id="chbActive" class="filled-in" @if(@$customItem->active) checked @endif>
                                        <label for="chbActive">Active</label>
                                    </div>
                                </div>
                            </form>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$customItem) Save @else Save @endif</button>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="item-rules">
                            @if(@$customItem)
                                <div class="align-left">
                                    <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#newRuleModal" data-user="{{$id}}">
                                        <span>Add Incompatible item </span>
                                    </button>
                                </div>

                            @else
                                You can not specify incompatible items before add.
                            @endif
                            <div class="table-responsive">
                                <table id="datatable" class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Item</th>
                                        <th>Incompatible item</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Item</th>
                                        <th>Incompatible item</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal -->
    <div class="modal fade" id="newRuleModal" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="smallModalLabel">Edit Users Wallet</h4>
                </div>
                <div class="modal-body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="tbAmount">Items</label>
                            <div class="form-line">
                                <select name="item" id="sbCustomItem" class="form-control selectpicker show-tick" data-live-search="true">
                                    <option value="">Select Item</option>
                                    @if(@$customItems)
                                        @foreach($customItems as $customItem)
                                            <option value="{{$customItem->id}}">{{$customItem->title}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect newRule">Add Incompatible Item</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->
    <script>
        var rulesDatatable;
        $(function () {
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });

            $('.newRule').on('click', function () {
                newRule();
            });

            @if(@$customItem)
                rulesDatatable = $('#datatable').DataTable({
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    ajax: {
                        url: '{{route('admin.custom.items.rules', $id)}}',
                        method: 'GET'
                    },
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'first_item_id', name: 'first_item_id'},
                        {data: 'second_item_id', name: 'second_item_id'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            @endif
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$customItem){{route('admin.custom.items.update',$id)}} @else {{route('admin.custom.items.store')}}@endif',
                    type: @if(@$customItem)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$customItem)"id": {{$id}},@endif
                        "_method": 'post',
                        "custom_category_id": $('#sbCustomCategory').val(),
                        "title": $('#tbTitle').val(),
                        "active": ($('#chbActive:checked').length)? 1 : 0,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$customItem)Item added @else Item Added @endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error !<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }

        @if(@$customItem)
        function newRule() {
            $('.newRule').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '{{route('admin.custom.items.rules.create', $id)}}',
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "id": {{$id}},
                        "second_item_id": $('#sbCustomItem').val(),
                    },
                    success: function (res) {
                        $('.newRule').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.modal-body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Item Added!</div>');
                            rulesDatatable.draw();
                        } else {
                            $('.alert').remove();
                            $('.modal-body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
        @endif

        function deleteItem(id) {
            console.log('delete');
            var confirm = window.confirm('Are You Sure?');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{route('admin.custom.items.index')}}/rules/delete/',
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                rulesDatatable.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed !</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>

@endsection
