@extends('admin.layouts.panel')

@section('style')
    <link href="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.css') }}" rel="stylesheet">
@endsection

@section('title','Custom Products')
@section('subtitle',' ')

@section('content')

    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="pull-right">
                        Sub Items Of An Item
                    </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.custom.subItems.create').'?id='.$id }}" class="btn btn-primary waves-effect">Add sub Item</a>
                        <a href="{{ route('admin.custom.categories.show',$customItemCat) }}" class="btn btn-primary waves-effect">Category<i class="material-icons">arrow_back</i></a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Item</th>
                                <th>Count</th>
                                <th>Price</th>
                                <th>Sort</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Item</th>
                                <th>Count</th>
                                <th>Price</th>
                                <th>Sort</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->
{{--    <script src="{{ asset('admin-asset/js/pages/tables/jquery-datatable.js') }}"></script>--}}

    <script>
        var table;
        $(function () {

            table = $('#datatable').DataTable({

                pageLength:100,
                responsive: true,
                processing: true,
                serverSide: true,
                rowReorder: true,
                columnDefs: [
                    { orderable: true, className: 'reorder', targets: 0 },
                    { orderable: false, targets: '_all' }
                ],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.custom.items.show',$id)}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'main_pic', name: 'main_pic'},
                    {data: 'title', name: 'title'},
                    {data: 'custom_item_id', name: 'custom_item_id'},
                    {data: 'count', name: 'count'},
                    {data: 'price', name: 'price'},
                    {data: 'order', name: 'order'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            table.on( 'row-reorder', function ( e, diff, edit ) {
                var items = [];
                for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                    var item = {};
                    var rowData = table.row( diff[i].node ).data();
                    item.id = parseInt(rowData.id);
                    item.newOrder = parseInt(rowData.order) + (parseInt(diff[i].oldPosition) - parseInt(diff[i].newPosition));
                    items.push(item);
                }

                $.ajax(
                    {
                        url: '{{route('admin.custom.subItems.setOrder')}}',
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        type: 'post',
                        dataType: "JSON",
                        data: {
                            "_method": 'post',
                            "items": items,
                        },
                        success: function (res) {
                            if (res.res) {
                                table.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Sorted!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            });
        });

        function deleteItem(id) {
            console.log('delete');
            var confirm = window.confirm('آیا از حذف آیتم اطمینان کامل دارید؟');
            if (confirm) {
                $.ajax(
                    {
                        url: '{{route('admin.custom.subItems.index')}}/' + id,
                        headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        type: 'delete',
                        dataType: "JSON",
                        data: {
                            "id": id,
                            "_method": 'DELETE'
                        },
                        success: function (res) {
                            if (res.res) {
                                table.draw();
                            }
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Removed!</div>');
                            window.setTimeout(function () {
                                $(".alert").alert('close');
                            }, 3000);
                        },
                        error: function (xhr, stat) {
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!</div>');
                            window.setTimeout(function () {
                                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                    $(this).remove();
                                });
                            }, 3000);
                            console.log(stat);
                        }
                    });
            }
        }
    </script>

@endsection
