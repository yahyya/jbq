@extends('admin.layouts.panel')

@section('title','Custom Products')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$customCategory)
                            Edit Category
                        @else
                            Add Category
                        @endif
                    </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.custom.categories.index')}}" class="btn btn-primary waves-effect">Back</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="tbTitle">Title</label>
                            <div class="form-line">
                                <input type="text" id="tbTitle" class="form-control" value="@if(@$customCategory){{$customCategory->title}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('min') ? ' has-error' : '' }}">
                            <label for="tbCount">Minimum </label>
                            <div class="form-line">
                                <input type="number" id="tbMin" class="form-control" value="@if(@$customCategory){{$customCategory->min}}@else{{1}}@endif" required>
                                @if ($errors->has('min'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('min') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('max') ? ' has-error' : '' }}">
                            <label for="tbCount">Maximum</label>
                            <div class="form-line">
                                <input type="number" id="tbMax" class="form-control" value="@if(@$customCategory){{$customCategory->max}}@else{{1}}@endif" required>
                                @if ($errors->has('max'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('max') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                            <label for="taDesc">Desc</label>
                            <div class="form-line">
                                <textarea rows="4" id="taDesc" class="form-control no-resize" placeholder="Description ....">@if(@$customCategory){{$customCategory->desc}}@endif</textarea>
                                @if ($errors->has('desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$customCategory) Save @else Save @endif</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->
    <script>
        $(function () {
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$customCategory){{route('admin.custom.categories.update',['id'=>$customCategory->id])}} @else {{route('admin.custom.categories.store')}}@endif',
                    type: @if(@$customCategory)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$customCategory)"id": {{$customCategory->id}},@endif
                        "_method": 'post',
                        "title": $('#tbTitle').val(),
                        "min": $('#tbMin').val(),
                        "max": $('#tbMax').val(),
                        "desc": $('#taDesc').val(),
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$customCategory)Edited @else Added @endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
