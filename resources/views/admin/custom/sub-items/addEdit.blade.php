@extends('admin.layouts.panel')

@section('style')
    <!-- Bootstrap Select -->
    <link href="{{asset('admin-asset/plugins/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <!-- Datatables RowReorder -->
    <link href="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.css') }}" rel="stylesheet">
    <!-- Dropzone -->
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title','Custom Product categories')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        @if(@$customSubItem)
                            Edit Sub Item
                        @else
                            Add Sub Item
                        @endif
                    </h2>
                    <div class="align-left">
                        <a href="{{ route('admin.custom.items.show',$id)}}" class="btn btn-primary waves-effect">Back</a>
                    </div>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sbCustomItem">Item</label>
                                    <select name="item" id="sbCustomItem" class="form-control show-tick" @if(!@$customItem) disabled @endif>
                                        <option value="">Select Item</option>
                                        @foreach($customItems as $customItem)
                                            <option value="{{$customItem->id}}" @if($customItem->id == @$customItem->custom_item_id || $customItem->id == $id) selected @endif>{{$customItem->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sbProductId">Assigned product</label>

                                    <select name="product" id="sbProductId" class="form-control show-tick" >
                                        <option value="">Select Product</option>
                                        @foreach(\App\Models\Product::all() as $product)
                                            <option value="{{$product->id}}" @if($product->id == @$customSubItem->product_id) selected @endif>{{$product->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="tbTitle">Title</label>
                            <div class="form-line">
                                <input type="text" id="tbTitle" class="form-control" value="@if(@$customSubItem){{$customSubItem->title}}@endif" placeholder="" required autofocus>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div><div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="tbPrice">Price</label>
                            <div class="form-line">
                                <input type="text" id="tbPrice" class="form-control" value="@if(@$customSubItem){{$customSubItem->price}}@endif" required>
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('count') ? ' has-error' : '' }}">
                            <label for="tbCount">Count</label>
                            <div class="form-line">
                                <input type="number" id="tbCount" class="form-control" value="@if(@$customSubItem){{$customSubItem->count}}@else{{1}}@endif" required>
                                @if ($errors->has('count'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('count') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }} clearfix">
                            <label>Status</label>
                            <div>
                                <input type="checkbox" id="chbActive" class="filled-in" @if(@$customSubItem->active) checked @endif>
                                <label for="chbActive">Active</label>
                            </div>
                        </div>
                        {{--@if(isset($customSubItem->image))--}}
                            {{--<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">--}}
                                {{--<label> تصویر</label>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-xs-6 col-md-3">--}}
                                        {{--<a href="javascript:void(0);" class="thumbnail">--}}
                                            {{--<img src="{{asset('uploads/images/sub-items/').'/'.$customSubItem->image}}" class="img-responsive">--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    </form>
                    <div class="col-sm-4">
                        <div class="form-group clearfix">
                            <label>Main Image</label>
                            <div class="col-sm-12">
                                @if(isset($customSubItem->main_pic))
                                    <a href="javascript:void(0);" class="thumbnail">
                                        <img width="100" src="{{asset('upload/images/sub-items/').'/'.$customSubItem->main_pic}}" class="img-responsive">
                                    </a>
                                @endif
                            </div>
                            <form class="dropzone" id="mainPic"></form>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group clearfix">
                            <label>Stack Image</label>
                            <div class="col-sm-12">
                                @if(isset($customSubItem->stack_pic))
                                    <a href="javascript:void(0);" class="thumbnail">
                                        <img width="100" src="{{asset('upload/images/sub-items/').'/'.$customSubItem->stack_pic}}" class="img-responsive">
                                    </a>
                                @endif
                            </div>
                            <form class="dropzone" id="stackPic"></form>
                        </div>
                    </div>
{{--                    <div class="col-sm-4">--}}
{{--                        <div class="form-group clearfix">--}}
{{--                            <label>تصویر استک بالایی</label>--}}
{{--                            <div class="col-sm-12">--}}
{{--                                @if(isset($customSubItem->stack_top_pic))--}}
{{--                                    <a href="javascript:void(0);" class="thumbnail">--}}
{{--                                        <img src="{{asset('uploads/images/sub-items/').'/'.$customSubItem->stack_top_pic}}" class="img-responsive">--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                            <form class="dropzone" id="stackTopPic"></form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">@if(@$customSubItem) Save @else Save @endif</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- Jquery DataTable Plugin Js -->


    <!-- Dropzone Js -->
    <script src="{{ asset('admin-asset/js/dropzone.min.js') }}"></script>

    <!-- Custom Js -->
    <script>
        var mainPic,stackPic,stackTopPic;
        Dropzone.autoDiscover = false;
        $(function () {
            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
            $("#mainPic").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 3,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t = responseText ;
                        window.mainPic = t.name;
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
            $("#stackPic").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 3,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t =responseText ;
                        window.stackPic = t.name;
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
            $("#stackTopPic").dropzone({
                url: "{{route('fileUpload')}}",
                maxFilesize: 3,
                maxFiles:1,
                init: function() {
                    this.on("success", function(file, responseText) {
                        var t =  responseText ;
                        window.stackTopPic = t.name;
                    });
                },
                headers: {
                    'X-CSRF-Token': "{{csrf_token()}}"
                },
                addRemoveLinks: true
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '@if(@$customSubItem){{route('admin.custom.subItems.update',$customSubItem->id)}} @else {{route('admin.custom.subItems.store')}}@endif',
                    type: @if(@$customSubItem)'put'@else'post'@endif,
                    dataType: "JSON",
                    data: {
                        @if(@$customSubItem)"id": {{$customSubItem->id}},@endif
                        "_method": 'post',
                        "custom_item_id": $('#sbCustomItem').val(),
                        "title": $('#tbTitle').val(),
                        "price": $('#tbPrice').val(),
                        "count": $('#tbCount').val(),
                        "active": ($('#chbActive:checked').length)? 1 : 0,
                        "main_pic": mainPic,
                        "product_id":$('#sbProductId').val(),
                        "stack_pic": stackPic,
                        "stack_top_pic": stackTopPic,
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$customSubItem) Edited @else Added Successfully @endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
