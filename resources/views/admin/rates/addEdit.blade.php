@extends('admin.layouts.panel')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-asset/css/dropzone.min.css')}}">
@endsection

@section('title',' Category')
@section('subtitle',' ')

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Review
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <form id="form">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="tbTitle">Name </label>
                            <div class="form-line">
                                <input type="text" id="tbName" class="form-control" value="@if(@$review){{$review->name}}@endif" placeholder="" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="tbEmail">Email </label>
                            <div class="form-line">
                                <input type="text" id="tbEmail" class="form-control" value="@if(@$review){{$review->email}}@endif" placeholder="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
                            <label for="sbProduct">Product</label>
                            <div class="form-line">
                                {{\App\Models\Product::find($review->product_id)->title_en}}
                            </div>
                        </div>
                        <div class="form-group">
                            Rate : {{$review->rate}}
                        </div>
                        <div class="form-group clearfix">
                            <label>Verify</label>
                            <div>
                                <input type="checkbox" id="cbVerify" class="filled-in" @if(@$review->verify) checked @endif>
                                <label for="cbVerify">Active</label>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                            <label for="tbText">Text</label>
                            <div class="form-line">
                                <textarea id="tbText" name="tbText" class="form-control" value="@if(@$review){{$review->text}}@endif" placeholder="" required autofocus>
                                    @if(@$review) {{$review->text}} @endif
                                </textarea>
                            </div>
                        </div>



                    </form>
                    <button type="submit" class="btn btn-primary m-t-15 waves-effect saveBtn">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')


    <!-- Custom Js -->

    <script>
        $(function () {


            $('#form').on('form:submit',function () {
                return false;
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });
            $('.saveBtn').on('click', function () {
//                if ($('#form').parsley().validate())
                    saveItem();
            });
        });

        function saveItem() {
            $('.saveBtn').attr('disabled', 'disabled');
            $.ajax(
                {
                    url: '{{route('admin.reviews.update',$review->id)}}',
                    type: 'put',
                    dataType: "JSON",
                    data: {
                        @if(@$review)"id": {{$review->id}},@endif
                        "_method": 'post',
                        "name": $('#tbName').val(),
                        "verify" :($('#cbVerify:checked').length)? 1 : 0,
                        "email": $('#tbEmail').val(),
                        "text": $('#tbText').val(),
                    },
                    success: function (res) {
                        $('.saveBtn').removeAttr('disabled');
                        if (res.res) {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-success alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>@if(@$category)Saved!@else Saved!@endif</div>');
                        } else {
                            $('.alert').remove();
                            $('.body').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="close"><span aria-hidden="true">&times;</span></button>Error!<br>' + res.msg + '</div>');
                        }
                        setTimeout(function(){
                            $('.alert').fadeOut('slow',function () {
                                $(this).remove();
                            });
                        }, 5000)
                    },
                    error: function (xhr, stat) {
                        $('.saveBtn').removeAttr('disabled');
                        $('.box-header').prepend('<div class="alert alert-danger alert-sm alert-dismissable clearfix"><button type="button" class="close" data-dismiss="alert" aria-label="close">&times;</button>Error!<br>' + res.msg + '</div>');
                    }
                });
        }
    </script>

@endsection
