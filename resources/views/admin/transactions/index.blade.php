@extends('admin.layouts.panel')

@section('style')
    {{--<link href="{{ asset('admin-asset/plugins/jquery-datatable/dataTables.rowReorder.min.css') }}" rel="stylesheet">--}}
@endsection

@section('title','Transactions')
@section('subtitle',' ')

@section('content')

    <!-- Basic Examples -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
All Transactions
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                {{--<i class="material-icons">more_vert</i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>User</th>
                                <th>Invoice Number</th>
                                <th>Factur Id</th>
                                <th>Port</th>
                                <th>Amount</th>
                                <th>Refrence Id</th>
                                <th>Refrence Code</th>
                                <th>Card Number</th>
                                <th>ip</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>id</th>
                                <th>User</th>
                                <th>Invoice Number</th>
                                <th>Factur Id</th>
                                <th>Port</th>
                                <th>Amount</th>
                                <th>Refrence Id</th>
                                <th>Refrence Code</th>
                                <th>Card Number</th>
                                <th>ip</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <!-- Custom Js -->
{{--    <script src="{{ asset('admin-asset/js/pages/tables/jquery-datatable.js') }}"></script>--}}

    <script>
        var table;
        $(function () {

            table = $('#datatable').DataTable({

                pageLength:100,
                responsive: true,
                processing: true,
                serverSide: true,
                columnDefs: [
                    { orderable: true, className: 'reorder', targets: 0 },
                    { orderable: false, targets: '_all' }
                ],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url: '{{route('admin.transactions.index')}}',
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    }
                },
                order: [[ 0, "desc" ]],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'user', name: 'user'},
                    {data: 'order_id', name: 'order_id'},
                    {data: 'factor_id', name: 'factor_id'},
                    {data: 'port', name: 'port'},
                    {data: 'price', name: 'price'},
                    {data: 'ref_id', name: 'ref_id'},
                    {data: 'tracking_code', name: 'tracking_code'},
                    {data: 'card_number', name: 'card_number'},
                    {data: 'ip', name: 'ip'},
                    {data: 'status', name: 'status'},
                    {data: 'payment_date', name: 'payment_date', orderable: false, searchable: false}
                ]
            });
        });

    </script>

@endsection
