

    @extends('header',['dontShowBasket'=>true,'meta'=>$pr->meta_keywords,'desc'=>$pr->description,'title'=>'JBQ - '.$pr->title_en])
@section('content')
    <style>
        .slick-track{
            margin-left: 0;
            margin-right: 0;
        }
    </style>
    <main id="content" role="main">
        <!-- breadcrumb -->
        <div class="bg-gray-13 bg-md-transparent">
            <div class="container">
                <!-- breadcrumb -->
                <div class="my-md-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="{{url('/')}}">
                                    {{__('jbq.Home')}}
                                </a></li>
                            <?php

                            $parents = $pr->category->getParents($pr->category_id);
                            ?>
                            @foreach($parents as $catSub)
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                                    <a href="{{url('/category')}}/{{$catSub->url}}">
                                        @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                            {{$catSub->title_ar}}
                                        @else
                                            {{$catSub->title_en}}
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                            <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">
                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                    {{$pr->title_ar}}
                                @else
                                    {{$pr->title_en}}
                                @endif
                            </li>
                        </ol>
                    </nav>
                </div>
                <!-- End breadcrumb -->
            </div>
        </div>
        <!-- End breadcrumb -->
        <div class="container">
            <!-- Single Product Body -->
            <div class="mb-xl-14 mb-6">
                <div class="row">
                    <div class="col-md-5 mb-4 mb-md-0">
                        <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                             data-infinite="true"
                             data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                             data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                             data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                             data-nav-for="#sliderSyncingThumb">
                            @foreach($pr->images as $img)
                                <div class="js-slide">
                                    <img class="img-fluid" src="{{$img->file_name}}" alt=" @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                    {{$pr->title_ar}}
                                    @else
                                    {{$pr->title_en}}
                                    @endif">
                                </div>
                            @endforeach
                        </div>

                        <div id="sliderSyncingThumb"   class="js-slick-carousel u-slick u-slick--slider-syncing u-slick--slider-syncing-size u-slick--gutters-1 u-slick--transform-off"
                             data-infinite="true"
                             data-slides-show="5"
                             data-is-thumbs="true"
                             data-nav-for="#sliderSyncingNav">
                            @foreach($pr->images as $img)
                                <div class="js-slide">
                                    <img class="img-fluid" src="{{$img->file_name}}" alt=" @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                    {{$pr->title_ar}}
                                    @else
                                    {{$pr->title_en}}
                                    @endif">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-7 mb-md-6 mb-lg-0">
                        <div class="mb-2">
                            <div class="border-bottom mb-3 pb-md-1 pb-3" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                                <a href="#" class="font-size-12 text-gray-5 mb-2 d-inline-block">
                                    @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                        {{$pr->category->title_ar}}
                                    @else
                                        {{$pr->category->title_en}}
                                    @endif
                                </a>
                                <h1 class="font-size-25 text-lh-1dot2">
                                    @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                        {{$pr->title_ar}}
                                    @else
                                        {{$pr->title_en}}
                                    @endif
                                </h1>
                                @if(!empty($pr->total))
                                    <div class="mt-3 mb-1" style="font-size:9px;text-align: center" >
                                        @php
                                            $pr->sold = empty($pr->sold) ? 0 : $pr->sold;
                                        @endphp
                                        <b>{{$pr->sold}}</b> {{__('jbq.Sold out of')}} <b>{{$pr->total}}</b>
                                    </div>
                                    <div class="rounded-pill bg-gray-3 position-relative" style="height:5px;">
                                        @php
                                            $total = $pr->total - ($pr->total - $pr->sold);

                                        @endphp
                                        <span style="width:{{$total}}%" class="position-absolute @if(App::getLocale()=='ar') right-0 @else left-0 @endif top-0 bottom-0 rounded-pill bg-primary"></span>
                                    </div>
                                @endif
                                <div class="mb-2">
{{--                                    <a class="d-inline-flex align-items-center small font-size-15 text-lh-1" href="#">--}}
{{--                                        <div class="text-warning mr-2">--}}
{{--                                            <small class="fas fa-star"></small>--}}
{{--                                            <small class="fas fa-star"></small>--}}
{{--                                            <small class="fas fa-star"></small>--}}
{{--                                            <small class="fas fa-star"></small>--}}
{{--                                            <small class="far fa-star text-muted"></small>--}}
{{--                                        </div>--}}
{{--                                        <span class="text-secondary font-size-13">(3 customer reviews)</span>--}}
{{--                                    </a>--}}
                                </div>

                            </div>
                            <div class="flex-horizontal-center flex-wrap mb-4">
                                <a href="#" onclick="addToWishList({{$pr->id}})" class="text-gray-6 font-size-13 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') ml-2 @else mr-2 @endif"><i class="ec ec-favorites favoriteIcon mr-1 font-size-15"></i> {{__('jbq.Wish List')}}</a>
                            </div>
                            <div class="mb-2" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                    {!! $pr->desc_ar !!}
                                @else
                                    {!! $pr->desc_en !!}
                                @endif
                            </div>
                            <div class="mb-4">
                                <div class="d-flex align-items-baseline">
                                    <small style="font-size:11px;color:#ccc">AED</small>
                                    @if(!empty($pr->fixed_price))
                                        <ins class="font-size-36 text-decoration-none">{{$pr->fixed_price}}</ins>
                                        <del class="font-size-20 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-2 @else ml-2 @endif text-gray-6">{{$pr->price}}</del>
                                    @else
                                        <ins class="font-size-36 text-decoration-none">{{$pr->price}}</ins>
                                    @endif
                                </div>
                            </div>
                            <div class="d-md-flex align-items-end mb-3">
                                <div class="max-width-150 mb-4 mb-md-0" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
                                    <h6 class="font-size-14">{{__('Quantity')}}</h6>
                                    <!-- Quantity -->
                                    <div class="border rounded-pill py-2 px-3 border-color-1">
                                        <div class="js-quantity row align-items-center">
                                            <div class="col">
                                                <input class="js-result form-control h-auto border-0 rounded p-0 shadow-none" type="text" value="1">
                                            </div>
                                            <div class="col-auto @if(\Illuminate\Support\Facades\App::getLocale()=='ar') pl-1 @else pr-1 @endif">
                                                <a class="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;">
                                                    <small class="fas fa-minus btn-icon__inner"></small>
                                                </a>
                                                <a class="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;">
                                                    <small class="fas fa-plus btn-icon__inner"></small>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Quantity -->
                                </div>


                                @if($pr->total>0 && $pr->sold>=$pr->total)
                                    @php
                                      $winner = \App\Models\Winner::where('product_id',$pr->id)->first();
                                      $user =  \App\Models\User::find( $winner->user_id);

                                    @endphp
                                    <div class="@if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-md-3 @else ml-md-3 @endif">
{{--                                     {{__('jbq.Winner')}} : {{$user->name}} {{$user->family}}--}}
                                    </div>
                                @else
                                    <div class="@if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-md-3 @else ml-md-3 @endif">
                                        <a href="#" onclick="addToCart({{$pr->id}})" class="btn px-5 btn-primary-dark transition-3d-hover"><i class="ec ec-add-to-cart @if(\Illuminate\Support\Facades\App::getLocale()=='ar') ml-2 @else mr-2 @endif font-size-20"></i>{{__('jbq.Add to Cart')}}</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Single Product Body -->
        <div class="mb-6" >
            <div id="Reviews" class="mx-md-2">
                <div class="position-relative mb-6">
                    <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-lg-down-bottom-0 pb-1 pb-xl-0 mb-n1 mb-xl-0">
                        <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                            <a class="nav-link active" href="#Reviews">
                                <div class="d-md-flex justify-content-md-center align-items-md-center">
                                    {{__('jbq.reviews')}}
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="mb-4 px-lg-4">
                    <div class="row mb-8">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <h2 class="font-size-30 font-weight-bold text-lh-1 mb-0">{{$pr->rateAvg}}</h2>
                                <div class="text-lh-1">{{__('jbq.overall')}}</div>
                            </div>

                            <!-- Ratings -->
                            <ul class="list-unstyled">
                                @for($i=4;$i>=0;$i--)
                                <li class="py-1">
                                    <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                        <div class="col-auto mb-2 mb-md-0">
                                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                @for($j=0;$j<$i+1;$j++)
                                                    <small class="fas fa-star"></small>
                                                @endfor
                                                @for($l=0;$l<4-$i;$l++)
                                                        <small class="fas fa-star text-muted"></small>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="col-auto mb-2 mb-md-0">
                                            <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                <div class="progress-bar" role="progressbar" style="width: {{$pr->getTotalReview()>0 ? ($pr->getAvgReviewByRate($i+1)/$pr->getTotalReview())*100 : 0 }}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-auto text-right">
                                            <span class="text-gray-90">{{$pr->getAvgReviewByRate($i+1)}}</span>
                                        </div>
                                    </a>
                                </li>
                                @endfor
                            </ul>
                            <!-- End Ratings -->
                        </div>
                        <div class="col-md-6">
                            <h3 class="font-size-18 mb-5">Add a review</h3>
                            <!-- Form -->
                                <div class="row align-items-center mb-4">
                                    <div class="col-md-4 col-lg-3">
                                        <label for="rating" class="form-label mb-0">{{__('jbq.Your Review')}}</label>
                                    </div>
                                    <div class="col-md-8 col-lg-9">
                                            <div class="text-warning text-ls-n2 font-size-16" id="rater">
                                            </div>
                                    </div>
                                </div>
                                <div class="js-form-message form-group mb-3 row">
                                    <div class="col-md-4 col-lg-3">
                                        <label for="descriptionTextarea" class="form-label">{{__('jbq.Your Review')}}</label>
                                    </div>
                                    <div class="col-md-8 col-lg-9">
                                                        <textarea class="form-control" rows="3" id="tbRTxt"
                                                                  data-msg="Please enter your message."
                                                                  data-error-class="u-has-error"
                                                                  data-success-class="u-has-success"></textarea>
                                    </div>
                                </div>
                                <div class="js-form-message form-group mb-3 row">
                                    <div class="col-md-4 col-lg-3">
                                        <label for="inputName" class="form-label">{{__('jbq.Name')}} <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-md-8 col-lg-9">
                                        <input type="text" class="form-control" name="name" id="tbRName" aria-label="Example" required
                                               data-msg="Please enter your name."
                                               data-error-class="u-has-error"
                                               data-success-class="u-has-success">
                                    </div>
                                </div>
                                <div class="js-form-message form-group mb-3 row">
                                    <div class="col-md-4 col-lg-3">
                                        <label for="emailAddress" class="form-label">{{__('jbq.Email address')}} <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-md-8 col-lg-9">
                                        <input type="email" class="form-control" name="emailAddress" id="tbREmail" aria-label="example@example.com" required
                                               data-msg="Please enter a valid email address."
                                               data-error-class="u-has-error"
                                               data-success-class="u-has-success">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="offset-md-4 offset-lg-3 col-auto">
                                        <button type="submit" class="btn btn-primary-dark btn-wide transition-3d-hover save-review">{{__('jbq.Add a review')}}</button>
                                    </div>
                                </div>
                            <!-- End Form -->
                        </div>
                    </div>

                    @forelse(\App\Models\Review::where('product_id',$pr->id)->where('verify',1)->get() as $review)
                    <!-- Review -->
                    <div class="border-bottom border-color-1 pb-4 mb-4">
                        <!-- Review Rating -->
                        <div class="d-flex justify-content-between align-items-center text-secondary font-size-1 mb-2">
                            <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                @for($i=0;$i<$review->rate;$i++)
                                    <small class="fas fa-star"></small>
                                @endfor
                                @for($i=0;$i<5-$review->rate;$i++)
                                    <small class="far fa-star text-muted"></small>
                                @endfor
                            </div>
                        </div>
                        <!-- End Review Rating -->

                        <p class="text-gray-90">
                            {{$review->text}}
                        </p>
                        <!-- Reviewer -->
                        <div class="mb-2">
                            <strong>{{$review->name}}</strong>
                            <span class="font-size-13 text-gray-23">- {{$review->created_at->ago()}}</span>
                        </div>
                        <!-- End Reviewer -->
                    </div>
                    <!-- End Review -->
                        @empty
                        <div class="text-center">
                            <h3>
                            {{__('jbq.No Reviews')}}
                            </h3>
                        </div>
                    @endforelse

                </div>
            </div>
        </div>
            <!-- Related products -->
            <div class="mb-6">
                <div class="d-flex justify-content-between align-items-center border-bottom border-color-1 flex-lg-nowrap flex-wrap mb-4">
                    <h3 class="section-title mb-0 pb-2 font-size-22">{{__('jbq.Related products')}}</h3>
                </div>
                <ul class="row list-unstyled products-group no-gutters">
                    @foreach(\App\Models\Product::where('category_id',$pr->category->id)->take(6)->get() as $related)
                        @include('product_box_slider',['pr'=>$related,'classes'=>'col-6 col-md-3 col-xl-2gdot4-only col-wd-2 product-item'])
                    @endforeach
                </ul>
            </div>
            <!-- End Related products -->
        </div>
    </main>
@endsection

@section('script')
    <script>
        var myRater=0;
        async function saveReview(callBack){
            $.ajax(
                {
                    url: '{{url('/review/save')}}',
                    type: 'post',
                    dataType: "JSON",
                    data: {
                        "_method": 'post',
                        "productId": {{$pr->id}},
                        "rate":myRater.getRating(),
                        "text":$('#tbRTxt').val(),
                        "name":$('#tbRName').val(),
                        "email":$('#tbREmail').val()
                    },
                    success: function (res) {
                        if(!res.res){
                            $('#Reviews').append('<div class="alert alert-warning alert-dismissible fade show" role="alert" >'+res.msg+'  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>');
                        } else {
                            $('#Reviews').append('<div class="alert alert-success alert-dismissible fade show" role="alert" >{{__('jbq.Success Review')}}  <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span></button></div>');
                        }
                    },
                    error: function (xhr, stat) {
                        alert('Error');
                    },
                    complete: function(){
                        callBack();
                    }
                });
        }
        $(window).on('load', function () {
             myRater = rater({element: document.querySelector("#rater"),rateCallback:function rateCallback(rating, done) {
                    this.setRating(rating);
                    done();
                }});
            $('.save-review').on('click',function(){
                $('.save-review').attr('disabled','disabled');
                saveReview(function(){
                    $('.save-review').removeAttr('disabled');
                });
            });
            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                direction: 'horizontal',
                pageContainer: $('.container'),
                breakpoint: 767.98,
                hideTimeOut: 0
            });

            // initialization of svg injector module
            // $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
        });

        $(function () {
            console.log('ready');
            // initialization of header
            $.HSCore.components.HSHeader.init($('#header'));

            // initialization of animation
            $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus();
                }
            });

            // initialization of popups
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of countdowns
            var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
                yearsElSelector: '.js-cd-years',
                monthsElSelector: '.js-cd-months',
                daysElSelector: '.js-cd-days',
                hoursElSelector: '.js-cd-hours',
                minutesElSelector: '.js-cd-minutes',
                secondsElSelector: '.js-cd-seconds'
            });


            // initialization of forms
            $.HSCore.components.HSFocusState.init();

            // initialization of form validation
            // $.HSCore.components.HSValidation.init('.js-validate', {
            //     rules: {
            //         confirmPassword: {
            //             equalTo: '#signupPassword'
            //         }
            //     }
            // });

            // initialization of show animations
            $.HSCore.components.HSShowAnimation.init('.js-animation-link');

            // initialization of fancybox
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of slick carousel
            $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of hamburgers
            $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                beforeClose: function () {
                    $('#hamburgerTrigger').removeClass('is-active');
                },
                afterClose: function() {
                    $('#headerSidebarList .collapse.show').collapse('hide');
                }
            });

            $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                e.preventDefault();

                var target = $(this).data('target');

                if($(this).attr('aria-expanded') === "true") {
                    $(target).collapse('hide');
                } else {
                    $(target).collapse('show');
                }
            });

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

            // initialization of select picker
            $.HSCore.components.HSSelectPicker.init('.js-select');

            $.HSCore.components.HSQantityCounter.init('.js-quantity');
        });


    </script>
@endsection
