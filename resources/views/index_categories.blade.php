
    <!-- Banner -->
    <div class="mb-5">
        <div class="row">
            @php
                $ics = \App\Models\IndexCategory::all();
            @endphp

            @foreach($ics as $ic)

            <div class="col-md-6 mb-4 mb-xl-0 col-xl-3">
                <a href="{{url('category')}}/{{$ic->id}}" class="d-black text-gray-90">
                    <div class="min-height-132 py-1 d-flex bg-gray-1 align-items-center">
                        <div class="col-6 col-xl-5 col-wd-6 pr-0">
                            <img class="img-fluid" src="{{asset('upload/index_category/')}}/{{$ic->file_name}}" alt="@if(\Illuminate\Support\Facades\App::getLocale()=='ar') {{$ic->title_ar}} @else {{$ic->title_en}} @endif">
                        </div>
                        <div class="col-6 col-xl-7 col-wd-6">
                            <div class="mb-2 pb-1 font-size-18 font-weight-light text-ls-n1 text-lh-23">
                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                    {!!$ic->desc_ar!!}
                                @else
                                    {!!$ic->desc_en!!}
                                @endif
                            </div>
                            <div class="link text-gray-90 font-weight-bold font-size-15" href="#">
                                {{__('jbq.Shop Now')}}
                                <span class="link__icon @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-1 @else ml-1 @endif">
                                    <span class="link__icon-inner"><i class="ec @if(\Illuminate\Support\Facades\App::getLocale()=='ar') ec-arrow-left-categproes @else ec-arrow-right-categproes @endif"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <!-- End Banner -->
    <!-- Deals-and-tabs -->
    <!-- End Deals-and-tabs -->


