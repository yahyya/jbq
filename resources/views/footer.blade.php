<div class="bg-primary py-3">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 mb-md-3 mb-lg-0">
                <div class="row align-items-center">
                    <div class="col-auto flex-horizontal-center">
                        <i class="ec ec-newsletter font-size-40"></i>
                        <h5 class="font-size-20 mb-0 @if(\Illuminate\Support\Facades\App::getLocale()=='ar') mr-3 @else ml-3 @endif">{{__('jbq.Sign up to Newsletter')}}</h5>
                    </div>
                    <div class="col my-4 my-md-0">

                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <!-- Subscribe Form -->
                <form class="js-validate js-form-message">
                    <label class="sr-only" for="subscribeSrEmail">{{__('jbq.Email address')}}</label>
                    <div class="input-group input-group-pill @if(\Illuminate\Support\Facades\App::getLocale()=='ar') flex-row-reverse @endif" >
                        <input type="email" class="form-control border-0 height-40" name="email" id="subscribeSrEmail" placeholder="{{__('jbq.Email address')}}" aria-label="{{__('jbq.Email address')}}" aria-describedby="subscribeButton" required
                               data-msg="{{__('jbq.Please enter a valid email address')}}">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-dark btn-sm-wide height-40 py-2" id="subscribeButton">{{__('jbq.Sign Up')}}</button>
                        </div>
                    </div>
                </form>
                <!-- End Subscribe Form -->
            </div>
        </div>
    </div>
</div>
<div class="pt-8 pb-4 bg-gray-13">
    <div class="container mt-1">
        <div class="row" @if(\Illuminate\Support\Facades\App::getLocale()=='ar') style="text-align: right" @endif>
            <div class="col-lg-5">
                <div class="mb-6">
                    <a href="#" class="d-inline-block">
                        <img src="{{asset("images/logo.png")}}" />
                    </a>
                </div>
                <div class="mb-4">
                    <div class="row no-gutters">
                        <div class="col-auto">
                            <i class="ec ec-support text-primary font-size-56"></i>
                        </div>
                        <div class="col @if(\Illuminate\Support\Facades\App::getLocale()=='ar') pr-3 @else pl-3 @endif">
                            <div class="font-size-13 font-weight-light">{{__('jbq.Got questions')}}? {{__('jbq.Call us 24/7')}}!</div>
                            <a href="tel:+042727700" class="font-size-20 text-gray-90">042727700, </a><a href="tel:042727700" class="font-size-20 text-gray-90">042727700</a>
                        </div>
                    </div>
                </div>
                <div class="mb-4">
                    <h6 class="mb-1 font-weight-bold">{{__('jbq.Contact info')}}</h6>
                    <address class="">
                        {{__('jbq.Address Text')}}
                    </address>
                </div>
                <div class="my-4 my-md-4">
                    <ul class="list-inline mb-0 opacity-7">
                        <li class="list-inline-item @if(\Illuminate\Support\Facades\App::getLocale()=='ar') ml-0 @else mr-0 @endif">
                            <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                <span class="fab fa-facebook-f btn-icon__inner"></span>
                            </a>
                        </li>
                        <li class="list-inline-item @if(\Illuminate\Support\Facades\App::getLocale()=='ar') ml-0 @else mr-0 @endif">
                            <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                <span class="fab fa-google btn-icon__inner"></span>
                            </a>
                        </li>
                        <li class="list-inline-item @if(\Illuminate\Support\Facades\App::getLocale()=='ar') ml-0 @else mr-0 @endif">
                            <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                <span class="fab fa-twitter btn-icon__inner"></span>
                            </a>
                        </li>
                        <li class="list-inline-item @if(\Illuminate\Support\Facades\App::getLocale()=='ar') ml-0 @else mr-0 @endif">
                            <a class="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                                <span class="fab fa-github btn-icon__inner"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-12 col-md mb-4 mb-md-0">
                        <h6 class="mb-3 font-weight-bold">{{__('jbq.Find it Fast')}}</h6>
                        <!-- List Group -->
                        <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                            @foreach(\App\Models\Category::all() as $cat)
                                <li><a class="list-group-item list-group-item-action" href="{{url('category')}}/{{$cat->id}}">
                                        @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                            {{$cat->title_ar}}
                                        @else
                                            {{$cat->title_en}}
                                        @endif</a></li>
                            @endforeach
                        </ul>
                        <!-- End List Group -->
                    </div>

{{--                    <div class="col-12 col-md mb-4 mb-md-0">--}}
{{--                        <!-- List Group -->--}}
{{--                        <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent mt-md-6">--}}
{{--                            <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Printers & Ink</a></li>--}}
{{--                            <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Software</a></li>--}}
{{--                            <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Office Supplies</a></li>--}}
{{--                            <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Computer Components</a></li>--}}
{{--                            <li><a class="list-group-item list-group-item-action" href="../shop/product-categories-5-column-sidebar.html">Accesories</a></li>--}}
{{--                        </ul>--}}
{{--                        <!-- End List Group -->--}}
{{--                    </div>--}}

                    <div class="col-12 col-md mb-4 mb-md-0">
                        <h6 class="mb-3 font-weight-bold">{{__('jbq.Customer Care')}}</h6>
                        <!-- List Group -->
                        <ul class="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                            <li><a class="list-group-item list-group-item-action" href="{{url('user')}}">{{__('jbq.My Account')}}</a></li>
                            <li><a class="list-group-item list-group-item-action" href="{{url('user/orders')}}">{{__('jbq.Order Tracking')}}</a></li>
                            <li><a class="list-group-item list-group-item-action" href="{{url('wishlist')}}">{{__('jbq.Wish List')}}</a></li>
                            <li><a class="list-group-item list-group-item-action" href="{{url('/terms-and-services')}}">{{__('jbq.Customer Service')}}</a></li>
                            <li><a class="list-group-item list-group-item-action" href="{{url('/terms-and-services')}}">{{__('jbq.Returns / Exchange')}}</a></li>
                            <li><a class="list-group-item list-group-item-action" href="{{url('/faq')}}">{{__('jbq.FAQs')}}</a></li>
                            <li><a class="list-group-item list-group-item-action" href="{{url('/terms-and-services')}}">{{__('jbq.Product Support')}}</a></li>
                        </ul>
                        <!-- End List Group -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/site.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
    });

    function addToWishList(id){
        $.ajax(
            {
                url: '{{url('/wishlist/add')}}',
                type: 'post',
                dataType: "JSON",
                data: {
                    "_method": 'post',
                    "cart":"wishlist",
                    'id':id
                },
                success: function (res) {
                   $('.favoriteIcon').css({'color':'red'});
                },
                error: function (xhr, stat) {
                    alert('Error');
                }
            });
    }

    function updateBasketUi(){
        $('#basketItemsList').html('');
        $.ajax(
            {
                url: '{{url('/cart')}}',
                type: 'get',
                dataType: "JSON",
                data: {
                    "_method": 'post',
                    "cart":"shopping"
                },
                success: function (res) {
                    $('.cartQty').html(res.res.length);
                    $('.cartSubtotal').html(res.subtotal);

                    if(res.res.length>0){
                        $.each(res.res,function(i,item){
                            $('#basketItemsList').append('<li class="border-bottom pb-3 mb-3">'+
                                '<div> <ul class="list-unstyled row mx-n2"><li class="px-2 col-auto"><img width="100" class="img-fluid" src="'+item.image+'"></li>'+
                            '<li class="px-2 col"><h5 class="text-blue font-size-14 font-weight-bold">'+item.title+'</h5><span class="font-size-14">'+item.qty+' × '+item.price+'</span></li>'+
                            '<li class="px-2 col-auto">'+
                                '<a href="#" onclick="removeFromBasket(\''+item.rowId+'\')" class="text-gray-90"><i class="ec ec-close-remove"></i></a>'+
                            '</li></ul></div></li>');
                        });
                    }
                },
                error: function (xhr, stat) {
                    alert('Error');
                }
            });
    }

    function addToCart(id){
        $('.btn.addToCart').attr('disabled','disabled');
        $.ajax(
            {
                url: '{{url('/cart/add')}}',
                type: 'post',
                dataType: "JSON",
                data: {
                    "_method": 'post',
                    "productId": id,
                    "qty": $('.js-quantity .js-result').val(),
                },
                success: function (res) {
                    $('.btn.addToCart').removeAttr('disabled');
                    if (res.res) {
                        updateBasketUi();
                    } else {
                        alert('Error');
                    }
                },
                error: function (xhr, stat) {
                    alert('Error');
                }
            });
    }

    function removeFromBasket(id){
        $.ajax(
            {
                url: '{{url('/cart/remove')}}',
                type: 'post',
                dataType: "JSON",
                data: {
                    "_method": 'post',
                    "rowId": id,
                },
                success: function (res) {
                    if(res.res)
                        updateBasketUi();
                },
                error: function (xhr, stat) {
                    alert('Error');
                }
            });
    }
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        const lazyContent = new LazyLoad({
        });
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            direction: 'horizontal',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });
    });
</script>
@yield('script')

</body>
</html>
