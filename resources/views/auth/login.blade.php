@extends('header',['dontShowBasket'=>true])
@section('content')
    <div class="container">
        <div class="mb-4 mt-5">
            {{--            <h1 class="text-center">{{__('jbq.Login')}}</h1>--}}
        </div>
        <div class="my-4 my-xl-8">
            <div class="row">
                <div class="col-md-5 m-xl-auto mr-md-auto mr-xl-0 mb-8 mb-md-0">
                    <!-- Title -->
                    <div class="border-bottom border-color-1 mb-6">
                        <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26">{{__('Login')}}</h3>
                    </div>
                    <p class="text-gray-90 mb-4">{{__('Welcome back! Sign in to your account.')}}</p>
                    <!-- End Title -->
                    <form method="POST" action="{{ route('login') }}" class="js-validate" novalidate="novalidate">
                    @csrf
                        <!-- Form Group -->
                        <div class="js-form-message form-group">
                            <label class="form-label" for="signinSrEmailExample3">{{__('Username or Email address')}}
                                <span class="text-danger">*</span>
                            </label>
                            <input  type="email" class="pr-10 form-control @error('email') is-invalid @enderror " name="email" id="signinSrEmailExample3" value="{{ old('email') }}" placeholder="{{__('Username or Email address')}}" aria-label="{{__('Username or Email address')}}" required
                                   data-msg="{{__('Please enter a valid email address.')}}"
                                   data-error-class="u-has-error"
                                   data-success-class="u-has-success">
                        </div>
                        <!-- End Form Group -->

                        <!-- Form Group -->
                        <div class="js-form-message form-group">
                            <label class="form-label" for="signinSrPasswordExample2">Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror " name="password"  id="signinSrPasswordExample2" placeholder="{{__('Password')}}" aria-label="{{__('Password')}}" required
                                   data-msg="{{__('Your password is invalid. Please try again.')}}"
                                   data-error-class="u-has-error"
                                   data-success-class="u-has-success">
                        </div>
                        <!-- End Form Group -->

                        <!-- Checkbox -->
                        <div class="js-form-message mb-3">
                            <div class="custom-control custom-checkbox d-flex align-items-center">
                                <input type="checkbox" class="custom-control-input " id="rememberCheckbox" name="remember"  {{ old('remember') ? 'checked' : '' }} required autocomplete="current-password"
                                       data-error-class="u-has-error"
                                       data-success-class="u-has-success">
                                <label class="custom-control-label form-label" for="rememberCheckbox">
                                    {{__('Remember me')}}
                                </label>
                            </div>
                        </div>
                        <!-- End Checkbox -->

                        <!-- Button -->
                        <div class="mb-1">
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary-dark-w px-5">{{__('Login')}}</button>
                            </div>
                            <div class="mb-2">
                                <a class="text-blue" href="{{route('password.request')}}">{{__('Lost your password?')}}</a> | <a class="text-blue" href="{{route('register')}}" >{{__('Dont have an account? Create one')}}</a>
                            </div>
                        </div>
                        <!-- End Button -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
