<!DOCTYPE html>
<html @if(\Illuminate\Support\Facades\App::getLocale()=='ar') dir="rtl" lang="ar" @else lang="en" @endif>
<head>
    <!-- Title -->
    <title>@if(!empty($title)) {{$title}} @else JBQ Shop @endif</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@if(!empty($desc)) {{$desc}} @else JBQ Shop @endif"/>
    <meta name="keywords" content="@if(!empty($meta)) {{$meta}} @else Shop @endif"/>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('')}}/favicon.png">

    <!-- CSS Implementing Plugins -->
    <link href="{{asset('css/site.css')}}" rel="stylesheet" media="screen"/>
    @if(App::getLocale()=='ar')
        <style>
            .dropdown-toggle::after,.u-header__nav-link-toggle::after{
                margin-left: auto!important;
                margin-right: 0.5rem!important;
            }
            body{
                font-family: "Tajawal", serif !important;
            }
        </style>
    @endif

    <script src="//code-eu1.jivosite.com/widget/CfXFbzY3i7" async></script>
</head>

<body >

<!-- ========== HEADER ========== -->
<header id="header" class="u-header u-header-left-aligned-nav">
    <div class="u-header__section">
        <!-- Topbar -->
        <div class="u-header-topbar py-2 d-none d-xl-block">
            <div class="container">
                <div class="d-flex align-items-center">
                    <div class="topbar-left">
                        <a href="{{url('/')}}" class="text-gray-110 font-size-13 hover-on-dark"></a>
                    </div>
                    <div class="topbar-right @if(App::getLocale()=='ar') mr-auto @else ml-auto @endif">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item @if(App::getLocale()=='ar') ml-0 @else mr-0 @endif u-header-topbar__nav-item u-header-topbar__nav-item-border u-header-topbar__nav-item-no-border u-header-topbar__nav-item-border-single">
                                <div class="d-flex align-items-center" style="top: -5px;position: relative;">
                                    <div class="position-relative">
                                        <a id="languageDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center u-header-topbar__nav-link font-weight-normal" href="javascript:;" role="button"
                                           aria-controls="languageDropdown"
                                           aria-haspopup="true"
                                           aria-expanded="false"
                                           data-unfold-event="hover"
                                           data-unfold-target="#languageDropdown"
                                           data-unfold-type="css-animation"
                                           data-unfold-duration="300"
                                           data-unfold-delay="300"
                                           data-unfold-hide-on-scroll="true"
                                           data-unfold-animation-in="slideInUp"
                                           data-unfold-animation-out="fadeOut">
                                            <span class="d-inline-block d-sm-none">{{App::getLocale()}}</span>
                                            <span class="d-none d-sm-inline-flex align-items-center"><i class="ec arrow-down-search @if(App::getLocale()=='ar') ml-1 @else mr-1 @endif"></i> @if(App::getLocale()=='ar') <img width="20" class=" @if(App::getLocale()=='ar') ml-1 @else mr-1 @endif" src="{{asset('images/uae.svg')}}" /> @else <img width="20" class=" @if(App::getLocale()=='ar') ml-1 @else mr-1 @endif" src="{{asset('images/en.svg')}}" /> @endif {{strtoupper(App::getLocale())}}</span>
                                        </a>

                                        <div id="languageDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="languageDropdownInvoker">
                                            @foreach (\Illuminate\Support\Facades\Config::get('languages') as $lang => $language)
                                                @if ($lang != App::getLocale())
                                                    <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}"> @if($lang=='ar') <img width="20" src="{{asset('images/uae.svg')}}" /> @else <img width="20" src="{{asset('images/en.svg')}}" /> @endif {{$language}}</a>
                                                @else
                                                    <a class="dropdown-item active" href="{{ route('lang.switch', $lang) }}">@if($lang=='ar') <img width="20" src="{{asset('images/uae.svg')}}" /> @else <img width="20" src="{{asset('images/en.svg')}}" /> @endif {{$language}}</a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>


                                </div>
                            </li>

                            @if(!auth()->check())
                                <li class="list-inline-item @if(App::getLocale()=='ar') ml-0 @else mr-0 @endif u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                    <!-- Account Sidebar Toggle Button -->

                                    <a id="sidebarNavToggler" style="background: #fed700;border-radius: 7px;padding: 5px;" href="{{route('login')}}" role="button" class="u-header-topbar__nav-link"
                                       aria-controls="sidebarContent"
                                       aria-haspopup="true"
                                       aria-expanded="false"
                                       data-unfold-event="click"
                                       data-unfold-hide-on-scroll="false"
                                       data-unfold-target="#sidebarContent"
                                       data-unfold-type="css-animation"
                                       data-unfold-animation-in="fadeInRight"
                                       data-unfold-animation-out="fadeOutRight"
                                       data-unfold-duration="500">
                                        <i class="ec ec-user @if(App::getLocale()=='ar') ml-1 @else mr-1 @endif"></i> {{__('jbq.Register')}} <span class="text-gray-50">{{__('jbq.Or')}}</span> {{__('jbq.Sing In')}}
                                    </a>

                                    <!-- End Account Sidebar Toggle Button -->
                                </li>
                            @else

                                <li class="list-inline-item @if(App::getLocale()=='ar') ml-0 @else mr-0 @endif u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                    <!-- Account Sidebar Toggle Button -->

                                    <a id="sidebarNavToggler" href="{{route('home')}}" role="button" class="u-header-topbar__nav-link"
                                       aria-controls="sidebarContent"
                                       aria-haspopup="true"
                                       aria-expanded="false"
                                       data-unfold-event="click"
                                       data-unfold-hide-on-scroll="false"
                                       data-unfold-target="#sidebarContent"
                                       data-unfold-type="css-animation"
                                       data-unfold-animation-in="fadeInRight"
                                       data-unfold-animation-out="fadeOutRight"
                                       data-unfold-duration="500">
                                        <i class="ec ec-user @if(App::getLocale()=='ar') ml-1 @else mr-1 @endif"></i> {{__('jbq.Hello')}} <span class="text-gray-50">{{auth()->user()->name}}</span>
                                    </a>
                                    <!-- End Account Sidebar Toggle Button -->
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Topbar -->

        <!-- Logo-Search-header-icons -->
        <div class="py-2 py-xl-2 bg-primary-down-lg">
            <div class="container my-0dot5 my-xl-0">
                <div class="row align-items-center">
                    <!-- Logo-offcanvas-menu -->
                    <div class="col-auto">
                        <!-- Nav -->
                        <nav class="navbar navbar-expand u-header__navbar py-0 justify-content-xl-between max-width-270 min-width-270">
                            <!-- Logo -->
                            <a class="order-1 order-xl-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-center" href="{{url('/')}}" aria-label="JBQ">
                               <img src="{{asset('images/logo.png')}}" width="100%" />
                            </a>

                        </nav>
                        <!-- End Nav -->

                        <!-- ========== HEADER SIDEBAR ========== -->

                        <!-- ========== END HEADER SIDEBAR ========== -->
                    </div>
                    <!-- End Logo-offcanvas-menu -->
                    <!-- Search Bar -->
                    <div class="col">
                        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space" >
                            <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                <ul class="navbar-nav u-header__navbar-nav">
                                    <li  class="nav-item u-header__nav-item">
                                        <a class="u-header__nav-link" href="{{url('/')}}" >
                                            {{__('jbq.Home')}}
                                        </a>
                                    </li>

                                    <li class="nav-item hs-has-mega-menu u-header__nav-item"
                                        data-event="click"
                                        data-animation-in="slideInUp"
                                        data-animation-out="fadeOut"
                                        data-position="left">
                                        <a id="homeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle text-sale" href="javascript:;" aria-haspopup="true" aria-expanded="false">  {{__('jbq.Products')}}</a>

                                        <!-- Home - Mega Menu -->
                                        <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="homeMegaMenu">
                                            <div class="row u-header__mega-menu-wrapper">
                                                <ul>
                                                    @foreach(\App\Models\Category::all() as $cat)
                                                        <li>
                                                            <a class="nav-link u-header__nav-link" href="{{url('category/')}}/{{$cat->url}}" aria-haspopup="true" aria-expanded="false">
                                                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                                    {{$cat->title_ar}}
                                                                @else
                                                                    {{$cat->title_en}}
                                                                @endif
                                                            </a>
                                                        </li>
                                                @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- End Home - Mega Menu -->
                                    </li>
                                    <li class="nav-item u-header__nav-item">
                                        <a class="nav-link u-header__nav-link" href="{{url('/about-us')}}" >
                                            {{__('jbq.About us')}}
                                        </a>
                                    </li>
                                    <li class="nav-item u-header__nav-item">
                                        <a class="nav-link u-header__nav-link" href="{{url('/contact-us')}}" >
                                            {{__('jbq.Contact us')}}
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </nav>
                    </div>
                    <!-- End Search Bar -->
                    <!-- Header Icons -->
                    <div class="col col-xl-auto @if(App::getLocale()=='ar') text-left text-xl-right pr-0 pr-xl-3 @else text-right text-xl-left pl-0 pl-xl-3 @endif position-static">
                        <div class="d-inline-flex">
                            <ul class="d-flex list-unstyled mb-0 align-items-center">
                                <!-- Search -->
                                <li class="col d-xl-none px-2 px-sm-3 position-static">
                                    <a id="searchClassicInvoker" class="font-size-22 text-gray-90 text-lh-1 btn-text-secondary" href="javascript:;" role="button"
                                       data-toggle="tooltip"
                                       data-placement="top"
                                       title="Search"
                                       aria-controls="searchClassic"
                                       aria-haspopup="true"
                                       aria-expanded="false"
                                       data-unfold-target="#searchClassic"
                                       data-unfold-type="css-animation"
                                       data-unfold-duration="300"
                                       data-unfold-delay="300"
                                       data-unfold-hide-on-scroll="true"
                                       data-unfold-animation-in="slideInUp"
                                       data-unfold-animation-out="fadeOut">
                                        <span class="ec ec-search"></span>
                                    </a>

                                    <!-- Input -->
                                    <div id="searchClassic" class="dropdown-menu dropdown-unfold dropdown-menu-right @if(App::getLocale()=='ar') right-0 @else left-0 @endif mx-2" aria-labelledby="searchClassicInvoker">
                                        <form class="js-focus-state input-group px-3">
                                            <input class="form-control" type="search" placeholder="{{__('jbq.Search Product')}}">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary px-3" type="button"><i class="font-size-18 ec ec-search"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- End Input -->
                                </li>
                                <!-- End Search -->
                                @if(!isset($dontShowBasket))
                                    @include('wish_list_and_basket')
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- End Header Icons -->
                </div>
            </div>
        </div>
        <!-- End Logo-Search-header-icons -->
        @if(@$dontShowBasket)
            @include('yellow_bar')
        @endif
        <!-- Vertical-and-secondary-menu -->
        <div class="d-none d-xl-block container">
            <div class="row">
                <!-- Vertical Menu -->
                <div class="col-md-auto d-none d-xl-block">
                    <div class="max-width-270 min-width-270">
                        <!-- Basics Accordion -->
                        <div id="basicsAccordion">
                            <!-- Card -->
                            @if(@!isset($dontShowBasket))
                            <div class="card border-0">
                                <div class="card-header card-collapse border-0" id="basicsHeadingOne">
                                    <button type="button" class="btn-link btn-remove-focus btn-block d-flex card-btn py-3 text-lh-1 px-4 shadow-none btn-primary rounded-top-lg border-0 font-weight-bold text-gray-90"
                                            data-toggle="collapse"
                                            data-target="#basicsCollapseOne"
                                            aria-expanded="true"
                                            aria-controls="basicsCollapseOne">
                                                <span class="@if(App::getLocale()=='ar') mr-0 ml-2 @else ml-0 mr-2 @endif text-gray-90 ">
                                                    <span class="fa fa-list-ul"></span>
                                                </span>
                                        <span class="@if(App::getLocale()=='ar') pr-1 @else pl-1 @endif text-gray-90">{{__('jbq.All Departments')}}</span>
                                    </button>
                                </div>
                                <div id="basicsCollapseOne" class="collapse show vertical-menu"
                                     aria-labelledby="basicsHeadingOne"
                                     data-parent="#basicsAccordion">
                                    <div class="card-body p-0">
                                        <nav class="js-mega-menu navbar navbar-expand-xl u-header__navbar u-header__navbar--no-space hs-menu-initialized">
                                            <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
                                                <ul class="navbar-nav u-header__navbar-nav">

                                                    @foreach(\App\Models\Category::where('show_sidebar',1)->get() as $cat)
                                                        <li class="nav-item u-header__nav-item"
                                                            data-event="hover"
                                                            data-position="left">
                                                            <a href="{{url('category/')}}/{{$cat->url}}" class="nav-link u-header__nav-link">
                                                                @if(\Illuminate\Support\Facades\App::getLocale()=='ar')
                                                                    {{$cat->title_ar}}
                                                                @else
                                                                    {{$cat->title_en}}
                                                                @endif
                                                            </a>
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!-- End Card -->
                        </div>
                        <!-- End Basics Accordion -->
                    </div>
                </div>
                <!-- End Vertical Menu -->
                <!-- Secondary Menu -->
                <div class="col">

                </div>
                <!-- End Secondary Menu -->
            </div>
        </div>
        <!-- End Vertical-and-secondary-menu -->
    </div>
</header>
<!-- ========== END HEADER ========== -->
<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main">
    @yield('content')
</main>
</div>
@include('footer')
