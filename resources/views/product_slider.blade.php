
<div class="owl-carousel owl-theme owl-carousel-product owl-rtl">


    @foreach($products as $product)

        @include('product_box',['product'=>$product,'classes'=>''])

    @endforeach

</div>
