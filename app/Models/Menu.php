<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    protected $table = 'menus';
    public function getUrl(){
        if(!empty($this->url)){
            return url($this->url);
        }
        if($this->category_id){
            return Category::find($this->category_id)->slug;
        }
        if($this->tag_id){
            return Tag::find($this->tag_id)->slug;
        }
    }

}
