<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHasRate extends Model
{
    protected $table = 'order_has_rate';

    public function order()
    {
        return $this->belongsTo('\App\Models\Order');
    }

    public function user()
    {
        return $this->belongsTo('\App\Models\User');
    }
}
