<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomSubItem extends Model
{
    use SoftDeletes;
    protected $table = 'custom_sub_item';

    public function item(){
        return $this->belongsTo('\App\Models\CustomItem','custom_item_id');
    }
}
