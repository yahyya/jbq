<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomCategory extends Model
{
    use SoftDeletes;
    protected $table = 'custom_category';

    public function items(){
        return $this->hasMany('App\Models\CustomItem')->where('active',1);
    }
}
