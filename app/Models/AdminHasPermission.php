<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminHasPermission extends Model
{
    protected $table = 'admin_has_permission';
    public $timestamps = false;
    public function permission(){
        return $this->hasMany('\App\Models\Permission');

    }
}
