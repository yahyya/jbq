<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


//ashkan
class Address extends Model
{
    public $timestamps = false;
    public function address()
    {
        return $this->belongsTo('\App\Models\UserHasAddress');
    }

    public function zone(){
        return $this->belongsTo('\App\Models\Zone');
    }
}
