<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialOffer extends Model
{
    use SoftDeletes;
    protected $table = 'special_offer';

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
