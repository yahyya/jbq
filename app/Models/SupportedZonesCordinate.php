<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportedZonesCordinate extends Model
{
    protected $table = 'supported_zones_cordinates';
}
