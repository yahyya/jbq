<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;
    protected $table= 'category';
    protected $dates = ['deleted_at'];
    public function products(){
        return $this->hasMany('\App\Models\Product')->where('active',1)->orderBy('order','desc');

    }


    public function getUrlAttribute($val){
        $url = '';
        $items = $this->getParents($this->id);
        foreach($items as $item){
            $url .= $item->slug . '/';
        }
        return $url;
    }

    public function getParents($id,&$items=[]){
        $item = Category::find($id);
        $items[] = $item;
        if(!is_null($item->parent_id)) {
            $this->getParents($item->parent_id,$items);
        }

        return array_reverse($items);
    }

}
