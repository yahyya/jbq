<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{
    use SoftDeletes;
    protected $table= 'order';

    public function courier(){
        return $this->belongsTo('\App\Models\Courier');
    }

    public function user(){
        return $this->belongsTo('\App\Models\User');
    }

    public function factor(){
        return $this->factors()->whereNull('deleted_at')->orderBy('id','desc')->first();
    }

    public function factors(){
        return $this->hasMany('\App\Models\Factor');
    }

    public function address(){
        return $this->belongsTo('\App\Models\Address');
    }
    public function shippingAddress(){
        return $this->belongsTo('\App\Models\Address','shipping_address_id','id');
    }

    public function discount(){
        return $this->hasMany('\App\Models\Discount');
    }

    /*
     * Getters
     */
    function getFaStatusAttribute()
    {
        switch ($this->status_id){
            case 0:
                return '<span class="text-muted">Pending</span>';
                break;
            case 1:
                return '<span class="text-warning">Preparing</span>';
                break;
            case 2:
                return '<span class="text-primary">Sent</span>';
                break;
            case 3:
                return '<span class="text-success">Deliered</span>';
                break;
            case 4:
                return '<span class="text-danger">Canceled</span>';
                break;
            case 5:
                return '<span class="text-danger">Returned</span>';
                break;
                case -1:
                return '<span class="text-muted">Suspended</span>';
                break;
        }
    }
    function getFaPaymentTypeAttribute()
    {
        switch ($this->payment_type) {
            case 0:
                return 'Online';
                break;
            case 1:
                return 'On Place';
                break;
            case 2:
                return 'Credit';
                break;
        }
    }

    function code(){
        return str_pad($this->id,'6','0',STR_PAD_LEFT);
    }

    function status(){
        if($this->status_id==5){
            return 4;
        }
        return $this->status_id;
    }

    function canRate(User $user){
        $canRate = false;
        if ($this->status_id == 3){
            $orderHasRate = OrderHasRate::where('order_id', $this->id)->where('user_id', $user->id)->first();
            if (!$orderHasRate)
                $canRate = true;
        }

        return $canRate;
    }
}
