<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHasAddress extends Model
{
    protected $table='user_has_addresses';
    public $timestamps  = false;
    public function user()
    {
        return $this->belongsTo('\App\Models\User');
    }

    public function address()
    {
        return $this->hasOne('\App\Models\Address');
    }



}
