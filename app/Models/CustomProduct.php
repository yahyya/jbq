<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CustomProduct extends Model
{

    protected $table = 'custom_product';

    public function items(){
        return $this->hasMany('\App\Models\CustomProductHasItem');
    }
}
