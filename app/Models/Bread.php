<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bread extends Model
{
    use SoftDeletes;
    protected $table = 'bread';
    protected $dates = ['deleted_at'];
}
