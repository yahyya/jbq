<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FactorHasItem extends Model
{
    use SoftDeletes;
    protected $table= 'factor_has_items';

    public function factor(){
        return $this->belongsTo('\App\Models\Factor');
    }

    public function product(){
        return $this->belongsTo('\App\Models\Product');
    }

    public function customProduct(){
        return $this->belongsTo('\App\Models\CustomProduct','custom_product_id','id');
    }

    public function bread(){
        return $this->belongsTo('\App\Models\Bread');
    }
}
