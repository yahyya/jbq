<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomRule extends Model
{
    protected $table = 'custom_rule';

    public function item(){
        return $this->belongsTo('\App\Models\CustomItem','first_item_id');
    }

    public function dismatch(){
        return $this->belongsTo('\App\Models\CustomItem','second_item_id');
    }
}
