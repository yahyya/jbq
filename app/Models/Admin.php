<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $table = 'admin';
    protected $fillable = [
        'name','family','email','password',
    ];
    protected $hidden = [
        'password','remember_token',
    ];
    public function permissions(){
        return $this->hasMany('\App\Models\AdminHasPermission');

    }
}
