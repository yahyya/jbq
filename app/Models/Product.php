<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Product extends Model
{
    use SoftDeletes;
    protected $table= 'product';

    public function category(){
        return $this->belongsTo('\App\Models\Category');
    }

    public function getUrlAttribute($val){
        $url = '';
        $items = $this->category->getParents($this->category_id);
        foreach($items as $item){
            $url .= $item->slug . '/';
        }
        if(!empty($this->slug))
            return $url.$this->slug;
        else
            return $url.str_replace(' ','-',$this->title_en);
    }

    public function image(){
        return $this->hasOne('App\Models\ProductHasImage')->where('default',1);
    }

    public function images(){
        return $this->hasMany('App\Models\ProductHasImage');
    }

    public function tags(){
        return $this->hasMany('App\Models\ProductTag');
    }

    public function getTitleValue($v){
        return Controller::convertNumbers($v);
    }

    public function getRateAvgValue(){
        return round(\App\Models\Review::where('product_id',$this->id)->where('verify',1)->avg('rate'),1);
    }

    public function getAvgReviewByRate($rate){
        return \App\Models\Review::where('product_id',$this->id)->where('verify',1)->where('rate',$rate)->count();
    }

    public function getTotalReview(){
        return \App\Models\Review::where('product_id',$this->id)->where('verify',1)->count();
    }

}
