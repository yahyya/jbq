<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Factor extends Model
{
    use SoftDeletes;
    protected $table= 'factors';

    public function items(){
        return $this->hasMany('\App\Models\FactorHasItem');
    }

    public function price(){
        return number_format($this->total) . ' Rials';
    }

}
