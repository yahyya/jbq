<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomItem extends Model
{
    use SoftDeletes;
    protected $table = 'custom_item';

    public function subItems(){
        return $this->hasMany('App\Models\CustomSubItem');
    }
}
