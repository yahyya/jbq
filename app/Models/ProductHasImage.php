<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class ProductHasImage extends Model
{
    public $timestamps = false;
    protected $table= 'product_has_image';

    // relations
    public function product()
    {
        return $this->hasOne('App\Models\Product');
    }

    public function getFileNameAttribute($val){

        if(!empty($val))
            return asset('product/300x300').'/'.$val;
        return asset('images/circle-preloader.svg');
    }

}
