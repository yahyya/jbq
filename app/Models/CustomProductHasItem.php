<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CustomProductHasItem extends Model
{

    protected $table = 'custom_product_has_item';
    public $timestamps = false;
    public function items(){
        return $this->belongsTo('\App\Models\CustomSubItem','custom_sub_item_id');
    }


}
