<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IndexCategory extends Model
{

    protected $table = 'index_categories';

    public function getCategory(){
        return $this->belongsTo(Category::class,'id');
    }
}
