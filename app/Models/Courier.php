<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Courier extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'courier';

    public function orders(){
        return $this->hasMany('\App\Models\Order');
    }
}
