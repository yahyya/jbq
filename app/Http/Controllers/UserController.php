<?php

namespace App\Http\Controllers;

use App\Models\FactorHasItem;
use App\Models\Order;
use App\Models\OrderItems;
use App\Models\Product;
use App\Models\ProductHasImage;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orderTransactions($id,Request $req){
        return DataTables::of(Transaction::where('order_id',$id)->orderBy('id', 'desc'))
            ->editColumn('payment_date', function($transaction){
                return Carbon::createFromTimestamp(strtotime($transaction->payment_date))->format('datetime');
            })
            ->make(true);
    }

    public function orderItems($id,Request $req){
        $order = Order::findOrFail($id);

        $orderItems = $order->factor()->items;

            return DataTables::of($orderItems)
                ->editColumn('product_id', function($item){

                        return @$item->product->title_en;

                })
                ->editColumn('price', function($item){

                        return @$item->product->price;

                })
                ->rawColumns(['status_id','action'])
                ->make(true);

    }
    public function addProduct(Request $req){
        return view('user.product_new');
    }

    public function editProduct($id,Request $req){
        $product = Product::find($id);
        return view('user.product_new',['product'=>$product]);
    }

    public function order($id,Request $req){
        $order = Order::find($id);
        return view('user.order',['order'=>$order]);
    }

    public function saveProduct(Request $request){
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255',
            'category_id' => 'required|numeric',
            'price' => 'required|numeric',
            'fixed_price' => 'nullable|numeric',
            'desc_en' => 'string',
            'desc_ar' => 'string',
            'images' => 'nullable',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }


        // create new product
        $product = new Product;

        $product->title_en = trim($request->title_en);
        $product->title_ar = trim($request->title_ar);
        $product->category_id = trim($request->category_id);
        $product->price = trim($request->price);
        $product->fixed_price = !empty($request->fixed_price) ? trim($request->fixed_price) : null;
        $product->order = @Product::where('category_id',$request->category_id)->orderBy('order','desc')->first()->order + 1;
        $product->desc_en= trim($request->desc_en);
        $product->desc_ar= trim($request->desc_ar);
        $product->user_id = auth()->user()->id;
        $product->active= false;

        if ($product->save()) {

            // Assign image to product
            if (!empty($request->images)) {
                $order = 1;
                foreach ($request->images as $file) {
                    if (asset('upload/temp/' . $file)) {
                        rename(Config::get('fileuploads.uploadFilePath') . $file, Config::get('fileuploads.uploadProductsPath') . $file);
                        $image = new ProductHasImage();
                        $image->product_id = $product->id;
                        $image->file_name = $file;
                        $image->order = $order;
                        if($order==1)
                            $image->default = true;
                        $image->save();
                        $order++;
                    } else {
                        return response()->json([
                            'res' => false,
                            'msg' => 'File not found!'
                        ]);
                    }
                }
            }



            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function updateProduct($id,Request $request){
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'title_ar' => 'required|string|max:255',
            'category_id' => 'required|numeric',
            'price' => 'required|numeric',
            'fixed_price' => 'nullable|numeric',
            'desc_en' => 'string',
            'desc_ar' => 'string',
            'images' => 'nullable',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }


        // create new product
        $product = Product::find($id);

        $product->title_en = trim($request->title_en);
        $product->title_ar = trim($request->title_ar);
        $product->category_id = trim($request->category_id);
        $product->price = trim($request->price);
        $product->fixed_price = !empty($request->fixed_price) ? trim($request->fixed_price) : null;
        $product->order = @Product::where('category_id',$request->category_id)->orderBy('order','desc')->first()->order + 1;
        $product->desc_en= trim($request->desc_en);
        $product->desc_ar= trim($request->desc_ar);
        $product->user_id = auth()->user()->id;
        $product->active= false;

        if ($product->save()) {

            // Assign image to product
            if (!empty($request->images)) {
                $order = 1;
                foreach ($request->images as $file) {
                    if (asset('upload/temp/' . $file)) {
                        rename(Config::get('fileuploads.uploadFilePath') . $file, Config::get('fileuploads.uploadProductsPath') . $file);
                        $image = new ProductHasImage();
                        $image->product_id = $product->id;
                        $image->file_name = $file;
                        $image->order = $order;
                        if($order==1)
                            $image->default = true;
                        $image->save();
                        $order++;
                    } else {
                        return response()->json([
                            'res' => false,
                            'msg' => 'File not found!'
                        ]);
                    }
                }
            }



            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function productsList($id,Request $req){
        $products =Product::select('product.id','product.title_en','category_id','product.price','product.active' ,'product.order')->with('category')
            ->orderBy('order','desc')
            ->orderBy('active','desc')
            ->where('user_id',$id)
            ->orderBy('product.id','desc');
        if(!empty($req->categoryId)){
            $products->where('category_id',$req->categoryId);
        }
        if(!is_null($req->status)){
            $products->where('active',$req->status);
        }
        return DataTables::of($products)
            ->addColumn('action', function ($product) {
//                        $res = '<a href="' . route('admin.products.edit',['id'=>$product->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.products.edit',['id'=>$product->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                $res = ' <a href="' . route('user.products.edit',$product->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="ec ec-compare"></i> Edit</a>';
                return $res;
            })
            ->addColumn('image',function($product){
                return '<img src="'.@$product->image->file_name.'" width="100" />';
            })
            ->editColumn('category_id', function ($product) {
                return @$product->category->title_en;
            })
            ->editColumn('active', function ($product) {
                if($product->active)
                    return 'Active';
                else
                    return 'InActive';
            })
            ->rawColumns(['action','image'])
            ->make(true);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ordersList($id,Request $req)
    {

        if(auth()->user()->type!='1'){
            $userOrders = Order::where('user_id',auth()->user()->id)
                ->select('factors.total','factors.amount','factors.discount_price','order.id', 'user_id', 'courier_id', 'status_id', 'recieve_date', 'payment_type', 'desc', 'order.created_at','tracking_code')
                ->leftJoin('factors','factors.order_id','=','order.id')->take(50)->get();
            return DataTables::of($userOrders)->editColumn('created_at',function($o){
                return Carbon::createFromTimestamp(strtotime($o->created_at))->ago();
            })->addColumn('action',function($o) {
                return '<a href="'.url('/user/order/').'/'.$o->id.'/" class="btn btn-success" >'.__('jbq.view').'</a>';
            })->editColumn('total',function($o){
                if(App::getLocale()=='ar'){
                    return Controller::priceFormat( $o->total);
                } else
                    return Controller::priceFormat($o->total) ;

            })
                ->editColumn('amount',function($o){
                    if(App::getLocale()=='ar'){
                        return Controller::priceFormat($o->amount) ;
                    } else
                        return Controller::priceFormat($o->amount);

                })

                ->make(true);
        }
        $userProducts = Product::where('user_id',$id)->get();
        $userProductsIds = $userProducts->map(function($t){
            return $t->id;
        });

        $userFactures = FactorHasItem::whereIn('product_id',$userProductsIds)->groupBy('factor_id')->take(50)->get();
        $userOrders = [];

        foreach($userFactures as $uf){
            $userOrders[] = $uf->factor;
        }

        return DataTables::of($userOrders)->editColumn('created_at',function($o){
            return Carbon::createFromTimestamp(strtotime($o->created_at))->ago();
        })->addColumn('action',function($o) {
            return '<a href="'.url('/user/order/').'/'.$o->order_id.'/" class="btn btn-success" >'.__('jbq.view').'</a>';
        })->editColumn('total',function($o){
                if(App::getLocale()=='ar'){
                    return !empty($o->total) ? $o->total . ' AED':0;
                } else
                    return !empty($o->total) ? $o->total . ' $':0;

        })
            ->editColumn('amount',function($o){
                if(App::getLocale()=='ar'){
                    return !empty($o->amount) ? $o->amount . ' AED':0;
                } else
                    return !empty($o->amount) ? $o->amount . ' $':0;

            })

            ->make(true);

    }

    public function showOrderItems(Request $req,$id){
        if ($req->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $orders = OrderItems::select(DB::raw('@rownum  := @rownum  + 1 AS rownum , order_items.*,products.title,products.price'))
                ->join('products','products.id','=','order_items.product_id')->where('order_id',$id);
            return DataTables::of($orders)
                ->editColumn('created_at',function($order){
                    return Jalalian::forge($order->created_at)->ago();
                })
                ->editColumn('discount',function($order){
                    return Controller::priceFormat($order->discount);
                })
                ->editColumn('amount',function($order){
                    return Controller::priceFormat($order->amount);
                })
                ->editColumn('image',function($order){
                    $product = Product::find($order->product_id);
                    return '<img width="50" src="'.asset('upload/product/').'/'.$product->image->file_name.'" />';
                })
                ->addColumn('action',function($order){
                    if($order->status=='0')
                        return '<a href="javascript:void(0)" onclick="removeItem('.$order->id.')" ><i class="fa fa-times" ></i></a>';
                    return '-';
                })
                ->editColumn('color',function($order){
                    if($order->color=='1'){
                        return 'مهتابی';
                    }
                    return 'آفتابی';
                })
                ->rawColumns(['action', 'image'])->make(true);
        }
    }
}
