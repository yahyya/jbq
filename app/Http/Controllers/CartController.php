<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Factor;
use App\Models\FactorHasItem;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Stripe\Stripe;

class CartController extends Controller
{
    public function view(Request $req){

        if($req->ajax()) {
            $products = self::getCartProducts();
            return response()->json(['res' => $products, 'subtotal' => Cart::instance('shopping')->subtotal()]);
        }
        return view('cart',['items'=>CartController::getCartProducts()]);
    }

    public function add(Request $req){
        $product = Product::findOrFail($req->productId);
        Cart::instance('shopping')->add($product->id, $product->title_en, @$req->qty ?? 1, $product->fixed_price ?? $product->price);
        return response()->json(['res'=>true]);
    }

    public function remove(Request $req){
        Cart::instance('shopping')->remove($req->rowId);
        return response()->json(['res'=>true]);
    }
    public static function getCartProducts(){
        $allCart = Cart::instance('shopping')->content();
        $products = [];
        foreach ($allCart as $cart) {
            $product = Product::find($cart->id);
            $products[] = [
                'rowId'=>$cart->rowId,
                'id' => $product->id,
                'title' => \Illuminate\Support\Facades\App::getLocale() == 'ar' ? $product->title_ar : $product->title_en,
                'image' => @$product->image->file_name,
                'qty' => $cart->qty,
                'price' => $product->fixed_price ?? $product->price
            ];
        }
        return $products;
    }

    function checkout(Request $req){
        return view('checkout',['products'=>self::getCartProducts()]);
    }

    function saveCheckout(Request $req){

        $products = self::getCartProducts();

        if(empty($products)){
            return view('checkout')->with(['pay_error'=>__('jbq.Cart Is Empty') ,'products'=>self::getCartProducts()]);
        }
        $req->validate([
           'firstName'=>'string|required',
           'lastName'=>'string|required',
           'phone'=>'numeric',
           'emailAddress'=>'email|required',
           'country'=>'string|required',
           'state'=>'string|required',
           'cityAddress'=>'string|required',
           'postcode'=>'string|required',
           'streetAddress'=>'string|required',
           'companyName'=>'string',
        ]);

        $createAccount = $req->createAnaccount;
        if(!empty($createAccount)){
            $user = new User();
            $user->name = $req->firstName;
            $user->family = $req->lastName;
            $user->mobile = $req->phone;
            $user->email = $req->emailAddress;
            $user->password = Hash::make($req->password);
            $user->save();
        } else{
            $user=  auth()->user();
        }

        $address = new Address();
        $address->first_name = $req->firstName;
        $address->last_name = $req->lastName;
        $address->country = $req->country;
        $address->state = $req->state;
        $address->city = $req->cityAddress;
        $address->postcode = $req->postcode;
        $address->apt = $req->apt;
        $address->street_address = $req->streetAddress;
        $address->company_name = $req->companyName;
        $address->email = $req->emailAddress;
        $address->phone = $req->phone;
        $address->user_id = $user->id;
        $address->save();


        if(!empty($req->shippingdiffrentAddress)){
            $shippingAddress = new Address();
            $shippingAddress->first_name = $req->sfirstName;
            $shippingAddress->last_name = $req->slastName;
            $shippingAddress->country = $req->scountry;
            $shippingAddress->state = $req->sstate;
            $shippingAddress->city = $req->scityAddress;
            $shippingAddress->postcode = $req->spostcode;
            $shippingAddress->apt = $req->sapt;
            $shippingAddress->street_address = $req->sstreetAddress;
            $shippingAddress->company_name = $req->scompanyName;
            $shippingAddress->email = $req->semailAddress;
            $shippingAddress->phone = $req->sphone;
            $shippingAddress->user_id = $user->id;
            $shippingAddress->save();
        }

        $order = new Order();
        $order->user_id = $user->id;
        $order->address_id = $address->id;
        if(@$shippingAddress)
            $order->shipping_address_id = $shippingAddress->id;
        $order->desc = $req->notes;
        $order->payment_type=0;//online
        $order->status_id =0;
        $order->save();

        $facture = new Factor();
        $facture->order_id = $order->id;
        $facture->amount = Cart::instance('shopping')->subtotal();
        $facture->total = Cart::instance('shopping')->subtotal();

        $facture->boxing_price=0;
        $facture->delivery_price=0;
        $facture->save();

        $products = self::getCartProducts();
        foreach($products as $pr){
           $product = Product::find($pr['id']);
            if($product->total>0){
                if($product->sold+1==$product->total){
                    $product->active=0;
                    $product->save();
                }
                if($product->sold+1>$product->total){
                    $product->active=0;
                    $product->save();
                    return view('checkout')->with(['pay_error'=>__('jbq.product_is_no_more_available') . ' : '. $pr['title'],'products'=>self::getCartProducts()]);
                } else {
                    $product->sold = $product->sold+1;
                    $product->save();
                }
            }
            $factureitem = new FactorHasItem();
            $factureitem->factor_id = $facture->id;
            $factureitem->product_id = $pr['id'];
            $factureitem->count = $pr['qty'];
            $factureitem->price = $pr['price'];
            $factureitem->bread_id = 0;
            $factureitem->save();

        }




        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        try {
            $session = \Stripe\Checkout\Session::create([
                'payment_method_types' => ['card'],
                'line_items' => [[
                    'price_data' => [
                        'currency' => 'usd',
                        'product_data' => [
                            'name' => 'Items',
                        ],
                        'unit_amount' => (int) $facture->total * 100,
                    ],
                    'quantity' => 1,
                ]],
                'mode' => 'payment',
                'success_url' => url('/checkout/success'),
                'cancel_url' => url('/checkout/failed'),
            ]);
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->price = $facture->total * 100;
            $transaction->order_id = $order->id;
            $transaction->factor_id = $facture->id;
            $transaction->ip = $_SERVER['REMOTE_ADDR'];
            $transaction->status = 'SENT_TO_STRIPE_PAGE';
            $transaction->tracking_code = 'OK';
            $transaction->save();
            Cart::instance('shopping')->destroy();
            return redirect($session->url);
        } catch (\Exception $ex){
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->price = $facture->total * 100;
            $transaction->order_id = $order->id;
            $transaction->factor_id = $facture->id;
            $transaction->ip = $_SERVER['REMOTE_ADDR'];
            $transaction->status = 'FAILED';
            $transaction->tracking_code = $ex->getMessage();
            $transaction->save();
            return view('checkout')->with(['pay_error'=>$ex->getMessage(),'products'=>self::getCartProducts()]);
        }




        return redirect($checkout_session->url);

    }

    public function success(Request $req){
        print "success";
        print_r($req->all());

    }
    public function failed(Request $req){
        print "failed";
        print_r($req->all());

    }
}
