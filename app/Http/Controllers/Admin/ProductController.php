<?php

namespace App\Http\Controllers\Admin;


use App\Models\Category;
use App\Models\Product;
use App\Models\ProductHasImage;
use App\Models\ProductTag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductController extends AdminController
{
    public function updateDefaultPic($id,Request $req){

        $thePic = ProductHasImage::find($id);
        if($req->def==1){
            ProductHasImage::where('product_id',$thePic->product_id)->update(['default'=>0]);
        }

        $thePic->default = $req->def;
        $thePic->alts = $req->alt;
        $thePic->save();
        return response()->json([
            'res' => true,
            'message' => 'Updated'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $products =Product::select('id','title_en','category_id','price','active' ,'order')->with('category')
                ->orderBy('order','desc')
                ->orderBy('active','desc')
                ->orderBy('id','desc');
            if(!empty($request->categoryId)){
                $products->where('category_id',$request->categoryId);
            }
            if(!is_null($request->status)){
                $products->where('active',$request->status);
            }
            return DataTables::of($products)
                ->addColumn('action', function ($product) {
//                        $res = '<a href="' . route('admin.products.edit',['id'=>$product->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.products.edit',['id'=>$product->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.products.edit',$product->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $product->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->addColumn('image',function($product){
                    return '<img src="'.@$product->image->file_name.'" width="100" />';
                })
                ->editColumn('category_id', function ($product) {
                    return @$product->category->title;
                })
                ->editColumn('active', function ($product) {
                    if($product->active)
                        return 'Active';
                    else
                        return 'InActive';
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        $categories = Category::all();
        return view('admin.products.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.products.addEdit',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'category_id' => 'required|numeric',
            'price' => 'required|numeric',
            'fixed_price' => 'nullable|numeric',
            'desc_en' => 'string',
            'active' => 'boolean',
            'images' => 'nullable',
            'slug'=>'required|string|unique:product'
        ]);


        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }
        // create new product
        $product = new Product;

        $product->title_en = trim($request->title_en);
        $product->title_ar = trim($request->title_ar);
        $product->category_id = trim($request->category_id);
        $product->price = trim($request->price);
        $product->fixed_price = !empty($request->fixed_price) ? trim($request->fixed_price) : null;
        $product->order = @Product::where('category_id',$request->category_id)->orderBy('order','desc')->first()->order + 1;
        $product->desc_en= trim($request->desc_en);
        $product->desc_ar= trim($request->desc_ar);
        $product->total= trim($request->total);
        $product->slug = $request->slug;
        $product->meta_keywords = $request->meta;
        $product->description = $request->desc;
        $product->active= trim($request->active);

        if ($product->save()) {

            // Assign image to product
            if (!empty($request->images)) {
                $order = 1;
                foreach ($request->images as $file) {
                    if (asset('upload/temp/' . $file)) {
                        rename(Config::get('fileuploads.uploadFilePath') . $file, Config::get('fileuploads.uploadProductsPath') . $file);
                        $image = new ProductHasImage;
                        $image->product_id = $product->id;
                        $image->file_name = $file;
                        $image->alt = $product->title_en . ' | ' . $product->title_ar;
                        $image->order = $order;
                        if($order==1)
                            $image->default=1;
                        $image->save();
                        $order++;
                    } else {
                        return response()->json([
                            'res' => false,
                            'msg' => 'File not found!'
                        ]);
                    }
                }
            }



            if(!empty($request->tags)){

                $tags=  explode(',',$request->tags);
                foreach($tags as $tag){
                    $prTag = new ProductTag();
                    $prTag->product_id = $product->id;
                    $prTag->tag_id = $tag;
                    $prTag->save();
                }
            }


            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $categories = Category::all();
        $product = Product::findOrFail($id);
//        foreach($product->breads as $bread)
//            if ($product->breads->contains($bread->id))
//                print($bread->id.',');
//        exit();
        return view('admin.products.addEdit',compact('categories' ,'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'category_id' => 'required|numeric',
            'price' => 'required|numeric',
            'fixed_price' => 'nullable|numeric',
            'desc_en' => 'string',
            'active' => 'boolean',
            'images' => 'nullable',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update product
        $product = Product::findOrFail($id);

        $product->title_en = trim($request->title_en);
        $product->title_ar = trim($request->title_ar);
        $product->category_id = trim($request->category_id);
        $product->price = trim($request->price);
        $product->fixed_price = !empty($request->fixed_price) ? trim($request->fixed_price) : null;
        $product->desc_en= trim($request->desc_en);
        $product->desc_ar= trim($request->desc_ar);
        $product->total= trim($request->total);
        $product->active= trim($request->active);
        $product->slug = $request->slug;
        $product->meta_keywords = $request->meta;
        $product->description = $request->desc;

        if ($product->save()) {

            // Assign image to product
            if (!empty($request->images)) {
                $order = 1;
                foreach ($request->images as $file) {
                    if (asset(Config::get('fileuploads.uploadFilePath') . $file)) {
                        rename(Config::get('fileuploads.uploadFilePath') . $file, Config::get('fileuploads.uploadProductsPath') . $file);
                        $image = new ProductHasImage;
                        $image->product_id = $product->id;
                        $image->file_name = $file;
                        $image->order = $order;
                        if($order==1)
                            $image->default=1;
                        $image->save();
                        $order++;
                    } else {
                        return response()->json([
                            'res' => false,
                            'msg' => 'File not found!'
                        ]);
                    }
                }
            }
            ProductTag::where('product_id',$product->id)->delete();
            if(!empty($request->tags)){
                $tags=  explode(',',$request->tags);
                foreach($tags as $tag){
                    $prTag = new ProductTag();
                    $prTag->product_id = $product->id;
                    $prTag->tag_id = $tag;
                    $prTag->save();
                }
            }


            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($product = Product::findOrFail($id)) {
            $product->delete();
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function destroyPic($id)
    {
        if ($productimage = ProductHasImage::findOrFail($id)) {
            @unlink(Config::get('fileuploads.uploadProductsPath'). $productimage->file_name);
            $productimage->delete();
            return response()->json([
                'res' => true,
                'message' => 'Product imageture deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function setOrder(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'category' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }
        foreach ($request->products as $pro){
            $product = Product::findOrFail($pro['id']);
            $product->order = $pro['newOrder'];
            $product->save();
        }
        return response()->json([
            'res' => true,
            'message' => 'Item order updated.'
        ]);
    }

}
