<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Api\v1\NotificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;
use Yajra\DataTables\DataTables;
use App\Device;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;
use Morilog\Jalali\jDateTime;

class DeviceController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $devices = Device::select('user.mobile as mobile', DB::raw('concat(user.name, \' \', user.family) as fullname'), 'devices.id as id', 'device_model', 'build_number', 'imei', 'devices.user_id', 'os', 'os_version', 'devices.created_at')->leftJoin('user', 'user.id', '=', 'devices.user_id');
//            $devices = Device::select('id', 'device_model', 'build_number', 'imei', 'user_id', 'os', 'os_version', 'created_at')->with(['user']);

            return Datatables::of($devices)
                ->editColumn('created_at', function ($device) {
                    return Jalalian::date('l d F Y H:i', $device->created_at);
                })
                ->addColumn('action', function ($device) {
                    return '<button onclick="notifyModal(' . $device->id . ')" class="btn btn-xs btn-info btn-notify"><i class="material-icons">notifications</i></button>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.devices.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function notify(Request $request)
    {
        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setContentAvailable(true);
        $optionBuiler->setPriority("high");

        $notificationBuilder = new PayloadNotificationBuilder(env('APP_NAME'));
        $notificationBuilder->setBody($request->get('message'))->setSound('default');

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();

        $devices = [];
        if ($request->get('id') == -1) {
            foreach (Device::all() as $device) {
                $devices[] = $device;
            }
        } else {
            $devices[] = Device::where('id',$request->get('id'))->first();
        }
        $tokenClouds = [];
        $appleTokenClouds = [];
        foreach ($devices as $device) {
            if (!empty($device->token_cloud)) {
                if ($device->os == 'iOS')
                    $appleTokenClouds[] = $device->token_cloud;
                else
                    $tokenClouds[] = $device->token_cloud;
            }
        }

        if (!empty($tokenClouds))
            $downstreamResponse = FCM::sendTo($tokenClouds, $option, $notification);
        $appleStream = '';
        if (!empty($appleTokenClouds))
            $appleStream = NotificationController::sendAppleNotification($appleTokenClouds,env('APP_NAME'),$request->message);

        if ($downstreamResponse->numberFailure() == 0  || @$appleStream['res'] == true)
            return response()->json([
                'res' => true,
                'msg' => 'Notification sent for device(s)',
                'devices' => $tokenClouds
            ]);
        else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong : ' . $downstreamResponse->numberFailure() . ' failed'
            ]);

    }

}
