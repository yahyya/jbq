<?php

namespace App\Http\Controllers\Admin;

use App\Models\Address;
use App\Models\Credit;
use App\Models\Device;
use App\Http\Controllers\Api\v1\NotificationController;
use App\Http\Controllers\Api\v1\PaymentController;
use App\Models\Order;
use App\Models\Referral;
use App\Models\Transaction;
use App\Models\UserHasAddress;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Morilog\Jalali\Jalalian;
use Morilog\Jalali\jDate;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(User::select('id', 'name', 'family', 'mobile', 'email','point')
                ->orderBy('id', 'desc'))
                ->addColumn('ordersCount', function ($user){
                    return $user->orders->count();
                })
                ->addColumn('action', function ($user) {
//                    $res = ' <a href="' . route('admin.users.edit',['id'=>$user->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = '';
                    if ($user->banned == 1) {
                        $res = $res . '<button onclick="toggleBanStatus(' . $user->id . ')" class="btn btn-xs btn-warning waves-effect"><i class="material-icons">visibility</i></button>';
                    } else {
                        $res = $res . '<button onclick="toggleBanStatus(' . $user->id . ')" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">block</i></button>';
                    }
                    $res = $res . ' <a href="' . route('admin.users.edit',  $user->id) . '" class="btn btn-xs btn-primary waves-effect">edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $user->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">delete</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'family' => 'required|string',
            'mobile' => 'required|numeric',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new user
        $user = new User;

        $user->name = trim($request->name);
        $user->family = trim($request->family);
        $user->mobile = trim($request->mobile);
        $user->email = trim($request->email);
        $user->type=$request->type;

        if ($user->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.addEdit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'family' => 'required|string',
            'mobile' => 'required|numeric',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update user
        $user = User::findOrFail($id);

        $user->name = trim($request->name);
        $user->family = trim($request->family);
        $user->mobile = trim($request->mobile);
        $user->email = trim($request->email);
        $user->type=$request->type;

        if ($user->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (User::findOrFail($id)) {
            User::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function toggleBanStatus(Request $request)
    {
        $user = User::findOrFail($request->id);
        ($user->banned) ? $user->banned = false : $user->banned = true;
        if ($user->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Ban status updated.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function addresses(Request $request, $id)
    {
        if ($request->ajax()) {

            return DataTables::of(Address::where('user_id',$id)->get())

                ->editColumn('address', function($address){
                    return $address->country . '-'.$address->state.'-'.$address->street_address.'-'.$address->postcode;
                })
                ->addColumn('action', function ($address) {
//                    $res = '<button onclick="toggleBanStatus(' . $address->id . ')" class="btn btn-xs btn-warning waves-effect"><i class="material-icons">visibility</i></button>';
//                    $res = $res . ' <a href="' . route('admin.addresss.addresses', ['id' => $address->id]) . '" class="btn btn-xs btn-primary waves-effect">آدرس ها</a>';
//                    $res = $res . ' <a href="' . route('admin.addresss.edit', ['id' => $address->id]) . '" class="btn btn-xs btn-primary waves-effect">ویرایش</a>';
                    $res = ' <button onclick="deleteAddress(' . $address->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">delete</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function credits(Request $request, $id)
    {
        if ($request->ajax()) {

            return DataTables::of(Credit::where('user_id',$id)->orderBy('id', 'desc'))
                ->make(true);
        }
    }

    public function transactions(Request $request, $id)
    {
        if ($request->ajax()) {
            $transactions = Transaction::where('gateway_transactions.user_id',$id)
                ->join('order','order.id','=','gateway_transactions.order_id', 'left outer')
                ->select('order.tracking_code as order_tracking_code','order.id as the_order_id','gateway_transactions.*')
                ->orderBy('gateway_transactions.id', 'desc');

            return DataTables::of($transactions)
                ->editColumn('price',function($order){
                    return self::priceFormat($order->price,false);
                })
                 ->editColumn('created_at', function($order){
                    return Carbon::create($order->created_at)->format('Y/m/d');
                })
                ->editColumn('created_at_time', function($order){
                    return Carbon::create($order->created_at)->format('H:i:s');
                })
                ->make(true);
        }
    }

    public function orders(Request $request, $id)
    {
        if ($request->ajax()) {

            return DataTables::of(Order::where('user_id',$id)->orderBy('id', 'desc'))
                ->editColumn('courier_id', function($order){
                    return $order->courier_id? $order->courier->name.' '.$order->courier->family : '-';
                })
                ->editColumn('status_id', function($order){
                    return $order->faStatus;
                })
                ->editColumn('payment_type', function($order){
                    return $order->faPaymentType;
                })
                ->editColumn('created_at', function($order){
                    return Jalalian::forge($order->created_at)->format('datetime');
                })
                ->addColumn('action', function ($order) {
                    $res = ' <a href="' . route('admin.orders.show', $order->id) . '" class="btn btn-xs btn-primary waves-effect">View</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $order->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">Delete</button>';
                    return $res;
                })
                ->rawColumns(['status_id','action'])
                ->make(true);
        }
    }

    public function referrals(Request $request, $id)
    {
        if ($request->ajax()) {

            return DataTables::of(Referral::where('user_id',$id)->orderBy('id', 'desc'))
                ->editColumn('created_at', function($order){
                    return Jalalian::forge($order->created_at)->format('datetime');
                })
                ->make(true);
        }
    }

    public function setBalance(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'type' => 'required|string',
            'transaction_id' => 'nullable|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new credit


        $amount = ($request->type == 'increase')? trim($request->amount) : '-'.trim($request->amount);
        $transaction = null;
        $user = User::find($id);
        $admin = Auth::user();
        if(!empty($request->transaction_id)) {
            $transaction = Transaction::find($request->transaction_id);
        }

        if (PaymentController::addCredit($amount,$user,$transaction,$admin)) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function notify(Request $request,$id)
    {


        Log::info(print_r($request->all(),true));
        if($request->get('id')=='-1'){
            $allDevices = Device::all();
            foreach($allDevices as $device){
                if (!empty($device->token_cloud)) {
                    NotificationController::sendNotification($request, $device, $request->message,!empty($request->url) ? $request->url : null,!empty($request->title) ? $request->title : null);
                }
            }

        } else {// not for all!
            $allDevices = Device::where('user_id', $id)->orderBy('updated_at', 'DESC')->get();
            foreach($allDevices as $device) {
                if (!empty($device->token_cloud)) {
                     NotificationController::sendNotification($request, $device, $request->message,!empty($request->url) ? $request->url : null,!empty($request->title) ? $request->title : null);
                }
            }

        }

    }
}
