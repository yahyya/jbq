<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderHasRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Morilog\Jalali\Jalalian;
use Morilog\Jalali\jDate;
use Yajra\DataTables\DataTables;

class OrderHasRateController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(OrderHasRate::select('id', 'user_id', 'order_id', 'personnel', 'quality','taste','delivery', 'desc', 'created_at')
                ->orderBy('id','desc'))
                ->editColumn('user_id', function($rate){
                    return ' <a href="' . route('admin.users.edit', ['id' => $rate->user_id]) . '" class="waves-effect">'.$rate->user->name.' '.$rate->user->family.'</a>';
                })
                ->editColumn('order_id', function($rate){
                    return ' <a href="' . route('admin.orders.show', ['id' => $rate->order_id]) . '" class="waves-effect">'.$rate->order->tracking_code.'</a>';
                })
                ->editColumn('created_at', function($rate){
                    return Jalalian::forge($rate->created_at)->format('datetime');
                })
//                ->addColumn('action', function ($rate) {
//                    $res = ' <a href="' . route('admin.rates.show', ['id' => $rate->id]) . '" class="btn btn-xs btn-primary waves-effect">مدیریت سفارش</a>';
//                    $res = $res . ' <button onclick="deleteItem(' . $rate->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">حذف</button>';
//                    return $res;
//                })
                ->rawColumns(['user_id','order_id','action'])
                ->make(true);
        }
        return view('admin.rates.index');
    }
}
