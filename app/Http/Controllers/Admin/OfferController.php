<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Category;
use App\Offer;
use App\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class OfferController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(Offer::select('id','type','product_id','category_id','percent','amount')
                ->orderBy('id','desc'))
                ->addColumn('action', function ($offer) {
                    $res = ' <a href="' . route('admin.offers.edit',['id'=>$offer->id]) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> ویرایش</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $offer->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> حذف</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.offers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('admin.offers.addEdit',compact('products','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'type' => 'required|numeric',
            'product_id' => 'nullable|numeric',
            'category_id' => 'nullable|numeric',
            'percent' => 'required|numeric',
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new offer
        $offer = new Offer();

        $offer->type = trim($request->type);

        if ($request->type)
            $offer->product_id = trim($request->product_id);
        else
            $offer->category_id = trim($request->category_id);

        $offer->percent = trim($request->percent);
        $offer->amount = trim($request->amount);

        if ($offer->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer = Offer::findOrFail($id);
        $products = Product::all();
        $categories = Category::all();
        return view('admin.offers.addEdit',compact('offer','products','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'type' => 'required|numeric',
            'product_id' => 'nullable|numeric',
            'category_id' => 'nullable|numeric',
            'percent' => 'required|numeric',
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update offer
        $offer = Offer::findOrFail($id);

        $offer->type = trim($request->type);

        if ($request->type)
            $offer->product_id = trim($request->product_id);
        else
            $offer->category_id = trim($request->category_id);

        $offer->percent = trim($request->percent);
        $offer->amount = trim($request->amount);

        if ($offer->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($offer = Offer::findOrFail($id)) {
            $offer->delete();
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }
}
