<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends AdminController
{
    use AuthenticatesUsers{
        logout as adminLogout;
    }

    public function login(Request $request){


        // checks if user is logged in redirects to dashboard
        if (auth()->guard('admin')->check() || auth()->viaRemember())
            return redirect()->route('admin.dashboard');

        // checks if function called with method post (login form submitted)
        if ($request->isMethod('post')){
            if (auth()->guard('admin')->attempt(['email'=>$request['email'],'password'=>$request['password']],$request->filled('remember')))
                return redirect()->route('admin.dashboard');
            return redirect()->back();

        }
        return view('admin.auth.login');
    }

    public function logout(Request $request){
//        $this->adminLogout($request);
        auth()->guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();

//        Auth::logout();
        return redirect()->route('admin.login');
    }
}
