<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\IndexCategory;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class IndexCategoryController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            DB::statement(DB::raw('set @rownum=0'));
            return DataTables::of(IndexCategory::select('id','title_en',DB::raw('@rownum  := @rownum  + 1 AS rownum'))->orderBy('id','desc'))
                ->addColumn('action', function ($category) {
//                        $res = '<a href="' . route('admin.categories.edit',['id'=>$category->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.categories.edit',['id'=>$category->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.indexCategories.edit',$category->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $category->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.index_categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.index_categories.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'active' => 'boolean',
            'category_id'=>'integer|required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new category
        $category = new IndexCategory();

        $category->title_en = trim($request->title_en);
        $category->title_ar = trim($request->title_ar);
        $category->desc_en = trim($request->desc_en);
        $category->desc_ar = trim($request->desc_ar);
        $category->category_id = $request->category_id;

        // check for category image and asign it to category
        if(isset($request->image)) {
            if (asset('upload/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadIndexCategoriesPath') . $request->image);
                $category->file_name = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($category->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = IndexCategory::findOrFail($id);
        return view('admin.index_categories.addEdit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'image' => 'nullable',
            'category_id'=>'integer|required',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update category
        $category = IndexCategory::findOrFail($id);

        $category->title_en = trim($request->title_en);
        $category->title_ar = trim($request->title_ar);
        $category->desc_en = trim($request->desc_en);
        $category->desc_ar = trim($request->desc_ar);
        $category->category_id = $request->category_id;

        // check for category image and asign it to category
        if(isset($request->image)) {
            if (asset('uploads/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadCategoriesPath') . $request->image);
                @unlink(Config::get('fileuploads.uploadIndexCategoriesPath') . $category->image);
                $category->file_name = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($category->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'done'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (IndexCategory::findOrFail($id)) {
            IndexCategory::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

        public function setOrder(Request $request)
        {
            foreach ($request->categories as $cat){
                $category = Category::findOrFail($cat['id']);
                $category->order = $cat['newOrder'];
                $category->save();
            }
            return response()->json([
                'res' => true,
                'message' => 'Item order updated.'
            ]);
        }
}
