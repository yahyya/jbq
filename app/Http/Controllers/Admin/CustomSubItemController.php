<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Config;
use Validator;
use App\Models\CustomItem;
use App\Models\CustomSubItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CustomSubItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(CustomSubItem::orderBy('order','desc'))
                ->addColumn('action', function ($customSubItem) {
                    $res = ' <a href="' . route('admin.custom.subItems.edit',$customSubItem->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> ویرایش</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $customSubItem->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> حذف</button>';
                    return $res;
                })
                ->editColumn('image', function ($courier) {
                    if (isset($courier->main_pic))
                        return '<img src="' . asset('upload/images/sub-items') . '/' . $customSubItem->main_pic . '" class="img-responsive" width="100" height="auto">';
                    else
                        return '';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.custom.subItem.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->id;
        $customItems = CustomItem::all();
        return view('admin.custom.sub-items.addEdit',compact('id','customItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'custom_item_id' => 'required',
            'price' => 'required|numeric',
            'count' => 'required|numeric',
            'active' => 'boolean',
            'main_pic' => 'required|string',
            'stack_pic' => 'required|string',
            'stack_top_pic' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new category
        $customSubItem = new CustomSubItem;

        $customSubItem->custom_item_id = trim($request->custom_item_id);
        $customSubItem->title = trim($request->title);
        $customSubItem->order = (!is_null($lastCustomSubItem = CustomSubItem::orderBy('order','desc')->first()))? $lastCustomSubItem->order + 1 : 1;
        $customSubItem->price = trim($request->price);
        $customSubItem->count = trim($request->count);
        $customSubItem->active = trim($request->active);
        $customSubItem->product_id = $request->product_id;

        if(isset($request->main_pic)) {
            if (asset('upload/temp/' . $request->main_pic)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->main_pic, Config::get('fileuploads.uploadSubItemsPath') . $request->main_pic);
                $customSubItem->main_pic = trim($request->main_pic);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        } else {
            return response()->json([
                'res' => false,
                'msg' => 'تصویر اصلی زیرآیتم آپلود نشده است.'
            ]);
        }

        if(isset($request->stack_pic)) {
            if (asset('upload/temp/' . $request->stack_pic)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->stack_pic, Config::get('fileuploads.uploadSubItemsPath') . $request->stack_pic);
                $customSubItem->stack_pic = trim($request->stack_pic);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        } else {
            return response()->json([
                'res' => false,
                'msg' => 'تصویر استک زیرآیتم آپلود نشده است.'
            ]);
        }

        if(isset($request->stack_top_pic)) {
            if (asset('upload/temp/' . $request->stack_top_pic)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->stack_top_pic, Config::get('fileuploads.uploadSubItemsPath') . $request->stack_top_pic);
                $customSubItem->stack_top_pic = trim($request->stack_top_pic);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($customSubItem->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customSubItem = CustomSubItem::findOrFail($id);
        $id = $customSubItem->custom_item_id;
        $customItems = CustomItem::all();
        return view('admin.custom.sub-items.addEdit',compact('id','customSubItem','customItems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'custom_item_id' => 'required',
            'price' => 'required|numeric',
            'count' => 'required|numeric',
            'active' => 'boolean',
            'main_pic' => 'nullable|string',
            'stack_pic' => 'nullable|string',
            'stack_top_pic' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update category
        $customSubItem = CustomSubItem::findOrFail($id);

        $customSubItem->custom_item_id = trim($request->custom_item_id);
        $customSubItem->title = trim($request->title);
        $customSubItem->price = trim($request->price);
        $customSubItem->count = trim($request->count);
        $customSubItem->active = trim($request->active);
        $customSubItem->product_id = $request->product_id;

        if(isset($request->main_pic)) {
            if (asset('upload/temp/' . $request->main_pic)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->main_pic, Config::get('fileuploads.uploadSubItemsPath') . $request->main_pic);
                $customSubItem->main_pic = trim($request->main_pic);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if(isset($request->stack_pic)) {
            if (asset('upload/temp/' . $request->stack_pic)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->stack_pic, Config::get('fileuploads.uploadSubItemsPath') . $request->stack_pic);
                $customSubItem->stack_pic = trim($request->stack_pic);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if(isset($request->stack_top_pic)) {
            if (asset('upload/temp/' . $request->stack_top_pic)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->stack_top_pic, Config::get('fileuploads.uploadSubItemsPath') . $request->stack_top_pic);
                $customSubItem->stack_top_pic = trim($request->stack_top_pic);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($customSubItem->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'done'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (CustomSubItem::findOrFail($id)) {
            CustomSubItem::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function setOrder(Request $request)
    {
        foreach ($request->items as $item){
            $customSubItem = CustomSubItem::findOrFail($item['id']);
            $customSubItem->order = $item['newOrder'];
            $customSubItem->save();
        }
        return response()->json([
            'res' => true,
            'message' => 'Item order updated.'
        ]);
    }
}
