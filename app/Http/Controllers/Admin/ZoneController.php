<?php

namespace App\Http\Controllers\Admin;

use App\Zone;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ZoneController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(Zone::select('id','title','price','long','lat')
                ->orderBy('id','desc'))
                ->addColumn('action', function ($zone) {
//                        $res = '<a href="' . route('admin.zones.edit',['id'=>$zone->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.zones.edit',['id'=>$zone->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.zones.edit',['id'=>$zone->id]) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> ویرایش</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $zone->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> حذف</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.zones.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.zones.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'lat' => 'numeric',
            'long' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new zone
        $zone = new Zone;

        $zone->title = trim($request->title);
        $zone->price = trim($request->price);
        $zone->long = trim($request->long);
        $zone->lat = trim($request->lat);

        if ($zone->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $zone = Zone::findOrFail($id);
        return view('admin.zones.addEdit',compact('zone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'lat' => 'numeric',
            'long' => 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update zone
        $zone = Zone::findOrFail($id);

        $zone->title = trim($request->title);
        $zone->price = trim($request->price);
        $zone->long = trim($request->long);
        $zone->lat = trim($request->lat);

        if ($zone->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Zone::findOrFail($id)) {
            Zone::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }
}
