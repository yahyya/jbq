<?php

namespace App\Http\Controllers\Admin;

use Morilog\Jalali\Jalalian;
use Morilog\Jalali\jDate;
use Validator;
use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CouponController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd(Coupon::all());
        if ($request->ajax()) {

            return DataTables::of(Coupon::select('id', 'code', 'value', 'type', 'start_date', 'end_date', 'count', 'used')
                ->orderBy('id', 'desc'))
                ->addColumn('action', function ($coupon) {
//                        $res = '<a href="' . route('admin.coupons.edit',['id'=>$coupon->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.coupons.edit',['id'=>$coupon->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.coupons.edit', ['id' => $coupon->id]) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i>ویرایش</a>';
//                    $res = $res . ' <button onclick="deleteItem(' . $coupon->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> حذف</button>';
                    return $res;
                })
                ->editColumn('type', function($coupon){
                    return ($coupon->type == 1)? 'درصد' : 'مبلغ';
                })
                ->editColumn('start_date', function($coupon){
                    return Jalalian::forge($coupon->start_date)->format('datetime');
                })
                ->editColumn('end_date', function($coupon){
                    return Jalalian::forge($coupon->end_date)->format('datetime');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.coupons.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupons.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'value' => 'required|numeric',
            'type' => 'required|boolean',
            'start_date' => 'nullable|string',
            'end_date' => 'nullable|string',
            'count' => 'nullable|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }


        // create new coupon
        $coupon = new Coupon;

        $coupon->code = trim($request->code);
        $coupon->value = trim($request->value);
        $coupon->type = trim($request->type);

        if (!empty($request->start_date)) {
            $startDate = explode('-', $request->start_date);
            $startDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($startDate[0], $startDate[1], $startDate[2]));
            $coupon->start_date = $startDate;
        }
        if (!empty($request->end_date)) {
            $endDate = explode('-', $request->end_date);
            $endDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($endDate[0], $endDate[1], $endDate[2]));
            $coupon->end_date = $endDate;
        }
        if (!empty($request->count)) {
            $coupon->count = trim($request->count);
        }

        if ($coupon->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);
        return view('admin.coupons.addEdit',compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'value' => 'required|numeric',
            'type' => 'required|boolean',
            'start_date' => 'nullable|string',
            'end_date' => 'nullable|string',
            'count' => 'nullable|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }


        // update coupon
        $coupon = Coupon::findOrFail($id);

        $coupon->code = trim($request->code);
        $coupon->value = trim($request->value);
        $coupon->type = trim($request->type);

        if (!empty($request->start_date)) {
            $startDate = explode('-', $request->start_date);
            $startDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($startDate[0], $startDate[1], $startDate[2]));
            $coupon->start_date = $startDate;
        }
        if (!empty($request->end_date)) {
            $endDate = explode('-', $request->end_date);
            $endDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($endDate[0], $endDate[1], $endDate[2]));
            $coupon->end_date = $endDate;
        }
        if (!empty($request->count)) {
            $coupon->count = trim($request->count);
        }

        if ($coupon->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item updated.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Coupon::findOrFail($id)) {
            Coupon::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }
}
