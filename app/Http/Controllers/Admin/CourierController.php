<?php

namespace App\Http\Controllers\Admin;

use App\Courier;
use App\Order;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CourierController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(Courier::select('id', 'name', 'family', 'mobile', 'email', 'image')
                ->orderBy('id', 'desc'))
                ->addColumn('action', function ($courier) {
//                        $res = '<a href="' . route('admin.couriers.edit',['id'=>$courier->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.couriers.edit',['id'=>$courier->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.couriers.edit', ['id' => $courier->id]) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> مدیریت پیک</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $courier->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> حذف</button>';
                    return $res;
                })
                ->editColumn('image', function ($courier) {
                    if (isset($courier->image))
                        return '<img src="' . asset('uploads/images/couriers') . '/' . $courier->image . '" class="img-responsive" width="100" height="auto">';
                    else
                        return '';
                })
                ->rawColumns(['image', 'action'])
                ->make(true);
        }
        return view('admin.couriers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.couriers.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'family' => 'string',
            'mobile' => 'nullable|numeric',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'address' => 'string',
            'image' => 'required',
            'desc' => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        $startDate = "";
        if (!empty($request->start_date)) {
            $startDate = explode('-', $request->start_date);
            $startDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($startDate[0], $startDate[1], $startDate[2]));
        }

        // create new courier
        $courier = new Courier;

        $courier->name = trim($request->name);
        $courier->family = trim($request->family);
        $courier->mobile = trim($request->mobile);
        $courier->email = trim($request->email);
        $courier->password = Hash::make(trim($request->password));
        $courier->address = trim($request->address);
        $courier->image = trim($request->image);
        $courier->start_date = $startDate;
        $courier->desc = trim($request->desc);

        // check for courier image and asign it to courier
        if (asset('uploads/temp/' . $request->image)) {
            rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadCouriersPath') . $request->image);
            $courier->image = trim($request->image);
        } else {
            return response()->json([
                'res' => false,
                'msg' => 'File not found!'
            ]);
        }

        if ($courier->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courier = Courier::findOrFail($id);
        $courierOrders = Order::select('id', 'user_id', 'courier_id', 'status_id', 'recieve_date', 'desc')->where('courier_id', $courier->id)->orderBy('id', 'desc')->get();

        foreach ($courierOrders as $courierOrder) {
            $courierOrder->user_name = @$courierOrder->user->name.' '.@$courierOrder->user->family;
            ($courierOrder->courier_id) ? @$courierOrder->courier_name = @$courierOrder->courier->name.' '.@$courierOrder->courier->family : '-';
            $courierOrder->status = $courierOrder->faStatus;
            $courierOrder->action = ' <a href="' . route('admin.orders.edit', ['id' => $courierOrder->id]) . '" class="btn btn-xs btn-primary waves-effect">جزییات سفارش</a>';
        }

        return view('admin.couriers.addEdit', compact('courier','courierOrders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'family' => 'string',
            'mobile' => 'nullable|numeric',
            'email' => 'required|email',
            'password' => 'nullable|min:6',
            'address' => 'string',
            'image' => 'nullable',
            'desc' => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        $startDate = "";
        if (!empty($request->start_date)) {
            $startDate = explode('-', $request->start_date);
            $startDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($startDate[0], $startDate[1], $startDate[2]));
        }

        // update courier
        $courier = Courier::findOrFail($id);

        $courier->name = trim($request->name);
        $courier->family = trim($request->family);
        $courier->mobile = trim($request->mobile);
        $courier->email = trim($request->email);
        if (isset($request->password))
            $courier->password = Hash::make(trim($request->password));
        $courier->address = trim($request->address);
        $courier->start_date = $startDate;
        $courier->desc = trim($request->desc);

        // check for courier image and asign it to courier
        if (isset($request->image)) {
            if (asset('uploads/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadCouriersPath') . $request->image);
                @unlink(Config::get('fileuploads.uploadCouriersPath') . $courier->image);
                $courier->image = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($courier->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'done'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Courier::findOrFail($id)) {
            Courier::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function orders(Request $request, $id)
    {
//        print 'orders'; exit();
        if ($request->ajax()) {

            $courierOrders = Order::select('id', 'user_id', 'courier_id', 'status_id', 'recieve_date', 'desc')->where('courier_id', $id)->orderBy('id', 'desc')->get();

            foreach ($courierOrders as $courierOrder) {
//                $courierOrder->user_name =
                $courierOrder->action = ' <a href="' . route('admin.orders.edit', ['id' => $courierOrder->id]) . '" class="btn btn-xs btn-primary waves-effect">جزییات سفارش</a>';
            }

            return $courierOrders;

//            return DataTables::of(Order::select('id', 'user_id', 'courier_id', 'status_id', 'recieve_date', 'desc')->where('courier_id', $id)
//                ->orderBy('id', 'desc'))
//                ->editColumn('user_id', function ($order) {
//                    return $order->user->name . ' ' . $order->user->family;
//                })
//                ->editColumn('courier_id', function ($order) {
//                    return $order->courier_id ? $order->courier->name . ' ' . $order->courier->family : '';
//                })
//                ->editColumn('status_id', function ($order) {
//                    return $order->faStatus;
//                })
//                ->addColumn('action', function ($order) {
//                    $res = ' <a href="' . route('admin.orders.edit', ['id' => $order->id]) . '" class="btn btn-xs btn-primary waves-effect">جزییات سفارش</a>';
//                    return $res;
//                })
//                ->rawColumns(['status_id', 'action'])
//                ->make(true);
        }
    }

    public function filterByDates(Request $request, $id)
    {
        if ($request->ajax()) {
            $fromDate = "";
            $toDate = "";
            if (!empty($request->from_date)) {
                $fromDate = explode('-', $request->from_date);
                $fromDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($fromDate[0], $fromDate[1], $fromDate[2]));
            }
            if (!empty($request->to_date)) {
                $toDate = explode('-', $request->to_date);
                $toDate = implode('-', \Morilog\Jalali\CalendarUtils::toGregorian($toDate[0], $toDate[1], $toDate[2]));
            }

            $courierOrders = Order::where('courier_id', $id)->whereBetween('created_at', array($fromDate, $toDate))->orderBy('id', 'desc')->get();
                foreach ($courierOrders as $courierOrder){
                    $courierOrder->user_name = $courierOrder->user->name.' '.$courierOrder->user->family;
                    ($courierOrder->courier_id) ? $courierOrder->courier_name = $courierOrder->courier->name.' '.$courierOrder->courier->family : '-';
                    $courierOrder->status = $courierOrder->faStatus;
                    $courierOrder->action = ' <a href="' . route('admin.orders.edit', ['id' => $courierOrder->id]) . '" class="btn btn-xs btn-primary waves-effect">جزییات سفارش</a>';
                }
            return response()->json($courierOrders);

//            return DataTables::of(Order::select('id', 'user_id', 'courier_id', 'status_id', 'recieve_date', 'desc')->where('courier_id', $id)->whereBetween('created_at', array($fromDate, $toDate))
//                ->orderBy('id', 'desc'))
//                ->editColumn('user_id', function ($order) {
//                    return $order->user->name . ' ' . $order->user->family;
//                })
//                ->editColumn('courier_id', function ($order) {
//                    return $order->courier_id ? $order->courier->name . ' ' . $order->courier->family : '';
//                })
//                ->editColumn('status_id', function ($order) {
//                    return $order->faStatus;
//                })
//                ->addColumn('action', function ($order) {
////                    $res = ' <a href="' . route('admin.orders.edit',['id'=>$order->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
////                    if ($order->banned == 1) {
////                        $res = '<button onclick="toggleBanStatus(' . $order->id . ')" class="btn btn-xs btn-warning waves-effect"><i class="material-icons">visibility</i></button>';
////                    } else {
////                        $res = '<button onclick="toggleBanStatus(' . $order->id . ')" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">block</i></button>';
////                    }
////                    $res = $res . ' <a href="' . route('admin.orders.address', ['id' => $order->id]) . '/" class="btn btn-xs btn-primary waves-effect">آدرس ها</a>';
//                    $res = ' <a href="' . route('admin.orders.edit', ['id' => $order->id]) . '" class="btn btn-xs btn-primary waves-effect">جزییات سفارش</a>';
////                    $res = $res . ' <button onclick="deleteItem(' . $order->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">حذف</button>';
//                    return $res;
//                })
//                ->rawColumns(['status_id', 'action'])
//                ->make(true);

        }
    }
}
