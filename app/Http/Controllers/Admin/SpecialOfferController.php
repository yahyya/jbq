<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Support\Facades\Config;
use Validator;
use App\Models\Category;
use App\Models\SpecialOffer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SpecialOfferController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(
                SpecialOffer::select('*')

                ->orderBy('special_offer.id','desc'))
                ->editColumn('image', function ($specialOffer) {
                    if(isset($specialOffer->image))
                        return '<img src="'.asset('upload/special-offer').'/'.$specialOffer->image.'" class="img-responsive" width="100" height="auto">';
                    else
                        return '';
                })
                ->addColumn('action', function ($specialOffer) {
                    $res = ' <a href="' . route('admin.specialOffers.edit',$specialOffer->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $specialOffer->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->rawColumns(['image','action'])
                ->make(true);
        }
        return view('admin.special-offers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.special-offers.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'product_id' => 'nullable|numeric',
            'image' => 'nullable|string|max:50',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new specialOffer
        $specialOffer = new SpecialOffer;

        $specialOffer->title_en = trim($request->title_en);
        $specialOffer->title_ar = trim($request->title_ar);
        $specialOffer->desc_en = trim($request->desc_en);
        $specialOffer->desc_ar = trim($request->desc_ar);
        $specialOffer->product_id = $request->product_id;
        $specialOffer->exp_date = date('Y-m-d H:i:s',strtotime($request->exp_date));

        if(isset($request->image)) {
            if (asset('upload/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadSpecialOffersPath') . $request->image);
                $specialOffer->image = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($specialOffer->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specialOffer = SpecialOffer::findOrFail($id);
        $categories = Category::all();
        $products = Product::where('category_id',$specialOffer->category_id)->get();
        return view('admin.special-offers.addEdit',compact('specialOffer','categories','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
            'product_id' => 'nullable|numeric',
            'image' => 'nullable|string|max:50',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update specialOffer
        $specialOffer = SpecialOffer::findOrFail($id);
        $specialOffer->title_en = trim($request->title_en);
        $specialOffer->title_ar = trim($request->title_ar);
        $specialOffer->desc_en = trim($request->desc_en);
        $specialOffer->desc_ar = trim($request->desc_ar);
        $specialOffer->product_id = $request->product_id;
        $specialOffer->exp_date = date('Y-m-d H:i:s',strtotime($request->exp_date));
        if(isset($request->image)) {
            if (asset('upload/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadSpecialOffersPath') . $request->image);
                $specialOffer->image = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($specialOffer->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($specialOffer = SpecialOffer::findOrFail($id)) {
            $specialOffer->delete();
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function categoryProducts(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        $products = Product::where('category_id',$request->category_id)->get();

        return response()->json([
            'res' => true,
            'products' => $products,
            'msg' => 'Item saved.'
        ]);
    }
}
