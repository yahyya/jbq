<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Courier;
use App\Models\Credit;
use App\Models\CustomProduct;
use App\Models\Device;
use App\Models\Factor;
use App\Models\FactorHasItem;
use App\Http\Controllers\Api\v1\NotificationController;
use App\Models\Order;
use App\Models\OrderHasRate;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Morilog\Jalali\Jalalian;
use Morilog\Jalali\jDate;
use Morilog\Jalali\jDateTime;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class OrderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getTotalAmounts(Request $request){
         $order = Order::
                leftJoin('factors','factors.order_id','=','order.id')->
                whereNull('factors.deleted_at')->
            select(DB::raw('SUM(factors.total) as sumPrice, count(*) as total'))
                ->orderBy('order.id','desc');
            if( $request->status!= -2 && !is_null($request->status)){
                $order->where('status_id',$request->status);
            }

            $timezone = 'Asia/Tehran';

            if($request->dateFrom){


//                $dateG = explode('-',$request->dateFrom);
//                $dateG = \Morilog\Jalali\CalendarUtils::toGregorian($dateG[0],$dateG[1],$dateG[2]);
//                $dateFrom = $dateG[0].'-'.$dateG[1].'-'.$dateG[2];

//                $today = Carbon::parse($dateFrom.' 7am', $timezone);
                $today = Carbon::parse($request->dateFrom, $timezone);
                $order->whereRaw(DB::raw('order.created_at >= "'.$today->format('Y-m-d H:i').'"'));
            }

            if($request->dateTo){
//                $dateG = explode('-',$request->dateTo);
//                $dateG = \Morilog\Jalali\CalendarUtils::toGregorian($dateG[0],$dateG[1],$dateG[2]);
//                $dateTo = $dateG[0].'-'.$dateG[1].'-'.$dateG[2];
//
//                $tomorrow = Carbon::parse($dateTo)->addDay(1)->hour(4);
                $tomorrow = Carbon::parse($request->dateTo)->addDay(1)->hour(4);
                $order->whereRaw(DB::raw('order.created_at < "'.$tomorrow->format('Y-m-d H:i').'"'));
            }

            if($request->today){
                $today = Carbon::parse('today 7am', $timezone);
                $tomorrow = Carbon::parse('tomorrow 4am', $timezone);
                $order->whereRaw(DB::raw('order.created_at < "'.$tomorrow->format('Y-m-d H:i').'" AND order.created_at >= "'.$today->format('Y-m-d H:i').'"'));
            }

            $orderDetail = $order->first();

            $total = $orderDetail ? $orderDetail->total :0 ;
            $totalPrices = $orderDetail?  $orderDetail->sumPrice : 0;
            return response()->json(['total'=>$total,'sum'=>Controller::priceFormat( $totalPrices,true)]);

    }

    public function checkNew(){
        $orders = Order::
                leftJoin('factors','factors.order_id','=','order.id')->
            select('factors.total','order.id', 'user_id', 'courier_id', 'status_id', 'recieve_date', 'payment_type', 'desc', 'order.created_at','tracking_code')
                ->orderBy('id','desc')->where('status_id',0)->whereRaw(DB::raw('order.created_at >= "'.date('Y-m-d H:i:s',time()-100000).'"'))->get();

        foreach($orders as $order){

           $order->status_id = $order->faStatus;
           $order->created_at = Jalalian::forge($order->created_at->toDateTimeString())->format('Y/m/d');
           $order->created_at_time = Jalalian::forge($order->created_at->toDateTimeString())->format('H:i:s');
           $order->payment_type = $order->faPaymentType;
           $order->total = Controller::priceFormat($order->total,true);
        }
        return response()->json($orders);
    }

    public function index(Request $request)
    {

        if ($request->ajax()) {

            $order = Order::
                leftJoin('factors','factors.order_id','=','order.id')->
            whereNull('factors.deleted_at')->
            select('factors.total','order.id', 'user_id', 'courier_id', 'status_id', 'recieve_date', 'payment_type', 'desc', 'order.created_at','tracking_code')
                ->orderBy('id','desc');
            if( $request->status!= -2 && !is_null($request->status)){
                $order->where('status_id',$request->status);
            }

            $timezone = 'Asia/Tehran';

            if($request->dateFrom){


//                $dateG = explode('-',$request->dateFrom);
//                $dateG = \Morilog\Jalali\CalendarUtils::toGregorian($dateG[0],$dateG[1],$dateG[2]);
//                $dateFrom = $dateG[0].'-'.$dateG[1].'-'.$dateG[2];

//                $today = Carbon::parse($dateFrom.' 7am', $timezone);
//                $today = Carbon::parse($request->dateFrom, $timezone);
//                $order->whereRaw(DB::raw('order.created_at >= "'.$today->format('Y-m-d H:i').'"'));
            }

            if($request->dateTo){
//                $dateG = explode('-',$request->dateTo);
//                $dateG = \Morilog\Jalali\CalendarUtils::toGregorian($dateG[0],$dateG[1],$dateG[2]);
//                $dateTo = $dateG[0].'-'.$dateG[1].'-'.$dateG[2];

//                $tomorrow = Carbon::parse($dateTo)->addDay(1)->hour(4);
//                $tomorrow = Carbon::parse($request->dateTo)->addDay(1)->hour(4);
//                $order->whereRaw(DB::raw('order.created_at < "'.$tomorrow->format('Y-m-d H:i').'"'));
            }

            if($request->today){
//                $today = Carbon::parse('today 7am', $timezone);
//                $tomorrow = Carbon::parse('tomorrow 4am', $timezone);
//                $order->whereRaw(DB::raw('order.created_at < "'.$tomorrow->format('Y-m-d H:i').'" AND order.created_at >= "'.$today->format('Y-m-d H:i').'"'));
            }



            return DataTables::of($order)
                ->editColumn('user_id', function($order){
                    return "<a target='_blank' href='". url("admin/users/".$order->user_id) . "/edit' >" . @$order->user->name.' '. @$order->user->family . "</a>";
                })
                ->editColumn('courier_id', function($order){
                    return @$order->courier_id? @$order->courier->name.' '.@$order->courier->family : '-';
                })
                ->editColumn('status_id', function($order){
                    return @$order->faStatus;
                })
                ->editColumn('payment_type', function($order){
                    return @$order->faPaymentType;
                })
                ->editColumn('created_at', function($order){
                    return Carbon::make($order->created_at)->ago();
                })
                ->editColumn('total',function($order){
                    return Controller::priceFormat($order->total,true);
                })
                ->editColumn('created_at_time', function($order){
                    return Carbon::make($order->created_at)->format('H:i:s');
                })

                ->addColumn('action', function ($order) {
//                    $res = ' <a href="' . route('admin.orders.edit',['id'=>$order->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
//                    if ($order->banned == 1) {
//                        $res = '<button onclick="toggleBanStatus(' . $order->id . ')" class="btn btn-xs btn-warning waves-effect"><i class="material-icons">visibility</i></button>';
//                    } else {
//                        $res = '<button onclick="toggleBanStatus(' . $order->id . ')" class="btn btn-xs btn-danger waves-effect"><i class="material-icons">block</i></button>';
//                    }
//                    $res = $res . ' <a href="' . route('admin.orders.address', ['id' => $order->id]) . '/" class="btn btn-xs btn-primary waves-effect">آدرس ها</a>';
                    $res = '<div style="text-align:right">';
                    $res .= ' <a title="Details" href="' . route('admin.orders.show',  $order->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="material-icons">visibility</i></a>';
                    if(!empty($order->desc)){
                        $res .= ' <a title="Comments" onclick="showDesc(\'' . htmlspecialchars($order->desc) . '\')" href="javascript:void(0)" class="btn btn-xs btn-primary waves-effect"><i class="material-icons">chat</i></a>';
                    }
                    $res .=' <a href="' . route('admin.orders.print', ['id' => $order->id]) . '" class="btn btn-xs btn-primary waves-effect"><i class="material-icons">print</i></a>';
                    $res = $res . ' <button style="float:left" title="Remove" onclick="deleteItem(' . $order->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="material-icons">close</i></button></div>';
                    return $res;
                })
                ->rawColumns(['status_id','action','user_id'])
                ->make(true);
        }
        return view('admin.orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $order = Order::findOrFail($id);
        $order->paied_at = Jalalian::forge($order->paied_at)->format('datetime');

        if ($request->ajax()) {

            $orderItems = $order->factor()->items;

            return DataTables::of($orderItems)
                ->editColumn('product_id', function($item){

                        return @$item->product->title_en;

                })
                ->editColumn('price', function($item){

                        return @$item->product->price;

                })
                ->rawColumns(['status_id','action'])
                ->make(true);
        }
        $couriers = Courier::all();
        return view('admin.orders.order',compact('order','couriers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $products = Product::all();
        $order = Order::findOrFail($id);
        $orderItems = $order->factor()->items;

        foreach ($orderItems as $item){
            $item->product_id = $item->id;
            $item->product_name =@$item->product->title_en;
            $item->price = @$item->product->price*$item->count;
            $res = ' <button onclick="editCount('.$item->product_id.')" class="btn btn-xs btn-primary waves-effect">Edit Total</button>';
            $res = $res . ' <button onclick="deleteItem(this,'.$item->product_id.')" class="btn btn-xs btn-danger btn-delete waves-effect">Remove</button>';
            $item->action = $res;
        }
        return view('admin.orders.edit',compact('id','orderItems','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'orderItems' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }


        // create new factor
        Factor::where('order_id',$id)->delete();
        $factor = new Factor;

        $factor->order_id = $id;


        if ($factor->save()) {
            $amount = 0;
            foreach ($request->orderItems as $item){
                $factorItem = new FactorHasItem;
                $factorItem->factor_id = $factor->id;

                $factorItem->product_id = $item['product_id'];
                $factorItem->count = $item['count'];
                $factorItem->price = $item['price'];
                $factorItem->save();
                $amount += $factorItem->price;
            }
            $factor->amount = $amount;
            $tax = 0;
            $factor->total = $amount + $tax;
            $factor->save();
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $factor  = $order->factors()->delete();
        $shippingAddress = $order->shippingAddress()->delete();
        $address = $order->address()->delete();
        $order->delete();
        return response()->json(['res'=>true]);
    }

    public function destroyItem($id)
    {
        if ($factorItem = FactorHasItem::findOrFail($id)) {
            $factor = $factorItem->factor_id;
            $factor::destroy($id);
            Factor::findOrFail($factor)->amount = FactorHasItem::where('factor_id',$factor)->price->count();
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function setName(Request $req,$id){
        $customProduct = CustomProduct::find($req->id);
        $customProduct->title = $req->name;
        $customProduct->save();
        return response()->json(['res'=>true]);
    }
    public function setStatus(Request $request,$id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'status_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        $order = Order::findOrFail($id);

        // set order status to return
        $order->status_id = trim($request->status_id);
        $order->reason = @!empty($request->reason) ? $request->reason : null;

        //
        $user = User::findOrFail($order->user_id);
        // check if status is retured, should return back money
        if ($request->status_id == 5 || $request->status_id == 4) {

            //check for order's payment_type
//            if ($order->payment_type != 1) { // payment_type != pay in place
//                // create new credit to return money
//                $credit = new Credit();
//                $credit->user_id = $order->user_id;
//                $credit->admin_id = auth()->id();
//                $credit->amount = $order->factor()->total;
//                if($request->status_id==5)
//                    $credit->desc = 'Order With Invoice Number' . $order->id . ' Has Returnedد';
//                if($request->status_id==4)
//                    $credit->desc = 'Order With Invoice Number ' . $order->id . ' Canceled';
//                $credit->save();
//
//
//                $user->balanceUpdate();
//            }
        }

        if($request->notification=='true') {
            $devices = Device::where('user_id', $order->user_id)->orderBy('updated_at', 'DESC')->get();
        foreach($devices as $device) {
            $txt = '';
            if ($request->status_id == 1)
                $txt = 'Your Order is preparing';
            if ($request->status_id == 2)
                $txt = 'Your Order Sent';
            if ($request->status_id == 3)
                $txt = 'Your Order Delivered';
            if ($request->staus_id == 4)
                $txt = 'Your Order Canceled';
            if ($request->staus_id == 5)
                $txt = 'Your Order Returned';
            NotificationController::sendNotification($request, $device, $txt);
        }
        }


        if($request->sms=='true'){

//            Controller::sendRawSms($user->mobile,$txt);
        }

        if ($order->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Order status updated.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function setCourier(Request $request,$id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'courier_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        $order = Order::findOrFail($id);

        $order->courier_id = trim($request->courier_id);

        if ($order->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Order assigned to courier.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function rates(Request $request, $id)
    {
        if ($request->ajax()) {
            return DataTables::of(OrderHasRate::where('order_id',$id)->orderBy('id', 'desc'))
                ->editColumn('user_id', function($rate){
                    $res= ' <a href="' . route('admin.users.edit', ['id' => $rate->user_id]) . '" class="waves-effect">'.$rate->user->name.' '.$rate->user->family.'</a>';

                    return $res;
                })
//                ->editColumn('order_id', function($rate){
//                    return ' <a href="' . route('admin.orders.show', ['id' => $rate->order_id]) . '" class="waves-effect">'.$rate->order->tracking_code.'</a>';
//                })
                ->editColumn('created_at', function($rate){
                    return Jalalian::forge($rate->created_at)->format('datetime');
                })
                ->editColumn('desc',function($rate){
                    if(!empty($rate->desc)){
                        return ' <a title="Show desc" onclick="showDesc(\'' . htmlspecialchars($rate->desc) . '\')" href="javascript:void(0)" class="btn btn-xs btn-primary waves-effect"><i class="material-icons">chat</i></a>';

                    }
                    return '-';
                })
//                ->addColumn('action', function ($rate) {
//                    $res = ' <a href="' . route('admin.rates.show', ['id' => $rate->id]) . '" class="btn btn-xs btn-primary waves-effect">مدیریت سفارش</a>';
//                    $res = $res . ' <button onclick="deleteItem(' . $rate->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">حذف</button>';
//                    return $res;
//                })
                ->rawColumns(['user_id','order_id','action','desc'])
                ->make(true);
        }
    }

    public function transactions(Request $request, $id)
    {
        if ($request->ajax()) {
            return DataTables::of(Transaction::where('order_id',$id)->orderBy('id', 'desc'))
                ->editColumn('payment_date', function($transaction){
                    return Jalalian::forge($transaction->payment_date)->format('datetime');
                })
                ->make(true);
        }
    }

    public function printInvoice(Request $req, $id)
    {
        $order = Order::findOrFail($id);
//        $order->id = ApiController::convertNumbersToPersian($order->id);
//        $order->date = ApiController::convertNumbersToPersian(jDate::forge($order->created_at)->format('Y/m/d'));
//        $order->time = ApiController::convertNumbersToPersian(jDate::forge($order->created_at)->format('H:i:s'));
        if($req->type=='1')
            return view('prints.invoice',compact('order'));
        if($req->type=='2')
            return view('prints.invoice_kitchen',compact('order'));
        if($req->type=='3')
            return view('prints.invoice_lable',compact('order'));
        else
            return view('prints.all',compact('order'));
    }
}
