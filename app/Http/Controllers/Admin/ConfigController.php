<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use \App\Config;
use Illuminate\Support\Facades\URL;
use Yajra\Datatables\Datatables;

class ConfigController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $count = Config::all()->count();
        if($request->ajax()){
            $configs = Config::orderBy('id','desc');
            return Datatables::of($configs)->addColumn('action', function ($configs) {
                return '<a href="'.URL::to('/admin/config/').'/'.$configs->id.'/edit" ><i class="fa fa-edit fa-lg" ></i></a>';
            })->make(true);
        }
        return view('admin.config.index',compact('count'));
    }

    public function create(){
        return false;
    }

    public function edit($id){
        $config = Config::find($id);
        return view('admin.config.addEdit',['config'=>$config]);
    }

    public function store(){
        return false;
    }

    public function show($id){
        $config = Config::find($id);
        return view('admin.config.view',['config'=>$config]);
    }



    public function update($id){

        $config = Config::find($id);
        $config->title= Input::get('title');

        $config->value = Input::get('value');

        if($config->save())
            return response()->json(['res'=>true]);
    }

    public function destroy($id){
        return false;
    }

}
