<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderHasRate;
use App\Models\Product;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;
use Morilog\Jalali\jDate;
use Yajra\DataTables\DataTables;

class ReviewController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(Review::select('*')
                ->orderBy('id','desc'))
                ->editColumn('product_id', function($rate){
                    return ' <a href="' . route('admin.products.show', $rate->product_id) . '" class="waves-effect">'.Product::find($rate->id)->title_en.'</a>';
                })
                ->editColumn('created_at', function($rate){
                    return Jalalian::forge($rate->created_at)->format('datetime');
                })
                ->addColumn('action', function ($rate) {
                    $res = ' <a href="' . route('admin.reviews.show', $rate->id) . '" class="btn btn-xs btn-primary waves-effect">View</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $rate->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">Remove</button>';
                    return $res;
                })
                ->rawColumns(['user_id','product_id','action'])
                ->make(true);
        }
        return view('admin.rates.index');
    }

    public function show($id,Request $req){
        $review = Review::find($id);
        return view('admin.rates.addEdit',['review'=>$review]);
    }

    public function update($id,Request $req){
        $review = Review::find($id);
        $validator = Validator::make($req->all(),[
            'text'=>'min:20',
            'name'=>'required',
            'email'=>'required|email',
        ]);
        if($validator->fails()){
            return response()->json(['res'=>false,'msg'=>implode('<br/>', $validator->getMessageBag()->all())]);
        }
        $review->text = $req->text;
        $review->name = $req->name;
        $review->verify = $req->verify;
        $review->email = $req->email;
        $review->save();
        return response()->json(['res'=>true]);
    }

    public function destroy($id,Request $req){
        if(Review::findOrFail($id)) {
            Review::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
    } else
        return response()->json([
            'res' => false,
            'message' => 'Something went wrong'
        ]);
    }
}
