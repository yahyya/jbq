<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\AdminHasPermission;
use App\Permission;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StatsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(Admin::select('id', 'name', 'family', 'mobile', 'email')
                ->orderBy('id', 'desc'))
                ->addColumn('action', function ($admin) {

                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.admins.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        return view('admin.admins.addEdit',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'family' => 'string',
            'mobile' => 'nullable|numeric',
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new admin
        $admin = new Admin;

        $admin->name = trim($request->name);
        $admin->family = trim($request->family);
        $admin->mobile = trim($request->mobile);
        $admin->email = trim($request->email);
        $admin->password = Hash::make(trim($request->password));

        if ($admin->save()) {

            // Assign permission to admin
            if (!empty($request->permissions)) {
                foreach ($request->permissions as $permission) {

                    $adminPermission = new AdminHasPermission;
                    $adminPermission->admin_id = $admin->id;
                    $adminPermission->permission_id = $permission;

                    if (!$adminPermission->save()) {
                        return response()->json([
                            'res' => false,
                            'msg' => 'Error on save product bread'
                        ]);
                    }
                }
            }

            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = Permission::all();
        $admin = Admin::findOrFail($id);
        return view('admin.admins.addEdit', compact('admin','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'email' => 'required|email',
            'password' => 'nullable|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update admin
        $admin = Admin::findOrFail($id);

        $admin->name = trim($request->name);
        $admin->email = trim($request->email);
        if (isset($request->password))
            $admin->password = Hash::make(trim($request->password));

        if ($admin->save()) {

            AdminHasPermission::where('admin_id',$admin->id)->delete();
            // Assign permission to admin
            if (!empty($request->permissions)) {
                foreach ($request->permissions as $permission) {

                    $adminPermission = new AdminHasPermission;
                    $adminPermission->admin_id = $admin->id;
                    $adminPermission->permission_id = $permission;

                    if (!$adminPermission->save()) {
                        return response()->json([
                            'res' => false,
                            'msg' => 'Error on save product bread'
                        ]);
                    }
                }
            }

            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Admin::findOrFail($id)) {
            Admin::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }
}
