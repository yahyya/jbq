<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\IndexCategory;
use App\Models\Tag;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class TagController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            DB::statement(DB::raw('set @rownum=0'));
            return DataTables::of(Tag::select('id','title_en',DB::raw('@rownum  := @rownum  + 1 AS rownum'))->orderBy('id','desc'))
                ->addColumn('action', function ($category) {
//                        $res = '<a href="' . route('admin.categories.edit',['id'=>$category->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.categories.edit',['id'=>$category->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.tag.edit',$category->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $category->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.tag.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tag.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new category
        $tag = new Tag();

        $tag->title_en = trim($request->title_en);
        $tag->title_ar = trim($request->title_ar);
        $tag->show_index = trim($request->show_index);


        if ($tag->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view('admin.tag.addEdit',compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title_en' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update category
        $tag = Tag::findOrFail($id);

        $tag->title_en = trim($request->title_en);
        $tag->title_ar = trim($request->title_ar);
        $tag->show_index = $request->show_index;



        if ($tag->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'done'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Tag::findOrFail($id)) {
            Tag::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }


}
