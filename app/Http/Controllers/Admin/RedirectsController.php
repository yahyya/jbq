<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderHasRate;
use App\Models\Product;
use App\Models\Redirect;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Morilog\Jalali\Jalalian;
use Morilog\Jalali\jDate;
use Yajra\DataTables\DataTables;

class RedirectsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(Redirect::select('*')
                ->orderBy('id','desc'))
                ->editColumn('created_at', function($rate){
                    return Jalalian::forge($rate->created_at)->format('datetime');
                })
                ->addColumn('action', function ($rate) {
                    $res = ' <button onclick="deleteItem(' . $rate->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">Remove</button>';
                    return $res;
                })
                ->rawColumns(['user_id','product_id','action'])
                ->make(true);
        }
        return view('admin.redirects.index');
    }

    public function create(Request $req){
        return view('admin.redirects.addEdit');
    }

    public function store(Request $req){
        $validator = Validator::make($req->all(), [
            'title' => 'required|string|max:255',
            'address' => 'required|url|unique:redirects',
            'redirect' => 'required|url',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }


        $redirect = new Redirect();
        $redirect->title = $req->title;
        $redirect->address = $req->address;
        $redirect->redirect = $req->redirect;
        $redirect->save();

        return response()->json(['res'=>true]);
    }

    public function destroy($id,Request $req){
        if(Redirect::findOrFail($id)) {
            Redirect::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
    } else
        return response()->json([
            'res' => false,
            'message' => 'Something went wrong'
        ]);
    }
}
