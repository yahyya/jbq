<?php

namespace App\Http\Controllers\Admin;

use App\Models\CustomRule;
use App\Models\CustomSubItem;
use Validator;
use App\Models\CustomCategory;
use App\Models\CustomItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CustomItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $catId = $request->id;
        $customCategories = CustomCategory::all();
        return view('admin.custom.items.addEdit',compact('catId','customCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'custom_category_id' => 'required',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new category
        $customItem = new CustomItem;

        $customItem->custom_category_id = trim($request->custom_category_id);
        $customItem->title = trim($request->title);
        $customItem->order = (!is_null($lastCustomItem = CustomItem::orderBy('order','desc')->first()))? $lastCustomItem->order + 1 : 1;
        $customItem->active = trim($request->active);

        if ($customItem->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $customItemCat = CustomItem::findOrFail($id)->custom_category_id;
        if ($request->ajax()) {

            return DataTables::of(CustomSubItem::where('custom_item_id',$id)->orderBy('order','desc'))
//                ->editColumn('custom_item_id', function ($customSubItem) {
//                    return $customSubItem->item->title;
//                })
                ->editColumn('main_pic', function ($customSubItem) {
                    if (isset($customSubItem->main_pic))
                        return '<img src="' . asset('upload/images/sub-items') . '/' . $customSubItem->main_pic . '" class="img-responsive" width="100" height="auto">';
                    else
                        return '';
                })
                ->addColumn('action', function ($customItem) {
                    $res = ' <a href="' . route('admin.custom.subItems.edit',$customItem->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $customItem->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->rawColumns(['main_pic','action'])
                ->make(true);
        }
        return view('admin.custom.items.show',compact('id','customItemCat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customItem = CustomItem::findOrFail($id);
        $customItems = CustomItem::where('id','!=',$customItem->id)->get();
        $catId = $customItem->custom_category_id;
        $customCategories = CustomCategory::all();
        return view('admin.custom.items.addEdit',compact('id','catId','customItem','customItems','customCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'custom_category_id' => 'required',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update category
        $customCategory = CustomItem::findOrFail($id);

        $customCategory->custom_category_id = trim($request->custom_category_id);
        $customCategory->title = trim($request->title);
        $customCategory->active = trim($request->active);

        if ($customCategory->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'done'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (CustomItem::findOrFail($id)) {
            CustomItem::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function setOrder(Request $request)
    {
        foreach ($request->categories as $cat){
            $category = CustomItem::findOrFail($cat['id']);
            $category->order = $cat['newOrder'];
            $category->save();
        }
        return response()->json([
            'res' => true,
            'message' => 'Item order updated.'
        ]);
    }

    public function rules(Request $request, $id)
    {
        if ($request->ajax()) {

            return DataTables::of(CustomRule::where('first_item_id',$id)
                ->orderBy('id', 'desc'))
                ->editColumn('first_item_id', function($rule){
                    return $rule->item->title;
                })
                ->editColumn('second_item_id', function($rule){
                    return $rule->dismatch->title;
                })
                ->addColumn('action', function ($rule) {
                    $res = ' <button onclick="deleteItem(' . $rule->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect">Remove</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function newRule(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'second_item_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new rule
        $rule = new CustomRule;

        $rule->first_item_id = $id;
        $rule->second_item_id = trim($request->second_item_id);

        if ($rule->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function destroyRule(Request $request)
    {
        if ($customRule = CustomRule::findOrFail($request->id)) {
            $customRule->delete();
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }
}
