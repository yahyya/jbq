<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Slider;
use Illuminate\Support\Facades\Config;
use Validator;
use App\Models\Category;
use App\Models\SpecialOffer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SliderController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(
                Slider::select('*')
                    )
                ->editColumn('image', function ($slider) {
                    if(isset($slider->file_name))
                        return '<img src="'.asset('upload/slider').'/'.$slider->file_name.'" class="img-responsive" width="100" height="auto">';
                    else
                        return '';
                })
                ->addColumn('action', function ($slider) {
                    $res = ' <a href="' . route('admin.sliders.edit',$slider->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $slider->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->rawColumns(['image','action'])
                ->make(true);
        }
        return view('admin.sliders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'product_id' => 'nullable|numeric',
            'image' => 'nullable|string|max:50',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new specialOffer
        $slider = new Slider();

        $slider->title = trim($request->title);
        $slider->desc_en = trim($request->desc_en);
        $slider->desc_ar = trim($request->desc_ar);
        $slider->product_id = $request->product_id;
        $slider->enabled = 1;

        if(isset($request->image)) {
            if (asset('upload/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadSliderPath') . $request->image);
                $slider->file_name = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($slider->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        $categories = Category::all();
        $products = Product::all();
        return view('admin.sliders.addEdit',compact('slider','categories','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'product_id' => 'nullable|numeric',
            'image' => 'nullable|string|max:50',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update specialOffer
        $slider = Slider::findOrFail($id);
        $slider->title = trim($request->title);
        $slider->desc_en = trim($request->desc_en);
        $slider->desc_ar = trim($request->desc_ar);
        $slider->product_id = $request->product_id;

        if(isset($request->image)) {
            if (asset('upload/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadSliderPath') . $request->image);
                $slider->file_name = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($slider->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($slider = Slider::findOrFail($id)) {
            $slider->delete();
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

}
