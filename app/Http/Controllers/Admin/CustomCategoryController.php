<?php

namespace App\Http\Controllers\Admin;

use App\Models\CustomItem;
use Validator;
use App\Models\CustomCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CustomCategoryController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(CustomCategory::orderBy('order','desc'))
                ->addColumn('action', function ($customCategory) {
//                        $res = '<a href="' . route('admin.categories.edit',['id'=>$category->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.categories.edit',['id'=>$category->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.custom.categories.show',$customCategory->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Items</a>';
                    $res = $res . ' <a href="' . route('admin.custom.categories.edit',$customCategory->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $customCategory->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.custom.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.custom.categories.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'min' => 'required|numeric',
            'max' => 'required|numeric',
            'desc' => 'required|string|max:300',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new category
        $customCategory = new CustomCategory;

        $customCategory->title = trim($request->title);
        $customCategory->order = (!is_null($lastCustomCategory = CustomCategory::orderBy('order','desc')->first()))? $lastCustomCategory->order + 1 : 1;
        $customCategory->min = trim($request->min);
        $customCategory->max = trim($request->max);
        $customCategory->desc = trim($request->desc);

        if ($customCategory->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax()) {

            return DataTables::of(CustomItem::where('custom_category_id',$id)->orderBy('order','desc'))
                ->addColumn('action', function ($customItem) {
                    $res = ' <a href="' . route('admin.custom.items.show',$customItem->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Items</a>';
                    $res = $res . ' <a href="' . route('admin.custom.items.edit',$customItem->id) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> Edit</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $customItem->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> Remove</button>';
                    return $res;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.custom.categories.show',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customCategory = CustomCategory::findOrFail($id);
        return view('admin.custom.categories.addEdit',compact('customCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:100',
            'min' => 'required|numeric',
            'max' => 'required|numeric',
            'desc' => 'required|string|max:300',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update category
        $customCategory = CustomCategory::findOrFail($id);

        $customCategory->title = trim($request->title);
        $customCategory->min = trim($request->min);
        $customCategory->max = trim($request->max);
        $customCategory->desc= trim($request->desc);

        if ($customCategory->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'done'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (CustomCategory::findOrFail($id)) {
            CustomCategory::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }

    public function setOrder(Request $request)
    {
        foreach ($request->categories as $cat){
            $category = CustomCategory::findOrFail($cat['id']);
            $category->order = $cat['newOrder'];
            $category->save();
        }
        return response()->json([
            'res' => true,
            'message' => 'Item order updated.'
        ]);
    }
}
