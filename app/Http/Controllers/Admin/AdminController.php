<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Config;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Morilog\Jalali\jDate;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function dashboard()
    {

        return view('admin.dashboard');
    }

    public function salesStatus(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        if (Config::where('item','salesStatus')->update(array('value' => $request->status))) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function customProductSalesStatus(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        if (Config::where('item','customProductSalesStatus')->update(array('value' => $request->status))) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    public function orderCounts()
    {
        $orders = Order::all();
        $newOrders = $orders->where('status_id',0)->count();
        $preparingOrders = $orders->where('status_id',1)->count();
        $sentOrders = $orders->where('status_id',2)->count();
        $deliveredOrders = $orders->where('status_id',3)->count();
        $canceledOrders = $orders->where('status_id',4)->count();
        $leavedOrders = $orders->where('status_id',-1)->count();

        return response()->json([
            'newOrdersCount' => $newOrders,
            'preparingOrdersCount' => $preparingOrders,
            'sentOrdersCount' => $sentOrders,
            'deliveredOrdersCount' => $deliveredOrders,
            'canceledOrdersCount' => $canceledOrders,
            'leavedOrdersCount' => $leavedOrders
        ]);
    }



    public function saveFCMToken(Request $req){
        $user = Auth::guard('admin')->user();
        $user->token = $req->token;
        $user->save();
        return response()->json(['res'=>true]);
    }

    public function dbFullBackup()
    {
        $dbHost = env('DB_HOST');
        $dbName = env('DB_DATABASE');
        $dbUser = env('DB_USERNAME');
        $dbPass = env('DB_PASSWORD');
        $output = '';
        $return = '';

        $fileName = $dbName . '-' . date("Y-m-d-H-i-s") . '.zip';
        $filePath = 'db-backups/' . $fileName;
        exec("mysqldump --host=$dbHost --user=$dbUser --password=$dbPass $dbName |zip > $filePath", $output, $return);
        logger($fileName);

        if (file_exists($filePath)) {
//            file_put_contents($fileName, fopen($filePath, 'r'));
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            flush(); // Flush system output buffer
            readfile($filePath);
            exit;
        } else
            return 'no such a file: '. $filePath;
    }
}
