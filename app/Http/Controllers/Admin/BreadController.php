<?php

namespace App\Http\Controllers\Admin;

use App\Bread;
use Illuminate\Support\Facades\Config;
use Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BreadController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            return DataTables::of(Bread::select('id','title','image')
                ->orderBy('id','desc'))
                ->addColumn('action', function ($bread) {
//                        $res = '<a href="' . route('admin.breads.edit',['id'=>$bread->id]) . '" class="btn btn-xs btn-primary"><i class="fa fa-eye fa-fw"></i> مشاهده</a>';
//                    $res = ' <a href="' . route('admin.breads.edit',['id'=>$bread->id]) . '?duplicate=true" class="btn btn-xs btn-info"><i class="fa fa-copy fa-fw"></i> کپی</a>';
                    $res = ' <a href="' . route('admin.breads.edit',['id'=>$bread->id]) . '" class="btn btn-xs btn-primary waves-effect"><i class="fa fa-pencil fa-fw"></i> ویرایش</a>';
                    $res = $res . ' <button onclick="deleteItem(' . $bread->id . ')" class="btn btn-xs btn-danger btn-delete waves-effect"><i class="fa fa-trash fa-fw"></i> حذف</button>';
                    return $res;
                })
                ->editColumn('image', function ($bread) {
                    if(isset($bread->image))
                        return '<img src="'.asset('uploads/images/breads').'/'.$bread->image.'" class="img-responsive" width="100" height="auto">';
                    else
                        return '';
                })
                ->rawColumns(['image','action'])
                ->make(true);
        }
        return view('admin.breads.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.breads.addEdit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'image' => 'required',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // create new bread
        $bread = new Bread;

        $bread->title = trim($request->title);
        $bread->active = trim($request->active);

        // check for bread image and asign it to bread
        if (asset('uploads/temp/'.$request->image)) {
            rename(Config::get('fileuploads.uploadFilePath').$request->image,Config::get('fileuploads.uploadBreadsPath').$request->image);
            $bread->image = trim($request->image);
        } else {
            return response()->json([
                'res' => false,
                'msg' => 'File not found!'
            ]);
        }

        if ($bread->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'Item saved.'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong!'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bread = Bread::findOrFail($id);
        return view('admin.breads.addEdit',compact('bread'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'image' => 'nullable',
            'active' => 'boolean',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'res' => false,
                'msg' => implode('<br>', $validator->errors()->all())
            ]);
        }

        // update bread
        $bread = Bread::findOrFail($id);

        $bread->title = trim($request->title);
        $bread->active = trim($request->active);

        // check for bread image and asign it to bread
        if(isset($request->image)) {
            if (asset('uploads/temp/' . $request->image)) {
                rename(Config::get('fileuploads.uploadFilePath') . $request->image, Config::get('fileuploads.uploadBreadsPath') . $request->image);
                @unlink(Config::get('fileuploads.uploadBreadsPath') . $bread->image);
                $bread->image = trim($request->image);
            } else {
                return response()->json([
                    'res' => false,
                    'msg' => 'File not found!'
                ]);
            }
        }

        if ($bread->save()) {
            return response()->json([
                'res' => true,
                'msg' => 'done'
            ]);
        } else
            return response()->json([
                'res' => false,
                'msg' => 'Something went wrong'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Bread::findOrFail($id)) {
            Bread::destroy($id);
            return response()->json([
                'res' => true,
                'message' => 'Deleted'
            ]);
        } else
            return response()->json([
                'res' => false,
                'message' => 'Something went wrong'
            ]);
    }
}
