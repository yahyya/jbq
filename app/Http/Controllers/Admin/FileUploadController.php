<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
class FileUploadController extends Controller
{
    public function fileUpload()
    {
        if (isset($_FILES['file']) && $_FILES['file']['error'] == 0) {
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $finalName = Str::random(8) . '.' . $extension;
            $destinationPath = Config::get('fileuploads.uploadFilePath') . $finalName;
            if (move_uploaded_file($_FILES['file']['tmp_name'], $destinationPath)) {
                return response()->json(array('status' => true, 'name' => $finalName));
            }
        }
    }
}
