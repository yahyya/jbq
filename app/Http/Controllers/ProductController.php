<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Review;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function saveReview(Request $req){
        $validator = Validator::make($req->all(),[
            'productId'=>'required',
            'rate'=>'required',
            'text'=>'min:20',
            'name'=>'required',
            'email'=>'required|email',
        ]);
        if($validator->fails()){
            return response()->json(['res'=>false,'msg'=>implode('<br/>', $validator->getMessageBag()->all())]);
        }

        $product = Product::findOrFail($req->productId);

        $productReview = new Review();
        $productReview->text = $req->text;
        $productReview->product_id = $product->id;
        $productReview->name = $req->name;
        $productReview->email = $req->email;
        $productReview->rate = $req->rate;
        $productReview->save();
        return response()->json(['res'=>true]);
    }
    public function view(Request $req,$product){

        $cateogries = explode('/',$product);
        if(!empty($cateogries)){
            $lastPart = end($cateogries);
        }
        $product= '';
        if(is_numeric($lastPart)) {
            $product = Product::find($lastPart);
        }
        if(empty($product)){
            $product = Product::where('slug',$lastPart)->first();
        }
        if(empty($product)){
            $lastPart = str_replace('-',' ',$lastPart);
            $product = Product::where('title_en','like','%'.$lastPart.'%')->orWhere('title_ar','like','%'.$lastPart.'%')->first();
        }
        if(empty($product)){
            abort(404);
        }

        return view('product',['pr'=>$product]);
    }

    public function index(Request $req){
        $products = Product::all();
        return view('products',['products'=>$products]);
    }

    public function customize(Request $req,$id){

        $product = Product::find($id);

        return view('product_custom',['product'=>$product]);
    }
}
