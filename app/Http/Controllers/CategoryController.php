<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CategoryController extends Controller
{
    public function view(Request $req,$category){
        $cateogries = explode('/',$category);
        if(!empty($category)){
            $category = end($cateogries);
        }
        if(is_numeric($category)) {
            $cat = Category::find($category);
        }
        if(empty($cat)){
            $cat = Category::where('slug',$category)->first();
        }
        if(empty($cat)){
            $cat = Category::where('title_en','like','%'.$category.'%')->orWhere('title_ar','like','%'.$category.'%')->first();
        }
        if(empty($cat)){
            abort(404);
        }
        return view('category',['cat'=>$cat]);
    }
}
