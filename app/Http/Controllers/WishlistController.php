<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class WishlistController extends Controller
{


    public function add(Request $req){
        $product = Product::findOrFail($req->id);
        Cart::instance('wishlist')->add($product->id, $product->title_en, 1, $product->fixed_price ?? $product->price);
        return response()->json(['res'=>true]);
    }

    public function remove(Request $req){
        Cart::instance('wishlist')->remove($req->rowId);
        return response()->json(['res'=>true]);
    }

    public function view(){
        return view('wishlist',['products'=>self::getListProducts()]);
    }

    public static function getListProducts(){
        $allCart = Cart::instance('wishlist')->content();
        $products = [];
        foreach ($allCart as $cart) {
            $product = Product::find($cart->id);
            $products[] = [
                'rowId'=>$cart->rowId,
                'id' => $product->id,
                'title' => \Illuminate\Support\Facades\App::getLocale() == 'ar' ? $product->title_ar : $product->title_en,
                'image' => @$product->image->file_name,
                'qty' => $cart->qty,
                'price' => $product->fixed_price ?? $product->price
            ];
        }
        return $products;
    }
}
